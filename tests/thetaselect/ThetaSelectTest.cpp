#include "TestCase.h"
#include "Column.h"
#include "FormatRaw.h"
#include "CacheCostCalculator.h"
#include "ColumnInputOperator.h"
#include "ThetaSelectOperator.h"

#include <random>
#include <functional>

namespace Ocelot
{

template<DataType DType, typename Type = GET_TYPE(DType)>
class ThetaSelectTest
{
public:
  ThetaSelectTest(Type low, Type up, bool low_set, bool up_set,
                  bool lower_inclusive, bool upper_inclusive,
                  bool negate_predicate, unsigned int count,
                  bool sort_ascending) :
    mLowerBound(low),
    mUpperBound(up),
    mLowerBoundSet(low_set),
    mUpperBoundSet(up_set),
    mLowerBoundInclusive(lower_inclusive),
    mUpperBoundInclusive(upper_inclusive),
    mNegatePredicate(negate_predicate),
    mCount(count),
    mSortAscending(sort_ascending) {

  }

  void test(Pointer<Device> dev) {
    std::vector<Type> host_data;
    ColumnPtr data_column;

    createDataColumn(dev, data_column, host_data);
    auto result_buffer = execute(dev, data_column);

    std::vector<unsigned int> host_result, device_result;
    host_result = createHostResult(host_data);

    device_result.resize(result_buffer->getElementCount());
    result_buffer->read(device_result.data()).wait();

    compareHostWithDeviceResult(host_result, device_result);
  }

private:
  void createDataColumn(Pointer<Device> dev, ColumnPtr& col,
                        std::vector<Type>& host_data) {
    auto sort_dir = mSortAscending ? TestCase::Ascending : TestCase::Descending;
    auto buffer = TestCase::generateBufferWithRandomData<DType>(
                    dev, mCount, TestCase::minValue<Type>(),
                    TestCase::maxValue<Type>(), host_data, sort_dir);

    col = TestCase::generateColumnWithRawData(DType, buffer);
    col->setSorted(mSortAscending);
    col->setRevSorted(!mSortAscending);
  }

  BufferPtr execute(Pointer<Device> dev, ColumnPtr input_col) {
    auto input_op = OperatorPtr(
                      DI_CREATE_INSTANCE(ColumnInputOperator<DType>,
                                         input_col));

    auto select_op = DI_CREATE_INSTANCE(ThetaSelectOperator<DType>, mLowerBound,
                                        mUpperBound, mLowerBoundSet,
                                        mUpperBoundSet, mLowerBoundInclusive,
                                        mUpperBoundInclusive,
                                        mNegatePredicate);
    select_op->registerParent(input_op, 0, 0);
    select_op->finish();

    return select_op->getResult(0)->template getData<Format::Bitmap>(dev);
  }

  std::vector<unsigned int> createHostResult(
      const std::vector<Type>& host_data) {
    std::vector<unsigned int> host_result;

    unsigned int bitmap = 0;
    unsigned int shift = 0;
    for (auto data : host_data) {
      bool result = !mLowerBoundSet || (data > mLowerBound);
      result &= !mUpperBoundSet || (data < mUpperBound);
      result |= TestCase::equal(data, mLowerBound) && mLowerBoundInclusive;
      result |= TestCase::equal(data, mUpperBound) && mUpperBoundInclusive;
      result = mNegatePredicate ? !result : result;
      result = TestCase::equal(data, TestCase::minValue<Type>()) ?
                 false : result;
      bitmap |= result ? (0x1 << shift) : 0;
      ++shift;

      if (shift > 31) {
        host_result.push_back(bitmap);
        bitmap = 0;
        shift = 0;
      }
    }

    if (shift > 0) {
      host_result.push_back(bitmap);
    }

    return host_result;
  }

  void compareHostWithDeviceResult(
      const std::vector<unsigned int>& host_result,
      const std::vector<unsigned int>& device_result) {
    REQUIRE(host_result.size() == device_result.size());
    REQUIRE(TestCase::compareBitmapVectors(host_result, device_result, mCount));
  }

  Type mLowerBound;
  Type mUpperBound;
  bool mLowerBoundSet;
  bool mUpperBoundSet;
  bool mLowerBoundInclusive;
  bool mUpperBoundInclusive;
  bool mNegatePredicate;
  unsigned int mCount;
  bool mSortAscending;
};

template<DataType DType, typename Type = GET_TYPE(DType)>
void test_theta_select(Pointer<Device> device, bool low_set, bool up_set,
                       bool lower_inclusive, bool upper_inclusive,
                       bool negate_predicate, bool sort_ascending) {
  auto counts = { 100u, 276u, 512u, 2048u, 5000u, 10000u };

  for (auto count : counts) {
    auto lower = TestCase::generateRandomNumber<Type>();
    auto upper = TestCase::generateRandomNumber<Type>();

    if (lower > upper) {
      std::swap(upper, lower);
    }

    ThetaSelectTest<DType> select_test(lower, upper, low_set, up_set,
                                       lower_inclusive, upper_inclusive,
                                       negate_predicate, count, sort_ascending);
    select_test.test(device);
  }
}

#define TEST_WITH_ALL_TYPES(...)                                               \
  test_theta_select<DTypeS8>(__VA_ARGS__);                                     \
  test_theta_select<DTypeS16>(__VA_ARGS__);                                    \
  test_theta_select<DTypeS32>(__VA_ARGS__);                                    \
  test_theta_select<DTypeF32>(__VA_ARGS__);

TEST_CASE("Testing ThetaSelect operator lower bound.", "[thetaselect-lower]") {
  OCL_TESTCASE_BASIC_INIT();

  for (auto lower_inclusive : { true, false }) {
    TEST_WITH_ALL_TYPES(device, true, false, lower_inclusive, false, false,
                        true);
  }
}

TEST_CASE("Testing ThetaSelect operator upper bound.", "[thetaselect-upper]") {
  OCL_TESTCASE_BASIC_INIT();

  for (auto upper_inclusive : { true, false }) {
    TEST_WITH_ALL_TYPES(device, false, true, false, upper_inclusive, false,
                        true);
  }
}

TEST_CASE("Testing ThetaSelect operator lower bound negate.",
          "[thetaselect-lower-negate]") {
  OCL_TESTCASE_BASIC_INIT();

  for (auto lower_inclusive : { true, false }) {
    TEST_WITH_ALL_TYPES(device, true, false, lower_inclusive, false, true,
                        true);
  }
}

TEST_CASE("Testing ThetaSelect operator upper bound negate.",
          "[thetaselect-upper-negate]") {
  OCL_TESTCASE_BASIC_INIT();

  for (auto upper_inclusive : { true, false }) {
    TEST_WITH_ALL_TYPES(device, false, true, false, upper_inclusive, true,
                        true);
  }
}

TEST_CASE("Testing ThetaSelect operator all parameters.",
          "[thetaselect-all-parameters]") {
  OCL_TESTCASE_BASIC_INIT();

  for (auto lower_inclusive : { true, false }) {
    for (auto upper_inclusive : { true, false }) {
      for (auto lower_set : { true, false }) {
        for (auto upper_set : { true, false }) {
          for (auto negate_predicate : { true, false }) {
            for (auto sort_ascending : { true, false }) {
              TEST_WITH_ALL_TYPES(device, lower_set, upper_set, lower_inclusive,
                                  upper_inclusive, negate_predicate,
                                  sort_ascending);
            }
          }
        }
      }
    }
  }
}

} //namespace Ocelot
