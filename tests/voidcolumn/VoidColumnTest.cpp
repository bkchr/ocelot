#include "TestCase.h"
#include "VoidColumn.h"

namespace Ocelot
{

TEST_CASE("Testing the voidcolumn util", "[voidcolumn-test]") {
  OCL_TESTCASE_BASIC_INIT();

  auto counts = { 100, 1000, 10000 };

  for (auto count : counts) {
    auto vcol = VoidColumn(device, count);

    REQUIRE(vcol.getResult() != nullptr);
    REQUIRE(vcol.getResult()->getCount() == count);

    std::vector<int> data, host_data;

    data.resize(count);
    auto event = vcol.getResult()->read(data.data());

    for (auto i = 0; i < count; ++i)
      host_data.push_back(i);

    event.wait();

    REQUIRE(std::equal(data.begin(), data.end(), host_data.begin()));
  }
}

} //namespace Ocelot
