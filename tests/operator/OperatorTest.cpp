#include "TestCase.h"
#include "Operator.h"
#include "FormatRaw.h"
#include "ColumnInputOperator.h"

#include <random>
#include <functional>

namespace Ocelot
{

template<DataType Type>
class SimpleOperator : public Operator
{
public:
  DI_CTOR(SimpleOperator, const float bound, DeviceSelectionManager* dsmgr) :
    Operator(dsmgr),
    mBound(bound)
  {}

  bool doesInputChannelSupportVectorizedData(
      unsigned short channel) const override final {
    return true;
  }
  unsigned short getMaximumNumberOfRuns() override final {
    return getInputChannelMaximumNumberOfRuns(0);
  }
  std::string getName() override final { return "SimpleOperator"; }

  void run(Pointer<Device> device) override final {
    auto buffer = getInputChannel(device, 0);

    std::string source = "__kernel void SimpleOperator(                 "
                         "              __global const float* data,     "
                         "              __global int* result,           "
                         "              const unsigned int num,         "
                         "              const float bound) {            "
                         "    unsigned int pos = get_global_id(0);      "
                         "    result[pos] = data[pos] < bound;          "
                         "}                                             ";

    auto kernel = device->createKernel("SimpleOperator", source);

    auto rbuffer = device->allocateBuffer<DTypeS32>(buffer->getCount());

    kernel->setArgs(in(buffer), out(rbuffer), buffer->getCount(), mBound);
    kernel->execute(buffer->getCount());

    addOutputChannelVectorized(rbuffer, 0, rbuffer->getType());
  }

private:
  float mBound;
};

TEST_CASE("Testing a simple operator", "[simple-operator]") {
  OCL_TESTCASE_BASIC_INIT();

  // generate the data
  std::vector<float> host_data;
  auto input_buffer =
      TestCase::generateBufferWithRandomData<DTypeF32>(
        device, dmanager->getGlobalVectorSize() * 2, 0, 1000, host_data);

  ColumnPtr input_col = Column::createInstance<DTypeF32>();
  input_col->registerData<Format::Raw>(
        input_buffer, CacheCost::CalculatorPtr(new CacheCost::Direct(1)));
  auto input_op =
      OperatorPtr(DI_CREATE_INSTANCE(ColumnInputOperator<DTypeF32>, input_col));

  // we need round this downward or we get precision errors at gpu's
  float bound = floor(host_data[host_data.size() / 2]);
  auto simple_op =
      OperatorPtr(DI_CREATE_INSTANCE(SimpleOperator<DTypeF32>, bound));
  simple_op->registerParent(input_op, 0, 0);

  simple_op->finish();
  auto simple_op_result = simple_op->getResult(0)->
      getData<Format::Raw, Location::Host, DTypeS32>();

  unsigned int local_count = 0;
  for (auto d : host_data)
    local_count += d < bound;

  unsigned int operator_count = 0;
  for (unsigned int i = 0; i < simple_op_result.get().first; ++i) {
    operator_count += simple_op_result.get().second.get()[i];
    if(simple_op_result.get().second.get()[i] > 1)
      std::cout<<simple_op_result.get().second.get()[i]<<std::endl;
  }

  REQUIRE(operator_count == local_count);
}

} //namespace Ocelot
