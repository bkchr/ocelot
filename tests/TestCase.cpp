#define CATCH_CONFIG_MAIN
#include "TestCase.h"

#include "Column.h"
#include "FormatRaw.h"
#include "CacheCostCalculator.h"
#include "InitBuffer.h"

#include <random>

namespace Ocelot
{

namespace TestCase
{

BufferPtr generateBuffer(Pointer<Device> dev, size_t count, DataType type,
                         const BufferFillFunction& func) {
  auto buffer = dev->allocateBuffer(type, count);

  if (func != nullptr) {
    func(buffer);
  } else {
    InitBuffer(buffer, InitBuffer::WithZeros);
  }

  return buffer;
}

ColumnPtr generateColumnWithRawData(DataType type, const BufferPtr& ptr) {
  auto col = Column::createInstance(type);
  col->registerData<Format::Raw>(ptr, CacheCost::Direct::createInstance(1));
  return col;
}

GET_TYPE(DTypeBitmap) maskOutUnusedBits(
    const GET_TYPE(DTypeBitmap) value, const unsigned int count,
    const unsigned int bitmap_index) {
  if ((bitmap_index + 1) * GET_COUNT_PER_ELEMENT(DTypeBitmap) > count) {
    return value &
        ((1 << (count - bitmap_index *
                GET_COUNT_PER_ELEMENT(DTypeBitmap))) - 1);
  } else {
    return value;
  }
}

bool compareBitmapVectors(const std::vector<GET_TYPE(DTypeBitmap)>& left,
                          const std::vector<GET_TYPE(DTypeBitmap)>& right,
                          unsigned int count) {
  if (left.size() != right.size()) {
    return false;
  }

  bool result = true;
  for (auto i = 0u; i < left.size(); ++i) {
    result &= maskOutUnusedBits(left[i], count, i) ==
              maskOutUnusedBits(right[i], count, i);

    if (maskOutUnusedBits(left[i], count, i) !=
        maskOutUnusedBits(right[i], count, i)) {
      result = false;
    }
  }

  return result;
}

} //namespace TestCase

} //namespace Ocelot
