#include "TestCase.h"

#include "GroupedAggregation.h"
#include "Buffer.h"

#include <iostream>

namespace Ocelot
{

template<DataType DType, DataType RDType,
         typename Type = GET_TYPE(DType), typename RType = GET_TYPE(RDType)>
class GroupedAggregationTest
{
public:
  GroupedAggregationTest(unsigned int count, bool nonil,
                        GroupedAggregation::Operation op) :
    mCount(count),
    mNoNil(nonil),
    mOperation(op) {
    mGroupCount = divisionWithRound(mCount, 10);
  }

  void test(Pointer<Device> dev) {
    std::vector<Type> data;
    auto buffer = TestCase::generateBufferWithRandomData<DType>(
                    dev, mCount, -100, 100, data);

    if (!mNoNil) {
      addNils(buffer, data);
    }

    BufferPtr groups;
    std::vector<unsigned int> groups_host;
    createGroups(dev, groups, groups_host);

    auto result =
        GroupedAggregation(buffer, groups, mGroupCount, mNoNil,
                           mOperation).getResult();
    compareHostWithDeviceResult(result, data, groups_host);
  }

private:
  void createGroups(Pointer<Device> dev, BufferPtr& groups,
                    std::vector<unsigned int>& groups_host) {
    groups_host.resize(mCount);
    std::generate(
          groups_host.begin(), groups_host.end(),
          [this] {
      return TestCase::generateRandomNumber<unsigned int>(0, mGroupCount - 1);
    });

    groups = dev->allocateBuffer(DTypeS32, mCount, groups_host.data());
  }

  void addNils(BufferPtr buffer, std::vector<Type>& data) {
    auto count = data.size() / 10;
    for (unsigned int i = 0; i < count; ++i) {
      data[TestCase::generateRandomNumber<unsigned int>(0, data.size() - 1)] =
          TestCase::minValue<Type>();
    }

    buffer->write(data.data()).wait();
  }

  void compareHostWithDeviceResult(
      BufferPtr result, const std::vector<Type>& data,
      const std::vector<unsigned int>& groups) {
    switch (mOperation) {
      case GroupedAggregation::Sum:
      case GroupedAggregation::SumLong:
        compareHostWithDeviceResultSum(result, data, groups);
        break;
      case GroupedAggregation::Min:
        compareHostWithDeviceResultMin(result, data, groups);
        break;
      case GroupedAggregation::Max:
        compareHostWithDeviceResultMax(result, data, groups);
        break;
      case GroupedAggregation::Count:
        compareHostWithDeviceResultCount(result, data, groups);
        break;
    }
  }

  void compareHostWithDeviceResultSum(
      BufferPtr result, const std::vector<Type>& data,
      const std::vector<unsigned int>& groups) {
    std::vector<RType> host_result(mGroupCount);
    std::fill(host_result.begin(), host_result.end(), 0);

    for (unsigned int i = 0; i < mCount; ++i) {
      if (skipValue(data[i])) {
        continue;
      }

      host_result[groups[i]] += data[i];
    }

    compareHostWithDeviceVector(host_result, result);
  }

  void compareHostWithDeviceResultMin(
      BufferPtr result, const std::vector<Type>& data,
      const std::vector<unsigned int>& groups) {
    std::vector<RType> host_result(mGroupCount);
    std::fill(host_result.begin(), host_result.end(),
              TestCase::maxValue<Type>());

    for (unsigned int i = 0; i < mCount; ++i) {
      if (skipValue(data[i])) {
        continue;
      }

      host_result[groups[i]] =
          std::min(static_cast<Type>(host_result[groups[i]]), data[i]);
    }

    compareHostWithDeviceVector(host_result, result);
  }

  void compareHostWithDeviceResultMax(
      BufferPtr result, const std::vector<Type>& data,
      const std::vector<unsigned int>& groups) {
    std::vector<RType> host_result(mGroupCount);
    std::fill(host_result.begin(), host_result.end(),
              TestCase::minValue<Type>());

    for (unsigned int i = 0; i < mCount; ++i) {
      if (skipValue(data[i])) {
        continue;
      }

      host_result[groups[i]] =
          std::max(static_cast<Type>(host_result[groups[i]]), data[i]);
    }

    compareHostWithDeviceVector(host_result, result);
  }

  void compareHostWithDeviceResultCount(
      BufferPtr result, const std::vector<Type>& data,
      const std::vector<unsigned int>& groups) {
    std::vector<RType> host_result(mGroupCount);
    std::fill(host_result.begin(), host_result.end(), 0);

    for (unsigned int i = 0; i < mCount; ++i) {
      if (skipValue(data[i])) {
        continue;
      }

      host_result[groups[i]]++;
    }

    compareHostWithDeviceVector(host_result, result);
  }

  void compareHostWithDeviceVector(const std::vector<RType>& host,
                                   BufferPtr result) {
    std::vector<RType> device_result(mGroupCount);
    result->read(device_result.data()).wait();

    REQUIRE(std::equal(
              host.begin(), host.end(),
              device_result.begin(), device_result.end(),
              [](auto left, auto right) {
                if (!TestCase::equal(left, right)) {
                  std::cout<<"Left: "<<left<<"  Right: "<<right<<std::endl;
                }

                return TestCase::equal(left, right);
    }));
  }

  bool skipValue(Type data) {
    return !mNoNil && TestCase::equal(data, TestCase::minValue<Type>());
  }

  unsigned int mCount;
  unsigned int mGroupCount;
  bool mNoNil;
  GroupedAggregation::Operation mOperation;
};

template<DataType DType, DataType RDType = DType>
void testGroupedAggregation(Pointer<Device> dev,
                            GroupedAggregation::Operation op) {
  auto counts = { 100, 1000, 10000, 1024 * 1024 };
  auto nonils = { true, false };

  for (auto count : counts) {
    for (auto nonil : nonils) {
      GroupedAggregationTest<DType, RDType>(count, nonil, op).test(dev);
    }
  }
}

TEST_CASE("Testing GroupedAggregation-Typecheck.",
          "[groupedaggregation-typecheck]") {
  auto result = std::is_same<long, ssize_t>::value;
  REQUIRE(result);
}

TEST_CASE("Testing GroupedAggregation-Sum.", "[groupedaggregation-sum]") {
  OCL_TESTCASE_BASIC_INIT();
  testGroupedAggregation<DTypeF32>(device, GroupedAggregation::Sum);
  testGroupedAggregation<DTypeS16, DTypeS32>(device, GroupedAggregation::Sum);
  testGroupedAggregation<DTypeS8, DTypeS32>(device, GroupedAggregation::Sum);
}

TEST_CASE("Testing GroupedAggregation-SumLong.",
          "[groupedaggregation-sumlong]") {
  OCL_TESTCASE_BASIC_INIT();
  testGroupedAggregation<DTypeS32, DTypeS64>(device,
                                             GroupedAggregation::SumLong);
}

TEST_CASE("Testing GroupedAggregation-Min.", "[groupedaggregation-min]") {
  OCL_TESTCASE_BASIC_INIT();
  testGroupedAggregation<DTypeS32>(device, GroupedAggregation::Min);
  testGroupedAggregation<DTypeF32>(device, GroupedAggregation::Min);
  testGroupedAggregation<DTypeS16>(device, GroupedAggregation::Min);
  testGroupedAggregation<DTypeS8>(device, GroupedAggregation::Min);
}

TEST_CASE("Testing GroupedAggregation-Max.", "[groupedaggregation-max]") {
  OCL_TESTCASE_BASIC_INIT();
  testGroupedAggregation<DTypeS32>(device, GroupedAggregation::Max);
  testGroupedAggregation<DTypeF32>(device, GroupedAggregation::Max);
  testGroupedAggregation<DTypeS16>(device, GroupedAggregation::Max);
  testGroupedAggregation<DTypeS8>(device, GroupedAggregation::Max);
}

TEST_CASE("Testing GroupedAggregation-Count.", "[groupedaggregation-count]") {
  OCL_TESTCASE_BASIC_INIT();
  testGroupedAggregation<DTypeS32, DTypeS64>(device, GroupedAggregation::Count);
  testGroupedAggregation<DTypeF32, DTypeS64>(device, GroupedAggregation::Count);
  testGroupedAggregation<DTypeS16, DTypeS64>(device, GroupedAggregation::Count);
  testGroupedAggregation<DTypeS8, DTypeS64>(device, GroupedAggregation::Count);
}

} //namespace Ocelot
