#include "TestCase.h"
#include "Column.h"
#include "FormatRaw.h"
#include "CacheCostCalculator.h"
#include "ColumnInputOperator.h"
#include "LeftFetchJoinOperator.h"

#include <random>
#include <functional>

namespace Ocelot
{

std::vector<unsigned int> generateShuffledIndices(unsigned int indices_count,
                                                  unsigned int data_count) {
  std::vector<unsigned int> indices;
  auto gen = TestCase::createRandomNumberGenerator<unsigned int>(
               0, data_count - 1);

  for (unsigned int i = 0; i < indices_count; ++i) {
    indices.push_back(gen());
  }

  return indices;
}

template<DataType DType>
BufferPtr executeLeftFetchJoin(Pointer<Device> dev, BufferPtr indices,
                               BufferPtr data) {
  auto indices_op = OperatorPtr(
                      DI_CREATE_INSTANCE(
                        ColumnInputOperator<DTypeS32>,
                        TestCase::generateColumnWithRawData(DTypeS32,
                                                            indices)));
  auto data_op = OperatorPtr(
                   DI_CREATE_INSTANCE(
                     ColumnInputOperator<DType>,
                     TestCase::generateColumnWithRawData(DType, data)));

  auto join = OperatorPtr(
                DI_CREATE_INSTANCE(LeftFetchJoinOperator<DType>));
  join->registerParent(indices_op, 0, 0);
  join->registerParent(data_op, 1, 0);
  join->finish();

  return join->getResult(0)->getData<Format::Raw>(dev);
}

template<DataType DType, typename Type = GET_TYPE(DType)>
std::vector<Type> generateHostResultBuffer(
    const std::vector<unsigned int>& indices, const std::vector<Type>& data) {
  std::vector<Type> result;

  for (auto index : indices) {
    result.push_back(data[index]);
  }

  return result;
}

template<DataType DType, typename Type = GET_TYPE(DType)>
void test_leftfetchjoin_operator(Pointer<Device> device) {
  INFO(GET_DATATYPE_NAME(DType));

  auto indices_count_values = { 10u, 100u, 1000u, 500000u, 5000000u };
  auto data_count_values = { 2000u, 200000u };

  for (auto indices_count : indices_count_values) {
    for (auto data_count : data_count_values) {
      std::vector<Type> data, host_result, device_result;
      auto data_buffer = TestCase::generateBufferWithRandomData<DType>(
                           device, data_count, 0,
                           TestCase::maxValue<Type>(), data);

      auto indices = generateShuffledIndices(indices_count, data_count);
      auto indices_buffer = device->allocateBuffer(DTypeS32, indices_count);
      indices_buffer->write(indices.data());

      auto result_buffer = executeLeftFetchJoin<DType>(device, indices_buffer,
                                                       data_buffer);

      REQUIRE(result_buffer->getCount() == indices_count);
      host_result = generateHostResultBuffer<DType>(indices, data);

      device_result.resize(result_buffer->getElementCount());
      result_buffer->template read<Type>(device_result.data()).wait();

      REQUIRE(std::equal(host_result.begin(), host_result.end(),
                         device_result.begin()));
    }
  }
}

#define TEST_WITH_ALL_TYPES(...)                                               \
  test_leftfetchjoin_operator<DTypeS8>(__VA_ARGS__);                           \
  test_leftfetchjoin_operator<DTypeS16>(__VA_ARGS__);                          \
  test_leftfetchjoin_operator<DTypeS32>(__VA_ARGS__);                          \
  test_leftfetchjoin_operator<DTypeS64>(__VA_ARGS__);                          \
  test_leftfetchjoin_operator<DTypeF32>(__VA_ARGS__);

TEST_CASE("Testing LeftFetchJoin operator.", "[leftfetchjoin]") {
  OCL_TESTCASE_BASIC_INIT();

  TEST_WITH_ALL_TYPES(device);
}

} //namespace Ocelot
