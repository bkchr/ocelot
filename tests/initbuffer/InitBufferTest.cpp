#include "TestCase.h"

#include "InitBuffer.h"

namespace Ocelot
{

template<DataType DType, typename Type = GET_TYPE(DType)>
void initbuffer_test(Pointer<Device> device, InitBuffer::Operation op,
                     Type compare_val) {
  auto counts = { 100, 1000, 10000, 1024 * 1024, 500000 };

  for (auto count : counts) {
    auto buffer = device->allocateBuffer(DType, count);
    InitBuffer(buffer, op);

    auto future = buffer->read<Type>();
    auto* data = future.get().get();

    auto count_if_func = [&compare_val](const auto val) {
      return TestCase::equal(val, compare_val);
    };
    REQUIRE(std::count_if(data, data + buffer->getCount(), count_if_func)
            == buffer->getCount());
  }
}

template<DataType DType, typename Type = GET_TYPE(DType)>
void initbuffer_zero_test(Pointer<Device> device) {
  initbuffer_test<DType>(device, InitBuffer::WithZeros, Type(0));
}

#define TEST_ALL_TYPES(func, device) \
  func<DTypeS8>(device);             \
  func<DTypeS16>(device);            \
  func<DTypeS32>(device);            \
  func<DTypeS64>(device);            \
  func<DTypeF32>(device);

TEST_CASE("Testing initbuffer with zeros", "[initbuffer-zero]") {
  OCL_TESTCASE_BASIC_INIT();
  TEST_ALL_TYPES(initbuffer_zero_test, device);
}

template<DataType DType, typename Type = GET_TYPE(DType)>
void initbuffer_max_test(Pointer<Device> device) {
  initbuffer_test<DType>(device, InitBuffer::WithMax,
                         TestCase::maxValue<Type>());
}

TEST_CASE("Testing initbuffer with maximum", "[initbuffer-max]") {
  OCL_TESTCASE_BASIC_INIT();
  TEST_ALL_TYPES(initbuffer_max_test, device);
}

template<DataType DType, typename Type = GET_TYPE(DType)>
void initbuffer_min_test(Pointer<Device> device) {
  initbuffer_test<DType>(device, InitBuffer::WithMin,
                         TestCase::minValue<Type>());
}

TEST_CASE("Testing initbuffer with minimum", "[initbuffer-min]") {
  OCL_TESTCASE_BASIC_INIT();
  TEST_ALL_TYPES(initbuffer_min_test, device);
}

} //namespace Ocelot
