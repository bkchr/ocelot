#include "TestCase.h"
#include "PrefixSum.h"

#include <bitset>
#include <iostream>

namespace Ocelot {

class PrefixSumTest
{
public:
  PrefixSumTest(unsigned int count, bool bitmap) :
    mCount(count),
    mTestBitmap(bitmap) {

  }

  void test(Pointer<Device> dev) {
    unsigned int host_sum;
    std::vector<unsigned int> host_result;
    BufferPtr buffer;

    if (mTestBitmap) {
      generateBufferAndHostSumBitmap(dev, buffer, host_sum, host_result);
    } else {
      generateBufferAndHostSum(dev, buffer, host_sum, host_result);
    }

    auto psum = PrefixSum(buffer);
    REQUIRE(host_sum == psum.getSum());

    std::vector<unsigned int> device_result(
          psum.getResultBuffer()->getElementCount());
    psum.getResultBuffer()->read(device_result.data()).wait();

    REQUIRE(std::equal(device_result.begin(), device_result.end(),
                       host_result.begin()));
  }

private:
  void generateBufferAndHostSumBitmap(
      Pointer<Device> dev, BufferPtr& result, unsigned int& host_sum,
      std::vector<unsigned int>& host_result) {
    std::vector<GET_TYPE(DTypeBitmap)> host_data;

    result = TestCase::generateBufferWithRandomData<DTypeBitmap>(
               dev, mCount, 0, 100, host_data);

    host_sum = 0;
    for (auto data : host_data) {
      host_result.push_back(host_sum);
      host_sum += std::bitset<sizeof(unsigned int) * 8>(data).count();
    }
  }

  void generateBufferAndHostSum(
      Pointer<Device> dev, BufferPtr& result, unsigned int& host_sum,
      std::vector<unsigned int>& host_result) {
    std::vector<GET_TYPE(DTypeS32)> host_data;

    result = TestCase::generateBufferWithRandomData<DTypeS32>(
               dev, mCount, 0, 100, host_data);

    host_sum = 0;
    for (auto data : host_data) {
      host_result.push_back(host_sum);
      host_sum += data;
    }

  }

  unsigned int mCount;
  bool mTestBitmap;
};

TEST_CASE("PrefixSum test", "[prefixsum]") {
  OCL_TESTCASE_BASIC_INIT();

  std::vector<size_t> test_counts = {
    10, 2048, 100000, 2048 * 2048,
    device->getMaxWorkgroupSize() * device->getMaxWorkgroupSize() };

  for (auto bitmap : { true, false }) {
    for (auto count : test_counts) {
      auto ptest = PrefixSumTest(count, bitmap);
      ptest.test(device);
    }
  }
}

} //namespace Ocelot
