#include "TestCase.h"
#include "Sort.h"

#include <algorithm>

namespace Ocelot {

template<DataType DType, typename Type = GET_TYPE(DType)>
class SortTest
{
public:
  SortTest(unsigned int count, bool unique_keys, bool negative_keys,
           Sort::Direction dir) :
    mCount(count),
    mUniqueKeys(unique_keys),
    mNegativeKeys(negative_keys),
    mSortDir(dir) {

  }

  bool test(Pointer<Device> dev) {
    std::vector<int> keys;
    std::vector<Type> values;
    auto host_key_value = generateTestData();

    for (auto& pair : host_key_value) {
      keys.push_back(pair.first);
      values.push_back(pair.second);
    }

    auto device_keys = dev->allocateBuffer(DTypeS32, mCount);
    device_keys->write(keys.data());
    auto device_values = dev->allocateBuffer(DType, mCount);
    device_values->write(values.data());

    auto sort = Sort(device_keys, device_values, mSortDir);
    std::sort(host_key_value.begin(), host_key_value.end(),
              [this](auto& first, auto& second) {
                if (mSortDir == Sort::Descending)
                  return first.first > second.first;
                else
                  return first.first < second.first;});

    sort.getDstKeys()->read(keys.data()).wait();
    sort.getDstValues()->read(values.data()).wait();

    for (size_t i = 0; i < keys.size(); ++i) {
      if (keys[i] != host_key_value[i].first ||
          // if we don't use unique keys, we don't compare the values
          // because the order isn't the same as produced by the OpenCl sort!
          (mUniqueKeys &&
           !TestCase::equal(values[i], host_key_value[i].second))) {
        return false;
      }
    }

    return true;
  }

private:
  std::vector<std::pair<int, Type>> generateTestData() {
    std::vector<std::pair<int, Type>> host_key_value;

    for (unsigned int i = 0; i < mCount; ++i) {
      unsigned int key;

      if (mNegativeKeys && mUniqueKeys) {
        key = mCount / 2 - i;
      } else if (mNegativeKeys) {
        key = mCount / 4 - std::rand() % (mCount / 2);
      } else if (mUniqueKeys) {
        key = i;
      } else {
        key = std::rand() % (mCount / 2);
      }

      host_key_value.push_back(std::make_pair(key, std::rand() % 1000));
    }

    if (mUniqueKeys) {
      std::shuffle(host_key_value.begin(),
                   host_key_value.end(), std::default_random_engine());
    }

    return host_key_value;
  }

  unsigned int mCount;
  bool mUniqueKeys;
  bool mNegativeKeys;
  Sort::Direction mSortDir;
};

TEST_CASE("Sort test for s32bit datatype.", "[sort-s32bit]") {
  OCL_TESTCASE_BASIC_INIT();

  std::vector<size_t> test_counts = { 100, 1000, 100000, 10000000 };

  for (auto unique_keys : { true, false }) {
    for (auto negative_keys : { true, false }) {
      for (auto sortdir : { Sort::Descending, Sort::Ascending }) {
        for (auto count : test_counts) {
          auto ptest = SortTest<DTypeS32>(count, unique_keys, negative_keys,
                                          sortdir);
          REQUIRE(ptest.test(device));
        }
      }
    }
  }
}

TEST_CASE("Sort test for f32bit datatype.", "[sort-f32bit]") {
  OCL_TESTCASE_BASIC_INIT();

  std::vector<size_t> test_counts = { 100, 1000, 100000, 10000000 };

  for (auto unique_keys : { true, false }) {
    for (auto negative_keys : { true, false }) {
      for (auto sortdir : { Sort::Descending, Sort::Ascending }) {
        for (auto count : test_counts) {
          auto ptest = SortTest<DTypeF32>(count, unique_keys, negative_keys,
                                          sortdir);
          REQUIRE(ptest.test(device));
        }
      }
    }
  }
}

} //namespace Ocelot
