#include "TestCase.h"

#include "BitOperation.h"
#include <iostream>

namespace Ocelot
{

using BitType = GET_TYPE(DTypeBitmap);

std::pair<BufferPtr, std::vector<BitType>> createBitmap(
    DevicePtr device, unsigned int count) {
  BufferPtr bitmap;
  std::vector<BitType> host_bitmap;
  bitmap = TestCase::generateBufferWithRandomData<DTypeBitmap>(
             device, count, TestCase::minValue<BitType>(),
             TestCase::maxValue<BitType>(), host_bitmap);

  return std::make_pair(bitmap, host_bitmap);
}

std::vector<BitType> createHostResult(
    BitOp op, const std::vector<BitType>& left,
    const std::vector<BitType>& right) {
  std::vector<BitType> result;

  auto vector_size = op == BIT_NOT ? left.size() :
                                     std::max(left.size(), right.size());
  for (auto i = 0u; i < vector_size; ++i) {
    switch (op) {
      case BIT_NOT:
        result.push_back(~left[i]);
        break;
      case BIT_AND:
      {
        if (i < std::min(left.size(), right.size())) {
          result.push_back(left[i] & right[i]);
        }
        break;
      }
      case BIT_OR:
      {
        if (i < std::min(left.size(), right.size())) {
          result.push_back(left[i] | right[i]);
        } else if (i < left.size()) {
          result.push_back(left[i]);
        } else {
          result.push_back(right[i]);
        }
        break;
      }
      case BIT_XOR:
      {
        if (i < std::min(left.size(), right.size())) {
          result.push_back(left[i] ^ right[i]);
        } else if (i < left.size()) {
          result.push_back(left[i]);
        } else {
          result.push_back(right[i]);
        }
        break;
      }
    }
  }

  return result;
}

void compareHostAndDeviceResult(const std::vector<BitType>& host_result,
                                const BufferPtr& device_buffer) {
  std::vector<BitType> device_result;
  device_result.resize(device_buffer->getElementCount());
  device_buffer->read(device_result.data()).wait();

  REQUIRE(host_result.size() == device_result.size());
  REQUIRE(TestCase::compareBitmapVectors(host_result, device_result,
                                         device_buffer->getCount()));
}

void test_bit_operation(const DevicePtr& device, const BitOp op) {
  auto counts = { 100u, 123u, 512u, 568u, 1024u, 2049u, 7569u, 87659u };

  for (auto lcount : counts) {
    auto left = createBitmap(device, lcount);

    for (auto rcount : counts) {
      auto right = createBitmap(device, rcount);
      auto result = BitOperation(op, left.first, right.first).getResult();

      auto host_result = createHostResult(op, left.second, right.second);
      compareHostAndDeviceResult(host_result, result);
    }
  }
}

TEST_CASE("Testing bit and operation", "[bit-and]") {
  OCL_TESTCASE_BASIC_INIT();

  test_bit_operation(device, BIT_AND);
}

TEST_CASE("Testing bit or operation", "[bit-or]") {
  OCL_TESTCASE_BASIC_INIT();

  test_bit_operation(device, BIT_OR);
}

TEST_CASE("Testing bit xor operation", "[bit-xor]") {
  OCL_TESTCASE_BASIC_INIT();

  test_bit_operation(device, BIT_XOR);
}

TEST_CASE("Testing bit not operation", "[bit-not]") {
  OCL_TESTCASE_BASIC_INIT();

  test_bit_operation(device, BIT_NOT);
}

} //namespace Ocelot
