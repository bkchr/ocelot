#ifndef TESTCASEDEFINES_H_
#define TESTCASEDEFINES_H_

#include "catch.hpp"

#include "DeviceManager.h"
#include "PlatformManager.h"
#include "CacheManager.h"
#include "Device.h"
#include "Buffer.h"
#include "Profiler.h"

#include <random>

namespace Ocelot
{
  class Column;

  namespace TestCase
  {

    #define OCL_TESTCASE_BASIC_INIT()                                          \
              Ocelot::Profiler::deleteSingletonPtr();                          \
              auto plmgr = std::make_shared<Ocelot::PlatformManager>();        \
              auto cmgr = std::make_shared<Ocelot::CacheManager>();            \
              auto dmanager = std::make_shared<Ocelot::DeviceManager>          \
                                (plmgr, cmgr, KERNEL_SOURCE_DIR);              \
              auto device = dmanager->getDevices().front()

    typedef std::function<void(BufferPtr&)> BufferFillFunction;
    /**
     * @brief Generates a buffer.
     * @param dev   The device which should be used to create the buffer.
     * @param count The number of elements in the buffer.
     * @param type  The datatype of the generated buffer.
     * @param func  The function which should be used to fill the buffer, if the
     *              function is null(default), the buffer is initialized with
     *              zeros.
     * @return      The generated buffer.
     */
    BufferPtr generateBuffer(Pointer<Device> dev, size_t count,
                             DataType type = DTypeS32,
                             const BufferFillFunction& func = nullptr);

    //! Wrapper to support lambda fill functions in the generateBuffer function
    template<typename Function>
    BufferPtr generateBuffer(Pointer<Device> dev, DataType type,
                             const Function& func) {
      return generateBuffer(dev, type, BufferFillFunction(func));
    }

    enum SortDir
    {
      Ascending,
      Descending,
      None
    };

    template<typename T,
             typename std::enable_if<
               std::is_floating_point<T>::value, int>::type = 0>
    auto createRandomNumberGenerator(const T lower_bound, const T upper_bound) {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_real_distribution<T> distribution(lower_bound, upper_bound);

      return [gen, distribution]() mutable { return distribution(gen); };
    }

    template<typename T,
             typename std::enable_if<std::is_integral<T>::value, int>::type = 0>
    auto createRandomNumberGenerator(const T lower_bound, const T upper_bound) {
      std::random_device rd;
      std::mt19937 gen(rd());
      std::uniform_int_distribution<T> distribution(lower_bound, upper_bound);

      return [gen, distribution]() mutable { return distribution(gen); };
    }

    /**
     * @brief Fills a buffer with random data.
     * @param ptr             The buffer which should be filled.
     * @param low             The lower bound of the generated numbers.
     * @param up              The upper bound of the generated numbers.
     * @param low             The lower bound of the generated numbers.
     * @param host_data[out]  The vector holding the random numbers which were
     *                        used to fill the buffer.
     */
    template<DataType DType, typename Type = GET_TYPE(DType)>
    void fillBufferWithRandomData(
        BufferPtr& ptr, Type low, Type up,
        std::vector<Type>& host_data = std::vector<Type>(),
        SortDir sortdir = None) {
      static_assert(std::is_arithmetic<Type>::value,
                    "The function only supports buffers with "
                    "arithmetic types.");

      host_data.resize(ptr->getElementCount());
      auto generator_function = createRandomNumberGenerator(low, up);

      std::generate(host_data.begin(), host_data.end(), generator_function);

      if (sortdir == Ascending) {
        std::sort(host_data.begin(), host_data.end(), std::less<Type>());
      } else if (sortdir == Descending) {
        std::sort(host_data.begin(), host_data.end(), std::greater<Type>());
      }

      ptr->write<Type>(host_data.data()).wait();
    }

    /**
     * @brief Wrapper function to support direct generation and filling of a
     *        buffer with random data.
     * @param dev The device which should be used to create the buffer.
     * @param count           The number of elements in the buffer.
     * @param type            The datatype of the generated buffer.
     * @param low             The lower bound of the generated numbers.
     * @param up              The upper bound of the generated numbers.
     * @param host_data[out]  The vector holding the random numbers which were
     *                        used to fill the buffer.
     * @return    The generated buffer.
     */
    template<DataType DType, typename Type = GET_TYPE(DType)>
    BufferPtr generateBufferWithRandomData(
        Pointer<Device> dev, size_t count, GET_TYPE(DType) low,
        GET_TYPE(DType) up, std::vector<Type>& host_data = std::vector<Type>(),
        SortDir sortdir = None) {
      return generateBuffer(dev, count, DType,
                            std::bind(fillBufferWithRandomData<DType>,
                                      std::placeholders::_1, low, up,
                                      std::ref(host_data), sortdir));
    }

    Pointer<Column> generateColumnWithRawData(DataType type,
                                              const BufferPtr& ptr);

    template<typename Type>
    inline
    typename std::enable_if<std::is_floating_point<Type>::value, bool>::type
    equal(Type left, Type right) {
      if (left == right) {
        return true;
      }

      auto rel_error = std::abs(left - right) /
                       std::max(std::abs(left), std::abs(right));

      return rel_error <= 0.01;
    }

    template<typename Type>
    inline
    typename std::enable_if<!std::is_floating_point<Type>::value, bool>::type
    equal(Type left, Type right) {
      return left == right;
    }

    template<typename LType>
    typename std::enable_if<std::is_floating_point<LType>::value, LType>::type
    //! The numeric limits for floating points are just their normalized minimum
    //! and maximum values, to get the real minimum, we need to take the minus
    //! maximum
    //! http://stackoverflow.com/questions/7973737/why-are-flt-max-and-flt-min-
    //! not-positive-and-negative-infinity-and-what-is-thei
    minValue() {
      return -std::numeric_limits<LType>::max();
    }

    template<typename LType>
    typename std::enable_if<!std::is_floating_point<LType>::value, LType>::type
    minValue() {
      return std::numeric_limits<LType>::min();
    }

    template<typename LType>
    LType maxValue() {
      return std::numeric_limits<LType>::max();
    }

    template<typename T>
    T generateRandomNumber(const T lower_bound = minValue<T>(),
                           const T upper_bound = maxValue<T>()) {
      return createRandomNumberGenerator(lower_bound, upper_bound)();
    }

    bool compareBitmapVectors(const std::vector<GET_TYPE(DTypeBitmap)>& left,
                              const std::vector<GET_TYPE(DTypeBitmap)>& right,
                              unsigned int count);

  } //namespace TestCase

} //namespace Ocelot

#endif // TESTCASEDEFINES_H_

