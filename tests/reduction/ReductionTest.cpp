#include "TestCase.h"
#include "Reduction.h"

#include <bitset>

namespace Ocelot {

enum RDTParameter
{
  Bitmap,
  NoBitmap,
  BitmapNullReduction,
  NullReduction,
  PositiveValuesOnly,
  NegativeValuesOnly,
  ParameterCount
};

template<DataType DType, typename Type = GET_TYPE(DType)>
class ReductionTest
{
public:
  ReductionTest(unsigned int count, Reduction::Operation op,
                Reduction::Option opt, RDTParameter param) :
    mCount(count),
    mOperation(op),
    mOption(opt),
    mTestParameter(param) {

  }

  bool test(Pointer<Device> dev) {
    auto test_data = generateTestData(dev);
    auto bitmap = generateBitmap(dev, test_data.first);

    auto reduction = Reduction(test_data.second, bitmap, mOperation, mOption);

    if (mOperation == Reduction::SumLong) {
      return compareResults<DTypeS64>(reduction, test_data.first);
    } else if ((mOperation == Reduction::Sum && GET_TYPE_SIZE(DType) < 4) ||
               mOperation == Reduction::Count ||
               mOperation == Reduction::BitCount) {
      return compareResults<DTypeS32>(reduction, test_data.first);
    } else {
      return compareResults<DType>(reduction, test_data.first);
    }
  }

private:
  template<DataType LDType>
  bool compareResults(const Reduction& red, const std::vector<Type>& data) {
    auto result = red.getResult<LDType>();
    auto host_result = getExpectedResult<LDType>(data);
    logResult(host_result, result);
    return TestCase::equal(result, host_result);
  }

  std::pair<std::vector<Type>, BufferPtr> generateTestData(Pointer<Device> dev) {
    std::vector<Type> data;

    std::random_device rd;
    std::mt19937_64 rand(rd());

    for (unsigned int i = 0; i < mCount; ++i) {
      if (mOperation == Reduction::Count)
        data.push_back((rand() % 10 < 5) ? 1 : TestCase::minValue<Type>());
      else if (mOperation == Reduction::BitCount)
        data.push_back(rand() % 10000);
      else if (mTestParameter == PositiveValuesOnly) {
        auto val = mod(rand(), std::numeric_limits<Type>::max());
        // we don't want to get zeros
        data.push_back(val != 0 ? val : 1);
      } else if (mTestParameter == NegativeValuesOnly) {
        auto val = -mod(rand(), std::numeric_limits<Type>::max());
        // we don't want to get zeros
        data.push_back(val != 0 ? val : -1);
      } else {
        auto val = mod(rand(), std::numeric_limits<Type>::max());
        data.push_back(rand() % 2 ? -val : val);
      }
    }

    auto buffer = dev->allocateBuffer(DType, mCount);

    if (mTestParameter != NullReduction &&
        mTestParameter != BitmapNullReduction) {
      buffer->write(data.data()).wait();
    } else {
      prepareBufferForNullReduction(buffer, data);
    }

    return std::make_pair(data, buffer);
  }

  void prepareBufferForNullReduction(BufferPtr& buffer,
                                     std::vector<Type>& data) {
    // the null reduction data differs at the host and at the device, so we
    // need to create an extra vector and modify the data
    auto device_data = data;

    auto count = mCount / 10;
    for (unsigned int i = 0; i < count; ++i) {
      auto pos = rand() % mCount;

      if (mOperation == Reduction::Min) {
        data[pos] = std::numeric_limits<Type>::max();
      } else if (mOperation == Reduction::Max) {
        data[pos] = TestCase::minValue<Type>();
      } else if (mOperation != Reduction::Count ||
                 mOption != Reduction::CountIgnoreNil){
        data[pos] = 0;
      }

      device_data[pos] = TestCase::minValue<Type>();
    }

    buffer->write(device_data.data()).wait();
  }

  template<typename LType>
  typename std::enable_if<std::is_floating_point<LType>::value, LType>::type
  mod(unsigned long val, LType max) {
    return fmod(val, max);
  }

  template<typename LType>
  typename std::enable_if<!std::is_floating_point<LType>::value, LType>::type
  mod(unsigned long val, LType max) {
    return val % max;
  }

  BufferPtr generateBitmap(Pointer<Device> dev, std::vector<Type>& host_data) {
    if (mTestParameter != Bitmap && mTestParameter != BitmapNullReduction)
      return nullptr;

    auto bitmap_count = divisionWithRound(mCount, 32);
    std::vector<unsigned int> host_bitmap(mCount);

    unsigned int bitmap = 0;
    for (unsigned int i = 0; i < mCount; ++i) {
      if (i % 32 == 0 && i > 0) {
        host_bitmap[i / 32 - 1] = bitmap;
        bitmap = 0;
      }

      // enable or disable values via the bitmap
      if (std::rand() % 1000 > 9) {
        if (mOperation == Reduction::Min) {
          host_data[i] = std::numeric_limits<Type>::max();
        } else if (mOperation == Reduction::Max) {
          host_data[i] = TestCase::minValue<Type>();
        } else {
          host_data[i] = 0;
        }
      } else {
        bitmap |= 1 << (i % 32);
      }
    }
    host_bitmap[(mCount - 1) / 32] = bitmap;

    auto buffer = dev->allocateBuffer(DTypeS32, bitmap_count);
    buffer->write(host_bitmap.data()).wait();

    return buffer;
  }

  template<DataType LDType, typename LType = GET_TYPE(LDType)>
  LType getExpectedResult(const std::vector<Type>& host_data) {
    LType host_result = 0;
    switch (mOperation) {
      case Reduction::Count:
        host_result = std::count_if(host_data.begin(), host_data.end(),
                                    [this](auto val) {
          if (mOption == Reduction::CountIgnoreNil)
            return val != 0;
          else
            return val == 1;
        });
        break;
      case Reduction::Sum:      
      case Reduction::SumLong:
        host_result = std::accumulate(host_data.begin(), host_data.end(),
                                      LType(0));
        break;
      case Reduction::BitCount:
        for (auto value : host_data)
          host_result += std::bitset<sizeof(int) * 8>(value).count();
        break;
      case Reduction::Min:
        host_result = *std::min_element(host_data.begin(), host_data.end());
        break;
      case Reduction::Max:
        host_result = *std::max_element(host_data.begin(), host_data.end());
        break;
    }

    return host_result;
  }

  template<typename RType>
  void logResult(RType expected, RType actual) {
    // we only log if the values doesn't match
    if (!TestCase::equal(expected, actual)) {
      WARN("Expected: "<<expected<<" but the actual value is: "<<actual<<
           " for operation: "<<mOperation<<" with test parameter: "<<
           mTestParameter<<" and count of "<<mCount<<"."<<
           " With Option: "<<mOption);
    }
  }

  unsigned int mCount;
  Reduction::Operation mOperation;
  Reduction::Option mOption;
  RDTParameter mTestParameter;
};

bool parameterAndOperationSupported(int param, Reduction::Operation op) {
  if ((param == NullReduction || param == BitmapNullReduction) &&
      op == Reduction::BitCount) {
    return false;
  }

  return true;
}

template<DataType DType>
void testReduction(
    bool exception_expected = false,
    const std::vector<Reduction::Operation>& test_ops =
      { Reduction::Min, Reduction::Max, Reduction::Sum, Reduction::SumLong,
        Reduction::BitCount, Reduction::Count }) {
  OCL_TESTCASE_BASIC_INIT();
  std::vector<size_t> test_counts = { 100, 2048, 100000,
                                      device->getMaxComputeUnits() *
                                      device->getLocalMemorySize() };

  std::vector<Reduction::Option> options = { Reduction::NoOption,
                                             Reduction::CountIgnoreNil };

  for (int parameter = 0; parameter < ParameterCount;
       ++parameter) {
    for (auto count : test_counts) {
      for (auto op : test_ops) {
        for (auto opt : options) {
          // we don't support this combination
          if (!parameterAndOperationSupported(parameter, op))
            continue;

          auto rtest = ReductionTest<DType>(
                         count, op, opt, static_cast<RDTParameter>(parameter));

          if (exception_expected) {
            REQUIRE_THROWS(rtest.test(device));
          } else {
            REQUIRE(rtest.test(device));
          }
        }
      }
    }
  }
}

TEST_CASE("Reduction test signed 8Bit", "[reduction-s8bit]") {
  testReduction<DTypeS8>(false, { Reduction::Sum, Reduction::Max,
                                  Reduction::Min, Reduction::Count });
  testReduction<DTypeS8>(true, { Reduction::SumLong, Reduction::BitCount});
}

TEST_CASE("Reduction test signed 16Bit", "[reduction-s16bit]") {
  testReduction<DTypeS16>(false, { Reduction::Sum, Reduction::Max,
                                   Reduction::Min, Reduction::Count });
  testReduction<DTypeS16>(true, { Reduction::SumLong, Reduction::BitCount });
}

TEST_CASE("Reduction test signed 32Bit", "[reduction-s32bit]") {
  testReduction<DTypeS32>(false, { Reduction::SumLong, Reduction::BitCount,
                                   Reduction::Max, Reduction::Min,
                                   Reduction::Count });
  testReduction<DTypeS32>(true, { Reduction::Sum });
}

TEST_CASE("Reduction test signed 64Bit", "[reduction-s64bit]") {
  testReduction<DTypeS64>(true);
}

TEST_CASE("Reduction test floating 32Bit", "[reduction-f32bit]") {
  testReduction<DTypeF32>(false, { Reduction::Max, Reduction::Min,
                                   Reduction::Count, Reduction::Sum });
  testReduction<DTypeF32>(true, { Reduction::SumLong, Reduction::BitCount });
}

} //namespace Ocelot
