#include "TestCase.h"
#include "Profiler.h"

namespace Ocelot
{

unsigned long eventsExtractDuration(const std::vector<Event>& events) {
  unsigned long duration = 0;
  for (auto& event : events) {
    duration += static_cast<unsigned long>(
                  event.duration<boost::chrono::nanoseconds>().count());
  }

  return duration;
}

std::vector<Event> transferSomeData(Pointer<Device> device) {
  const unsigned int data_size = 500;
  auto buffer = device->allocateBuffer<DTypeS32>(data_size);

  std::vector<unsigned int> host_data;
  for (unsigned int i = 0; i < data_size; ++i) {
    host_data.push_back(i);
  }

  return buffer->write(host_data.data(), host_data.size()).getEvents();
}

TEST_CASE("Testing the profiler", "[profiler-test]") {
  OCL_TESTCASE_BASIC_INIT();
  OCL_OPERATION_SCOPE(
        OCL_GENERATE_OPERATION_ID(), "TestCaseOperator");

  std::vector<Event> events;

  {
    OCL_PROFILE_HOST_SCOPE();
    events = transferSomeData(device);
  }

  REQUIRE(Profiler::getSingleton().getProfilingInformation(
            OperationIndex::getCurrentOperationId(), PI_All_Runtimes) > 0);

  auto duration = eventsExtractDuration(events);
  REQUIRE(Profiler::getSingleton().getProfilingInformation(
            OperationIndex::getCurrentOperationId(), PI_Input_Transfer_Time) ==
            duration);
}

TEST_CASE("Testing OCL_PROFILE", "[profiler-profile-define]") {
  OCL_TESTCASE_BASIC_INIT();
  std::vector<Event> events;

  for (auto i = 0; i < 20; ++i) {

    auto duration = OCL_PROFILE(PI_Input_Transfer_Time,
                                events = transferSomeData(device););

    auto duration_direct = eventsExtractDuration(events);
    REQUIRE(duration == duration_direct);
  }
}

TEST_CASE("Testing nested profiling", "[profiler-nested]") {
  OCL_TESTCASE_BASIC_INIT();
  std::vector<Event> events, events2;
  OperationId parent, child;

  {
    parent = OCL_GENERATE_OPERATION_ID();
    OCL_OPERATION_SCOPE(parent, "Parent");

    OCL_PROFILE_HOST_SCOPE();
    events = transferSomeData(device);

    {
      child = OCL_GENERATE_OPERATION_ID();
      OCL_OPERATION_SCOPE(child, "Child");
      OCL_PROFILE_HOST_SCOPE();
      events2 = transferSomeData(device);
    }

    {
      OCL_OPERATION_SCOPE(child, "Child");
      OCL_PROFILE_HOST_SCOPE();
      auto tmp_events = transferSomeData(device);
      events2.insert(events2.end(), tmp_events.begin(), tmp_events.end());
    }
  }

  auto parent_full_runtime = Profiler::getSingleton().getProfilingInformation(
                               parent, PI_Host_Runtime |
                               PI_Nested_Operations_Runtime);

  auto parent_only_runtime = Profiler::getSingleton().getProfilingInformation(
                               parent, PI_Host_Runtime);

  auto child_full_runtime = Profiler::getSingleton().getProfilingInformation(
                              child, PI_Host_Runtime |
                              PI_Nested_Operations_Runtime);

  REQUIRE(parent_full_runtime == parent_only_runtime + child_full_runtime);

  parent_full_runtime = Profiler::getSingleton().getProfilingInformation(
                          parent, PI_Input_Transfer_Time |
                          PI_Nested_Operations_Runtime);

  parent_only_runtime = Profiler::getSingleton().getProfilingInformation(
                          parent, PI_Input_Transfer_Time);

  child_full_runtime = Profiler::getSingleton().getProfilingInformation(
                         child, PI_Input_Transfer_Time |
                         PI_Nested_Operations_Runtime);

  REQUIRE(parent_full_runtime == parent_only_runtime + child_full_runtime);
  REQUIRE(parent_only_runtime == eventsExtractDuration(events));
  REQUIRE(child_full_runtime == eventsExtractDuration(events2));
}

TEST_CASE("Testing 2x nested profiling", "[profiler-2x-nested]") {
  OCL_TESTCASE_BASIC_INIT();
  std::vector<Event> p_events, c0_events, c1_events, c2_events;
  OperationId parent, child0, child1, child2;

  {
    parent = OCL_GENERATE_OPERATION_ID();
    OCL_OPERATION_SCOPE(parent, "Parent");

    OCL_PROFILE_HOST_SCOPE();
    p_events = transferSomeData(device);

    {
      child0 = OCL_GENERATE_OPERATION_ID();
      OCL_OPERATION_SCOPE(child0, "Child0");
      OCL_PROFILE_HOST_SCOPE();
      c0_events = transferSomeData(device);

      {
        child2 = OCL_GENERATE_OPERATION_ID();
        OCL_OPERATION_SCOPE(child2, "Child2");
        OCL_PROFILE_HOST_SCOPE();
        c2_events = transferSomeData(device);
      }
    }

    {
      child1 = OCL_GENERATE_OPERATION_ID();
      OCL_OPERATION_SCOPE(child1, "Child1");
      OCL_PROFILE_HOST_SCOPE();
      c1_events = transferSomeData(device);

      {
        OCL_OPERATION_SCOPE(child2, "Child2");
        OCL_PROFILE_HOST_SCOPE();
        auto tmp_events = transferSomeData(device);
        c2_events.insert(c2_events.end(), tmp_events.begin(), tmp_events.end());
      }
    }
  }

  auto parent_full_runtime = Profiler::getSingleton().getProfilingInformation(
                               parent, PI_Host_Runtime |
                               PI_Nested_Operations_Runtime);

  auto parent_only_runtime = Profiler::getSingleton().getProfilingInformation(
                               parent, PI_Host_Runtime);

  auto child0_only_runtime = Profiler::getSingleton().getProfilingInformation(
                               child0, PI_Host_Runtime);

  auto child1_only_runtime = Profiler::getSingleton().getProfilingInformation(
                               child1, PI_Host_Runtime);

  auto child2_full_runtime = Profiler::getSingleton().getProfilingInformation(
                               child2, PI_Host_Runtime |
                               PI_Nested_Operations_Runtime);

  auto child2_only_runtime = Profiler::getSingleton().getProfilingInformation(
                               child2, PI_Host_Runtime);

  REQUIRE(parent_full_runtime == parent_only_runtime + child0_only_runtime +
          child1_only_runtime + child2_only_runtime);

  REQUIRE(child2_full_runtime == child2_only_runtime);

  parent_full_runtime = Profiler::getSingleton().getProfilingInformation(
                          parent, PI_Input_Transfer_Time |
                          PI_Nested_Operations_Runtime);

  parent_only_runtime = Profiler::getSingleton().getProfilingInformation(
                          parent, PI_Input_Transfer_Time);

  child0_only_runtime = Profiler::getSingleton().getProfilingInformation(
                          child0, PI_Input_Transfer_Time);

  child1_only_runtime = Profiler::getSingleton().getProfilingInformation(
                          child1, PI_Input_Transfer_Time);

  child2_full_runtime = Profiler::getSingleton().getProfilingInformation(
                          child2, PI_Input_Transfer_Time |
                          PI_Nested_Operations_Runtime);

  child2_only_runtime = Profiler::getSingleton().getProfilingInformation(
                          child2, PI_Input_Transfer_Time);

  REQUIRE(parent_full_runtime == parent_only_runtime + child0_only_runtime +
          child1_only_runtime + child2_only_runtime);

  REQUIRE(child0_only_runtime == eventsExtractDuration(c0_events));
  REQUIRE(child1_only_runtime == eventsExtractDuration(c1_events));
  REQUIRE(child2_full_runtime == child2_only_runtime);
  REQUIRE(child2_full_runtime == eventsExtractDuration(c2_events));
}

} //namespace Ocelot
