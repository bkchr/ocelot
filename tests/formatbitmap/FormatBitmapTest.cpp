#include "TestCase.h"

#include "FormatBitmap.h"
#include "FormatRaw.h"
#include "Column.h"
#include "CacheCostCalculator.h"
#include "PrefixSum.h"

#include <bitset>

namespace Ocelot {

using BitmapType = GET_TYPE(DTypeBitmap);
  
void createBitmapCol(unsigned int count, ColumnPtr& col, 
                     std::vector<BitmapType>& host_data, Pointer<Device> dev) {
  col = Column::createInstance<DTypeS32>();

  REQUIRE(col != nullptr);

  auto buffer = TestCase::generateBufferWithRandomData<DTypeBitmap>(
                  dev, count, 0, 1000000, host_data);
  col->registerData<Format::Bitmap>(buffer,
                                    CacheCost::Direct::createInstance(1));
}
  
TEST_CASE("Testing FormatBitmap registration.", "[bitmap-register]") {
  OCL_TESTCASE_BASIC_INIT();
  const auto count = GET_COUNT_PER_ELEMENT(DTypeBitmap) * 100;
  std::vector<BitmapType> host_data, device_data;
  ColumnPtr column;
  createBitmapCol(count, column, host_data, device);

  REQUIRE(column->hasFormat<Format::Bitmap>());
  REQUIRE_FALSE(column->hasFormat<Format::Raw>());
  REQUIRE(column->getData<Format::Bitmap>(device) != nullptr);
  REQUIRE(column->getCount() == count);

  auto colbuffer = column->getData<Format::Bitmap>(device);
  REQUIRE(colbuffer->getElementCount() == host_data.size());
  device_data.resize(colbuffer->getElementCount());
  colbuffer->read(device_data.data()).wait();

  REQUIRE(std::equal(host_data.begin(), host_data.end(), device_data.begin()));
}

TEST_CASE("Testing FormatBitmap get offsets.", "[bitmap-get-offsets]") {
  OCL_TESTCASE_BASIC_INIT();

  const auto count = GET_COUNT_PER_ELEMENT(DTypeBitmap) * 100;
  std::vector<BitmapType> host_data, offsets_col, offsets_psum;
  ColumnPtr column;
  createBitmapCol(count, column, host_data, device);

  auto bitmap = column->getData<Format::Bitmap>(device);
  auto offsets = column->getData<Format::Bitmap>(device, true);
  PrefixSum psum(bitmap);

  REQUIRE(offsets->getElementCount() ==
          psum.getResultBuffer()->getElementCount());

  offsets_col.resize(offsets->getElementCount());
  offsets_psum.resize(offsets->getElementCount());
  offsets->read(offsets_col.data()).wait();
  psum.getResultBuffer()->read(offsets_psum.data()).wait();

  REQUIRE(std::equal(offsets_col.begin(), offsets_col.end(),
                     offsets_psum.begin()));
}

TEST_CASE("Testing FormatBitmap register and get DeviceDataVector.",
          "[bitmap-register-get-devicedatavector]") {
  OCL_TESTCASE_BASIC_INIT();

  const auto subbuffer_count = 2;
  const auto count = subbuffer_count * dmanager->getGlobalVectorSize() /
                     GET_TYPE_SIZE(DTypeBitmap);
  std::vector<BitmapType> host_data, device_data;

  auto buffer = TestCase::generateBufferWithRandomData<DTypeBitmap>(
                  device, count, 0, 1000000, host_data);

  REQUIRE(buffer->getMaxNumberOfSubBuffer() == subbuffer_count);

  auto column = Column::createInstance<DTypeS32>();
  column->registerDeviceDataVector<Format::Bitmap>(
        buffer->createSubBuffer(0), CacheCost::Direct::createInstance(1), 0);
  column->registerDeviceDataVector<Format::Bitmap>(
        buffer->createSubBuffer(1), CacheCost::Direct::createInstance(1), 1);

  REQUIRE(column->getCount() == buffer->getCount());

  auto buffer2 = column->getData<Format::Bitmap>(device);
  REQUIRE(buffer2->getCount() == buffer->getCount());
  device_data.resize(buffer2->getElementCount());
  buffer2->read(device_data.data()).wait();

  REQUIRE(std::equal(host_data.begin(), host_data.end(), device_data.begin()));
}

void compareRawWithBitmap(const std::vector<BitmapType> &raw_data,
                          const std::vector<BitmapType> &bitmap_data) {
  unsigned int raw_index = 0, index = 0;
  for (auto bitmap : bitmap_data) {
    std::bitset<sizeof(GET_TYPE(DTypeBitmap)) * 8> bitset(bitmap);

    for (auto i = 0u; i < bitset.size(); ++i) {
      if (bitset.test(i)) {
        REQUIRE(index == raw_data[raw_index++]);
      } else if (raw_index < raw_data.size()) {
        REQUIRE(index != raw_data[raw_index]);
      }
      ++index;
    }
  }
}

TEST_CASE("Testing FormatBitmap to FormatRaw conversion.",
          "[bitmap-to-raw-conversion]") {
  OCL_TESTCASE_BASIC_INIT();

  const auto counts = { 1u, 5u, 7u, 8u, 27u, 33u, 45u };

  for (auto itr_count : counts) {
    const auto count = GET_COUNT_PER_ELEMENT(DTypeBitmap) * itr_count;
    std::vector<BitmapType> host_data_bitmap, device_data_raw;
    ColumnPtr column;
    createBitmapCol(count, column, host_data_bitmap, device);

    auto buffer = column->getData<Format::Raw>(device);
    REQUIRE(buffer != nullptr);
    REQUIRE(buffer->getCount() == count);
    REQUIRE(buffer->getType() == DTypeS32);

    device_data_raw.resize(buffer->getElementCount());
    buffer->read(device_data_raw.data()).wait();

    compareRawWithBitmap(device_data_raw, host_data_bitmap);
  }
}

void createFormatRawColumnAndTransferBitmapToHost(
    bool dense, unsigned int count, const Pointer<Device> &device,
    std::vector<BitmapType> &raw_data, std::vector<BitmapType> &bitmap_data) {
  for (auto i = 0u; i < count; ++i) {
    if (dense || std::rand() % 2) {
      raw_data.push_back(i);
    }
  }
  auto buffer = device->allocateBuffer(DTypeS32, raw_data.size());
  buffer->write(raw_data.data()).wait();

  ColumnPtr column = Column::createInstance(DTypeS32);
  column->setDense(dense);
  column->setSorted(true);
  column->registerData<Format::Raw>(buffer,
                                    CacheCost::Direct::createInstance(1));

  auto bitmap_buffer = column->getData<Format::Bitmap>(device);
  REQUIRE(bitmap_buffer != nullptr);
  //TODO
  //REQUIRE(bitmap_buffer->getCount() == buffer->getCount());
  REQUIRE(bitmap_buffer->getType() == DTypeBitmap);

  bitmap_data.resize(bitmap_buffer->getElementCount());
  bitmap_buffer->read(bitmap_data.data()).wait();
}

void test_raw_to_bitmap(bool dense) {
  OCL_TESTCASE_BASIC_INIT();

  const auto counts = { 32u, 64u, 100u, 1000u, 2345u, 5347u, 6000u, 98756u,
                        32u * 1024u };

  for (auto count : counts) {
    std::vector<BitmapType> raw, bitmap;
    createFormatRawColumnAndTransferBitmapToHost(dense, count, device, raw,
                                                 bitmap);
    compareRawWithBitmap(raw, bitmap);
  }
}

TEST_CASE("Testing FormatRaw to FormatBitmap dense conversion.",
          "[raw-to-bitmap-conversion-dense]") {
  test_raw_to_bitmap(true);
}

TEST_CASE("Testing FormatRaw to FormatBitmap sparse conversion.",
          "[raw-to-bitmap-conversion-sparse]") {
  test_raw_to_bitmap(false);
}


} //namespace Ocelot
