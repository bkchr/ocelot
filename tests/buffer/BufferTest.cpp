#include "TestCase.h"

#include "Platform.h"

#include "boost/compute/user_event.hpp"

#include <condition_variable>
#include <thread>
#include <chrono>

namespace Ocelot
{

#define TEST_WITH_ALL_TYPES(func, ...)                                         \
  func<DTypeS8>(__VA_ARGS__);                                                  \
  func<DTypeS16>(__VA_ARGS__);                                                 \
  func<DTypeS32>(__VA_ARGS__);                                                 \
  func<DTypeS64>(__VA_ARGS__);                                                 \
  func<DTypeBitmap>(__VA_ARGS__);                                              \
  func<DTypeF32>(__VA_ARGS__);

template<DataType DType>
void test_buffer_allocation(Pointer<Device> device) {
  auto counts = { 127u, 128u, 256u, 257u, 777u, 847u, 1024u };

  for (auto count : counts) {
    auto buffer = device->allocateBuffer<DType>(count);

    INFO(GET_DATATYPE_NAME(DType));
    INFO(count);

    REQUIRE_FALSE(buffer == nullptr);
    REQUIRE(buffer->getCount() == count);
    REQUIRE(buffer->getElementCount() ==
            divisionWithRound(buffer->getCount(),
                              GET_COUNT_PER_ELEMENT(DType)));
    auto size_in_bytes = divisionWithRound(GET_TYPE_SIZE(DType) * count,
                                           GET_COUNT_PER_ELEMENT(DType));

    if (size_in_bytes % GET_TYPE_SIZE(DType) != 0) {
      size_in_bytes += GET_TYPE_SIZE(DType) -
                       (size_in_bytes % GET_TYPE_SIZE(DType));
    }

    REQUIRE(buffer->getSizeInBytes() == size_in_bytes);
  }
}

TEST_CASE("Testing buffer allocation", "[buffer-allocation]") {
  OCL_TESTCASE_BASIC_INIT();

  TEST_WITH_ALL_TYPES(test_buffer_allocation, device);
}

template<DataType DType>
void test_buffer_read_write(Pointer<Device> device) {
  std::vector<GET_TYPE(DType)> host_data;
  unsigned int count = 1000;
  for (unsigned int i = 0; i <= count; ++i) {
    host_data.push_back(i);
  }

  for (unsigned int i = 1; i <= 10; ++i) {
    unsigned int localcount = i * count / 10;
    auto buffer = device->allocateBuffer<DType>(localcount);

    REQUIRE_FALSE(buffer == nullptr);

    buffer->write(host_data.data());
    std::vector<GET_TYPE(DType)> localdata(buffer->getElementCount());
    buffer->read(localdata.data()).wait();

    REQUIRE(std::equal(host_data.begin(),
                       host_data.begin() + buffer->getElementCount(),
                       localdata.begin()));
  }
}

TEST_CASE("Testing buffer read and write", "[buffer-read-write]") {
  OCL_TESTCASE_BASIC_INIT();

  TEST_WITH_ALL_TYPES(test_buffer_read_write, device);
}

template<DataType DType>
void test_buffer_full_read(Pointer<Device> device) {
  const unsigned int count = 1000;
  std::vector<GET_TYPE(DType)> host_data, result_data(count);

  for (unsigned int i = 0; i < count; ++i) {
    host_data.push_back(i);
  }

  auto buffer = device->allocateBuffer<DType>(count);
  buffer->write(host_data.data());
  buffer->read(result_data.data()).wait();

  REQUIRE(std::equal(host_data.begin(),
                     host_data.begin() + buffer->getElementCount(),
                     result_data.begin()));
}

TEST_CASE("Testing buffer full read", "[buffer-full-read]") {
  OCL_TESTCASE_BASIC_INIT();

  TEST_WITH_ALL_TYPES(test_buffer_full_read, device);
}

TEST_CASE("Testing buffer clone", "[buffer-clone]") {
  OCL_TESTCASE_BASIC_INIT();

  std::vector<int> host_data;

  unsigned int count = 1000;
  for (unsigned int i = 0; i < count; ++i) {
    host_data.push_back(i);
  }

  auto buffer = device->allocateBuffer<DTypeS32>(count);
  buffer->write(host_data.data(), count);
  auto buffer2 = buffer->clone();
  auto future = buffer2->read<int>();
  int* data = future.get().get();

  REQUIRE(std::equal(host_data.begin(), host_data.end(), data));
}

TEST_CASE("Testing buffer clone at other device",
          "[buffer-clone-other-device]") {
  OCL_TESTCASE_BASIC_INIT();

  // we need at least 2 devices for this test
  if (dmanager->getDevices().size() < 2)
    return;

  std::vector<int> host_data;

  unsigned int count = 1000;
  for (unsigned int i = 0; i < count; ++i) {
    host_data.push_back(i);
  }

  auto buffer = device->allocateBuffer<DTypeS32>(count);
  buffer->write(host_data.data(), count);
  auto buffer2 = buffer->clone(dmanager->getDevices()[1]);
  auto future = buffer2->read<int>();
  int* data = future.get().get();

  REQUIRE(std::equal(host_data.begin(), host_data.end(), data));
}

template<DataType DType>
void test_buffer_subbuffer_create(Pointer<Device> device,
                                  Pointer<DeviceManager> dmanager) {
  INFO(GET_DATATYPE_NAME(DType));

  std::vector<GET_TYPE(DType)> host_data;

  unsigned int vector_count = dmanager->getGlobalVectorSize() /
                              GET_TYPE_SIZE(DType);

  const auto sub_buffer_count = 2;
  unsigned int count = vector_count * sub_buffer_count;
  for (unsigned int i = 0; i < count; ++i) {
    host_data.push_back(i);
  }

  auto buffer = device->allocateBuffer<DType>(count);

  REQUIRE(buffer->getMaxNumberOfSubBuffer() == sub_buffer_count);

  buffer->write(host_data.data());
  auto buffer2 = buffer->createSubBuffer(0);

  REQUIRE(buffer2->getCount() == vector_count);
  REQUIRE(buffer2->getCount() % GET_COUNT_PER_ELEMENT(DTypeBitmap) == 0);

  std::vector<GET_TYPE(DType)> result_data(buffer2->getElementCount());
  buffer2->read(result_data.data()).wait();

  REQUIRE(std::equal(host_data.begin(),
                     host_data.begin() + buffer2->getElementCount(),
                     result_data.begin()));
}

TEST_CASE("Testing buffer subbuffer create", "[buffer-subbuffer-create]") {
  OCL_TESTCASE_BASIC_INIT();

  TEST_WITH_ALL_TYPES(test_buffer_subbuffer_create, device, dmanager);
}

template<DataType DType>
void test_buffer_copy(Pointer<Device> device,
                                  Pointer<DeviceManager> dmanager) {
  unsigned int vector_count = dmanager->getGlobalVectorSize() /
                              sizeof(GET_TYPE(DType));

  auto counts = { vector_count * 2, vector_count * 2 + 1, vector_count * 3,
                  vector_count * 3 + 10 };

  for (auto count : counts) {
    std::vector<GET_TYPE(DType)> host_data;

    auto buffer = device->allocateBuffer<DType>(count);
    for (unsigned int i = 0; i < buffer->getElementCount(); ++i) {
      host_data.push_back(i);
    }

    REQUIRE(buffer->getMaxNumberOfSubBuffer() ==
            divisionWithRound(count, vector_count));

    buffer->write(host_data.data());

    auto buffer2 = device->allocateBuffer<DType>(count);
    unsigned int offset = 0;
    for (unsigned short i = 0; i < buffer->getMaxNumberOfSubBuffer(); ++i) {
      auto sub_buffer = buffer->createSubBuffer(i);
      sub_buffer->copy(buffer2, sub_buffer->getCount(), 0, offset);
      offset += sub_buffer->getCount();
    }

    auto future = buffer2->template read<GET_TYPE(DType)>();
    auto data = future.get().get();

    REQUIRE(std::equal(host_data.begin(), host_data.end(), data));
  }
}

TEST_CASE("Testing buffer copy", "[buffer-copy]") {
  OCL_TESTCASE_BASIC_INIT();

  TEST_WITH_ALL_TYPES(test_buffer_copy, device, dmanager);
}

TEST_CASE("Testing buffer subbuffer event handling",
          "[buffer-subbuffer-events]") {
  OCL_TESTCASE_BASIC_INIT();

  unsigned int sub_buffer_count = 2;
  unsigned int vector_count = dmanager->getGlobalVectorSize() / sizeof(int);
  unsigned int count = vector_count * sub_buffer_count;
  auto buffer = device->allocateBuffer<DTypeS32>(count);

  REQUIRE(buffer->getMaxNumberOfSubBuffer() == sub_buffer_count);

  auto event = boost::compute::user_event(device->getPlatform()->getContext());
  buffer->addEvent(event);

  auto sub_buffer = buffer->createSubBuffer(0);
  std::condition_variable sub_buffer_condition;
  std::mutex mutex;
  std::unique_lock<std::mutex> lock(mutex);

  auto thread = std::thread([sub_buffer, &sub_buffer_condition]() {
    auto kernel = sub_buffer->getDevice()->getKernel("init_zero",
                                                     sub_buffer->getType());
    kernel->setArg(0, inout(sub_buffer));
    kernel->execute(sub_buffer->getCount()).wait();
    sub_buffer_condition.notify_all();
  });

  REQUIRE(sub_buffer_condition.wait_for(lock, std::chrono::seconds(10))
          == std::cv_status::timeout);

  event.set_status(boost::compute::event::complete);

  REQUIRE(sub_buffer_condition.wait_for(lock, std::chrono::seconds(10))
          == std::cv_status::no_timeout);

  thread.join();
}

} //namespace Ocelot
