#include "TestCase.h"
#include "Column.h"
#include "FormatRaw.h"
#include "CacheCostCalculator.h"

#include <random>
#include <functional>

namespace Ocelot
{

TEST_CASE("Testing column general setup.", "[column-general-setup]") {
  OCL_TESTCASE_BASIC_INIT();

  auto column = Column::createInstance<DTypeS32>();
  auto column2 = Column::createInstance<DTypeS32>();

  REQUIRE(column != nullptr);
  REQUIRE(column2 != nullptr);
  REQUIRE(column->getType() == column2->getType());
}

TEST_CASE("Testing column format registration.", "[column-format-register]") {
  OCL_TESTCASE_BASIC_INIT();

  auto column = Column::createInstance<DTypeS32>();
  const auto count = 100;

  std::vector<int> data(count);
  std::generate_n(data.begin(), count, []() { static int i = 0; return i++; });

  column->registerData<Format::Raw>(data.data(), data.size());

  REQUIRE(column->hasFormat<Format::Raw>());
}

TEST_CASE("Testing column format host to device data transformation.",
          "[column-format-host-to-device]") {
  OCL_TESTCASE_BASIC_INIT();

  auto column = Column::createInstance<DTypeS32>();
  const auto count = 100;

  std::vector<int> data(count), result_data;
  std::generate_n(data.begin(), count, []() { static int i = 0; return i++; });

  column->registerData<Format::Raw>(data.data(), data.size());

  REQUIRE(column->getCount() == data.size());

  auto buffer = column->getData<Format::Raw>(device);

  REQUIRE(buffer != nullptr);
  
  result_data.resize(data.size());
  buffer->read(result_data.data()).wait();
  
  REQUIRE(std::equal(data.begin(), data.end(), result_data.begin()));
}

TEST_CASE("Testing column format device to host data transformation.",
          "[column-format-device-to-host]") {
  OCL_TESTCASE_BASIC_INIT();

  auto column = Column::createInstance<DTypeS32>();
  std::vector<int> data;
  auto buffer = TestCase::generateBufferWithRandomData<DTypeS32>(device, 100, 0,
                                                                 100, data);

  column->registerData<Format::Raw>(buffer,
                                    CacheCost::Direct::createInstance(1));

  REQUIRE(column->getCount() == buffer->getCount());

  auto result_data =
      column->getData<Format::Raw, Location::Host, DTypeS32>().get();

  REQUIRE(result_data.second != nullptr);
  REQUIRE(result_data.first == buffer->getCount());
  REQUIRE(std::equal(data.begin(), data.end(), result_data.second.get()));
}

TEST_CASE("Testing column device data vector registration.",
          "[column-register-device-data-vector]") {
  OCL_TESTCASE_BASIC_INIT();

  auto column = Column::createInstance<DTypeS32>();
  std::vector<int> data, data2;
  auto buffer =
      TestCase::generateBufferWithRandomData<DTypeS32>(device, 100, 0, 100,
                                                       data);
  auto buffer2 =
      TestCase::generateBufferWithRandomData<DTypeS32>(device, 100, 0, 100,
                                                       data2);

  // register the two vectors independent
  column->registerDeviceDataVector<Format::Raw>(buffer,
                                      CacheCost::Direct::createInstance(1), 0);
  column->registerDeviceDataVector<Format::Raw>(buffer2,
                                      CacheCost::Direct::createInstance(1), 1);

  REQUIRE(column->getCount() == buffer->getCount() + buffer2->getCount());

  // retrieve both vectors as one buffer
  auto result_data =
      column->getData<Format::Raw, Location::Host, DTypeS32>().get();

  REQUIRE(result_data.second != nullptr);
  REQUIRE(result_data.first == buffer->getCount() + buffer2->getCount());
  REQUIRE(std::equal(data.begin(), data.end(), result_data.second.get()));
  REQUIRE(std::equal(data2.begin(), data2.end(),
                     result_data.second.get() + buffer->getCount()));
}

} //namespace Ocelot
