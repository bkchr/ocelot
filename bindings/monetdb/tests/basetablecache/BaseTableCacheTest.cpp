#include "MonetDBTestCase.h"

#include "MonetDBBaseTableCache.h"

namespace Ocelot
{

namespace MonetDB
{

TEST_CASE("Testing base table cache registration.",
          "[basetablecache-register-test]") {
  OCL_MDB_TEST_INIT();

  BaseTableCache cache;
  cache.registerBatAsBaseTable(0);

  REQUIRE(cache.isBatBaseTable(0));
  REQUIRE_FALSE(cache.isBatBaseTable(1));
}

} //namespace MonetDB

} //namespace Ocelot
