#ifndef MONETDBTESTCASE_H_
#define MONETDBTESTCASE_H_

#include "MonetDBCommon.h"

#include "Singleton.h"

#include "catch.hpp"

struct CLIENT;

namespace Ocelot
{

  namespace MonetDB
  {
    class Optimizer;
    class BaseTableCache;

    class TestCase : public Singleton<TestCase>
    {
    public:

      void executeTests(CLIENT* client);

      void init(const Pointer<BaseTableCache>& btcache = nullptr,
                const Pointer<Optimizer>& optimizer = nullptr);

      void executeSql(const std::string& query, bool ocelot_optimizer = true);

    private:
      CLIENT* createClientForSql();
      void freeClient(CLIENT* client);
      void setSqlOptimizer(bool ocelot_optimizer, CLIENT* client);
      void executeSqlIntern(const std::string& query, CLIENT* client);

      CLIENT* pClient;
    };

    #define OCL_MDB_TEST_INIT()                                                \
      Ocelot::MonetDB::TestCase::getSingleton().init()

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBTESTCASE_H_

