#define CATCH_CONFIG_RUNNER
#include "MonetDBTestCase.h"

#include "MonetDBSystem.h"
#include "MonetDBOptimizer.h"

MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "sql_execute.h"
  #include "sql_scenario.h"
MDB_INCLUDE_END()

namespace Ocelot
{

namespace MonetDB
{

void TestCase::executeTests(CLIENT* client) {
  static Catch::Session csession;

  pClient = client;

  csession.run();

  // after all tests are finished, we recreate a fresh state of the system
  init();
}

void TestCase::init(const Pointer<BaseTableCache>& btcache,
                    const Pointer<Optimizer>& optimizer) {
  System::deleteSingletonPtr();

  System::getSingleton().setBaseTableCache(btcache);
  System::getSingleton().setOptimizer(optimizer);

  System::getSingleton().setup(pClient);
}

void TestCase::executeSql(const std::string& query, bool ocelot_optimizer) {
  // create the client for the query
  auto client = createClientForSql();

  setSqlOptimizer(ocelot_optimizer, client);
  executeSqlIntern(query, client);

  freeClient(client);
}

void TestCase::executeSqlIntern(const std::string &query, CLIENT* client) {
  StringLiteral query_str(query);
  REQUIRE(SQLstatementIntern(client, query_str,
                             "testcase"_mstr, true, false,
                             nullptr) == MAL_SUCCEED);
}

CLIENT* TestCase::createClientForSql() {
  auto client = MCinitClient(CONSOLE, nullptr, nullptr);
  client->nspace = newModule(NULL, putName("user", 4));
  client->user = 0;
  setScenario(client, "sql"_mstr);
  SQLinitClient(client);
  MSinitClientPrg(client, "user"_mstr, "main"_mstr);
  MCinitClientThread(client);

  return client;
}

void TestCase::freeClient(CLIENT* client) {
  SQLexitClient(client);
}

void TestCase::setSqlOptimizer(bool ocelot_optimizer, CLIENT* client) {
  std::string optimizer_query = "set optimizer='";
  if (ocelot_optimizer) {
    optimizer_query += Optimizer::OptimizerName.asStdString();
  } else {
    optimizer_query += "default_pipe";
  }

  optimizer_query += "';";
  executeSqlIntern(optimizer_query, client);
}

} //namespace MonetDB

} //namespace Ocelot
