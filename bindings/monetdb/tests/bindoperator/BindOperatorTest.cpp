#include "MonetDBTestCase.h"

#include "MonetDBBaseTableCache.h"

namespace Ocelot
{

namespace MonetDB
{

class BaseTableCacheCalled : public BaseTableCache
{
public:

  void registerBatAsBaseTable(bat bid) override {
    ++mRegisteredBaseTables;
    BaseTableCache::registerBatAsBaseTable(bid);
  }

  std::pair<unsigned short, Pointer<Ocelot::Operator>>
      getBatAsOperator(bat bid, bool createifnotexist) override {
    auto result = BaseTableCache::getBatAsOperator(bid, createifnotexist);

    if (result.second != nullptr) {
      ++mRetrivedBaseTables;
    }

    return result;
  }

  unsigned int getRegisteredBaseTables() {
    return mRegisteredBaseTables;
  }

  unsigned int getRetrivedBaseTables() {
    return mRetrivedBaseTables;
  }

private:
  unsigned int mRegisteredBaseTables = 0;
  unsigned int mRetrivedBaseTables = 0;
};

TEST_CASE("Testing bind operator with sql.", "[bind-operator-sql-test]") {
  auto cache = std::make_shared<BaseTableCacheCalled>();

  TestCase::getSingleton().init(cache);

  TestCase::getSingleton().executeSql("select l_partkey from lineitem;");

  REQUIRE(cache->getRegisteredBaseTables() > 0);
  REQUIRE(cache->getRetrivedBaseTables() == 1);
}

} //namespace MonetDB

} //namespace Ocelot
