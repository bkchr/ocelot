#ifndef MONETDBBATCACHE_H_
#define MONETDBBATCACHE_H_

#include "MonetDBCommon.h"

#include "MonetDBCache.h"

#include <unordered_map>
#include <unordered_set>
#include <mutex>

namespace Ocelot
{
  class Operator;
  class Column;

  namespace MonetDB
  {
    class BaseTableCache;

    class BatCache : public Cache
    {
    public:
      BatCache(Pointer<BaseTableCache> basetablecache);

      std::pair<unsigned short, Pointer<Ocelot::Operator>> getBatAsOperator(
          bat bid, bool createifnotexist) override;

    private:
      Pointer<BaseTableCache> mBaseTableCache;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBBATCACHE_H_

