#ifndef MONETDBMALUTILS_H_
#define MONETDBMALUTILS_H_

#include "MonetDBCommon.h"
#include "Exception.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "monetdb5/mal/mal.h"
  #include "monetdb5/mal/mal_instruction.h"
  #include "monetdb5/mal/mal_interpreter.h"
MDB_INCLUDE_END()

#include <type_traits>

namespace Ocelot
{

  namespace MonetDB
  {
    class QueryContext;

    // Helper function to generate a valid string object.
    #define putStr(s) putName(s, strlen(s))

    inline InstrPtr pushPtr(MalBlkPtr mb, InstrPtr q, ptr val) {
      if (q == nullptr)
        return nullptr;

      ValRecord cst;
      cst.vtype = TYPE_ptr;
      cst.val.pval = val;
      cst.len = 0;

      return pushArgument(mb, q, defConstant(mb, TYPE_ptr, &cst));
    }

    /** Gets the argument of a mal function call.
     *  @param index The index of the argument,
     *               the first argument has the index 0.
     */
    template<typename Type>
    typename std::enable_if<std::is_pointer<Type>::value, Type>::type
    inline getMalArg(MalStkPtr stk, InstrPtr pci, int index) {
      return static_cast<Type>(*(ptr*)getArgReference(stk, pci,
                                                      pci->retc + index));
    }

    template<typename Type>
    typename std::enable_if<!std::is_pointer<Type>::value, Type>::type
    inline getMalArg(MalStkPtr stk, InstrPtr pci, int index) {
      return *static_cast<Type*>(getArgReference(stk, pci, pci->retc + index));
    }

    template<typename Type>
    typename std::enable_if<std::is_pointer<Type>::value, Type>::type
    inline getMalReturnArg(MalStkPtr stk, InstrPtr pci, int index) {
      if (index >= pci->retc) {
        THROW_EXCEPTION("No return argument with index ", index, " available!");
      }

      return static_cast<Type>(*(ptr*)getArgReference(stk, pci, index));
    }

    template<typename Type>
    typename std::enable_if<!std::is_pointer<Type>::value, Type&>::type
    inline getMalReturnArg(MalStkPtr stk, InstrPtr pci, int index) {
      if (index >= pci->retc) {
        THROW_EXCEPTION("No return argument with index ", index, " available!");
      }

      return *static_cast<Type*>(getArgReference(stk, pci, index));
    }

    inline int getMalRetCount(InstrPtr pci) {
      return pci->retc;
    }

    inline int getMalArgCount(InstrPtr pci) {
      return pci->argc - getMalRetCount(pci);
    }    

    class AtomType
    {
    public:
      AtomType(int type);
      AtomType(const AtomType&& old);

      int getSize() const;
      std::string getName() const;

    private:
      int mType;
    };

    class Bat
    {
    public:
      Bat(bat id);
      Bat(int type, unsigned int count, int role = TRANSIENT);
      ~Bat();
      // Delete copy constructor, until we need them
      Bat(const Bat&) = delete;
      Bat(const Bat&& bat);

      bat getId() const;
      cl_uint getSizeInBytes();
      cl_uint getCount();
      void setCount(cl_uint count);
      char* getBasePtr();
      int getType() const;
      AtomType getAtomType() const;
      std::string getAtomTypeName() const;
      DataType getDataType() const;
      unsigned int getTypeByteSize() const;
      bool isDense();
      void setDense(bool dense);
      bool isSorted();
      void setSorted(bool sorted);
      bool isRevSorted();
      void setRevSorted(bool revsorted);
      bool isKey();
      void setKey(bool key);
      oid getSeqBase();
      void setSeqBase(oid base);
      bool hasNoNil() const;

      void keepRef(MalStkPtr stk, InstrPtr pci, int index);
      void setAsResult(MalStkPtr stk, InstrPtr pci, int index);
      void setupStringDictionary(const Bat& origin_bat);
      var_t locateStr(const char* str);
      void registerAsCopy(const Bat& original, QueryContext* context);

      /**
       * @brief Checks if one of the given types is equal to the type of the
       *        bat.
       * @return True if one given type is equal to the bat type, otherwise
       *         false
       */
      template<typename Arg, typename ...Args>
      bool checkType(Arg&& arg, Args&&... args) {
        if (arg == getType()) {
          return true;
        } else {
          return checkType(std::forward<Args>(args)...);
        }
      }

      template<typename Arg>
      bool checkType(Arg&& arg) {
        if (arg == getType()) {
          return true;
        } else {
          return false;
        }
      }

    private:
      void acquire();
      void release();

      BAT* pBat;
      bat mId;
      bool mReclaim;
      bool mUnfix;
    };

    template<>
    inline Bat getMalArg<Bat>(MalStkPtr stk, InstrPtr pci, int index) {
      return Bat(getMalArg<bat>(stk, pci, index));
    }

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBMALUTILS_H_

