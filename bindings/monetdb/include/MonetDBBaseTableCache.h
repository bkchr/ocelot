#ifndef MONETDBBASETABLECACHE_H_
#define MONETDBBASETABLECACHE_H_

#include "MonetDBCommon.h"

#include "MonetDBBatCache.h"

#include <mutex>
#include <unordered_set>

namespace Ocelot
{
  class Operator;

  namespace MonetDB
  {

    class BaseTableCache : public Cache
    {
    public:
      BaseTableCache();

      virtual void registerBatAsBaseTable(bat bid);
      bool isBatBaseTable(bat bid);

    private:
      //! All bat's that are registered as base table
      std::unordered_set<bat> mBaseTables;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBBASETABLECACHE_H_

