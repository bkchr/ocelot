#ifndef MONETDBSORTOPERATOR_H_
#define MONETDBSORTOPERATOR_H_

#include "MonetDBOperator.h"

namespace Ocelot
{

  namespace MonetDB
  {

    class SortOperator : public Operator
    {
    public:
      std::string getName() const override final;
      std::string getModule() const override final;

      void run(QueryContext* context, MalStkPtr stk,
               InstrPtr pci, int first_arg_index) const override final;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBSORTOPERATOR_H_

