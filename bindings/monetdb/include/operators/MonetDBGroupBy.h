#ifndef MONETDBGROUPBY_H_
#define MONETDBGROUPBY_H_

#include "MonetDBOperator.h"
#include "GroupedAggregation.h"

namespace Ocelot
{

  namespace MonetDB
  {
    class GroupBy : public Operator
    {
    public:
      GroupBy(GroupedAggregation::Operation op);

      std::string getName() const;
      std::string getModule() const;
      void run(QueryContext* context, MalStkPtr stk, InstrPtr pci,
               int first_arg_index) const;

    private:
      GroupedAggregation::Operation mOp;

      int getResultType(int input_type) const;
    };
  }

} //namespace Ocelot

#endif // MONETDBGROUPBY_H_
