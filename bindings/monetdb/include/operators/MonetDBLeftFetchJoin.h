#ifndef MONETDBLEFTFETCHJOIN_H_
#define MONETDBLEFTFETCHJOIN_H_

#include "MonetDBOperator.h"

namespace Ocelot
{

  namespace MonetDB
  {

    class LeftFetchJoinOperator : public Operator
    {
    public:
      std::string getName() const override final;
      std::string getModule() const override final;

      void run(QueryContext* context, MalStkPtr stk,
               InstrPtr pci, int first_arg_index) const override final;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBLEFTFETCHJOIN_H_

