#ifndef MONETDBSYNC_H_
#define MONETDBSYNC_H_

#include "MonetDBOperator.h"

namespace Ocelot
{

  namespace MonetDB
  {

    class SyncOperator : public Operator
    {
    public:
      std::string getName() const override final { return _getName(); }
      static std::string _getName() { return "sync"; }
      std::string getModule() const override final { return _getModule(); }
      static std::string _getModule();
      bool isResultDeviceResident(int index) const override final;

      void run(QueryContext* context, MalStkPtr stk,
               InstrPtr pci, int first_arg_index) const override final;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBSYNC_H_

