#ifndef MONETDBBINDOPERATOR_H_
#define MONETDBBINDOPERATOR_H_

#include "MonetDBOperator.h"

namespace Ocelot
{

  namespace MonetDB
  {

    class BindOperator : public Operator
    {
    public:
      std::string getName() const override final;
      std::string getModule() const override final;
      bool isResultDeviceResident(int index) const override final;

      void run(QueryContext* context, MalStkPtr stk,
               InstrPtr pci, int first_arg_index) const override final;

      std::pair<int, bool> insertInPlan(
          MalBlkPtr mplan, InstrRecord* const* instructions, int index,
          int count, int query_context,
          std::vector<int>& device_resident_vars) override final;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBBINDOPERATOR_H_

