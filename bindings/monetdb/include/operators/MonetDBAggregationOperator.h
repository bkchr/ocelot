#ifndef MONETDBAGGREGATIONOPERATOR_H_
#define MONETDBAGGREGATIONOPERATOR_H_

#include "MonetDBOperator.h"
#include "Reduction.h"
#include "MonetDBMalUtils.h"

#include <unordered_map>

namespace Ocelot
{
  class Column;
  class DeviceSelectionManager;

  namespace MonetDB
  {
    class AggregationOperator : public Operator
    {
    public:
      DI_CTOR(AggregationOperator, Reduction::Operation op,
              DeviceSelectionManager* dmanager);

      std::string getName() const override final;
      std::string getModule() const override final;
      bool isResultDeviceResident(int index) const override final;

      void run(QueryContext* context, MalStkPtr stk,
               InstrPtr pci, int first_arg_index) const override final;

    private:
      void setup();

      bool checkTypeAndOperation(const Bat& bat) const;
      void registerTypeOperations(int type, int operations);

      void doSum(const Pointer<Buffer>& buffer, MalStkPtr stk,
                 InstrPtr pci) const;
      void doMaxMin(const Pointer<Buffer>& buffer, MalStkPtr stk,
                    InstrPtr pci) const;
      void doCount(const Pointer<Column>& col, Pointer<Device> dev, MalStkPtr stk,
                   InstrPtr pci, int first_arg_index) const;

      template<typename RedType, typename MDBType = RedType>
      void setResult(MalStkPtr stk, InstrPtr pci, const Reduction& red) const {
        getMalReturnArg<MDBType>(stk, pci, 0) = red.getResult<RedType>();
      }

      Reduction::Operation mOperation;
      //! Saves the supported operations per type
      std::unordered_map<int, int> mTypeOperationSupport;
      DeviceSelectionManager* pDeviceSelectionManager;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBAGGREGATIONOPERATOR_H_

