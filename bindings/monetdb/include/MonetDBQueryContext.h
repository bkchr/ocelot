#ifndef MONETDBQUERYCONTEXT_H_
#define MONETDBQUERYCONTEXT_H_

#include "MonetDBCommon.h"

#include "MonetDBBatCache.h"
#include "DependencyInjection.h"

#include <vector>
#include <unordered_set>

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "gdk.h"
MDB_INCLUDE_END()

namespace Ocelot
{
  class Column;
  class Operator;
  class DeviceSelectionManager;

  namespace MonetDB
  {
    class Bat;

    class QueryContext
    {
    public:
      DI_CTOR(QueryContext, const Pointer<BaseTableCache>& cache);

      void registerBatAsBaseTable(bat bid);
      void registerBat(bat bid, const Pointer<Ocelot::Operator>& op,
                       unsigned short channel);
      void registerBat(const Bat& bat, const Pointer<Ocelot::Operator>& op,
                       unsigned short channel);
      Pointer<Column> getBatAsColumn(bat bid, bool createifnotexist = false);
      Pointer<Column> getBatAsColumn(const Bat& bat,
                                     bool createifnotexist = false);
      std::pair<unsigned short, Pointer<Ocelot::Operator>>
          getBatAsOperator(bat bid);
      std::pair<unsigned short, Pointer<Ocelot::Operator>>
          getBatAsOperator(const Bat& bat);

    private:
      std::unordered_set<bat> mUsedBats;
      BatCache mCache;
      Pointer<BaseTableCache> mBaseTableCache;
      DeviceSelectionManager* pDeviceSelectionManager;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBQUERYCONTEXT_H_

