#ifndef MONETDBCACHE_H_
#define MONETDBCACHE_H_

#include "MonetDBCommon.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "gdk.h"
MDB_INCLUDE_END()

#include <mutex>

namespace Ocelot
{
  class Operator;
  class Column;

  namespace MonetDB
  {

    class Cache
    {
    public:
      /**
       * @brief Constructs the cache.
       * @param copybatforcache True = bat is copied at transfer into cache.
       *                        False = bat pointer is used directly
       */
      Cache(bool copybatforcache);

      virtual void registerBat(bat bid, Pointer<Ocelot::Operator> op,
                               unsigned short channel);
      virtual Pointer<Column> getBatAsColumn(bat bid, bool createifnotexist);
      virtual std::pair<unsigned short, Pointer<Ocelot::Operator>>
          getBatAsOperator(bat bid, bool createifnotexist);
      /* Removes a bat from the cache.
       * @param bid The bat id.
       */
      virtual void removeBat(bat bid);

    protected:
      std::pair<unsigned short, Pointer<Ocelot::Operator>> cacheBat(bat bid);

      struct CacheEntry
      {
        Pointer<Ocelot::Operator> sOperator;
        unsigned short sChannel;

        CacheEntry() : sOperator(nullptr), sChannel(-1) {}

        CacheEntry(Pointer<Ocelot::Operator> op, unsigned short channel) :
          sOperator(op),
          sChannel(channel) {}

        CacheEntry(const CacheEntry& entry) :
          sOperator(entry.sOperator),
          sChannel(entry.sChannel) {}

        CacheEntry(const CacheEntry&& entry) :
          sOperator(std::move(entry.sOperator)),
          sChannel(std::move(entry.sChannel)) {}

        CacheEntry& operator=(const CacheEntry& other) {
          sOperator = other.sOperator;
          sChannel = other.sChannel;
          return *this;
        }

        CacheEntry& operator=(const CacheEntry&& other) {
          sOperator = std::move(other.sOperator);
          sChannel = std::move(other.sChannel);
          return *this;
        }
      };

      std::unordered_map<bat, CacheEntry> mCache;
      std::recursive_mutex mMutex;
      bool mCopyBatForCache;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBCACHE_H_

