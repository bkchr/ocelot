#ifndef MONETDBSYSTEM_H_
#define MONETDBSYSTEM_H_

#include "MonetDBCommon.h"

#include "Singleton.h"

#include <string>

struct CLIENT;
struct MALBLK;

namespace Ocelot
{
  class PlatformManager;
  class CacheManager;
  class DeviceManager;

  namespace MonetDB
  {
    class Optimizer;
    class BaseTableCache;
    class OperatorCatalogue;
    class QueryContext;

    class System : public Singleton<System>
    {
    public:
      System();

      bool setup(CLIENT* client);

      int optimize(MALBLK* mplan);

      static const std::string& getModuleName();

      void setBaseTableCache(const Pointer<BaseTableCache>& btcache);
      void setOptimizer(const Pointer<Optimizer>& optimizer);

      QueryContext* createQueryContext();

    private:
      void init(CLIENT* client);

      Pointer<Optimizer> mOptimizer;
      Pointer<BaseTableCache> mBaseTableCache;
      Pointer<OperatorCatalogue> mOperatorCatalogue;
      Pointer<PlatformManager> mPlatformManager;
      Pointer<CacheManager> mCacheManager;
      Pointer<DeviceManager> mDeviceManager;

      bool mSetupCalled = false;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBSYSTEM_H_

