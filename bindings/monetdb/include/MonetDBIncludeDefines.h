#ifndef MONETDBINCLUDEDEFINES_H_
#define MONETDBINCLUDEDEFINES_H_

namespace Ocelot
{

  namespace MonetDB
  {

    #define MDB_INCLUDE_BEGIN()                                                \
      _Pragma("GCC diagnostic push")                                           \
      _Pragma("GCC diagnostic ignored \"-Wpedantic\"")                         \
      extern "C" {                                                             \

    #define MDB_INCLUDE_END()                                                  \
      }                                                                        \
      _Pragma("GCC diagnostic pop")

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBINCLUDEDEFINES_H_

