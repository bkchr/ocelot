#ifndef MONETDBOPTIMIZER_H_
#define MONETDBOPTIMIZER_H_

#include "MonetDBCommon.h"

#include <utility>
#include <string>

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "monetdb5/mal/mal.h"
MDB_INCLUDE_END()

struct CLIENT;

namespace Ocelot
{

  namespace MonetDB
  {
    class Operator;
    class BaseTableCache;
    class OperatorCatalogue;

    class Optimizer
    {
    public:
      Optimizer(CLIENT* client, const Pointer<OperatorCatalogue>& opcatalogue);

      int optimize(MALBLK* mplan);

      static const StringLiteral OptimizerName;

    private:
      void registerInMonetDB(CLIENT* client);
      bool createNewStatementList(MALBLK* mplan);
      std::pair<int, int> buildPlan(MALBLK* mplan, InstrRecord** orig,
                                    int orig_count, int query_context,
                                    std::vector<int>& device_resident_vars);
      void finishPlan(MALBLK* mplan, InstrRecord** orig,
                      int orig_count, int orig_index, int orig_max_count);

      int insertNativeMonetDBOperator(MALBLK* mplan, InstrRecord* instr,
                                      int query_context,
                                      std::vector<int>& device_resident_vars);

      int insertSyncOperator(MALBLK* mplan, int var_id, int query_context,
                             std::vector<int>& device_resident_vars);

      std::pair<int, int> insertCreateQueryContext(MALBLK* mplan);
      int insertFreeQueryContext(MALBLK* mplan, int query_context);

      int handleEndSymbol(MALBLK* mplan, int query_context);

      Pointer<OperatorCatalogue> mOperatorCatalogue;
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBOPTIMIZER_H_

