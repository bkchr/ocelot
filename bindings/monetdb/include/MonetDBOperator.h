#ifndef MONETDBOPERATOR_H_
#define MONETDBOPERATOR_H_

#include "MonetDBCommon.h"

#include <string>

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "monetdb5/mal/mal.h"
MDB_INCLUDE_END()

namespace Ocelot
{
  class Operator;

  namespace MonetDB
  {
    class QueryContext;

    class Operator
    {
    public:
      virtual std::string getName() const = 0;
      virtual std::string getModule() const = 0;
      virtual bool isResultDeviceResident(int index) const { (void)index;
                                                             return true; }

      virtual std::pair<int, bool> insertInPlan(
            MalBlkPtr mplan, InstrRecord* const* instructions, int index,
            int count, int query_context,
            std::vector<int>& device_resident_vars);
      
      virtual void run(QueryContext* context, MalStkPtr stk, 
                       InstrPtr pci, int first_arg_index) const = 0;

      static void invoke(MalStkPtr stk, InstrPtr pci);

      template<template<DataType> class OpType, typename ...Args>
      static Pointer<Ocelot::Operator> createOperatorInstance(
          DataType data_type, Args&&... args) {
        return createOperatorInstance<OpType, 0>(
              data_type, std::forward<Args>(args)...);
      }

    private:
      template<template<DataType> class OpType, DataType DType,
               typename ...Args>
      static typename std::enable_if<DType < DataTypeNum,
        Pointer<Ocelot::Operator>>::type
      createOperatorInstance(DataType data_type, Args&&... args) {
        if (DType == data_type) {
          return Pointer<Ocelot::Operator>(DI_CREATE_INSTANCE(
                OpType<DType>,
                std::forward<Args>(args)...));
        } else {
          return createOperatorInstance<OpType, DType + 1>(
                data_type, std::forward<Args>(args)...);
        }
      }

      template<template<DataType> class OpType, DataType DType,
               typename ...Args>
      static typename std::enable_if<DType == DataTypeNum,
        Pointer<Ocelot::Operator>>::type
      createOperatorInstance(DataType data_type, Args&&...) {
        THROW_EXCEPTION("Couldn't create an operator instance for data type ",
                        data_type,
                        ", because the type is unknown type!");
      }
    };

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBOPERATOR_H_

