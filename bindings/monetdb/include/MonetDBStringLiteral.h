#ifndef MONETDBSTRINGLITERAL_H_
#define MONETDBSTRINGLITERAL_H_

#include "MonetDBIncludeDefines.h"

MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "gdk.h"
MDB_INCLUDE_END()

#include <string>

namespace Ocelot
{

  namespace MonetDB
  {

    class StringLiteral
    {
    public:
      StringLiteral(const char* chars, std::size_t count) {
        mCount = count + 1;
        mString = new char[mCount];
        std::memset(mString, 0, mCount);
        std::copy(chars, chars + count, mString);
      }

      StringLiteral(const std::string& str) {
        mCount = str.size() + 1;

        mString = new char[mCount];
        std::copy(str.c_str(), str.c_str() + mCount, mString);
      }

      StringLiteral(StringLiteral&& other) {
        mString = std::move(other.mString);
        other.mString = nullptr;
        mCount = std::move(other.mCount);
      }

      StringLiteral(StringLiteral& other) {
        mCount = other.mCount;

        mString = new char[mCount];
        std::copy(other.mString, other.mString + mCount, mString);
      }

      ~StringLiteral() {
        delete[] mString;
      }

      operator str* const () {
        return &mString;
      }

      operator str() const {
        return mString;
      }

      std::string asStdString() const {
        return std::string(mString, mCount - 1);
      }

    private:
      str mString;
      std::size_t mCount;
    };

  } //namespace MonetDB

} //namespace Ocelot

inline Ocelot::MonetDB::StringLiteral operator"" _mstr (const char* chars,
                                                        std::size_t count) {
  return Ocelot::MonetDB::StringLiteral(chars, count);
}

#endif // MONETDBSTRINGLITERAL_H_

