#ifndef MONETDBMALLCALLBACKS_H
#define MONETDBMALLCALLBACKS_H

extern "C"
{
  // MonetDB
  #include "monetdb_config.h"
  #include "monetdb5/mal/mal_client.h"

  extern str ocl_mal_setup(Client cntxt, MalBlkPtr mb, MalStkPtr stk,
                           InstrPtr pci);

  extern str ocl_mal_optimize(Client cntxt, MalBlkPtr mb, MalStkPtr stk,
                              InstrPtr p);

  extern str ocl_mal_invoke_operator(Client cntxt, MalBlkPtr mb, MalStkPtr stk,
                                     InstrPtr pci);

  extern str ocl_mal_create_query_context(ptr* context);

  extern str ocl_mal_free_query_context(ptr* context);

  extern str ocl_mal_execute_tests(Client cntxt, MalBlkPtr mb, MalStkPtr stk,
                                   InstrPtr pci);

} //extern "C"

#endif // MONETDBMALLCALLBACKS_H

