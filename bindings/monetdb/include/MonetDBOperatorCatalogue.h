#ifndef MONETDBOPERATORCATALOGUE_H_
#define MONETDBOPERATORCATALOGUE_H_

#include "MonetDBCommon.h"

#include <unordered_map>
#include <memory>

namespace Ocelot
{

  namespace MonetDB
  {
    class Operator;

    struct OperatorInstanceCreator
    {
      virtual ~OperatorInstanceCreator() {}
      virtual Pointer<Operator> createInstance() = 0;
    };

    class OperatorCatalogue
    {
    public:
      OperatorCatalogue();

      Operator* getOperator(const std::string& module,
                            const std::string& name) const;

      static void registerOperator(const Pointer<OperatorInstanceCreator>& op);

    private:
      static std::vector<Pointer<OperatorInstanceCreator>>&
        getRegisteredOperatorVector();

      std::unordered_map<std::string, Pointer<Operator>> mOperatorsMap;
    };

    template<typename Op>
    struct OperatorCatalogueRegistration
    {};

    /*
     * Registers a monetdb operator. If the operators has multiple variants,
     * all these variants has to be registered with
     * MDB_REGISTER_OPERATOR_VARIANT define!
     *
     * @param opname The Operator class
     * @param ...    Constructor arguments
     */
    #define MDB_REGISTER_OPERATOR(opname, ...)                                 \
      template<>                                                               \
      struct OperatorCatalogueRegistration<opname> :                           \
        public OperatorInstanceCreator                                         \
      {                                                                        \
        Pointer<Operator> createInstance() override {                          \
          return Pointer<Operator>(DI_CREATE_INSTANCE(opname, __VA_ARGS__));   \
        }                                                                      \
      private:                                                                 \
        static bool mRegister;                                                 \
      };                                                                       \
      bool OperatorCatalogueRegistration<opname>::mRegister =                  \
        (OperatorCatalogue::registerOperator(                                  \
          std::make_shared<OperatorCatalogueRegistration<opname>>()), true)

    /*
     * Registers a monetdb operator variant.
     *
     * @param opname The Operator class
     * @param vname  An unique identifier for this variant
     * @param ...    Constructor arguments
     */
    #define MDB_REGISTER_OPERATOR_VARIANT(opname, vname, ...)                  \
      class opname##vname {};                                                  \
      template<>                                                               \
      struct OperatorCatalogueRegistration<opname##vname> :                    \
        public OperatorInstanceCreator                                         \
      {                                                                        \
        Pointer<Operator> createInstance() override {                          \
          return Pointer<Operator>(DI_CREATE_INSTANCE(opname, __VA_ARGS__));   \
        }                                                                      \
      private:                                                                 \
        static bool mRegister;                                                 \
      };                                                                       \
      bool OperatorCatalogueRegistration<opname##vname>::mRegister =           \
        (OperatorCatalogue::registerOperator(                                  \
          std::make_shared<OperatorCatalogueRegistration<opname##vname>>()),   \
          true)

  } //namespace MonetDB

} //namespace Ocelot

#endif // MONETDBOPERATORCATALOGUE_H_

