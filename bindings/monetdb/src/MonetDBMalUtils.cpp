#include "MonetDBMalUtils.h"

#include "MonetDBQueryContext.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "gdk/gdk_bbp.h"

  #define LIBGDK
  #include "gdk/gdk_private.h"
  #undef LIBGDK
MDB_INCLUDE_END()

namespace Ocelot
{

namespace MonetDB
{

AtomType::AtomType(int type) :
 mType(ATOMtype(type)) {

}

AtomType::AtomType(const AtomType &&old) :
  mType(std::move(old.mType)) {

}

int AtomType::getSize() const {
  return ATOMsize(mType);
}

std::string AtomType::getName() const {
  return ATOMname(mType);
}

Bat::Bat(bat bid) :
  pBat(nullptr),
  mId(bid),  
  mReclaim(false),
  mUnfix(true) {
  acquire();
}

Bat::Bat(int type, unsigned int count, int role) :
  pBat(nullptr),
  mId(-1),
  mReclaim(true),
  mUnfix(false) {
  pBat = BATnew(TYPE_void, type, count, role);
  // We need to set this to 0 or monetdb won't output all tuples
  pBat->hseqbase = 0;
  mId = pBat->batCacheid;
  BATsetcount(pBat, count);
}

Bat::Bat(const Bat&& bat) :
  pBat(std::move(bat.pBat)),
  mId(std::move(bat.mId)),
  mReclaim(std::move(bat.mReclaim)),
  mUnfix(std::move(bat.mUnfix)) {

}

Bat::~Bat() {
  release();
}

void Bat::acquire() {
  pBat = BATdescriptor(mId);

  if (pBat == nullptr) {
    THROW_EXCEPTION("Bat with id ", mId, " doesn't exist!");
  }
}

void Bat::release() {
  if (mUnfix && mReclaim) {
    THROW_EXCEPTION("Either mUnfix or mReclaim can be set, "
                    "not both at the same time!");
  }

  if (pBat && mUnfix) {
    BBPunfix(mId);
  }
  if (pBat && mReclaim) {
    BBPreclaim(pBat);
  }

  pBat = nullptr;
}

bat Bat::getId() const {
  return mId;
}

cl_uint Bat::getCount() {
  return BATcount(pBat);
}

void Bat::setCount(cl_uint count) {
  BATextend(pBat, count);
  BATsetcount(pBat, count);
}

cl_uint Bat::getSizeInBytes() {
  return BATcount(pBat) * getTypeByteSize();
}

char* Bat::getBasePtr() {
  return Tloc(pBat, BUNfirst(pBat));
}

int Bat::getType() const {
  return pBat->ttype;
}

AtomType Bat::getAtomType() const {
  return AtomType(getType());
}

DataType Bat::getDataType() const {
  switch (getType()) {
    case TYPE_flt:
    case TYPE_dbl:
      return DTypeF32;
    case TYPE_str:
    {
      auto size = getTypeByteSize();
      switch (size) {
        case 1:
          return DTypeS8;
        case 2:
          return DTypeS16;
        default:
          return DTypeS32;
      }
    }
    case TYPE_sht:
      return DTypeS16;
    case TYPE_bte:
    case TYPE_bit:
      return DTypeS8;
    case TYPE_wrd:
    {
      auto size = getTypeByteSize();

      switch (size) {
        case 4:
          return DTypeS32;
        case 8:
          return DTypeS64;
        default:
          THROW_EXCEPTION("Unknow size!");
      }
    }
    default:
      return DTypeS32;
  }
}

unsigned int Bat::getTypeByteSize() const {
  auto size = Tsize(pBat);
  if (size > 0) {
    return size;
  } else {
    return getAtomType().getSize();
  }
}

bool Bat::isDense() {
  return BATtdense(pBat);
}

void Bat::setDense(bool dense) {
  pBat->tdense = dense;
}

bool Bat::isSorted() {
  return pBat->tsorted;
}

void Bat::setSorted(bool sorted) {
  pBat->tsorted = sorted;
}

bool Bat::isRevSorted() {
  return pBat->trevsorted;
}

void Bat::setRevSorted(bool revsorted) {
  pBat->trevsorted = revsorted;
}

bool Bat::isKey() {
  return pBat->tkey;
}

void Bat::setKey(bool key) {
  pBat->tkey = key;
}

oid Bat::getSeqBase() {
  return pBat->tseqbase;
}

void Bat::setSeqBase(oid base) {
  pBat->tseqbase = base;
}

bool Bat::hasNoNil() const {
  return pBat->T->nonil;
}

void Bat::keepRef(MalStkPtr stk, InstrPtr pci, int index) {
  setAsResult(stk, pci, index);
  mReclaim = false;
  BBPkeepref(mId);
}

void Bat::setAsResult(MalStkPtr stk, InstrPtr pci, int index) {
  auto arg_type = ATOMtype(getArgType(stk->blk, pci, index));
  auto type = ATOMtype(getType());

  if (isaBatType(arg_type) && getColumnType(arg_type) != type) {
    THROW_EXCEPTION("Bat type and return argument type doesn't match!");
  }

  getMalReturnArg<bat>(stk, pci, index) = mId;
}

void Bat::setupStringDictionary(const Bat& origin_bat) {
  if (getType() != TYPE_str)
    return;

  if (origin_bat.pBat->batRestricted == BAT_READ) {
    assert(origin_bat.pBat->T->vheap->parentid > 0);

    BBPshare(origin_bat.pBat->T->vheap->parentid);
    pBat->T->vheap = origin_bat.pBat->T->vheap;
  } else {
    pBat->T->vheap = static_cast<Heap*>(GDKzalloc(sizeof(Heap)));
    pBat->T->vheap->parentid = pBat->batCacheid;
    pBat->T->vheap->farmid = BBPselectfarm(pBat->batRole, TYPE_str, varheap);

    if (origin_bat.pBat->T->vheap->filename) {
      char* nme = BBP_physical(pBat->batCacheid);
      pBat->T->vheap->filename = GDKfilepath(NOFARM, NULL, nme, "theap");
    }

    HEAPcopy(pBat->T->vheap, origin_bat.pBat->T->vheap);
  }

  pBat->tvarsized = 1;
}

var_t Bat::locateStr(const char* str) {
  return strLocate(pBat->T->vheap, str);
}

void Bat::registerAsCopy(const Bat& original, QueryContext* context) {
  auto origop = context->getBatAsOperator(original);
  context->registerBat(mId, origop.second, origop.first);
}

} //namespace MonetDB

} //namespace Ocelot
