#include "MonetDBLeftFetchJoin.h"

#include "MonetDBQueryContext.h"
#include "LeftFetchJoinOperator.h"
#include "MonetDBMalUtils.h"
#include "MonetDBOperatorCatalogue.h"
#include "Log.h"

namespace Ocelot
{

namespace MonetDB
{

MDB_REGISTER_OPERATOR(LeftFetchJoinOperator);

std::string LeftFetchJoinOperator::getName() const {
  return "leftfetchjoin";
}

std::string LeftFetchJoinOperator::getModule() const {
  return "algebra";
}

void LeftFetchJoinOperator::run(QueryContext* context, MalStkPtr stk,
                                InstrPtr pci, int first_arg_index) const {
  auto left = getMalArg<Bat>(stk, pci, first_arg_index);
  auto right = getMalArg<Bat>(stk, pci, first_arg_index + 1);

  if (!left.checkType(TYPE_oid, TYPE_void)) {
    THROW_EXCEPTION("Left bat must be of type oid!");
  }

  if (!right.isDense() && !right.checkType(TYPE_str)) {
    LOG_WARN("Right bat isn't dense!");
  }

  auto result = Bat(right.getType(), left.getCount());
  result.setSorted(left.isSorted() && right.isSorted());
  result.setRevSorted(left.isRevSorted() && right.isRevSorted());
  result.setKey(left.isKey() && right.isKey());
  result.setupStringDictionary(right);

  if (right.checkType(TYPE_void)) {
    // The result is a copy of the left input
    if (left.checkType(TYPE_void)) {
      result.setDense(true);
      result.setKey(true);
      result.setSeqBase(0);
    } else {
      // we got content, so register the result as copy of the left input
      result.registerAsCopy(left, context);
    }

    result.keepRef(stk, pci, 0);
    return;
  } else if (left.checkType(TYPE_void)) {
    result.registerAsCopy(right, context);
    result.keepRef(stk, pci, 0);
    return;
  }

  // if one of them is empty, the result will also be empty
  if (left.getCount() == 0 || right.getCount() == 0) {
    result.keepRef(stk, pci, 0);
    return;
  }

  auto leftop = context->getBatAsOperator(left);
  auto rightop = context->getBatAsOperator(right);
  auto op = createOperatorInstance<Ocelot::LeftFetchJoinOperator>(
              right.getDataType());

  op->registerParent(leftop.second, 0, leftop.first);
  op->registerParent(rightop.second, 1, rightop.first);

  context->registerBat(result, op, 0);
  result.keepRef(stk, pci, 0);
}

} //namespace MonetDB

} //namespace Ocelot
