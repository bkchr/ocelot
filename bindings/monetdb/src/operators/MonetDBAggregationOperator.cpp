#include "MonetDBAggregationOperator.h"

#include "MonetDBQueryContext.h"
#include "LeftFetchJoinOperator.h"
#include "MonetDBOperatorCatalogue.h"
#include "DeviceSelectionManager.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb5/modules/atoms/mtime.h"
MDB_INCLUDE_END()

namespace Ocelot
{

namespace MonetDB
{

// register all variants
MDB_REGISTER_OPERATOR_VARIANT(AggregationOperator, Sum, Reduction::SumLong);
MDB_REGISTER_OPERATOR_VARIANT(AggregationOperator, Min, Reduction::Min);
MDB_REGISTER_OPERATOR_VARIANT(AggregationOperator, Max, Reduction::Max);
MDB_REGISTER_OPERATOR_VARIANT(AggregationOperator, Count, Reduction::Count);

AggregationOperator::AggregationOperator(Reduction::Operation op,
                                         DeviceSelectionManager* dmanager) :
  mOperation(op),
  pDeviceSelectionManager(dmanager) {
  setup();
}

void AggregationOperator::setup() {
  registerTypeOperations(TYPE_any, Reduction::Count);
  registerTypeOperations(TYPE_int, Reduction::Min | Reduction::Max |
                         Reduction::Count | Reduction::SumLong);
  registerTypeOperations(TYPE_flt, Reduction::Min | Reduction::Max |
                         Reduction::Count | Reduction::SumLong);
  registerTypeOperations(TYPE_date, Reduction::Min | Reduction::Max |
                         Reduction::Count);
}

std::string AggregationOperator::getName() const {
  switch (mOperation) {
    case Reduction::SumLong:
      return "sum";
    case Reduction::Min:
      return "min";
    case Reduction::Max:
      return "max";
    case Reduction::Count:
      return "count";
    default:
      THROW_EXCEPTION("Unknow operation: ", mOperation, "!");
  }
}

std::string AggregationOperator::getModule() const {
  return "aggr";
}

bool AggregationOperator::isResultDeviceResident(int index) const {
  (void)index;
  return false;
}

void AggregationOperator::registerTypeOperations(int type, int operations) {
  mTypeOperationSupport[type] = operations;
}

bool AggregationOperator::checkTypeAndOperation(const Bat& bat) const {
  auto find = mTypeOperationSupport.find(bat.getType());

  if (find != mTypeOperationSupport.end())
    return find->second & mOperation;

  // as last step try the any type
  return mTypeOperationSupport.find(TYPE_any)->second & mOperation;
}

void AggregationOperator::run(QueryContext* context, MalStkPtr stk,
                              InstrPtr pci, int first_arg_index) const {
  auto bat = getMalArg<Bat>(stk, pci, first_arg_index);

  if (!checkTypeAndOperation(bat)) {
    THROW_EXCEPTION("Wrong type for reduction(operation: ", mOperation, "!",
                    " Type is: \"", bat.getAtomType().getName(), "\".");
  }

  auto device = pDeviceSelectionManager->getDevice();
  auto col = context->getBatAsColumn(bat, true);

  switch (mOperation) {
    case Reduction::SumLong:
    {
      auto buffer = col->getData<Format::Raw>(device);
      doSum(buffer, stk, pci);
      break;
    }
    case Reduction::Min:
    case Reduction::Max:
    {
      auto buffer = col->getData<Format::Raw>(device);
      doMaxMin(buffer, stk, pci);
      break;
    }
    case Reduction::Count:
      doCount(col, device, stk, pci, first_arg_index);
      break;
    default:
      THROW_EXCEPTION("Unknow operation: ", mOperation, "!");
  }
}

void AggregationOperator::doSum(const Pointer<Buffer>& buffer, MalStkPtr stk,
                                InstrPtr pci) const {
  auto op = mOperation;

  if (op != Reduction::SumLong) {
    THROW_EXCEPTION("Wrong operation for doSum function!");
  }

  if (buffer->getType() == DTypeF32)
    op = Reduction::Sum;

  auto reduction = Reduction(buffer, nullptr, op);

  if (op == Reduction::Sum) {
    setResult<float>(stk, pci, reduction);
  } else {
    setResult<long>(stk, pci, reduction);
  }
}

void AggregationOperator::doMaxMin(const Pointer<Buffer>& buffer, MalStkPtr stk,
                                   InstrPtr pci) const {
  auto reduction = Reduction(buffer, nullptr, mOperation);

  if (buffer->getType() == DTypeF32) {
    setResult<float>(stk, pci, reduction);
  } else {
    setResult<int>(stk, pci, reduction);
  }
}

void AggregationOperator::doCount(
      const Pointer<Column>& col, Pointer<Device> dev, MalStkPtr stk,
      InstrPtr pci, int first_arg_index) const {
  BufferPtr buffer = nullptr;
  auto op = mOperation;
  Reduction::Option opt = Reduction::NoOption;

  if (!col->hasFormat<Format::Raw>() && col->hasFormat<Format::Bitmap>()) {
    buffer = col->getData<Format::Bitmap>(dev);
    op = Reduction::BitCount;
  } else {
    buffer = col->getData<Format::Raw>(dev);

    if (getMalArgCount(pci) > first_arg_index + 1 &&
        getMalArg<bool>(stk, pci, first_arg_index + 1)) {
      opt = Reduction::CountIgnoreNil;
    }
  }

  auto reduction = Reduction(buffer, nullptr, op, opt);
  setResult<int, wrd>(stk, pci, reduction);
}

} //namespace MonetDB

} //namespace Ocelot
