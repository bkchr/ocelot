#include "MonetDBSortOperator.h"

#include "SortOperator.h"

#include "MonetDBOperatorCatalogue.h"
#include "MonetDBMalUtils.h"
#include "MonetDBQueryContext.h"

namespace Ocelot
{

namespace MonetDB
{

MDB_REGISTER_OPERATOR(SortOperator);

std::string SortOperator::getName() const {
  return "subsort";
}

std::string SortOperator::getModule() const {
  return "algebra";
}

void SortOperator::run(QueryContext* context, MalStkPtr stk, InstrPtr pci,
                       int first_arg_index) const {
  auto bat = getMalArg<Bat>(stk, pci, first_arg_index);
  bool reverse = getMalArg<bit>(stk, pci, getMalArgCount(pci) - 2);
  Sort::Direction sort_dir = reverse ? Sort::Descending : Sort::Ascending;

  auto sorted = Bat(bat.getType(), bat.getCount());
  auto order = Bat(TYPE_oid, bat.getCount());
  auto groups = Bat(TYPE_void, 0);

  if (bat.getCount() > 0) {
    auto keys = context->getBatAsOperator(bat);
    auto sort_op = createOperatorInstance<Ocelot::SortOperator>(
                     bat.getDataType(), sort_dir);
    sort_op->registerParent(keys.second, 0, keys.first);

    sorted.setSorted(true);

    if (sort_dir) {
      order.setSorted(bat.isSorted());
      order.setRevSorted(bat.isRevSorted());
    } else {
      order.setSorted(bat.isRevSorted());
      order.setRevSorted(bat.isSorted());
    }

    context->registerBat(sorted, sort_op, 0);
    context->registerBat(order, sort_op, 1);
  }

  sorted.keepRef(stk, pci, 0);

  if (getMalRetCount(pci) > 1) {
    order.keepRef(stk, pci, 1);
  }

  if (getMalRetCount(pci) > 2) {
    groups.keepRef(stk, pci, 2);
  }
}

} //namespace MonetDB

} //namespace Ocelot
