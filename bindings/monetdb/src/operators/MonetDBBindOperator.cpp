#include "MonetDBBindOperator.h"

#include "MonetDBMalUtils.h"
#include "MonetDBQueryContext.h"
#include "MonetDBOperatorCatalogue.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "monetdb5/mal/mal_instruction.h"
MDB_INCLUDE_END()

namespace Ocelot
{

namespace MonetDB
{

MDB_REGISTER_OPERATOR(BindOperator);

std::string BindOperator::getName() const {
  return "bind";
}

std::string BindOperator::getModule() const {
  return "sql";
}

bool BindOperator::isResultDeviceResident(int index) const {
  (void)index;
  return false;
}

void BindOperator::run(QueryContext* context, MalStkPtr stk, InstrPtr pci,
                       int first_arg_index) const {
  auto bat = getMalArg<Bat>(stk, pci, first_arg_index);
  context->registerBatAsBaseTable(bat.getId());
}

std::pair<int, bool> BindOperator::insertInPlan(
    MalBlkPtr mplan, InstrRecord* const* instructions, int index,
    int count, int query_context, std::vector<int>& device_resident_vars) {
  (void)count;

  auto instr = instructions[index];
  // insert the original instruction
  pushInstruction(mplan, instr);

  auto bind = newInstruction(mplan, ASSIGNsymbol);

  getArg(bind, 0) = newTmpVariable(mplan, TYPE_void);
  // push the first return argument of the original function
  bind = pushArgument(mplan, bind, instr->argv[0]);

  // insert the new instruction
  auto actions = Operator::insertInPlan(
                   mplan, &bind, 0, 1, query_context,
                   device_resident_vars).first;

  freeInstruction(bind);

  return std::make_pair(actions + 1, false);
}

} //namespace MonetDB

} //namespace Ocelot
