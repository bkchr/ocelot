#include "MonetDBSyncOperator.h"

#include "Column.h"
#include "FormatRaw.h"
#include "MonetDBSystem.h"
#include "Exception.h"
#include "MonetDBMalUtils.h"
#include "MonetDBOperatorCatalogue.h"
#include "MonetDBQueryContext.h"

namespace Ocelot
{

namespace MonetDB
{

MDB_REGISTER_OPERATOR(SyncOperator);

std::string SyncOperator::_getModule() {
  return System::getModuleName();
}

bool SyncOperator::isResultDeviceResident(int index) const {
  (void)index;
  return false;
}

void SyncOperator::run(QueryContext* context, MalStkPtr stk,
                       InstrPtr pci, int first_arg_index) const {
  auto bat = getMalArg<Bat>(stk, pci, first_arg_index);

  auto column = context->getBatAsColumn(bat);

  if (column == nullptr) {
    return;
  }

  if (bat.getTypeByteSize() != GET_TYPE_SIZE(column->getType())) {
    THROW_EXCEPTION("Bat and column byte size doesn't match!");
  }

  bat.setCount(column->getCount());
  column->getData<Format::Raw, Location::Host, char>(bat.getBasePtr()).wait();
}

} //namespace MonetDB

} //namespace Ocelot
