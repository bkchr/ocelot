#include "MonetDBGroupBy.h"

#include "GroupByOperator.h"

#include "MonetDBMalUtils.h"
#include "MonetDBQueryContext.h"
#include "MonetDBOperatorCatalogue.h"

namespace Ocelot
{

namespace MonetDB
{

MDB_REGISTER_OPERATOR_VARIANT(GroupBy, Sum, GroupedAggregation::Sum);
MDB_REGISTER_OPERATOR_VARIANT(GroupBy, Min, GroupedAggregation::Min);
MDB_REGISTER_OPERATOR_VARIANT(GroupBy, Max, GroupedAggregation::Max);
MDB_REGISTER_OPERATOR_VARIANT(GroupBy, Count, GroupedAggregation::Count);

GroupBy::GroupBy(GroupedAggregation::Operation op) :
  mOp(op) {

}

std::string GroupBy::getModule() const {
  return "aggr";
}

std::string GroupBy::getName() const {
  switch (mOp) {
    case GroupedAggregation::Min:
      return "submin";
    case GroupedAggregation::Max:
      return "submax";
    case GroupedAggregation::Sum:
      return "subsum";
    case GroupedAggregation::Count:
      return "subcount";
    default:
      THROW_EXCEPTION("Unknown operation!");
  }
}

void GroupBy::run(QueryContext* context, MalStkPtr stk, InstrPtr pci,
                  int first_arg_index) const {
  Bat input = getMalArg<Bat>(stk, pci, first_arg_index);
  Bat groups = getMalArg<Bat>(stk, pci, first_arg_index + 1);
  auto group_count = getMalArg<Bat>(stk, pci, first_arg_index + 2).getCount();

  if (mOp != GroupedAggregation::Count &&
      !input.checkType(TYPE_int, TYPE_flt)) {
    THROW_EXCEPTION("GroupBy min, max or sum only support float's or int's!");
  }

  auto result = Bat(getResultType(input.getType()), group_count);
  result.setDense(true);
  // as we only have one result, we are sorted
  result.setSorted(true);

  auto input_op = context->getBatAsOperator(input);
  auto groups_op = context->getBatAsOperator(groups);

  // for int's we use the sumlong operation!
  auto op = mOp == GroupedAggregation::Sum && input.getType() == TYPE_int ?
              GroupedAggregation::SumLong : mOp;
  auto result_op = createOperatorInstance<GroupByOperator>(
                     input.getDataType(), op, group_count, input.hasNoNil());
  result_op->registerParent(input_op.second, 0, input_op.first);
  result_op->registerParent(groups_op.second, 1, groups_op.first);

  context->registerBat(result, result_op, 0);
  result.keepRef(stk, pci, 0);
}

int GroupBy::getResultType(int input_type) const {
  switch (mOp) {
    case GroupedAggregation::Count:
      return TYPE_wrd;
    case GroupedAggregation::Sum:
      if (input_type == TYPE_int) {
        return TYPE_lng;
      }
    default:
      return input_type;
  }
}

} //namespace MonetDB

} //namespace Ocelot
