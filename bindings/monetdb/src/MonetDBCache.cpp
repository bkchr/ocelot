#include "MonetDBCache.h"

#include "Column.h"
#include "Operator.h"
#include "MonetDBOperator.h"
#include "ColumnInputOperator.h"
#include "MonetDBMalUtils.h"

namespace Ocelot
{

namespace MonetDB
{

#define LOCK std::unique_lock<std::recursive_mutex> lock(mMutex)

Cache::Cache(bool copybatforcache) :
   mCopyBatForCache(copybatforcache) {

}

void Cache::registerBat(bat bid, OperatorPtr op, unsigned short channel) {
  LOCK;
  mCache[bid] = CacheEntry(op, channel);
}

ColumnPtr Cache::getBatAsColumn(bat bid, bool createifnotexist) {
  auto op = getBatAsOperator(bid, createifnotexist);

  if (op.second == nullptr) {
    return nullptr;
  }

  // if we want to get the column, we first need to create them
  op.second->finish();
  return op.second->getResult(op.first);
}

std::pair<unsigned short, OperatorPtr>
  Cache::getBatAsOperator(bat bid, bool createifnotexist) {
  LOCK;
  auto find = mCache.find(bid);

  if (find != mCache.end())
    return std::make_pair(find->second.sChannel, find->second.sOperator);

  if (createifnotexist) {
    return cacheBat(bid);
  } else {
    return std::make_pair(0, nullptr);
  }
}

void Cache::removeBat(bat bid) {
  LOCK;
  auto find = mCache.find(bid);

  if (find == mCache.end())
    return;

  mCache.erase(find);
}

std::pair<unsigned short, OperatorPtr> Cache::cacheBat(bat bid) {
  auto bat = Bat(bid);

  auto col = Column::createInstance(bat.getDataType());
  col->registerData<Format::Raw>(bat.getBasePtr(), bat.getSizeInBytes(),
                                 mCopyBatForCache);

  auto op =
      Operator::createOperatorInstance<ColumnInputOperator>(bat.getDataType(),
                                                            col);
  registerBat(bid, op, 0);

  return std::make_pair(0, op);
}


} //namespace MonetDB

} //namespace Ocelot
