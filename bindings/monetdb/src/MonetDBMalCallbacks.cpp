#include "MonetDBCommon.h"

MDB_INCLUDE_BEGIN()
  #include "MonetDBMalCallbacks.h"
MDB_INCLUDE_END()

#include "MonetDBSystem.h"
#include "MonetDBQueryContext.h"
#include "MonetDBOperator.h"
#include "MonetDBTestCase.h"

MDB_INCLUDE_BEGIN()
  #include "monetdb5/optimizer/opt_prelude.h"
  #include "monetdb5/optimizer/opt_pipes.h"
  #include "monetdb5/optimizer/opt_statistics.h"
MDB_INCLUDE_END()

str ocl_mal_setup(Client cntxt, MalBlkPtr mb, MalStkPtr stk, InstrPtr pci) {
  (void)mb;
  (void)stk;
  (void)pci;

  auto result = Ocelot::MonetDB::System::getSingleton().setup(cntxt);

  if (result)
    return MAL_SUCCEED;
  else
    return createException(MAL, "ocl_mal_setup",
                           "Error setting up the Ocelot MonetDB bindings!");
}

#define DEBUG_OPT_OCELOT 62
#define OPTDEBUGocelot if (optDebug & ((lng)1 << DEBUG_OPT_OCELOT))

str ocl_mal_optimize(Client cntxt, MalBlkPtr mb, MalStkPtr stk, InstrPtr p) {
  lng time = 0, start = GDKusec();
  int actions = 0;
  str modnme;
  str fcnnme;
  str msg = MAL_SUCCEED;
  Symbol s = NULL;

  if (p && p->argc > 1) {
    if (getArgType(mb, p, 1) != TYPE_str || getArgType(mb, p, 2) != TYPE_str ||
        !isVarConstant(mb, getArg(p, 1)) || !isVarConstant(mb, getArg(p, 2))) {
      return createException(MAL, "optimizer.ocelot", ILLARG_CONSTANTS);
    }

    if (stk != NULL) {
      modnme = *(str*)getArgReference(stk, p, 1);
      fcnnme = *(str*)getArgReference(stk, p, 2);
    } else {
      modnme = getArgDefault(mb, p, 1);
      fcnnme = getArgDefault(mb, p, 2);
    }

    removeInstruction(mb, p);
    s = findSymbol(cntxt->nspace, putName(modnme, strlen(modnme)),
                   putName(fcnnme, strlen(fcnnme)));

    if (s == NULL) {
      char buf[1024];
      snprintf(buf, 1024, "%s.%s", modnme, fcnnme);
      return createException(MAL, "optimizer.openCL",
                             RUNTIME_OBJECT_UNDEFINED ":%s", buf);
    }

    mb = s->def;
    stk = 0;
  } else if(p) {
    removeInstruction(mb, p);
  }

  if (mb->errors) {
    /* when we have errors, we still want to see them */
    addtoMalBlkHistory(mb, "ocelot"_mstr);
    return MAL_SUCCEED;
  }

  actions = Ocelot::MonetDB::System::getSingleton().optimize(mb);
  msg = optimizerCheck(cntxt, mb, "optimizer.ocelot"_mstr, actions,
                       time = (GDKusec() - start));

  OPTDEBUGocelot {
    mnstr_printf(cntxt->fdout, "=FINISHED ocelot %d\n", actions);
    printFunction(cntxt->fdout, mb, 0, LIST_MAL_ALL);
  }

  DEBUGoptimizers {
    mnstr_printf(cntxt->fdout, "#opt_reduce: " LLFMT " ms\n", time);
  }

  QOTupdateStatistics("ocelot"_mstr, actions, time);
  addtoMalBlkHistory(mb, "ocelot"_mstr);

  return msg;
}

str ocl_mal_invoke_operator(Client cntxt, MalBlkPtr mb, MalStkPtr stk,
                            InstrPtr pci) {
  (void)cntxt;
  (void)mb;

  try {
    Ocelot::MonetDB::Operator::invoke(stk, pci);
  }
  catch(const std::exception& ex) {
    return createException(MAL, "ocelot.invokeOperator", "%s", ex.what());
  }

  return MAL_SUCCEED;
}

str ocl_mal_create_query_context(ptr* context) {
  auto cppcontext =
      Ocelot::MonetDB::System::getSingleton().createQueryContext();

  *context = cppcontext;

  return MAL_SUCCEED;
}

str ocl_mal_free_query_context(ptr* context) {

  // cast and delete the pointer
  auto cppcontext = static_cast<Ocelot::MonetDB::QueryContext*>(*context);
  delete cppcontext;

  return MAL_SUCCEED;
}

str ocl_mal_execute_tests(Client cntxt, MalBlkPtr mb, MalStkPtr stk,
                          InstrPtr pci) {
  (void)mb;
  (void)stk;
  (void)pci;

  Ocelot::MonetDB::TestCase::deleteSingletonPtr();
  Ocelot::MonetDB::TestCase::getSingleton().executeTests(cntxt);
  Ocelot::MonetDB::TestCase::deleteSingletonPtr();

  return MAL_SUCCEED;
}
