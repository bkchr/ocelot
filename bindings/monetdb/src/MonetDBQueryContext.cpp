#include "MonetDBQueryContext.h"

#include "MonetDBBaseTableCache.h"
#include "MonetDBMalUtils.h"

#include <algorithm>

namespace Ocelot
{

namespace MonetDB
{

QueryContext::QueryContext(const Pointer<BaseTableCache>& cache) :
  mCache(cache),
  mBaseTableCache(cache) {

}

void QueryContext::registerBatAsBaseTable(bat bid) {
  mBaseTableCache->registerBatAsBaseTable(bid);
}

void QueryContext::registerBat(bat bid, const Pointer<Ocelot::Operator>& op,
                               unsigned short channel) {
  if (mBaseTableCache->isBatBaseTable(bid)) {
    THROW_EXCEPTION("Bat with id ", bid, " is registered as base table. Base "
                                         "tables should never be registered, "
                                         "they should only be retrieved "
                                         "with getBatAsColumn/"
                                         "getBatAsOperator!");
  }

  mCache.registerBat(bid, op, channel);
}

void QueryContext::registerBat(const Bat& bat,
                               const Pointer<Ocelot::Operator>& op,
                               unsigned short channel) {
  registerBat(bat.getId(), op, channel);
}

Pointer<Column> QueryContext::getBatAsColumn(bat bid, bool createifnotexist) {
  return mCache.getBatAsColumn(bid, createifnotexist);
}

Pointer<Column> QueryContext::getBatAsColumn(const Bat& bat,
                                             bool createifnotexist) {
  return getBatAsColumn(bat.getId(), createifnotexist);
}

std::pair<unsigned short, Pointer<Ocelot::Operator>>
    QueryContext::getBatAsOperator(bat bid) {
  return mCache.getBatAsOperator(bid, true);
}

std::pair<unsigned short, Pointer<Ocelot::Operator>>
    QueryContext::getBatAsOperator(const Bat& bat) {
  return getBatAsOperator(bat.getId());
}

} //namespace MonetDB

} //namespace Ocelot
