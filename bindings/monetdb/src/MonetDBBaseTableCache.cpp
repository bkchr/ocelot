#include "MonetDBBaseTableCache.h"

namespace Ocelot
{

namespace MonetDB
{

#define LOCK std::unique_lock<std::recursive_mutex> lock(mMutex)

BaseTableCache::BaseTableCache() :
  Cache(false) {}

void BaseTableCache::registerBatAsBaseTable(bat bid) {
  LOCK;
  mBaseTables.insert(bid);
}

bool BaseTableCache::isBatBaseTable(bat bid) {
  LOCK;
  return mBaseTables.find(bid) != mBaseTables.end();
}

} //namespace MonetDB

} //namespace Ocelot
