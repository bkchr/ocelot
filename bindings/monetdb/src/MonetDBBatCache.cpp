#include "MonetDBBatCache.h"

#include "MonetDBBaseTableCache.h"

namespace Ocelot
{

namespace MonetDB
{

#define LOCK std::unique_lock<std::recursive_mutex> lock(mMutex)

BatCache::BatCache(Pointer<BaseTableCache> basetablecache) :
  Cache(true),
  mBaseTableCache(basetablecache) {

}

std::pair<unsigned short, Pointer<Ocelot::Operator>> BatCache::getBatAsOperator(
    bat bid, bool createifnotexist) {
  if (mBaseTableCache->isBatBaseTable(bid))
    return mBaseTableCache->getBatAsOperator(bid, createifnotexist);

  return Cache::getBatAsOperator(bid, createifnotexist);
}

} //namespace MonetDB

} //namespace Ocelot
