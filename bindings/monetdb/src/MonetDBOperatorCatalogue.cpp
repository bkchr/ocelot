#include "MonetDBOperatorCatalogue.h"

#include "MonetDBOperator.h"

#include <assert.h>

namespace Ocelot
{

namespace MonetDB
{

OperatorCatalogue::OperatorCatalogue() {
  for (auto creator : getRegisteredOperatorVector()) {
    auto op = creator->createInstance();
    mOperatorsMap[op->getModule() + op->getName()] = op;
  }
}

Operator* OperatorCatalogue::getOperator(
    const std::string& module, const std::string& name) const {
  auto find = mOperatorsMap.find(module + name);

  if (find != mOperatorsMap.end())
    return find->second.get();
  else
    return nullptr;
}

void OperatorCatalogue::registerOperator(
    const Pointer<OperatorInstanceCreator>& op) {
  getRegisteredOperatorVector().push_back(op);
}

std::vector<Pointer<OperatorInstanceCreator>>& OperatorCatalogue::
  getRegisteredOperatorVector() {
  static std::vector<Pointer<OperatorInstanceCreator>> operators;

  return operators;
}

} //namespace MonetDB

} //namespace Ocelot
