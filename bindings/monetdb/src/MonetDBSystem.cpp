#include "MonetDBSystem.h"

#include "MonetDBOptimizer.h"
#include "Exception.h"
#include "MonetDBBaseTableCache.h"
#include "MonetDBOperatorCatalogue.h"
#include "MonetDBQueryContext.h"

#include "PlatformManager.h"
#include "CacheManager.h"
#include "DeviceManager.h"
#include "Log.h"

extern "C" {
  #include <stdio.h>
  #include <assert.h>
  #include "monetdb_config.h"
  #include "gdk.h"
}

#include <iostream>

namespace Ocelot
{

namespace MonetDB
{

class MonetDBLogBackend : public LogBackend {
private:
  void write(const std::string& str) override final {
    std::cout<<"Ocelot "<<str<<std::endl;
  }
};

System::System() {
  Log::getSingleton().setBackend(std::make_shared<MonetDBLogBackend>());
}

bool System::setup(CLIENT* client) {
  if (mSetupCalled)
    return true;

  try {
    init(client);
  } catch(Exception& ex) {
    THRprintf(GDKerr, "%s\n", ex.what());

    return false;
  }

  mSetupCalled = true;
  return true;
}

int System::optimize(MALBLK* mplan) {
  return mOptimizer->optimize(mplan);
}

const std::string& System::getModuleName() {
  static std::string name("ocelot");
  return name;
}

void System::init(CLIENT* client) {
  mPlatformManager = Pointer<PlatformManager>(
                       DI_CREATE_INSTANCE(PlatformManager));
  mCacheManager = Pointer<CacheManager>(DI_CREATE_INSTANCE(CacheManager));
  const std::string kernel_path = GDKgetenv("monet_mod_path") +
                                  std::string("/clkernel");
  mDeviceManager = Pointer<DeviceManager>(
                     DI_CREATE_INSTANCE(DeviceManager, mPlatformManager,
                                        mCacheManager, kernel_path));

  if (mBaseTableCache == nullptr) {
    mBaseTableCache = std::make_shared<BaseTableCache>();
  }

  if (mOperatorCatalogue == nullptr) {
    mOperatorCatalogue = std::make_shared<OperatorCatalogue>();
  }

  if (mOptimizer == nullptr) {
    mOptimizer = std::make_shared<Optimizer>(client, mOperatorCatalogue);
  }
}

void System::setBaseTableCache(const Pointer<BaseTableCache>& btcache) {
  if (mSetupCalled) {
    THROW_EXCEPTION("The basetable cache can only be set before calling "
                    "setup()!");
  }

  mBaseTableCache = btcache;
}

void System::setOptimizer(const Pointer<Optimizer>& optimizer) {
  if (mSetupCalled) {
    THROW_EXCEPTION("The optimizer can only be set before calling setup()!");
  }

  mOptimizer = optimizer;
}

QueryContext* System::createQueryContext() {
  return DI_CREATE_INSTANCE(QueryContext, mBaseTableCache);
}

} //namespace MonetDB

} //namespace Ocelot
