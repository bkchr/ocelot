#include "MonetDBOptimizer.h"

#include "Exception.h"
#include "MonetDBOperatorCatalogue.h"
#include "MonetDBMalUtils.h"
#include "MonetDBSystem.h"
#include "MonetDBOperator.h"
#include "MonetDBSyncOperator.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "monetdb5/optimizer/opt_prelude.h"
  #include "monetdb5/optimizer/opt_pipes.h"
  #include "monetdb5/optimizer/opt_statistics.h"
MDB_INCLUDE_END()

namespace Ocelot
{

namespace MonetDB
{

const StringLiteral Optimizer::OptimizerName = "ocelot_pipe"_mstr;

Optimizer::Optimizer(CLIENT* client,
                     const Pointer<OperatorCatalogue>& opcatalogue) :
  mOperatorCatalogue(opcatalogue) {
  registerInMonetDB(client);
}

int Optimizer::optimize(MALBLK* mplan) {
  auto orig = mplan->stmt;
  auto orig_count = mplan->stop;
  auto orig_max_count = mplan->ssize;

  if (!createNewStatementList(mplan)) {
    return 0;
  }

  auto query_context = insertCreateQueryContext(mplan);
  std::vector<int> device_resident_vars;
  auto planr = buildPlan(mplan, orig, orig_count, query_context.second,
                         device_resident_vars);

  finishPlan(mplan, orig, orig_count, planr.first, orig_max_count);

  // + 1 is the signature copying in createNewStatementList
  return planr.second + 1 + query_context.first;
}

void Optimizer::registerInMonetDB(CLIENT* client) {
  // if the pipe is already defined, we skip the registration
  if (isOptimizerPipe(OptimizerName)) {
    return;
  }

  // Pipeline based on the sequential pipeline + our own optimizer call
  auto result = addPipeDefinition(client, OptimizerName,
                                  "optimizer.inline();"
                                  "optimizer.remap();"
                                  "optimizer.costModel();"
                                  "optimizer.coercions();"
                                  "optimizer.evaluate();"
                                  "optimizer.aliases();"
                                  "optimizer.pushselect();"
                                  "optimizer.mergetable();"
                                  "optimizer.deadcode();"
                                  "optimizer.commonTerms();"
                                  "optimizer.joinPath();"
                                  "optimizer.reorder();"
                                  "optimizer.deadcode();"
                                  "optimizer.reduce();"
                                  "optimizer.matpack();"
                                  "optimizer.querylog();"
                                  "optimizer.multiplex();"
                                  "optimizer.garbageCollector();"
                                  "optimizer.ocelot();"
                                  "optimizer.aliases();"
                                #ifdef NDEBUG
                                  // deadcode removes our comments,
                                  // but in debug mode we want them!
                                  "optimizer.deadcode();"
                                #endif
                                  "optimizer.generator();"
                                  "optimizer.garbageCollector();"_mstr);

  if (result != MAL_SUCCEED) {
    THROW_EXCEPTION("Couldn't register the optimizer in MonetDB! Error: ",
                    result);
  }
}

bool Optimizer::createNewStatementList(MALBLK* mplan) {
  auto signature = mplan->stmt[0];

  if (newMalBlkStmt(mplan, mplan->ssize) < 0)
    return false;

  pushInstruction(mplan, signature);
  return true;
}

std::pair<int, int> Optimizer::buildPlan(
      MALBLK* mplan, InstrRecord** orig, int orig_count, int query_context,
      std::vector<int>& device_resident_vars) {
  int i = 1, actions = 0;

  for (; i < orig_count; ++i) {
    auto instr = orig[i];

    if (instr->token == ENDsymbol) {
      actions += handleEndSymbol(mplan, query_context);
      actions += insertNativeMonetDBOperator(mplan, instr, query_context,
                                             device_resident_vars);
      break;
    }

    auto op = mOperatorCatalogue->getOperator(
                getModuleId(instr), getFunctionId(instr));

    if (op) {
      auto result = op->insertInPlan(mplan, orig, i, orig_count, query_context,
                                     device_resident_vars);

      actions += result.first;
      if (result.second) {
        freeInstruction(instr);
      }
    } else {
      actions += insertNativeMonetDBOperator(mplan, instr, query_context,
                                             device_resident_vars);
    }
  }

  return std::make_pair(++i, actions);
}

void Optimizer::finishPlan(
    MALBLK* mplan, InstrRecord** orig, int orig_count, int orig_index,
    int orig_max_count) {
  // Retain optimizer steps after the ENDsymbol.
  for (; orig_index < orig_count; ++orig_index) {
    if (orig[orig_index])
      pushInstruction(mplan, orig[orig_index]);
  }

  // And clear the rest of the plan.
  for (; orig_index < orig_max_count; ++orig_index) {
    if (orig[orig_index])
      freeInstruction(orig[orig_index]);
  }
}

int Optimizer::insertNativeMonetDBOperator(
      MALBLK* mplan, InstrRecord* instr, int query_context,
      std::vector<int>& device_resident_vars) {
  int actions = 0;

  for (int i = instr->retc; i < instr->argc; ++i) {
    auto find = std::find(device_resident_vars.begin(),
                          device_resident_vars.end(), instr->argv[i]);

    if (find != device_resident_vars.end()) {
      actions += insertSyncOperator(mplan, instr->argv[i], query_context,
                                    device_resident_vars);
    }
  }

  pushInstruction(mplan, instr);

  return actions;
}

int Optimizer::insertSyncOperator(MALBLK* mplan, int var_id,
                                  int query_context,
                                  std::vector<int>& device_resident_vars) {
  auto sync = newInstruction(mplan, ASSIGNsymbol);

  getArg(sync, 0) = newTmpVariable(mplan, TYPE_void);
  sync = pushArgument(mplan, sync, var_id);

  auto sync_op =
      mOperatorCatalogue->getOperator(SyncOperator::_getModule(),
                                      SyncOperator::_getName());
  auto result = sync_op->insertInPlan(mplan, &sync, 0, 1, query_context,
                                      device_resident_vars);

  if (result.second) {
    freeInstruction(sync);
  }

  return result.first;
}

int Optimizer::handleEndSymbol(MALBLK* mplan, int query_context) {
  return insertFreeQueryContext(mplan, query_context);
}

std::pair<int, int> Optimizer::insertCreateQueryContext(MALBLK* mplan) {
  auto instr = newInstruction(mplan, ASSIGNsymbol);

  setModuleId(instr, putStr(System::getModuleName().c_str()));
  setFunctionId(instr, putStr("createQueryContext"));
  getArg(instr, 0) = newVariable(mplan, (str)GDKstrdup("query_context"),
                                 TYPE_ptr);

  pushInstruction(mplan, instr);

  return std::make_pair(1, getArg(instr, 0));
}

int Optimizer::insertFreeQueryContext(MALBLK* mplan, int query_context) {
  auto instr = newInstruction(mplan, ASSIGNsymbol);

  setModuleId(instr, putStr(System::getModuleName().c_str()));
  setFunctionId(instr, putStr("freeQueryContext"));
  getArg(instr, 0) = newTmpVariable(mplan, TYPE_void);
  instr = pushArgument(mplan, instr, query_context);

  pushInstruction(mplan, instr);

  return 1;
}

} //namespace MonetDB

}
