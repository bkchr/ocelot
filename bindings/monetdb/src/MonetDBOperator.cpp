#include "MonetDBOperator.h"

#include "MonetDBMalUtils.h"
#include "MonetDBSystem.h"
#include "MonetDBQueryContext.h"
#include "Profiler.h"

// MonetDB includes
MDB_INCLUDE_BEGIN()
  #include "monetdb_config.h"
  #include "monetdb5/mal/mal_instruction.h"
  #include "monetdb5/mal/mal_interpreter.h"
  #include "mal_builder.h"
MDB_INCLUDE_END()

namespace Ocelot
{

namespace MonetDB
{

std::pair<int, bool> Operator::insertInPlan(
      MalBlkPtr mplan, InstrRecord* const* instructions, int index, int count,
      int query_context, std::vector<int>& device_resident_vars) {
  (void)count;
  auto instr = instructions[index];
  auto new_instr = newInstruction(mplan, ASSIGNsymbol);

  setModuleId(new_instr, putStr(System::getModuleName().c_str()));
  setFunctionId(new_instr, putStr("invokeOperator"));

  // copy the return values
  for (int i = 0; i < instr->retc; ++i) {
    int var_id = instr->argv[i];
    new_instr = pushReturn(mplan, new_instr, var_id);

    if (isResultDeviceResident(i)) {
      device_resident_vars.push_back(var_id);
    }
  }

  // store the context and the operator pointer
  new_instr = pushPtr(mplan, new_instr, this);
  new_instr = pushArgument(mplan, new_instr, query_context);

  // copy the arguments
  for (int i = instr->retc; i < instr->argc; ++i) {
    new_instr = pushArgument(mplan, new_instr, instr->argv[i]);
  }

  int actions = 1;
#ifndef NDEBUG
  // add the operator name as comment, so that we can identify operator in the
  // mal plan
  newComment(mplan, getName().c_str());
  ++actions;
#endif
  pushInstruction(mplan, new_instr);

  return std::make_pair(actions, true);
}

void Operator::invoke(MalStkPtr stk, InstrPtr pci) {
  auto op = getMalArg<Operator*>(stk, pci, 0);
  auto context = getMalArg<QueryContext*>(stk, pci, 1);

  OCL_PROFILE_OPERATION_SCOPE(op->getModule() + ":" + op->getName());
  op->run(context, stk, pci, 2);
}

} //namespace MonetDB

} //namespace Ocelot
