#ifndef DEPENDENCYINJECTION_H_
#define DEPENDENCYINJECTION_H_

#include "Exception.h"
#include "Singleton.h"
#include "ClassIDGenerator.h"
#include "RemoveAllQualifiers.h"
#include "TypedefExistsUtil.h"

#include <unordered_map>
#include <functional>

namespace Ocelot
{

  namespace DI
  {

    class Object
    {
    public:
      virtual ~Object() {}
    };

    template<typename Type, bool Pointer = true>
    class ObjectSpecialized : public Object
    {
    public:
      ObjectSpecialized(Type* obj) : pObject(obj) {}

      Type* getObject() const { return pObject; }

    private:
      Type* pObject;
    };

    template<typename Type>
    class ObjectSpecialized<Type, false> : public Object
    {
    public:
      ObjectSpecialized(Type obj) : pObject(obj) {}

      Type getObject() const { return pObject; }

    private:
      Type pObject;
    };

    class ConstantBase {};

    template<typename Type, typename Identifier>
    class Constant : public ConstantBase
    {
    public:
      Constant(const Type& value) :
        mValueGet([value]() { return value; }) {}

      template<typename Function>
      Constant(const Function& value_get) :
        mValueGet(value_get) {}

      operator Type() const {
        return mValueGet();
      }

      typedef Type type;

    private:
      const std::function<Type ()> mValueGet = nullptr;
    };

    #define DI_DECLARE_CONSTANT(NAME, TYPE)                                    \
      class NAME##TYPE##Identifier;                                            \
      using NAME = DI::Constant<TYPE, NAME##TYPE##Identifier>

    #define DI_REGISTER_CONSTANT(CONSTANTTYPE, VALUE)                          \
      Ocelot::DI::System::getSingleton().                                      \
                 registerConstant<CONSTANTTYPE>(CONSTANTTYPE(VALUE))

    #define DI_UNREGISTER_CONSTANT(CONSTANTTYPE)                               \
      Ocelot::DI::System::getSingleton().unregisterConstant<CONSTANTTYPE>()

    class System : public Singleton<System>
    {
    public:
      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type>
      void registerObject(Type* dep) {
        if (getObjectIntern<Type>())
          THROW_EXCEPTION("Object already exists!");

        mObjects.insert(
              std::pair<ClassID, Object*>(
                ClassIDGenerator<Type>::getID(),
                new ObjectSpecialized<Type, true>(dep)));
      }

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type>
      void unregisterObject() {
        auto obj = getObjectIntern<Type>();

        if (!obj)
          return;

        delete obj;
        mObjects.erase(ClassIDGenerator<Type>::getID());
      }

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type>
      void registerConstant(const Type& constant) {
        if (getObjectIntern<Type, false>())
          THROW_EXCEPTION("Constant already exists!");

        mObjects.insert(
              std::pair<ClassID, Object*>(
                ClassIDGenerator<Type>::getID(),
                new ObjectSpecialized<Type, false>(constant)));
      }

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type>
      void unregisterConstant() {
        auto obj = getObjectIntern<Type, false>();

        if (!obj)
          return;

        delete obj;
        mObjects.erase(ClassIDGenerator<Type>::getID());
      }

      OCL_TYPEDEF_EXISTS_STRUCTURE(DIConstructorSupport);

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type,
               typename ...Args>
      typename std::enable_if<OCL_TYPEDEF_EXISTS(Type, DIConstructorSupport),
                              Type*>::type
      createInstance(Args&&... args) {
        return Type::template DIConstructorArguments<0, void>::Ctor::
                createInstance(this, std::forward<Args>(args)...);
      }

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type,
               typename ...Args>
      typename std::enable_if<!OCL_TYPEDEF_EXISTS(Type, DIConstructorSupport),
                              Type*>::type
      createInstance(Args&&... args) {
        return new Type(std::forward<Args>(args)...);
      }

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type>
      typename std::enable_if<!std::is_base_of<
                                 ConstantBase, InputType>::value, Type*>::type
      getObject() {
        static_assert(!std::is_fundamental<Type>::value,
                      "You're trying to get an object of a fundamental type. "
                      "You're probably forgot to add arguments!");

        auto obj = getObjectIntern<Type>();

        if (!obj)
          return nullptr;

        return obj->getObject();
      }

      template<typename InputType, typename Type =
               typename remove_all_qualifiers<InputType>::type>
      typename std::enable_if<std::is_base_of<
                                ConstantBase, InputType>::value, Type>::type
      getObject() {
        auto obj = getObjectIntern<Type, false>();

        if (!obj)
          THROW_EXCEPTION("Couldn't find a Constant!");

        return obj->getObject();
      }

    private:
      template<typename Type, bool Pointer = true>
      ObjectSpecialized<Type, Pointer>* getObjectIntern() {
        auto find = mObjects.find(ClassIDGenerator<Type>::getID());

        if (find == mObjects.end())
          return nullptr;

        return static_cast<ObjectSpecialized<Type, Pointer>*>(find->second);
      }

      std::unordered_map<ClassID, Object*> mObjects;
    };

    template<typename Type, typename UseInstanceArgs, size_t Index = 0,
             typename Enable = void>
    struct ArgumentResolver
    {
      template<typename InstanceArgsTuple>
      static Type get(System* sys, InstanceArgsTuple&) {
        return sys->getObject<Type>();
      }
    };

    template<typename Type, typename UseInstanceArgs, size_t Index>
    struct ArgumentResolver<Type, UseInstanceArgs, Index,
        typename std::enable_if<UseInstanceArgs::value>::type>
    {
      template<typename InstanceArgsTuple>
      static Type get(System*, InstanceArgsTuple& tuple) {
        return std::get<Index>(tuple);
      }
    };

    namespace detail
    {
      template<size_t CTorArgIndex, typename CTorArgsTuple,
               size_t InstanceArgIndex, typename InstanceArgsTuple>
      struct ArgumentCompare
      {
        const static constexpr bool value = std::is_same<
          typename remove_all_qualifiers<
            typename std::tuple_element<CTorArgIndex %
                                        std::tuple_size<CTorArgsTuple>::value,
                                        CTorArgsTuple>::type>::type,
          typename remove_all_qualifiers<
            typename std::tuple_element<InstanceArgIndex %
                                        std::tuple_size<InstanceArgsTuple>::value,
                                        InstanceArgsTuple>::type>::type
                     >::type::value
                && CTorArgIndex < std::tuple_size<CTorArgsTuple>::value
                && InstanceArgIndex < std::tuple_size<InstanceArgsTuple>::value;
      };

      template<bool CTorArgEnd, bool CTorArgEQInstanceArg,
               typename CTorArgsTuple, std::size_t CTorIndex,
               typename InstanceArgsTuple, std::size_t InstanceArgsIndex,
               typename ...Result>
      struct ArgumentResolverTupleBuilderImpl;

      template<typename CTorArgsTuple, std::size_t CTorIndex,
               typename InstanceArgsTuple, std::size_t InstanceArgsIndex,
               typename ...Result>
      struct ArgumentResolverTupleBuilderImplHelper
      {
        typedef typename ArgumentResolverTupleBuilderImpl<
          CTorIndex == std::tuple_size<
                CTorArgsTuple>::value,
                ArgumentCompare<CTorIndex, CTorArgsTuple, InstanceArgsIndex,
                                InstanceArgsTuple>::value,
                CTorArgsTuple,
                CTorIndex,
                InstanceArgsTuple,
                InstanceArgsIndex,
                Result...>::type type;
      };

      template<bool CTorArgEQInstanceArg, typename CTorArgsTuple,
               std::size_t CTorIndex, typename InstanceArgsTuple,
               std::size_t InstanceArgsIndex, typename ...Result>
      struct ArgumentResolverTupleBuilderImpl<true, CTorArgEQInstanceArg,
                                              CTorArgsTuple, CTorIndex,
                                              InstanceArgsTuple,
                                              InstanceArgsIndex, Result...>
      {
        typedef std::tuple<Result...> type;
      };

      template<typename CTorArgsTuple, std::size_t CTorIndex,
               typename InstanceArgsTuple, std::size_t InstanceArgsIndex,
               typename ...Result>
      struct ArgumentResolverTupleBuilderImpl<false, true, CTorArgsTuple,
          CTorIndex, InstanceArgsTuple, InstanceArgsIndex, Result...>
      {
        typedef typename ArgumentResolverTupleBuilderImplHelper<CTorArgsTuple,
          CTorIndex + 1, InstanceArgsTuple, InstanceArgsIndex + 1, Result...,
          ArgumentResolver<typename std::tuple_element<CTorIndex,
                          CTorArgsTuple>::type, std::true_type,
                          InstanceArgsIndex>>::type type;
      };

      template<typename CTorArgsTuple, std::size_t CTorIndex,
               typename InstanceArgsTuple, std::size_t InstanceArgsIndex,
               typename ...Result>
      struct ArgumentResolverTupleBuilderImpl<false, false, CTorArgsTuple,
          CTorIndex, InstanceArgsTuple, InstanceArgsIndex, Result...>
      {
        typedef typename ArgumentResolverTupleBuilderImplHelper<CTorArgsTuple,
        CTorIndex + 1, InstanceArgsTuple, InstanceArgsIndex, Result...,
        ArgumentResolver<typename std::tuple_element<CTorIndex,
                         CTorArgsTuple>::type, std::false_type,
                         InstanceArgsIndex>>::type type;
      };

      template<bool InstanceArgsAvailable, typename CTorArgsTuple,
               typename ...InstanceArgs>
      struct ArgumentResolverTupleBuilder;

      template<typename CTorArgsTuple>
      struct ArgumentResolverTupleBuilder<false, CTorArgsTuple>
      {
        struct DummyInstanceArg;
        typedef typename ArgumentResolverTupleBuilderImplHelper<CTorArgsTuple,
          0, std::tuple<DummyInstanceArg>, 0>::type type;
      };

      template<typename CTorArgsTuple, typename ...InstanceArgs>
      struct ArgumentResolverTupleBuilder<true, CTorArgsTuple, InstanceArgs...>
      {
        typedef typename ArgumentResolverTupleBuilderImplHelper<CTorArgsTuple,
          0, std::tuple<InstanceArgs...>, 0>::type type;
      };
    }

    template<typename CTorArgsTuple, typename ...InstanceArgs>
    struct ArgumentResolverTupleBuilder
    {
      typedef typename detail::ArgumentResolverTupleBuilder<
        sizeof...(InstanceArgs) != 0,
        CTorArgsTuple, InstanceArgs...>::type type;
    };

    template<size_t N>
    struct ExpandArgumentResolverTupleToInstance
    {
      template<typename Type, typename Tuple, typename InstanceArgsTuple,
               typename ...Args>
      static Type* createInstance(
                 System* sys, InstanceArgsTuple& tuple, Args&&... args) {
        return ExpandArgumentResolverTupleToInstance<N - 1>::template
                 createInstance<Type, Tuple>(sys, tuple,
                 std::tuple_element<N, Tuple>::type::get(sys, tuple),
                 std::forward<Args>(args)...);
      }
    };

    template<>
    struct ExpandArgumentResolverTupleToInstance<0>
    {
      template<typename Type, typename Tuple, typename InstanceArgsTuple,
               typename ...Args>
      static Type* createInstance(
                 System* sys, InstanceArgsTuple& tuple, Args&&... args) {
        return new Type(std::tuple_element<0, Tuple>::type::get(sys, tuple),
                 args...);
      }
    };

    template<class, class>
    class ClassConstructor;

    template<typename Type, typename R, typename ...Args>
    class ClassConstructor<Type, R(Args...)>
    {
    public:
      template<typename ...InstanceArgs>
      static Type* createInstance(System* sys, InstanceArgs&&... args) {
        // TODO, check if sizeof...(InstanceArgs) is equal to the number of
        // arguments extracted out of the InstanceArgs
        static_assert(sizeof...(InstanceArgs) <= sizeof...(Args),
                      "Too much arguments for the constructor!");

        typedef typename ArgumentResolverTupleBuilder<std::tuple<Args...>,
            InstanceArgs...>::type argument_resolver_tuple;
        auto instance_args_tuple =
                 std::make_tuple(std::forward<InstanceArgs>(args)...);
        return ExpandArgumentResolverTupleToInstance<
                   std::tuple_size<argument_resolver_tuple>::value - 1>::
                 template createInstance<Type, argument_resolver_tuple>(
                            sys, instance_args_tuple);
      }
    };

    template<typename Type>
    class Variable
    {
    public:
      Variable() : pVar(System::getSingleton().getObject<Type*>()){}

      Type* operator->() const { return pVar; }

    private:
      Type* pVar;
    };

    #define DI_REGISTER_EXTERN_OBJECT(TYPE, INSTANCE)                          \
      Ocelot::DI::System::getSingleton().registerObject<TYPE>(INSTANCE)

    #define DI_REGISTER_OBJECT(TYPE)                                           \
      DI_REGISTER_EXTERN_OBJECT(TYPE, this)

    #define DI_UNREGISTER_OBJECT(TYPE)                                         \
      Ocelot::DI::System::getSingleton().unregisterObject<TYPE>()

    #define DI_CREATE_INSTANCE(TYPE, ...)                                      \
      Ocelot::DI::System::getSingleton().createInstance<TYPE>(__VA_ARGS__)

    #define DI_CTOR_FIRST(CLASS, ...)                                          \
      typedef bool DIConstructorSupport;                                       \
      template<size_t N, typename Dummy>                                       \
      struct DIConstructorArguments {};                                        \
      template<typename Dummy>                                                 \
      struct DIConstructorArguments<0, Dummy>                                  \
      {                                                                        \
        static void arguments(__VA_ARGS__);                                    \
        typedef Ocelot::DI::ClassConstructor<CLASS, decltype(arguments)> Ctor; \
      };                                                                       \
      CLASS ( __VA_ARGS__ )

    #define DI_CTOR(CLASS, ...)                                                \
      DI_CTOR_FIRST(CLASS, __VA_ARGS__)

    #define DI_VAR(CLASS, NAME)                                                \
      Ocelot::DI::Variable<CLASS> NAME

  } //namespace DI

} //namespace ocelot

#endif // DEPENDENCYINJECTION_H_
