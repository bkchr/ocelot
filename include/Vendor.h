#ifndef VENDOR_H_
#define VENDOR_H_

#include <string>

namespace Ocelot
{
  enum Vendor : unsigned char
  {
    Vendor_AMD,
    Vendor_Intel,
    Vendor_Nvidia,
    Vendor_Unknown
  };

  Vendor identifyVendor(const std::string &name);

}  //namespace Ocelot

#endif // VENDOR_H_
