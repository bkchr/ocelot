#ifndef FUTURE_H_
#define FUTURE_H_

#include "Event.h"

namespace Ocelot
{

  template<typename Type>
  class Future
  {
  public:
    Future(const Type& data,
           const std::vector<Event>& events = std::vector<Event>()) :
      mResult(data),
      mEvents(events) {
    }

    Future(const Type& data,
           const Event& event) :
      mResult(data),
      mEvents({ event }) {
    }

    template<typename FType>
    Future(const Type& data, const Future<FType>& event) :
      mResult(data),
      mEvents(event.getEvents()) {
    }

    Future(const Future<Type>& other) :
      mResult(other.mResult),
      mEvents(other.mEvents) {
    }

    Future(Future<Type>&& other) :
      mResult(std::move(other.mResult)),
      mEvents(std::move(other.mEvents)) {
    }

    Future& operator=(const Future<Type>& other) {
      if (this != &other) {
        mResult = other.mResult;
        mEvents = other.mEvents;
      }

      return *this;
    }

    Future& operator=(Future<Type>&& other) {
      if (this != &other) {
        mResult = std::move(other.mResult);
        mEvents = std::move(other.mEvents);
      }

      return *this;
    }

    const std::vector<Event>& getEvents() const { return mEvents; }
    std::vector<Event> getEvents() { return mEvents; }

    void wait() const {
      if (!mEvents.empty()) {
        for (auto& event : mEvents)
          const_cast<Event&>(event).wait();
      }
    }

    Type& get() {
      wait();

      return mResult;
    }

    Type& getWithoutWaiting() {
      return mResult;
    }

  private:
    Type mResult;
    std::vector<Event> mEvents;
  };

  template<>
  class Future<void>
  {
  public:
    Future(const Event& event) :
      mEvents({ event }) {
    }

    Future(const std::vector<Event>& events = std::vector<Event>()) :
      mEvents(events) {
    }

    template<typename FType>
    Future(const Future<FType>& event) :
      mEvents(event.getEvents()) {
    }

    Future(const Future<void>& other) :
      mEvents(other.mEvents) {
    }

    Future(Future<void>&& other) :
      mEvents(std::move(other.mEvents)) {
    }

    Future& operator=(const Future<void>& other) {
      if (this != &other) {
        mEvents = other.mEvents;
      }

      return *this;
    }

    Future& operator=(Future<void>&& other) {
      if (this != &other) {
        mEvents = std::move(other.mEvents);
      }

      return *this;
    }

    const std::vector<Event>& getEvents() const { return mEvents; }
    std::vector<Event> getEvents() { return mEvents; }

    void wait() const {
      if (!mEvents.empty()) {
        for (auto& event : mEvents)
          const_cast<Event&>(event).wait();
      }
    }

    void get() {
      wait();
    }

  private:
    std::vector<Event> mEvents;
  };

  template<typename Type, typename EventType>
  Future<Type> make_Future(const Type& data,
                           const EventType& event) {
    return Future<Type>(data, event);
  }

  template<typename EventType>
  Future<void> make_Future(const EventType& event) {
    return Future<void>(event);
  }

} //namespace Ocelot

#endif // FUTURE_H_

