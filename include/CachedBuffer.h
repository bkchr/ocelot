#ifndef VECTORIZEDCACHEDBUFFER_
#define VECTORIZEDCACHEDBUFFER_

#include "Defines.h"
#include "Buffer.h"
#include "CacheManager.h"
#include "DependencyInjection.h"
#include "CacheCostCalculator.h"

#include <boost/thread/shared_mutex.hpp>

namespace Ocelot
{

  class CachedBuffer
  {
  public:
    CachedBuffer();

    void registerVector(const BufferPtr& ptr,
                        const CacheCost::CalculatorPtr& cost,
                        unsigned short index);
    BufferPtr getVector(Pointer<Device> dev, unsigned short index);

    void registerBuffer(const BufferPtr& ptr,
                        const CacheCost::CalculatorPtr& cost);
    BufferPtr getBuffer(Pointer<Device> dev);

    Future<std::shared_ptr<void>> getHostData();
    Future<void> getHostData(void* ptr);

  private:
    BufferPtr mergeVectors(Pointer<Device> dev);
    std::vector<BufferPtr> getBuffersForHostTransfer();
    Future<void> getHostData(void* ptr, const std::vector<BufferPtr>& buffers);

    std::vector<ResourceId> getVectors();

    std::map<unsigned short, ResourceId> mVectors;
    ResourceId mBufferId;
    DI_VAR(CacheManager, pCacheManager);
    boost::shared_mutex mMutex;
  };

} //namespace Ocelot

#endif // VECTORIZEDCACHEDBUFFER_

