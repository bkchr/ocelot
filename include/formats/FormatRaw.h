#ifndef FORMATRAW_H_
#define FORMATRAW_H_

#include "Defines.h"

#include "Buffer.h"
#include "Column.h"
#include "DependencyInjection.h"
#include "Exception.h"
#include "CachedBuffer.h"
#include "Profiler.h"
#include "CacheCostCalculator.h"

namespace Ocelot
{

  namespace Format
  {

    class Raw
    {
    public:
      Raw(const Pointer<Column>& col, DataType type) :
        mColumn(col),
        mType(type) {

      }
      
      template<typename Type>
      Raw(const Pointer<Column>& col, DataType type, const Type* data,
          cl_uint count) :
        mColumn(col),
        mType(type) {
        assert(count > 0);
        assert(data != nullptr);

        mCount = divisionWithRound(count * sizeof(Type),
                                   GET_TYPE_SIZE(mType));

        copyData(data, count);
      }

      template<typename Type>
      Raw(const Pointer<Column>& col, DataType type, Type* data, cl_uint count,
          bool copydata) :
        mColumn(col),
        mType(type) {
        assert(count > 0);
        assert(data != nullptr);

        mCount = divisionWithRound(count * sizeof(Type),
                                   GET_TYPE_SIZE(mType));

        if (copydata) {
          copyData(data, count);
        } else {
          mData = std::shared_ptr<void>(data, [](char* ) {});
        }
      }

      Raw(const Pointer<Column>& col, DataType type, const BufferPtr& ptr,
          const CacheCost::CalculatorPtr& cost) :
        mColumn(col),
        mType(type) {
        assert(ptr != nullptr);

        pCachedBuffer = new CachedBuffer();
        pCachedBuffer->registerBuffer(ptr, cost);
        mCount = ptr->getCount();
      }

      template<DataType Type>
      auto getHostData() {
        return getHostData<GET_TYPE(Type)>();
      }

      template<typename InputType,
               typename Type = typename remove_all_qualifiers<InputType>::type>
      auto getHostData() {
        std::lock_guard<std::recursive_mutex> lock(mHostDataLock);

        if (mData == nullptr) {
          transferDataToHost();
        }

        auto count = divisionWithRound(
                       mCount * GET_TYPE_SIZE(mType),
                       sizeof(Type));

        return make_Future(
              std::make_pair(count,
                             std::static_pointer_cast<const Type>(mData)),
              mDataEvents);
      }

      template<typename Type>
      Future<void> getHostData(Type* ptr) {
        std::lock_guard<std::recursive_mutex> lock(mHostDataLock);

        if (mData == nullptr) {
          return pCachedBuffer->getHostData(ptr);
        }

        // we need a copy or it could happen that the DataEvents vector gets
        // modified while the wait function iterates over the vector
        auto events_copy = mDataEvents;
        Event::wait(events_copy);
        char* cptr = static_cast<char*>(mData.get());
        std::copy(cptr, cptr + getSizeInBytes(), ptr);

        return Future<void>();
      }

      BufferPtr getDeviceData(Pointer<Device> dev) {
        std::lock_guard<std::mutex> lock(mDeviceDataLock);

        if (pCachedBuffer == nullptr) {
          transferDataToDevice(dev);
        }

        return pCachedBuffer->getBuffer(dev);
      }

      BufferPtr getDeviceDataVector(Pointer<Device> dev, unsigned short index) {
        std::lock_guard<std::mutex> lock(mDeviceDataLock);

        if (pCachedBuffer == nullptr) {
          transferDataToDevice(dev);
        }

        return pCachedBuffer->getVector(dev, index);
      }

      void registerDeviceDataVector(const BufferPtr& ptr,
                                    const CacheCost::CalculatorPtr& cost,
                                    unsigned short index) {
        assert(mData == nullptr);

        std::lock_guard<std::mutex> lock(mDeviceDataLock);

        if (pCachedBuffer == nullptr) {
          pCachedBuffer = new CachedBuffer();
        }

        pCachedBuffer->registerVector(ptr, cost, index);
        mCount += ptr->getCount();
      }

      cl_uint getCount() {
        return mCount;
      }

      cl_uint getSizeInBytes() {
        return mCount * GET_TYPE_SIZE(mType);
      }

    private:
      BufferPtr transferDataToDevice(Pointer<Device> dev) {
        assert(mData != nullptr);
        assert(pCachedBuffer == nullptr);

        OperationId opid = Invalid_OperationId;
        BufferPtr buffer = nullptr;
        {
          OCL_PROFILE_OPERATION_SCOPE("FormatRaw::transferDataToDevice");
          buffer = dev->allocateBuffer(mType, mCount);
          buffer->writeBytes(mData.get(),
                             mCount * GET_TYPE_SIZE(mType), 0);
          opid = OCL_CURRENT_OPERATION_ID();
        }

        pCachedBuffer = new CachedBuffer();
        pCachedBuffer->registerBuffer(
              buffer, CacheCost::CalculatorPtr(new CacheCost::Operation(opid)));

        return buffer;
      }

      void transferDataToHost() {
        assert(pCachedBuffer != nullptr);

        auto future = pCachedBuffer->getHostData();
        mData = future.getWithoutWaiting();
        mDataEvents = future.getEvents();
        registerEventCompleteCallback(mDataEvents);
      }

      void registerEventCompleteCallback(std::vector<Event> events) {
        for (auto event : events) {
          event.set_callback(std::bind(&Raw::handleOnEventComplete, this,
                                       event));
        }
      }

      void handleOnEventComplete(Event event) {
        std::lock_guard<std::recursive_mutex> lock(mHostDataLock);
        auto find = std::find(mDataEvents.begin(), mDataEvents.end(), event);

        if (find != mDataEvents.end())
          mDataEvents.erase(find);
      }

      template<typename Type>
      void copyData(const Type* data, cl_uint count) {
        mData = std::shared_ptr<void>(new char[getSizeInBytes()],
            [](char* del) { delete[] del; });
        std::copy(data, data + count, static_cast<Type*>(mData.get()));
      }

      Pointer<Column> mColumn;
      DataType mType;
      DI_VAR(CacheManager, pCacheManager);
      CachedBuffer* pCachedBuffer = nullptr;
      std::shared_ptr<void> mData = nullptr;
      std::vector<Event> mDataEvents;
      std::recursive_mutex mHostDataLock;
      std::mutex mDeviceDataLock;
      cl_uint mCount = 0;
    };

  } //namespace Format

} //namespace Ocelot

#endif // FORMATRAW_H_

