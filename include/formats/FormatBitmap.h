#ifndef FORMATBITMAP_H_
#define FORMATBITMAP_H_

#include "Defines.h"

#include <mutex>

namespace Ocelot
{
  class Column;
  class Buffer;
  class Device;

  namespace CacheCost
  {
    class Calculator;
  }

  namespace Format
  {
    class Raw;

    class Bitmap
    {
    public:
      Bitmap(const Pointer<Column>& col, DataType type);
      Bitmap(const Pointer<Column>& col, DataType type,
             const Pointer<Buffer>& ptr,
             const Pointer<CacheCost::Calculator>& cost);

      Pointer<Buffer> getDeviceData(Pointer<Device> dev, bool offsets = false);
      Pointer<Buffer> getDeviceDataVector(Pointer<Device> dev,
                                          unsigned short index,
                                          bool offsets = false);
      void registerDeviceDataVector(const Pointer<Buffer>& ptr,
                                    const Pointer<CacheCost::Calculator>& cost,
                                    unsigned short index);

      cl_uint getCount();
      cl_uint getSizeInBytes();

    private:
      std::pair<Pointer<Buffer>, Pointer<CacheCost::Calculator>>
        createOffsets(Pointer<Buffer> bitmap);

      Pointer<Column> mColumn;
      DataType mType;
      Pointer<Raw> mBitmap;
      Pointer<Raw> mOffsets;
      std::mutex mOffsetsMutex;
    };

  } //namespace Format

} //namespace Ocelot

#endif // FORMATBITMAP_H_

