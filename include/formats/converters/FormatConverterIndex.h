#ifndef FORMATCONVERTERINDEX_H_
#define FORMATCONVERTERINDEX_H_

#include "Defines.h"
#include "ClassIDGenerator.h"

namespace Ocelot
{
  class Column;

  namespace Format
  {
    class Converter;

    class ConverterIndex
    {
    public:
      template<typename Converter, typename From, typename To>
      static void registerConverter() {
        auto converter = std::make_shared<ConverterAllocatorImpl<Converter>>();
        getConverters()[ClassIDGenerator<From>::getID()]
                       [ClassIDGenerator<To>::getID()] = converter;
      }

      static Pointer<Converter> getConverter(
          const Pointer<Column>& col, ClassID from, ClassID to) {
        auto converters = getConverters();

        auto findfrom = converters.find(from);

        if (findfrom == converters.end())
          return nullptr;

        auto findto = findfrom->second.find(to);

        if (findto == findfrom->second.end())
          return nullptr;

        return findto->second->allocate(col);
      }

    private:
      class ConverterAllocator
      {
      public:
        virtual Pointer<Converter> allocate(const Pointer<Column>& col) = 0;
      };

      template<typename Type>
      class ConverterAllocatorImpl : public ConverterAllocator
      {
      public:
        Pointer<Converter> allocate(const Pointer<Column>& col) override final {
          return std::make_shared<Type>(col);
        }
      };

      using ConverterMap =
        std::unordered_map<ClassID,
                           std::unordered_map<ClassID,
                                              Pointer<ConverterAllocator>>>;
      static ConverterMap& getConverters() {
        static ConverterMap map;
        return map;
      }
    };

    #define OCL_FORMAT_REGISTER_CONVERTER(converter, from, to)                 \
      struct converter##from##to##Registration                                 \
      {                                                                        \
      private:                                                                 \
        static bool mRegister;                                                 \
      };                                                                       \
      bool converter##from##to##Registration::mRegister =                      \
      (ConverterIndex::registerConverter<converter, from, to>(), true)


  } //namespace Format

} //namespace Ocelot

#endif // FORMATCONVERTERINDEX_H_

