#ifndef FORMATCONVERTER_H_
#define FORMATCONVERTER_H_

#include "Defines.h"

namespace Ocelot
{
  class Device;

  namespace Format
  {

    /**
     * @brief The interface for a format converter.
     *
     * The derived class needs an constructor which takes a column pointer.
     * The registration of the converter is done with the macro
     * OCL_FORMAT_REGISTER_CONVERTER. The implementation is responsible for
     * tracking which data is converted. The function calls need to be
     * threadsafe. The lifetime of the object is the same as the destination
     * format.
     */
    class Converter
    {
    public:
      virtual ~Converter() {}
      /**
       * @brief Convert the host data from one format to another format.
       */
      virtual void convertHostData() = 0;
      /**
       * @brief Convert the device data from one format to another format.
       */
      virtual void convertDeviceData(Pointer<Device>) = 0;
      /**
       * @brief Convert one device data vector with the given index from one
       *        format to another format.
       */
      virtual void convertDeviceDataVector(Pointer<Device>, unsigned short) = 0;
    };

  } //namespace Format

} //namespace Ocelot

#endif // FORMATCONVERTER_H_

