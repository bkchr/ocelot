#ifndef FORMATRAWTOBITMAPCONVERTER_H_
#define FORMATRAWTOBITMAPCONVERTER_H_

#include "FormatConverter.h"

namespace Ocelot
{
  class Column;
  class Buffer;

  namespace CacheCost
  {
    class Calculator;
    using CalculatorPtr = Pointer<Calculator>;
  }

  namespace Format
  {

    class RawToBitmapConverter : public Converter
    {
    public:
      RawToBitmapConverter(Pointer<Column> col);

      void convertHostData();
      void convertDeviceData(Pointer<Device> dev);
      void convertDeviceDataVector(Pointer<Device> dev, unsigned short index);

    private:
      void generateFilledBitmap(Pointer<Device> dev, unsigned int tuples,
                                Pointer<Buffer>& result,
                                CacheCost::CalculatorPtr& cost);

      void encodeBitmap(Pointer<Device> dev, Pointer<Buffer> buffer,
                        Pointer<Buffer>& result,
                        CacheCost::CalculatorPtr& cost);

      Pointer<Column> mCol;
    };

  }

} //namespace Ocelot

#endif // FORMATRAWTOBITMAPCONVERTER_H_
