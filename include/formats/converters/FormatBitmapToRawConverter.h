#ifndef FORMATBITMAPTORAWCONVERTER_H_
#define FORMATBITMAPTORAWCONVERTER_H_

#include "FormatConverter.h"

namespace Ocelot
{
  class Column;
  class Buffer;

  namespace CacheCost
  {
    class Calculator;
    using CalculatorPtr = Pointer<Calculator>;
  }

  namespace Format
  {

    class BitmapToRawConverter : public Converter
    {
    public:
      BitmapToRawConverter(Pointer<Column> col);

      void convertHostData();
      void convertDeviceData(Pointer<Device> dev);
      void convertDeviceDataVector(Pointer<Device> dev, unsigned short index);

    private:
      void materializeBuffer(Pointer<Device> dev, Pointer<Buffer> buffer,
                             Pointer<Buffer> offsets, Pointer<Buffer>& result,
                             CacheCost::CalculatorPtr& cost);

      void expandBuffer(Pointer<Device> dev, Pointer<Buffer> buffer,
                        Pointer<Buffer>& result,
                        CacheCost::CalculatorPtr& cost);

      Pointer<Column> mCol;
    };

  }

} //namespace Ocelot

#endif // FORMATBITMAPTORAWCONVERTER_H_

