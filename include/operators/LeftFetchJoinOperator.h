#ifndef LEFTFETCHJOINOPERATOR_H_
#define LEFTFETCHJOINOPERATOR_H_

#include "Defines.h"
#include "Operator.h"
#include "FormatBitmap.h"

namespace Ocelot
{

  template<DataType Type>
  class LeftFetchJoinOperator : public Operator
  {
  public:
    DI_CTOR(LeftFetchJoinOperator, DeviceSelectionManager* dsmgr) :
      Operator(dsmgr) {
    }

    void run(Pointer<Device> dev) override final {
      if (hasInputChannelFormat<Format::Bitmap>(0)) {
        THROW_EXCEPTION("Implement LeftFetchJoinOperator bitmap support!!");
      } else {
        executeFetchJoin(dev);
      }
    }

    bool doesInputChannelSupportVectorizedData(
        unsigned short channel) const override final {
      return channel == 0;
    }
    unsigned short getMaximumNumberOfRuns() override final {
      return getInputChannelMaximumNumberOfRuns(0);
    }
    std::string getName() override final {
      return "LeftFetchJoinOperator";
    }

  private:
    void executeFetchJoin(Pointer<Device> dev) {
      auto left = getInputChannel(dev, 0);
      auto right = getInputChannel(dev, 1);
      auto tuples = left->getCount();
      auto result = dev->allocateBuffer(Type, tuples);
      auto kernel = dev->getKernel("fetchJoin", Type);

      kernel->setArgs(in(left), tuples, in(right), out(result),
                      kernel->getTuplesPerThread(tuples));
      kernel->execute();

      addOutputChannelVectorized(result, 0, Type);
    }
  };

} //namespace Ocelot

#endif // LEFTFETCHJOINOPERATOR_H_

