#ifndef SORTOPERATOR_H_
#define SORTOPERATOR_H_

#include "Defines.h"
#include "Operator.h"
#include "Sort.h"
#include "VoidColumn.h"

namespace Ocelot
{

  template<DataType Type>
  class SortOperator : public Operator
  {
  public:
    DI_CTOR(SortOperator, DeviceSelectionManager* dsmgr, Sort::Direction dir) :
      Operator(dsmgr),
      mSortDir(dir) {
    }

    void run(Pointer<Device> dev) override final {
      auto keys = getInputChannel(dev, 0);

      BufferPtr values;
      if (hasInputChannel(1)) {
        values = getInputChannel(dev, 1);
      } else {
        values = VoidColumn(dev, keys->getCount()).getResult();
      }

      auto result = Sort(keys, values, mSortDir, dev).getResult();

      addOutputChannel(result.first, 0, result.first->getType());
      addOutputChannel(result.second, 1, result.second->getType());
    }

    bool doesInputChannelSupportVectorizedData(
        unsigned short channel) const override final {
      (void)channel;
      return false;
    }
    unsigned short getMaximumNumberOfRuns() override final {
      return 1;
    }
    std::string getName() override final {
      return "SortOperator";
    }

  private:
    Sort::Direction mSortDir;
  };

}

#endif // SORTOPERATOR_H_

