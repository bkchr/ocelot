#ifndef GROUPBYOPERATOR_H_
#define GROUPBYOPERATOR_H_

#include "Defines.h"
#include "Operator.h"
#include "GroupedAggregation.h"

namespace Ocelot
{

  template<DataType Type>
  class GroupByOperator : public Operator
  {
  public:
    DI_CTOR(GroupByOperator,
            DeviceSelectionManager* dsmgr, GroupedAggregation::Operation op,
            unsigned int group_count, bool no_nil) :
      Operator(dsmgr),
      mOp(op),
      mGroupCount(group_count),
      mNoNil(no_nil) {
    }

    void run(Pointer<Device> device) override final {
      auto input = getInputChannel(device, 0);
      auto groups = getInputChannel(device, 1);

      auto agg = GroupedAggregation(input, groups, mGroupCount, mNoNil, mOp);

      addOutputChannel(agg.getResult(), 0, agg.getResult()->getType());
    }

    bool doesInputChannelSupportVectorizedData(
        unsigned short) const override final {
      return false;
    }
    unsigned short getMaximumNumberOfRuns() override final {
      return 1;
    }
    std::string getName() override final {
      return "GroupByOperator";
    }

  private:
    GroupedAggregation::Operation mOp;
    unsigned int mGroupCount;
    bool mNoNil;
  };

} //namespace Ocelot

#endif // GROUPBYOPERATOR_H_

