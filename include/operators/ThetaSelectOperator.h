#ifndef THETASELECT_H_
#define THETASELECT_H_

#include "Defines.h"
#include "Operator.h"
#include "FormatBitmap.h"

namespace Ocelot
{

  template<DataType DType, typename Type = GET_TYPE(DType)>
  class ThetaSelectOperator : public Operator
  {
  public:
    DI_CTOR(ThetaSelectOperator, DeviceSelectionManager* dsmgr, Type low,
            Type up, bool low_set, bool up_set, bool lower_inclusive,
            bool upper_inclusive, bool negate_predicate) :
      Operator(dsmgr),
      mLowerBound(low),
      mUpperBound(up),
      mLowerBoundSet(low_set),
      mUpperBoundSet(up_set),
      mLowerBoundInclusive(lower_inclusive),
      mUpperBoundInclusive(upper_inclusive),
      mNegatePredicate(negate_predicate) {
    }

    void run(Pointer<Device> dev) override final {
      auto input = getInputChannel(dev, 0);
      auto result = dev->allocateBuffer(DTypeBitmap, input->getCount());

      auto kernel = dev->getKernel("theta_select", input->getType());

      kernel->setArgs(in(input), out(result), input->getCount(), mLowerBound,
                      mUpperBound, mLowerBoundSet, mUpperBoundSet,
                      mLowerBoundInclusive, mUpperBoundInclusive,
                      mNegatePredicate);
      kernel->execute(result->getSizeInBytes());

      addOutputChannelVectorized<Format::Bitmap>(result, 0, DTypeS32);
    }

    bool doesInputChannelSupportVectorizedData(
        unsigned short channel) const override final {
      (void)channel;
      return true;
    }

    unsigned short getMaximumNumberOfRuns() override final {
      return getInputChannelMaximumNumberOfRuns(0);
    }

    std::string getName() override final {
      return "ThetaSelectOperator";
    }

  private:
    Type mLowerBound;
    Type mUpperBound;
    bool mLowerBoundSet;
    bool mUpperBoundSet;
    bool mLowerBoundInclusive;
    bool mUpperBoundInclusive;
    bool mNegatePredicate;
  };

} //namespace Ocelot

#endif // THETASELECT_H_
