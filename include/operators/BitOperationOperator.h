#ifndef BITOPERATIONOPERATOR_H
#define BITOPERATIONOPERATOR_H

#include "Defines.h"
#include "BitOperation.h"
#include "Operator.h"
#include "FormatBitmap.h"

namespace Ocelot
{

  template<DataType DType, typename Type = GET_TYPE(DType)>
  class BitOperationOperator : public Operator
  {
  public:
    DI_CTOR(BitOperationOperator, DeviceSelectionManager* dsmgr, BitOp op) :
      Operator(dsmgr),
      mOperation(op) {
    }

    void run(Pointer<Device> dev) override final {
      auto left = getInputChannel(dev, 0);

      BufferPtr right = nullptr;
      if (mOperation != BIT_NOT) {
        right = getInputChannel(dev, 1);
      }

      auto bitop = BitOperation(mOperation, left, right, false, dev);

      addOutputChannelVectorized<Format::Bitmap>(bitop.getResult(), 0, DType);
    }

    bool doesInputChannelSupportVectorizedData(
        unsigned short channel) const override final {
      (void)channel;
      return true;
    }

    unsigned short getMaximumNumberOfRuns() override final {
      return getInputChannelMaximumNumberOfRuns(0);
    }

    std::string getName() override final {
      return "BitOperationOperator";
    }

  private:
    BitOp mOperation;
  };

}  //namespace Ocelot

#endif // BITOPERATIONOPERATOR_H
