#ifndef OPERATORCOLUMNINPUT_H_
#define OPERATORCOLUMNINPUT_H_

#include "Defines.h"
#include "Operator.h"

namespace Ocelot
{

  template<DataType Type>
  class ColumnInputOperator : public Operator
  {
  public:
    DI_CTOR(ColumnInputOperator,
            const ColumnPtr& col, GlobalVectorSize globalvecsize,
            DeviceSelectionManager* dsmgr) :
      Operator(dsmgr),
      mGlobalVectorSize(globalvecsize) {
      addOutputChannel(col, 0);
      mCount = col->getCount();
    }

    void run(Pointer<Device>) override final {}

    bool doesInputChannelSupportVectorizedData(
        unsigned short) const override final {
      return false;
    }
    unsigned short getMaximumNumberOfRuns() override final {
      return Buffer::getMaxNumberOfSubBuffer(mCount, Type, mGlobalVectorSize);
    }
    std::string getName() override final {
      return "ColumnInputOperator";
    }

  private:
    size_t mCount = 0;
    GlobalVectorSize mGlobalVectorSize;
  };

} //namespace Ocelot

#endif // OPERATORCOLUMNINPUT_H_

