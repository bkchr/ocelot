#ifndef DEVICEMANAGER_H_
#define DEVICEMANAGER_H_

#include "Defines.h"
#include "Vendor.h"
#include "DependencyInjection.h"

#include <vector>
#include <unordered_map>

#include <boost/compute/device.hpp>

namespace Ocelot
{
  class Device;
  class PlatformManager;
  class CacheManager;
  class Platform;
  class DeviceSelectionManager;
  
	class DeviceManager
	{
	public:
    DeviceManager(Pointer<PlatformManager> plmgr,
                  Pointer<CacheManager> cachemgr,
                  const std::string& kernel_path);
		~DeviceManager();

    const std::vector<Pointer<Device>>& getDevices() const { return mDevices; }
    GlobalVectorSize::type getGlobalVectorSize() const {
      return mGlobalVectorSizeValue;
    }

	private:
    void init(Pointer<CacheManager> cachemgr, const std::string& kernel_path);
    typedef std::unordered_map<Platform*, std::vector<boost::compute::device>>
    PlatformDeviceMap;
    PlatformDeviceMap getDevicesPerPlatform();
    void initDevices(const PlatformDeviceMap& devices,
                     Pointer<CacheManager> cachemgr,
                     const std::string& kernel_path);
    void initPlatformContexts(const PlatformDeviceMap& devices);
    void initGlobalVectorSizeConstant();

		void deinit();

    std::vector<Pointer<Device>> mDevices;
    Pointer<PlatformManager> mPlatformManager;
    Pointer<DeviceSelectionManager> mDeviceSelectionManager;
    GlobalVectorSize::type mGlobalVectorSizeValue = 0;
	};

}	//namespace Ocelot

#endif // DEVICEMANAGER_H_
