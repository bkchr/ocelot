#ifndef PLATFORMMANAGER_H
#define PLATFORMMANAGER_H

#include <vector>

namespace Ocelot
{
  class Platform;

  class PlatformManager
  {
  public:
    PlatformManager();
    ~PlatformManager();

    const std::vector<Platform*>& getPlatforms() const { return mPlatforms; }

  private:
    void init();
    void deinit();

    std::vector<Platform*> mPlatforms;
  };

} //namespace Ocelot

#endif // PLATFORMMANAGER_H
