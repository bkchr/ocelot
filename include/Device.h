#ifndef DEVICE_H_
#define DEVICE_H_

#include "Defines.h"
#include "Vendor.h"
#include "Buffer.h"
#include "KernelLibrary.h"

#include <boost/compute/device.hpp>

#include <string>
#include <functional>
#include <atomic>

namespace Ocelot
{
  class Platform;
  class Scheduler;
  class CacheManager;

  namespace CacheCost
  {
    class Calculator;
    typedef std::shared_ptr<Calculator> CalculatorPtr;
  }

  class Device : public std::enable_shared_from_this<Device>
  {
  public:
    enum Type
    {
      GPU = boost::compute::device::gpu,
      CPU = boost::compute::device::cpu
    };

    Device(const boost::compute::device& device, Platform* platform,
           Pointer<CacheManager> cachemgr);
    ~Device();
    void init(const std::string& kernel_path);

    Platform* getPlatform() const { return pPlatform; }
    cl_device_id getId() const { return mDevice.get(); }

    Vendor getVendor() const { return mVendor; }
    const std::string getPlatformName() const { return mDevice.vendor(); }
    bool isHostUnifiedMemory() const;
    cl_ulong getGlobalMemorySize() const;
    cl_ulong getLocalMemorySize() const { return mDevice.local_memory_size(); }
    cl_ulong getMaxMemoryAlloc() const;
    cl_uint getMaxComputeUnits() const { return mDevice.compute_units(); }
    size_t getMaxWorkgroupSize() const { return mDevice.max_work_group_size(); }
    std::vector<size_t> getMaxWorkItemSizes() const {
      return mDevice.get_info<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
    }
    cl_uint getMemBaseAddrAlign() const;

    bool checkOpenClVersion(int major, int minor) const;

    size_t getUniqueId() const { return mUniqueId; }
    static size_t getUniqueId(const boost::compute::device& device);
    Type getType() const { return static_cast<Type>(mDevice.type()); }

    BufferPtr allocateBuffer(DataType type, unsigned int count,
                             void* host_ptr = nullptr,
                             bool ptr_stays_valid = false);

    template<typename Type>
    BufferPtr allocateBuffer(DataType type, unsigned int count,
                             const std::shared_ptr<Type>& host_ptr = nullptr,
                             bool ptr_stays_valid = false) {
      return allocateBuffer(type, count, host_ptr.get(), ptr_stays_valid);
    }

    template<DataType Type = DTypeS32>
    BufferPtr allocateBuffer(unsigned int count,
                             GET_TYPE(Type)* host_ptr = nullptr,
                             bool ptr_stays_valid = false) {
      return allocateBuffer(Type, count, host_ptr, ptr_stays_valid);
    }

    template<DataType Type = DTypeS32>
    BufferPtr allocateBuffer(
        unsigned int count, const std::shared_ptr<GET_TYPE(Type)>& host_ptr,
        bool ptr_stays_valid = false) {
      return allocateBuffer(Type, count, host_ptr.get(), ptr_stays_valid);
    }

    BufferPtr allocateBufferOutOfVectors(
        const std::vector<ResourceId>& vectors);

    operator boost::compute::device() const { return mDevice; }

    Scheduler* getScheduler() { return pScheduler; }

    ResourceId registerBufferInCache(const BufferPtr& ptr,
                                     const CacheCost::CalculatorPtr& cost);
    void unregisterBufferInCache(const ResourceId id);
    BufferPtr getCachedBuffer(const ResourceId id);

    template<DataType Type = DTypeS32>
    KernelPtr getKernel(const std::string& name) {
      return getKernel(name, Type);
    }

    KernelPtr getKernel(const std::string& name, DataType type) {
      return pKernelLibrary->getKernel(name, type);
    }

    template<DataType Type = DTypeS32>
    KernelPtr createKernel(const std::string& name, const std::string& source) {
      return pKernelLibrary->createKernel<Type>(name, source);
    }

  private:
    void initPlatformNameAndVendor();

    //! Calculates the buffer size in bytes, also takes alignment into account
    unsigned int calculateBufferSizeInBytes(unsigned int count, DataType type);
    boost::compute::buffer allocateBufferWithTest(
        unsigned int size, cl_mem_flags flags, void* host_ptr);
    std::vector<BufferPtr> getVectorBuffersForResourceIds(
        const std::vector<ResourceId>& ids);

    boost::compute::device mDevice;
    Vendor mVendor;
    size_t mUniqueId;
    Platform* pPlatform;
    Scheduler* pScheduler;
    Pointer<CacheManager> pCacheManager;
    KernelLibrary* pKernelLibrary;
    std::atomic_ulong mRemainingSpace;
  };

  using DevicePtr = Pointer<Device>;

} //namespace Ocelot

#endif // DEVICE_H_
