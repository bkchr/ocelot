#ifndef EVENT_H_
#define EVENT_H_

#include <boost/compute/event.hpp>

namespace Ocelot
{

  class Event : public boost::compute::event
  {
  public:
    Event() : boost::compute::event() {}
    Event(const boost::compute::event& event) : boost::compute::event(event) {}
    Event(boost::compute::event&& event) : boost::compute::event(event) {}

    template<typename EventType>
    Event& operator=(const EventType& other) {
      boost::compute::event::operator =(other);
      return *this;
    }

    template<typename EventType>
    Event& operator=(EventType&& other) {
      boost::compute::event::operator =(other);
      return *this;
    }

    void wait() const;
    static void wait(const std::vector<Event>& events);
  };

} //namespace Ocelot

#endif // EVENT_H_

