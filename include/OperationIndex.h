#ifndef OPERATIONINDEX_H_
#define OPERATIONINDEX_H_

#include "Defines.h"

#include <atomic>
#include <string>

namespace Ocelot
{

  class OperationIndex
  {
  public:
    static OperationId generateId();
    static void startOperation(OperationId id, const std::string& name);
    static void endOperation();
    static OperationId getCurrentOperationId();
    static bool hasParentOperation();
    static OperationId getParentOperationId();
    static const std::string& getCurrentOperationName();
    static OperationId getNumberOfOperations();

  private:
    struct OperationInfo
    {
      OperationId sId;
      std::string sName;

      OperationInfo() : sId(Invalid_OperationId) {}
      OperationInfo(const OperationId id, const std::string& name) :
        sId(id), sName(name) {}
    };

    static thread_local std::vector<OperationInfo> mCurrentOperation;
    static std::atomic<OperationId> mNextOperationId;
  };

  class OperationScope
  {
  public:
    OperationScope(OperationId id, const std::string& name) {
      OperationIndex::startOperation(id, name);
    }

    ~OperationScope() {
      OperationIndex::endOperation();
    }
  };

  #define OCL_OPERATION_SCOPE(id, name)                                        \
            Ocelot::OperationScope oscope(id, name)

  #define OCL_GENERATE_OPERATION_ID() Ocelot::OperationIndex::generateId()

  #define OCL_CURRENT_OPERATION_ID()                                           \
            Ocelot::OperationIndex::getCurrentOperationId()

} //namespace Ocelot

#endif // OPERATIONINDEX_H_
