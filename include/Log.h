#ifndef LOG_H_
#define LOG_H_

#include "Defines.h"

#include "Singleton.h"

namespace Ocelot
{
  class LogBackend
  {
  public:
    virtual void write(const std::string& str) = 0;
  };


  class Log : public Singleton<Log>
  {
  public:
    enum Level {
      Debug,
      Info,
      Warn
    };

    void setBackend(const Pointer<LogBackend>& backend) {
      mBackend = backend;
    }

    template<typename ...Args>
    void write(Level level, Args&&... args) {
      if (mBackend == nullptr) {
        return;
      }

      std::stringstream stream;
      writeLevel(level, stream);
      writeIntern(stream, std::forward<Args>(args)...);
      mBackend->write(stream.str());
    }

  private:
    void writeLevel(Level level, std::stringstream& stream) {
      switch (level) {
        case Debug:
          stream<<"Debug";
          break;
        case Info:
          stream<<"Info";
          break;
        case Warn:
          stream<<"Warn";
          break;
        default:
          THROW_EXCEPTION("Unknown logging level! ", level);
      }
      stream<<": ";
    }

    template<typename FirstArg, typename ...Args>
    void writeIntern(std::stringstream& stream, const FirstArg& arg,
                     Args&&... args) {
      stream<<arg;
      writeIntern(stream, std::forward<Args>(args)...);
    }

    template<typename Arg>
    void writeIntern(std::stringstream& stream, const Arg& arg) {
      stream<<arg;
    }

    Pointer<LogBackend> mBackend = nullptr;
  };

  #define LOG_DEBUG(...)                                                \
    Ocelot::Log::getSingleton().write(Ocelot::Log::Debug, __VA_ARGS__)

  #define LOG_INFO(...)                                                 \
    Ocelot::Log::getSingleton().write(Ocelot::Log::Info, __VA_ARGS__)

  #define LOG_WARN(...)                                                 \
    Ocelot::Log::getSingleton().write(Ocelot::Log::Warn, __VA_ARGS__)

} //namespace Ocelot

#endif // LOG_H_

