#ifndef CONCURRENTCONTEXT_H_
#define CONCURRENTCONTEXT_H_

#include "Defines.h"
#include "Event.h"
#include "Buffer.h"

namespace Ocelot
{

  class ConcurrentContext
  {
  public:
    void addEventsToWaitList(const BufferPtr& ptr,
                             boost::compute::wait_list& list) const;

    void addEvents(const std::vector<Event>& events);

  private:
    bool filterEvent(const Event& event) const;

    void* operator new(size_t) = delete;
    void* operator new[](size_t) = delete;

    std::vector<Event> mEvents;
  };

} //namespace Ocelot

#endif // CONCURRENTCONTEXT_H_

