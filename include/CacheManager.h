#ifndef CACHEMANAGER_H_
#define CACHEMANAGER_H_

#include "Defines.h"

#include "Buffer.h"

#include <unordered_map>
#include <atomic>
#include <type_traits>

namespace Ocelot
{
  class Device;
  class Cache;

  namespace CacheCost
  {
    class Calculator;
    typedef std::shared_ptr<Calculator> CalculatorPtr;
  }

  class CacheManager
  {
  public:
    CacheManager();
    ~CacheManager();

    void registerDevice(Device* device);
    void unregisterDevice(Device* device);

    ResourceId registerBuffer(const BufferPtr& ptr,
                              const CacheCost::CalculatorPtr& cost);
    void unregisterBuffer(const ResourceId id);
    BufferPtr getBuffer(const ResourceId id, Pointer<Device> device);
    typedef std::function<unsigned short (const Pointer<Device>)>
            DeviceChooseFunction;
    BufferPtr getBuffer(const ResourceId id,
                        const DeviceChooseFunction& choose);
    // Yeah someone thought that the automatic conversion from lambda to an
    // std::function isn't needed...
    template<typename Function>
    BufferPtr getBuffer(const ResourceId id, const Function& choose) {
      return getBuffer(id, DeviceChooseFunction(choose));
    }

    CacheCost::CalculatorPtr getBufferCost(const ResourceId id);

    bool freeUpSpace(size_t required_space, Pointer<Device> device);

    BufferPtr getBufferForHostTransfer(const ResourceId id);
    std::vector<BufferPtr> getBuffersForHostTransfer(
        const std::vector<ResourceId>& ids);

  private:
    BufferPtr findAndTransferBuffer(const ResourceId id, Cache* dst_cache);
    Cache* findCacheWithBuffer(const ResourceId id);

    std::unordered_map<Device*, Cache*> mCaches;
    std::atomic<ResourceId> mNextResourceId;
  };

} //namespace Ocelot

#endif // CACHEMANAGER_H_
