#ifndef BUFFER_H_
#define BUFFER_H_

#include "Defines.h"
#include "DependencyInjection.h"

#include <boost/compute/buffer.hpp>

#include <memory>
#include <mutex>

namespace boost
{
  namespace compute
  {
    class wait_list;
  }
}

namespace Ocelot
{
  class Device;

  class Buffer : public std::enable_shared_from_this<Buffer>
  {
  public:
    DI_CTOR(Buffer, const boost::compute::buffer& buffer, unsigned int count,
            WPointer<Device> device, const DataType type,
            const GlobalVectorSize vec_size);
    virtual ~Buffer();

    cl_uint getSizeInBytes() const;
    static cl_uint getSizeInBytes(DataType type, cl_uint count);
    cl_uint getCount() const;
    //! Returns getCount() / GET_COUNT_PER_ELEMENT(mType)
    cl_uint getElementCount() const;
    DataType getType() const { return mType; }

    operator boost::compute::buffer() const { return mBuffer; }

    template<DataType Type>
    Future<void> write(const std::shared_ptr<GET_TYPE(Type)>& host_ptr,
                       size_t count = 0, size_t offset = 0) {
      auto size_offset = mapCountAndOffsetToByte(count, offset);
      return writeBytes(host_ptr.get(), size_offset.first, size_offset.second);
    }

    template<DataType Type>
    Future<void> write(const GET_TYPE(Type)* host_ptr, size_t count = 0,
                       size_t offset = 0) {
      auto size_offset = mapCountAndOffsetToByte(count, offset);
      return writeBytes(host_ptr, size_offset.first, size_offset.second);
    }

    template<typename Type>
    Future<void> write(const std::shared_ptr<Type>& host_ptr, size_t count = 0,
                       size_t offset = 0) {
      auto size_offset = mapCountAndOffsetToByte(count, offset);
      return writeBytes(host_ptr.get(), size_offset.first, size_offset.second);
    }

    template<typename Type>
    Future<void> write(const Type* host_ptr, size_t count = 0,
                       size_t offset = 0) {
      auto size_offset = mapCountAndOffsetToByte(count, offset);
      return writeBytes(host_ptr, size_offset.first, size_offset.second);
    }

    Future<void> writeBytes(const void* host_ptr, size_t size, size_t offset);

    template<DataType Type>
    Future<GET_TYPE(Type)*>
    read(GET_TYPE(Type)* host_ptr, size_t count = 0, size_t offset = 0) const {
      return read<GET_TYPE(Type)>(host_ptr, count, offset);
    }

    template<typename Type>
    Future<Type*>
    read(Type* host_ptr, size_t count = 0, size_t offset = 0) const {
      auto size_offset = mapCountAndOffsetToByte(count, offset);
      auto size_in_bytes = count ? size_offset.first :
                                   getSizeInBytes();
      auto future = readBytes(host_ptr, size_in_bytes, size_offset.second);
      return make_Future(host_ptr, future.getEvents());
    }

    template<DataType Type>
    Future<std::shared_ptr<GET_TYPE(Type)>>
    read(size_t count = 0, size_t offset = 0) const {
      return read<GET_TYPE(Type)>(count, offset);
    }

    template<typename Type = void>
    Future<std::shared_ptr<Type>>
    read(size_t count = 0, size_t offset = 0) const;

    Future<void*> readBytes(void* host_ptr, size_t size,
                            size_t offset = 0) const;

    std::shared_ptr<Buffer> clone() const;
    std::shared_ptr<Buffer> clone(Pointer<Device> dst_device) const;
    std::shared_ptr<Buffer> createSubBuffer(unsigned short index);
    size_t getMaxNumberOfSubBuffer() const;
    static size_t getMaxNumberOfSubBuffer(size_t count, DataType type,
                                          const cl_ulong vector_size);

    void copy(std::shared_ptr<Buffer>& dst, size_t count,
              size_t src_offset = 0, size_t dst_offset = 0);

    void addEvent(Event event);
    void addEvents(std::vector<Event> events);

    typedef std::function<bool (const Event&)> EventFilterFunction;
    void addEventsToWaitList(boost::compute::wait_list& wait_list,
                             const EventFilterFunction& filter = nullptr) const;

    void addToKernel(size_t index, boost::compute::kernel& kernel);

    Pointer<Device> getDevice() const { return mDevice.lock(); }

    void writeToFile(const std::string& file);
    void writeToFile(std::ofstream& stream);

  private:
    void handleOnEventCompleted(Event event,
                                const std::shared_ptr<Buffer>& this_buffer);
    void addEventIntern(Event event);

    template<DataType DType, DataType DTypeCast = DType>
    void writeToFileTemplate(std::ofstream& stream) {
      std::vector<GET_TYPE(DType)> data;
      data.resize(getCount());
      read(data.data()).wait();

      for (auto itr : data) {
        stream<<static_cast<GET_TYPE(DTypeCast)>(itr)<<std::endl;
      }
    }

    std::pair<unsigned int, unsigned int> mapCountAndOffsetToByte(
        const unsigned int count, const unsigned int offset) const;

    boost::compute::buffer mBuffer;
    WPointer<Device> mDevice;
    std::vector<Event> mEvents;
    std::recursive_mutex mMutex;
    DataType mType;
    //! As we need to align the buffers for some types, the real buffer size
    //! may be bigger
    unsigned int mCount;
    GlobalVectorSize mGlobalVectorSize;
  };

  template<typename Type>
  Future<std::shared_ptr<Type>> Buffer::read(size_t count, size_t offset) const
  {
    auto size_offset = mapCountAndOffsetToByte(count, offset);
    unsigned int size_in_bytes = count > 0 ? size_offset.first :
                                             getSizeInBytes();

    Type* host_ptr = new Type[divisionWithRound(size_in_bytes, sizeof(Type))];

    auto result = readBytes(host_ptr, size_in_bytes, size_offset.second);

    auto returnptr = std::shared_ptr<Type>(host_ptr,
                                           [](Type* del) { delete[] del; });
    return make_Future(returnptr, result);
  }

  template<>
  Future<std::shared_ptr<void>>
  Buffer::read<void>(size_t count, size_t offset) const;

  template<>
  void Buffer::writeToFileTemplate<DTypeBitmap>(std::ofstream& stream);

  using BufferPtr = std::shared_ptr<Buffer>;

} //namespace Ocelot

#endif // BUFFER_H_
