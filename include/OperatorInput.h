#ifndef OPERATORINPUT_H_
#define OPERATORINPUT_H_

#include "Defines.h"
#include "Buffer.h"
#include "Exception.h"
#include "FormatRaw.h"
#include "Profiler.h"

#include <unordered_set>

namespace Ocelot
{
  class Operator;

  class OperatorInput
  {
  public:
    DI_CTOR(OperatorInput, Pointer<Operator> input, unsigned short channel,
                  bool vectorized, const GlobalVectorSize vector_size);

    template<typename Format = Format::Raw>
    BufferPtr getBuffer(Pointer<Device> dev, unsigned short run_index) {
      generateBuffer(run_index);

      auto result = getResult();

      if (mVectorized) {
        return result->template getDeviceDataVector<Format>(dev, run_index);
      } else {
        return result->template getData<Format>(dev);
      }
    }

    template<typename Format>
    bool hasFormat(unsigned short run_index) {
      generateBuffer(run_index);

      return getResult()->hasFormat<Format>();
    }

    unsigned short getMaximumNumberOfRuns();

  private:
    void generateBuffer(unsigned short run_index, bool force_finish = false);
    ColumnPtr getResult();
    bool runAlreadyExecuted(unsigned short run_index);

    Pointer<Operator> pInput;
    unsigned short mChannel;
    bool mVectorized;
    // Saves the indices of the executed runs
    std::unordered_set<unsigned short> mExecutedRuns;
    std::mutex mExecutedRunsMutex;
    bool mFinished = false;
    GlobalVectorSize mVectorSize;
  };

} //namespace Ocelot

#endif // OPERATORINPUT_H_

