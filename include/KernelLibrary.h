#ifndef KERNELLIBRARY_H_
#define KERNELLIBRARY_H_

#include "Defines.h"
#include "Kernel.h"

#include <string>
#include <unordered_map>
#include <mutex>

#include <boost/compute/program.hpp>
#include <boost/compute/kernel.hpp>

namespace Ocelot
{
  class Device;

  class KernelLibrary
  {
  public:
    KernelLibrary(const std::string& dir, Device* device);
    ~KernelLibrary();

    KernelPtr getKernel(const std::string& name, DataType type) {
      assert(type < DataTypeNum);
      return createKernelPtr(createKernel(name, type));
    }

    template<DataType Type>
    KernelPtr createKernel(const std::string& name, const std::string& source) {
      assert(Type < DataTypeNum);
      return createKernelPtr(createKernel(name, source, Type));
    }

  private:
    void init();
    void initPrograms(const std::string& source);
    std::string readAllKernels();
    void exportKernelsForDebugging(const std::string& source);

    boost::compute::kernel createKernel(const std::string& name,
                                        const DataType type);
    boost::compute::kernel createKernel(const std::string& name,
                                        const std::string& source,
                                        const DataType type);

    KernelPtr createKernelPtr(const boost::compute::kernel& kernel);

    std::string getBuildOptions(const DataType type);

    Device* pDevice;
    std::vector<boost::compute::program> mPrograms;
    bool mHasCacheSupport;
    std::vector<std::unordered_map<std::string, boost::compute::kernel>> mCache;
    std::mutex mCacheMutex;
    std::string mKernelDir;
    std::string mKernelSourceDebuggingFileName;
  };

} //namespace Ocelot

#endif // KERNELLIBRARY_H
