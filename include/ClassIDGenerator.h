#ifndef CLASSIDGENERATOR_H_
#define CLASSIDGENERATOR_H_

namespace Ocelot
{

  typedef unsigned long ClassID;

  class ClassIDCounter
  {
  public:
    static ClassID getNextID() {
      static ClassID nextid = 0;
      return nextid++;
    }
  };

  template<typename Type>
  class ClassIDGenerator
  {
  public:
    static const ClassID getID() {
      static ClassID id = ClassIDCounter::getNextID();
      return id;
    }
  };

} //namespace Ocelot

#endif // CLASSIDGENERATOR_H_
