#ifndef DEVICESELECTIONMANAGER_H_
#define DEVICESELECTIONMANAGER_H_

#include "Defines.h"

namespace Ocelot
{
  class Device;
  class DeviceManager;

  class DeviceSelectionManager
  {
  public:
    DeviceSelectionManager(DeviceManager* manager);
    ~DeviceSelectionManager();

    Pointer<Device> getDevice();

  private:
    DeviceManager* pDeviceManager;
  };

}

#endif // DEVICESELECTIONMANAGER_H_

