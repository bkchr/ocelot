#ifndef BITOPERATION_H_
#define BITOPERATION_H_

#include "Defines.h"
#include "Buffer.h"
#include "Device.h"

namespace Ocelot
{
  enum BitOp
  {
    BIT_AND,
    BIT_OR,
    BIT_XOR,
    BIT_NOT
  };

  class BitOperation
  {
  public:
    BitOperation(BitOp op, BufferPtr left, BufferPtr right,
                 bool reuse_left = false, const DevicePtr device = nullptr);
    BitOperation(BitOp op, BufferPtr input, bool reuse_input = false,
                 const DevicePtr device = nullptr);

    BufferPtr getResult() const { return mResult; }

  private:
    void initAndExecute(bool reuse_left);
    void initResult(bool reuse_left);
    void prepareResult();

    void executeNotOperation();
    void executeAndXorOrOperation();

    BitOp mOperation;
    DevicePtr mDevice;
    BufferPtr mLeft;
    BufferPtr mRight;
    BufferPtr mResult;

    const static std::string KernelNames[];
  };

} //namespace Ocelot

#endif // BITOPERATION_H_
