#ifndef INITBUFFER_H
#define INITBUFFER_H

#include "Defines.h"

namespace Ocelot
{
  class Buffer;

  class InitBuffer
  {
  public:
    enum Operation {
      WithZeros,
      WithMax,
      WithMin
    };

    InitBuffer(const Pointer<Buffer>& buffer, const Operation op);

  private:
    void initWithZeros(const Pointer<Buffer>& buffer);
    void initWithMax(const Pointer<Buffer>& buffer);
    void initWithMin(const Pointer<Buffer>& buffer);
  };

} //namespace Ocelot

#endif // INITBUFFER_H

