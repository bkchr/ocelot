#ifndef GROUPEDAGGREGATION_H
#define GROUPEDAGGREGATION_H

#include "Defines.h"

namespace Ocelot
{
  class Buffer;
  class Kernel;
  class Device;
  class ConcurrentContext;

  class GroupedAggregation
  {
  public:
    enum Operation
    {
      Sum = 1,
      Min = 2,
      Max = 4,
      Count = 8,
      SumLong = 16
    };

    GroupedAggregation(const Pointer<Buffer>& input,
                       const Pointer<Buffer>& groups,
                       unsigned int group_count, bool no_nil, Operation op);

    Pointer<Buffer> getResult() const { return mResult; }

  private:
    void run();
    void executeSeqAgg(Pointer<Buffer> tmp_result, unsigned int seq_offset,
                       unsigned int seq_size, unsigned int seq_result_offset,
                       unsigned int tuples_per_thread,
                       ConcurrentContext& context);
    void executeParPart(Pointer<Buffer> tmp_result,
                        unsigned int tuples_per_thread, unsigned int seq_size,
                        unsigned int processors, unsigned int local_size,
                        ConcurrentContext& context);

    bool useLocalMemory();
    unsigned int getPartitionsPerGroup();
    unsigned int getAggregationTableSize();
    unsigned int getAggregationTableCount();
    unsigned int getLocalSize();
    DataType getDataTypeForResultBuffer();
    DataType getDataTypeForTmpBuffers();

    void initBuffer(Pointer<Buffer> buffer);

    void setupSeqAggKernels(unsigned int tuples_per_thread);
    void setupKernels();
    void setupSumKernels();
    void setupSumLongKernels();
    void setupMinKernels();
    void setupMaxKernels();
    void setupCountKernels();
    void setupKernelsGeneral(const std::string& par_agg,
                             const std::string& seq_agg,
                             const std::string& final_agg,
                             const DataType final_agg_type);
    void replaceSumLongSequentialKernel();

    Pointer<Buffer> mInput;
    Pointer<Buffer> mGroups;
    Pointer<Buffer> mResult;

    Operation mOperation;
    unsigned int mGroupCount;
    bool mNoNil;

    Pointer<Kernel> mParAgg;
    Pointer<Kernel> mSeqAgg;
    Pointer<Kernel> mFinalAgg;
    Pointer<Device> mDevice;
  };

} //namespace Ocelot

#endif // GROUPEDAGGREGATION_H

