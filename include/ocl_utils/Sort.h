#ifndef SORT_H_
#define SORT_H_

#include "Buffer.h"
#include "Kernel.h"

namespace Ocelot
{
  class Device;

  class Sort
  {
  public:
    enum Direction : unsigned int
    {
      Descending,
      Ascending
    };

    Sort(const BufferPtr& keys, const BufferPtr& values,
         Direction dir = Ascending, Pointer<Device> dev = nullptr);
    Sort(const BufferPtr& src_keys, const BufferPtr& src_values,
         const BufferPtr& dst_keys, const BufferPtr& dst_values,
         Direction dir = Ascending, Pointer<Device> dev = nullptr);

    std::pair<BufferPtr, BufferPtr> getResult() const {
      return std::make_pair(mDstKeys, mDstValues);
    }

    BufferPtr getDstValues() const {
      return mDstValues;
    }

    BufferPtr getDstKeys() const {
      return mDstKeys;
    }

  private:
    void run(Direction dir, Pointer<Device> dev);

    void setupBuffers(Pointer<Device> dev);
    void executeSingleElementSort();
    void executeSort(Pointer<Device> dev, Direction dir);
    void executeTranspose(Pointer<Device> dev, unsigned int hist_global_size,
                          unsigned int hist_local_size, bool swaprowsandcols);
    void executeSortPasses(Pointer<Device> dev, unsigned int dir, unsigned int radix,
                           unsigned int bits, unsigned int global_size,
                           unsigned int local_size, KernelPtr& histogram_par,
                           KernelPtr& histogram_par_convert,
                           KernelPtr& reorder_par,
                           KernelPtr& reorder_par_convert);
    bool useTranspose(Pointer<Device> dev);
    std::pair<unsigned int, unsigned int> calculateGlobalAndLocalSize(
        Pointer<Device> dev, const unsigned int radix, KernelPtr& histogram_par);
    unsigned int calculateEPTValue(Pointer<Device> dev, unsigned int local_size);

    BufferPtr mSrcValues;
    BufferPtr mSrcKeys;
    BufferPtr mDstValues;
    BufferPtr mDstKeys;
  };

} //namespace Ocelot

#endif // SORT_H_

