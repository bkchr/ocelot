#ifndef GLOBALVECTORSIZEDETERMINATION_H_
#define GLOBALVECTORSIZEDETERMINATION_H_

#include "Defines.h"

#include <map>

namespace Ocelot
{
  class DeviceManager;
  class Device;

  class GlobalVectorSizeDetermination
  {
  public:
    GlobalVectorSizeDetermination(DeviceManager* mgr);

    cl_ulong getGlobalVectorSize() const {
      return mGlobalVectorSize;
    }

  private:
    void init(DeviceManager* mgr);

    cl_ulong getBestVectorSizeForDevice(Pointer<Device> device);
    void alignGlobalVectorSize(const std::vector<Pointer<Device>>& devices);
    cl_ulong getBestVectorSize(const std::map<cl_ulong, double>& size_time_map);

    cl_ulong mGlobalVectorSize = 0;
  };

} //namespace Ocelot

#endif // GLOBALVECTORSIZEDETERMINATION_H_

