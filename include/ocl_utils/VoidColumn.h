#ifndef VOIDCOLUMN_H_
#define VOIDCOLUMN_H_

#include "Defines.h"

namespace Ocelot
{
  class Buffer;
  class Device;

  class VoidColumn
  {
  public:
    VoidColumn(Pointer<Device> device, unsigned int count);

    Pointer<Buffer> getResult();

  private:
    void generate(Pointer<Device> dev, unsigned int count);

    Pointer<Buffer> mResult;
  };

} //namespace Ocelot

#endif // VOIDCOLUMN_H_

