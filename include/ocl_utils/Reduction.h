#ifndef REDUCTION_H_
#define REDUCTION_H_

#include "Defines.h"
#include "Kernel.h"

#include <numeric>

namespace Ocelot
{
  class Device;

  class Reduction
  {
  public:
    enum Operation
    {
      Sum = 1,
      SumLong = 2,
      Min = 4,
      Max = 8,
      Count = 16,
      BitCount = 32
    };

    enum Option : unsigned int
    {
      NoOption = 0,
      CountIgnoreNil = 1
    };

    Reduction(const BufferPtr& input, const BufferPtr& bitmap,
              Operation op, Option opt = NoOption);

    template<typename Type>
    Type getResult() const {
      if (GET_DATATYPE(Type) != mResult->getType()) {
        THROW_EXCEPTION("The requested return type is different to the "
                        "internal buffer type!", " Requested: ",
                        GET_DATATYPE_NAME(GET_DATATYPE(Type)),
                        " internal result type: ",
                        GET_DATATYPE_NAME(mResult->getType()), " . Operation: ",
                        mOperation);
      }

      return getResultInternal<Type>();
    }

    template<DataType Type>
    GET_TYPE(Type) getResult() const {
      return getResult<GET_TYPE(Type)>();
    }

  private:
    //! Checks if we support the operation and input type combination, if not,
    //! an exception is thrown!
    void checkOperation();
    void run();

    void setupResultBuffer(Pointer<Device> dev);
    void setupKernels(Pointer<Device> dev);
    void setupKernelArgs(Pointer<Device> dev, unsigned int local_size);

    template<typename Type>
    Type getResultInternal() const {
      if (mTuplesPerThread == 0) {
        // only load the result from the slow kernel
        return *mResult->read<Type>(1, mResult->getCount() - 1).get().get();
      } else {
        switch (mOperation)
        {
          case Sum:
          case SumLong:
          case BitCount:
          case Count:
            return getSumBitCountOrCountResult<Type>();
          case Min:
            return getMinMaxResult<Type>(true);
          case Max:
            return getMinMaxResult<Type>(false);
        }
      }

      THROW_EXCEPTION("Couldn't get the result!");
    }

    template<typename Type>
    Type getSumBitCountOrCountResult() const {
      auto host_result = getResultAtHost<Type>();
      return std::accumulate(host_result.begin(), host_result.end(),
                             static_cast<Type>(0));
    }

    template<typename Type>
    Type getMinMaxResult(bool min) const {
      auto host_result = getResultAtHost<Type>();

      // if we have slow kernel elements, we use the full buffer, otherwise we
      // ignore the last value of the buffer
      auto end_itr = mSlowKernelElements > 0 ?
                       host_result.end() : host_result.begin() +
                       host_result.size() - 1;

      if (min)
        return *std::min_element(host_result.begin(), end_itr);
      else
        return *std::max_element(host_result.begin(), end_itr);
    }

    template<typename Type>
    std::vector<Type> getResultAtHost() const {
      std::vector<Type> host_result(mResult->getCount());
      mResult->read(host_result.data()).wait();
      return host_result;
    }

    BufferPtr mInput;
    BufferPtr mInputBitmap;
    BufferPtr mResult;
    Operation mOperation;
    Option mOption;
    KernelPtr mSlowKernel = nullptr;
    KernelPtr mFastKernel = nullptr;
    unsigned int mTuplesPerThread = 0;
    unsigned int mSlowKernelElements = 0;
  };

  template<>
  long Reduction::getResult<long>() const;

}

#endif // REDUCTION_H_

