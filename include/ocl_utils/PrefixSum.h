#ifndef _PREFIXSUM_H_
#define _PREFIXSUM_H_

#include "Defines.h"
#include "Buffer.h"
#include "Future.h"
#include "Kernel.h"

#include <atomic>

namespace Ocelot
{
  class Device;

  class PrefixSum
  {
  public:
    PrefixSum(const BufferPtr& buffer);
    PrefixSum(const BufferPtr& buffer, const BufferPtr& resultbuffer);

    unsigned int getSum();
    BufferPtr getResultBuffer() const {
      return mResultBuffer;
    }

  private:
    void run();
    void createTmpBuffer(Pointer<Device> device, unsigned int elements);
    void setupKernels(Pointer<Device> device, bool bitmap);
    void setupLocalAndWorkSize(bool bitmap);
    void executePrefixSum(const BufferPtr& input,
                          const unsigned int input_offset,
                          const unsigned int elements, const BufferPtr& output,
                          const unsigned int output_offset,
                          const BufferPtr& tmp, unsigned int tmp_offset,
                          bool bitmap);

    void executeParallelPart(KernelPtr& par_kernel, KernelPtr& seq_kernel,
                             const BufferPtr& input,
                             const unsigned int input_offset,
                             const unsigned int elements,
                             const BufferPtr& output,
                             const unsigned int output_offset,
                             const BufferPtr& tmp, unsigned int tmp_offset);

    BufferPtr mInputBuffer = nullptr;
    BufferPtr mTmpBuffer = nullptr;
    BufferPtr mResultBuffer = nullptr;
    unsigned int mWorkSize = 0;
    unsigned int mLocalSize = 0;
    KernelPtr mSeqKernel = nullptr;
    KernelPtr mSeqKernelBM = nullptr;
    KernelPtr mParKernel = nullptr;
    KernelPtr mParKernelBM = nullptr;
    KernelPtr mIncKernel = nullptr;
    bool mResultSumRequested;
    unsigned int mResultSum = 0;
    OperationId mOperationId;
  };

} //namespace Ocelot

#endif // _PREFIXSUM_H_

