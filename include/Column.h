#ifndef COLUMN_H_
#define COLUMN_H_

#include "Defines.h"
#include "Buffer.h"
#include "Device.h"
#include "DependencyInjection.h"
#include "CacheManager.h"
#include "FunctionExistsUtil.h"
#include "CacheCostCalculator.h"
#include "FormatConverterIndex.h"
#include "FormatConverter.h"
#include "Exception.h"

#include <boost/thread/shared_mutex.hpp>

namespace Ocelot
{

  namespace Location
  {
    using Device = std::true_type;
    using Host = std::false_type;
  }

  class Column : public std::enable_shared_from_this<Column>
  {
  public:
    Column(DataType type) : mType(type) {}
    ~Column() {}

    static Pointer<Column> createInstance(DataType type) {
      return std::make_shared<Column>(type);
    }

    template<DataType Type>
    static Pointer<Column> createInstance() {
      return createInstance(Type);
    }

    DataType getType() const { return mType; }

    template<typename Format, typename Loc, typename ...Args,
             typename std::enable_if<
               std::is_same<Loc, Location::Device>::value, int>::type = 0>
    auto getData(Pointer<Device> dev, Args&&... args) {
      return getData(dev, std::forward<Args>(args)...);
    }

    template<typename Format, typename Loc, DataType Type, typename ...Args,
             typename std::enable_if<
               std::is_same<Loc, Location::Host>::value, int>::type = 0>
    auto getData(Args&&... args) {
      return getData<Format, Loc, GET_TYPE(Type)>(std::forward<Args>(args)...);
    }

    template<typename Format, typename Loc, typename Type, typename ...Args,
             typename std::enable_if<
               std::is_same<Loc, Location::Host>::value, int>::type = 0>
    auto getData(Args&&... args) {
      auto format = getFormat<Format>();

      if (!format) {
        format = storeFormatConverter<Format>();
      }

      return format->template getHostData<Type>(std::forward<Args>(args)...);
    }

    template<typename Format, typename ...Args>
    auto getData(Pointer<Device> dev, Args&&... args) {
      auto format = getFormat<Format>();

      if (!format) {
        format = storeFormatConverter<Format>();
      }

      return format->getDeviceData(dev, std::forward<Args>(args)...);
    }

    template<typename Format, typename ...Args>
    void registerData(Args&&... args) {
      storeFormat<Format>(true, std::forward<Args>(args)...);
    }

    OCL_FUNCTION_EXISTS_STRUCTURE(registerDeviceDataVector);

    template<typename Format, typename ...Args>
    void registerDeviceDataVector(
        const BufferPtr& ptr, const CacheCost::CalculatorPtr& cost,
        unsigned short index, Args&&... args) {
      static_assert(OCL_FUNCTION_EXISTS(Format, registerDeviceDataVector),
                    "Format doesn't support registerDeviceDataVector!");

      auto format = storeFormat<Format>(false);
      format->getFormat()->registerDeviceDataVector(
            ptr, cost, index, std::forward<Args>(args)...);
    }

    OCL_FUNCTION_EXISTS_STRUCTURE(getDeviceDataVector);

    template<typename Format, typename ...Args>
    BufferPtr getDeviceDataVector(
        Pointer<Device> dev, unsigned short index, Args&&... args) {
      static_assert(OCL_FUNCTION_EXISTS(Format, getDeviceDataVector),
                    "Format doesn't support getDeviceDataVector!");

      auto format = getFormat<Format>();

      if (!format) {
        format = storeFormatConverter<Format>();
      }

      return format->getDeviceDataVector(dev, index,
                                         std::forward<Args>(args)...);
    }

    template<typename Format>
    bool hasFormat() {
      return getFormat<Format>() != nullptr;
    }

    cl_uint getCount() {
      boost::shared_lock<boost::shared_mutex> lock(mMutex);
      assert(!mFormats.empty());
      // every format should now the size of it's column

      for (const auto& format : mFormats) {
        if (format.second->hasFormat()) {
          return format.second->getCount();
        }
      }

      THROW_EXCEPTION("Something went wrong!");
    }

    bool isSorted() const { return mSorted; }
    void setSorted(const bool sorted) { mSorted = sorted; }

    bool isRevSorted() const { return mRevSorted; }
    void setRevSorted(const bool revsorted) { mRevSorted = revsorted; }

    bool isDense() const { return mDense; }
    void setDense(const bool dense) { mDense = dense; }

  private:
    bool mSorted = false;
    bool mRevSorted = false;
    bool mDense = false;

    class FormatStore
    {
    public:
      virtual ~FormatStore() {}
      virtual bool hasFormat() const = 0;
      virtual cl_uint getCount() const = 0;
    };

    template<typename FormatType>
    class FormatStoreImpl : public FormatStore
    {
    public:
      FormatStoreImpl(const Pointer<FormatType>& format) : mFormat(format) {}
      FormatStoreImpl(const Pointer<Format::Converter>& conv) :
        mConverter(conv),
        mHostDataConverted(false),
        mDeviceDataConverted(false) {}

      auto getFormat() {
        return mFormat;
      }

      bool hasFormat() const override final {
        return mFormat != nullptr;
      }

      void setFormat(const Pointer<FormatType>& format) {
        mFormat = format;
      }

      template<typename ...Args>
      auto getDeviceData(Pointer<Device> dev, Args&&... args) {
        checkAndConvert([this, &dev]() {
          mConverter->convertDeviceData(dev);
          mDeviceDataConverted = true;
        });

        return mFormat->getDeviceData(dev, std::forward<Args>(args)...);
      }

      template<typename Type, typename ...Args>
      auto getHostData(Args&&... args) {
        checkAndConvert([this]() {
          mConverter->convertHostData();
          mHostDataConverted = true;
        });

        return mFormat->template getHostData<Type>(std::forward<Args>(args)...);
      }

      template<typename ...Args>
      auto getDeviceDataVector(
          Pointer<Device> dev, unsigned short index, Args&&... args) {
        checkAndConvert([this, &dev, index]() {
          if (std::find(mConvertedIndices.begin(), mConvertedIndices.end(),
                        index) != mConvertedIndices.end()) {
            return;
          }

          mConverter->convertDeviceDataVector(dev, index);
          mConvertedIndices.push_back(index);
        });

        return mFormat->getDeviceDataVector(dev, index,
                                            std::forward<Args>(args)...);
      }

      cl_uint getCount() const override final {
        return mFormat->getCount();
      }

    private:
      template<typename Function>
      void checkAndConvert(const Function& func) {
        if (mConverter != nullptr && !mHostDataConverted &&
            !mDeviceDataConverted) {
          std::lock_guard<std::recursive_mutex> lock(mConvertMutex);

          if (mHostDataConverted || mDeviceDataConverted) {
            return;
          }

          func();
        }
      }

      Pointer<Format::Converter> mConverter = nullptr;
      Pointer<FormatType> mFormat;
      std::atomic_bool mHostDataConverted;
      std::atomic_bool mDeviceDataConverted;
      std::vector<unsigned short> mConvertedIndices;
      std::recursive_mutex mConvertMutex;
    };

    template<typename Format>
    Pointer<FormatStoreImpl<Format>> getFormat() {
      boost::shared_lock<boost::shared_mutex> lock(mMutex);
      return getFormatInternal<Format>();
    }

    template<typename Format>
    Pointer<FormatStoreImpl<Format>> getFormatInternal() {
      auto find = mFormats.find(ClassIDGenerator<Format>::getID());

      if (find != mFormats.end()) {
        return std::static_pointer_cast<
            FormatStoreImpl<Format>>(find->second);
      }

      return nullptr;
    }

    template<typename Format, typename ...Args>
    auto storeFormat(bool errorifexist, Args&&... args) {
      boost::unique_lock<boost::shared_mutex> lock(mMutex);

      auto store = getFormatInternal<Format>();
      bool stored = store != nullptr;

      if (stored && store->hasFormat()) {
        if (errorifexist) {
          THROW_EXCEPTION("Format already registered!");
        } else {
          return store;
        }
      }

      auto format = std::make_shared<Format>(shared_from_this(), mType,
                                             std::forward<Args>(args)...);

      if (stored) {
        store->setFormat(format);
      } else {
        store = std::make_shared<FormatStoreImpl<Format>>(format);

        mFormats.insert(std::make_pair(ClassIDGenerator<Format>::getID(),
                                       store));
      }

      return store;
    }

    template<typename Format>
    auto storeFormatConverter() {
      boost::unique_lock<boost::shared_mutex> lock(mMutex);

      auto converter = createFormatConverter<Format>();

      if (converter == nullptr) {
        THROW_EXCEPTION("No known converter for format ",
                        ClassIDGenerator<Format>::getID(), " found");
      }

      auto store = std::make_shared<FormatStoreImpl<Format>>(converter);

      mFormats.insert(std::make_pair(ClassIDGenerator<Format>::getID(), store));
      return store;
    }

    template<typename FormatType>
    Pointer<Format::Converter> createFormatConverter() {
      auto id = ClassIDGenerator<FormatType>::getID();

      for (auto format : mFormats) {
        auto converter = Format::ConverterIndex::getConverter(
                           shared_from_this(), format.first, id);

        if (converter != nullptr)
          return converter;
      }

      return nullptr;
    }

    std::unordered_map<ClassID, Pointer<FormatStore>> mFormats;
    boost::shared_mutex mMutex;
    DataType mType;
  };

  using ColumnPtr = Pointer<Column>;
}

#endif // COLUMN_H_
