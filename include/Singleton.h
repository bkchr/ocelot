#ifndef SINGLETON_H_
#define SINGLETON_H_

namespace Ocelot
{

  template<typename Type>
  class Singleton
  {
  public:
    static Type& getSingleton() {
      return* getSingletonPtr();
    }

    static Type* getSingletonPtr() {
      if (pInstance == nullptr)
        pInstance = new Type();

      return pInstance;
    }

    static void deleteSingletonPtr() {
      delete pInstance;
      pInstance = nullptr;
    }

  protected:
    Singleton() {}

  private:
    Singleton(Singleton&) = delete;

    static Type* pInstance;
  };

  template<typename Type>
  Type* Singleton<Type>::pInstance = nullptr;

} //namespace Ocelot

#endif // SINGLETON_H_
