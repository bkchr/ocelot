#ifndef OPERATOR_H
#define OPERATOR_H

#include "Defines.h"
#include "Column.h"
#include "FormatRaw.h"
#include "OperatorInput.h"
#include "DependencyInjection.h"
#include "CacheCostCalculator.h"
#include "OperatorInput.h"

#include <map>
#include <mutex>
#include <atomic>
#include <memory>

namespace Ocelot
{
  class Device;
  class DeviceSelectionManager;

  class Operator
  {
  public:
    Operator(DeviceSelectionManager* dsmgr);

    virtual void run(Pointer<Device> device) = 0;

    virtual bool doesInputChannelSupportVectorizedData(
        unsigned short channel) const = 0;
    virtual unsigned short getMaximumNumberOfRuns() = 0;
    virtual std::string getName() = 0;

    ColumnPtr getResult(unsigned short channel) const {
      assert(mOutputChannels.find(channel) != mOutputChannels.end());

      return mOutputChannels.find(channel)->second;
    }

    void next();
    void finish();

    void registerParent(Pointer<Operator> op, unsigned short inchannel,
                        unsigned short outchannel);

    bool isWorkingOnVectorizedData() const;

  protected:
    template<typename Format = Format::Raw>
    BufferPtr getInputChannel(Pointer<Device> dev, unsigned short channel) {
      if (!hasInputChannel(channel)) {
        THROW_EXCEPTION("Inputchannel ", channel, " for operator ", getName(),
                        " doesn't exist!");
      }

      return mInputChannels[channel]->
          template getBuffer<Format>(dev, getCurrentRunIndex());
    }

    bool hasInputChannel(unsigned short channel) {
      return mInputChannels.find(channel) != mInputChannels.end();
    }

    template<typename Format>
    bool hasInputChannelFormat(unsigned short channel) {
      if (!hasInputChannel(channel)) {
        THROW_EXCEPTION("Inputchannel ", channel, " for operator ", getName(),
                        " doesn't exist!");
      }

      return mInputChannels[channel]->
          template hasFormat<Format>(getCurrentRunIndex());
    }

    unsigned short getInputChannelMaximumNumberOfRuns(unsigned short channel);

    template<typename Format = Format::Raw>
    void addOutputChannelVectorized(const BufferPtr& ptr,
                                    unsigned short channel,
                                    DataType type) {
      auto cost = CacheCost::CalculatorPtr(
                    new CacheCost::Operation(mOperationId,
                                             PI_All_Runtimes_Excluding_Nested));

      getOutputChannelColumn(type, channel)->
          template registerDeviceDataVector<Format>(ptr, cost,
                                                    getCurrentRunIndex());
    }

    template<typename Format = Format::Raw>
    void addOutputChannel(const BufferPtr& ptr, unsigned short channel,
                          DataType type) {
      assert(mOutputChannels.find(channel) == mOutputChannels.end());

      auto cost = std::make_shared<CacheCost::Operation>(
                    mOperationId, PI_All_Runtimes_Excluding_Nested);

      getOutputChannelColumn(type, channel)->registerData<Format>(ptr, cost);
    }

    void addOutputChannel(const ColumnPtr& ptr, unsigned short channel) {
      std::lock_guard<std::mutex> lock(mOutputChannelMutex);

      assert(mOutputChannels.find(channel) == mOutputChannels.end());
      mOutputChannels[channel] = ptr;
    }

    unsigned short getCurrentRunIndex() const { return mCurrentRunIndex; }

  private:
    ColumnPtr getOutputChannelColumn(DataType type, unsigned short channel) {
      std::lock_guard<std::mutex> lock(mOutputChannelMutex);

      auto find = mOutputChannels.find(channel);

      if (find != mOutputChannels.end())
        return find->second;

      auto col = Column::createInstance(type);
      mOutputChannels[channel] = col;
      return col;
    }

    void startRun();

    std::map<unsigned short, Pointer<OperatorInput>> mInputChannels;
    std::map<unsigned short, ColumnPtr> mOutputChannels;
    DeviceSelectionManager* pDeviceSelectionManager;
    static thread_local unsigned short mCurrentRunIndex;
    std::mutex mFinishMutex;
    std::mutex mOutputChannelMutex;
    std::atomic<unsigned short> mNumberOfRuns;
    OperationId mOperationId;
    bool mWorkingOnVectorizedData = false;
    bool mFinished = false;
  };

  using OperatorPtr = Pointer<Operator>;

}	//namespace Ocelot

#endif // OPERATOR_H
