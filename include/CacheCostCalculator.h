#ifndef CACHECOSTCALCULATOR_H_
#define CACHECOSTCALCULATOR_H_

#include "Defines.h"

#include "Profiler.h"

#include <memory>
#include <atomic>

namespace Ocelot
{
  class CacheManager;
  
  namespace CacheCost
  {
    
    class Calculator
    {
    public:
      virtual ~Calculator() {}
      virtual unsigned long getCost() = 0;
      virtual void setCacheManager(CacheManager* cmgr) {}
    };
    
    typedef std::shared_ptr<Calculator> CalculatorPtr;
    
    class Direct : public Calculator
    {
    public:
      Direct(unsigned long cost) : mCost(cost) {}
      
      unsigned long getCost() override final { return mCost; }

      static CalculatorPtr createInstance(unsigned long cost) {
        return CalculatorPtr(new Direct(cost));
      }
      
    private:
      unsigned long mCost;
    };
    
    class Operation : public Calculator
    {
    public:
      Operation(OperationId id, ProfilingInformation flags = PI_All_Runtimes);
      
      unsigned long getCost() override final { return mCost; }

      static CalculatorPtr createInstance(
          OperationId id, ProfilingInformation flags = PI_All_Runtimes) {
        return CalculatorPtr(new Operation(id, flags));
      }
      
    private:
      std::atomic<unsigned long> mCost;
    };
    
    class Aggregation : public Calculator
    {
    public:
      Aggregation(const std::vector<ResourceId>& costids);

      unsigned long getCost() override final;
      void setCacheManager(CacheManager* cmgr) override final;

    private:
      std::vector<CalculatorPtr> mCosts;
      std::vector<ResourceId> mCostIds;
    };
  
  } //namespace CacheCost
  
} //namespace Ocelot

#endif // CACHECOSTCALCULATOR_H_

