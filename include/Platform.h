#ifndef PLATFORM_H
#define PLATFORM_H

#include "Vendor.h"

#include <boost/compute/platform.hpp>
#include <boost/compute/context.hpp>

#include <string>
#include <vector>

namespace Ocelot
{
  class Device;
  class CacheManager;

  class Platform
  {
  public:
    Platform(const boost::compute::platform& platform);

    Vendor getVendor() const { return mVendor; }
    std::string getName() const { return mPlatform.name(); }
    boost::compute::context getContext() const { return mContext; }
    cl_platform_id getId() const { return mPlatform.id(); }

    void createOpenCLContext(
        const std::vector<boost::compute::device> &devices);

    struct AvailableDevice
    {
      boost::compute::device sDevice;
      bool sNativePlatform;

      AvailableDevice(const boost::compute::device& device, bool native) :
        sDevice(device),
        sNativePlatform(native) {}
    };

    std::vector<AvailableDevice> getAvailableDevices();

  private:
    void init();

    boost::compute::platform mPlatform;
    Vendor mVendor;
    boost::compute::context mContext;
  };

} //namespace Ocelot

#endif // PLATFORM_H
