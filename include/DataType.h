#ifndef DATATYPE_H_
#define DATATYPE_H_

// size_t
#include <cstddef>
#include <assert.h>
#include <type_traits>
#include <string>

namespace Ocelot
{

  typedef unsigned int DataType;

  enum : DataType
  {
    DTypeS8,
    DTypeS16,
    DTypeS32,
    DTypeS64,
    DTypeBitmap,
    DTypeF32,
    DataTypeNum
  };

  template<typename Type>
  struct DataTypeConverter
  {};

  template<DataType Type>
  struct DataTypeOpenClMapping
  {};

  template<DataType Type>
  struct TypeConverter
  {};

  #define CREATE_DATATYPE_CONVERTER_EXTENDED(type, dtype, ocl_type,       \
                                             count_per_element)           \
    template<>                                                            \
    struct DataTypeOpenClMapping<dtype>                                   \
    {                                                                     \
      static DataType getMapping() { return ocl_type; }                   \
    };                                                                    \
                                                                          \
    template<>                                                            \
    struct DataTypeConverter<type>                                        \
    {                                                                     \
      static DataType getDataType() { return dtype; }                     \
    };                                                                    \
                                                                          \
    template<>                                                            \
    struct TypeConverter<dtype>                                           \
    {                                                                     \
      typedef type Type;                                                  \
      static std::string getName() { return #dtype; }                     \
      static unsigned int getCountPerElement() {                          \
        return count_per_element;                                         \
      }                                                                   \
    }

  #define CREATE_DATATYPE_CONVERTER(type, dtype)                          \
    CREATE_DATATYPE_CONVERTER_EXTENDED(type, dtype, dtype, 1)

  #define GET_DATATYPE(type)                                              \
    Ocelot::DataTypeConverter<type>::getDataType()

  #define GET_TYPE(etype)                                                 \
    typename Ocelot::TypeConverter<etype>::Type

  #define GET_TYPE_SIZE(dtype)                                            \
    Ocelot::TypeInformation::getTypeSize(dtype)

  #define GET_DATATYPE_OPENCL_MAPPING(dtype)                              \
    Ocelot::TypeInformation::getOpenCLTypeMapping(dtype)

  #define GET_DATATYPE_NAME(dtype)                                        \
    Ocelot::TypeInformation::getTypeName(dtype)

  //! Returns the number of elements per element, this functions is
  //! mainly needed for DTypeBitmap, because DTypeBitmap is using a
  //! unsigned int, but can store 32 individual values
  #define GET_COUNT_PER_ELEMENT(dtype)                                    \
    Ocelot::TypeInformation::getCountPerElement(dtype)

  class TypeInformation
  {
    struct TypeInfo
    {
      size_t sSizeInBytes;
      DataType sOpenClType;
      std::string sName;
      unsigned int sCountPerElement;
    };

  public:
    static size_t getTypeSize(const DataType type) {
      assert(type < DataTypeNum);
      return getTypeInfoArray()[type].sSizeInBytes;
    }

    static DataType getOpenCLTypeMapping(const DataType type) {
      assert(type < DataTypeNum);
      return getTypeInfoArray()[type].sOpenClType;
    }

    static const std::string& getTypeName(const DataType type) {
      assert(type < DataTypeNum);
      return getTypeInfoArray()[type].sName;
    }

    static unsigned int getCountPerElement(const DataType type) {
      assert(type < DataTypeNum);
      return getTypeInfoArray()[type].sCountPerElement;
    }

  private:
    static TypeInfo* getTypeInfoArray() {
      static TypeInfo typeinfos[DataTypeNum] = { { } };
      return typeinfos;
    }

    template<DataType T, DataType U>
    struct is_same : std::false_type {};

    template<DataType T>
    struct is_same<T, T> : std::true_type {};

    template<DataType Type>
    static typename std::enable_if<
    !is_same<Type, DataTypeNum>::value, void>::type
    initTypeSizeArray() {
      getTypeInfoArray()[Type].sSizeInBytes = sizeof(GET_TYPE(Type));
      getTypeInfoArray()[Type].sOpenClType =
          DataTypeOpenClMapping<Type>::getMapping();
      getTypeInfoArray()[Type].sName =
          TypeConverter<Type>::getName();
      getTypeInfoArray()[Type].sCountPerElement =
          TypeConverter<Type>::getCountPerElement();
      initTypeSizeArray<Type + 1>();
    }

    template<DataType Type>
    static typename std::enable_if<
    is_same<Type, DataTypeNum>::value, void>::type
    initTypeSizeArray() {}

    struct Initializer
    {
      Initializer() { initTypeSizeArray<0>(); }
    };

    static Initializer mStaticInitializer;
  };

  CREATE_DATATYPE_CONVERTER(char, DTypeS8);
  CREATE_DATATYPE_CONVERTER(short, DTypeS16);
  CREATE_DATATYPE_CONVERTER(int, DTypeS32);
  CREATE_DATATYPE_CONVERTER(float, DTypeF32);
  CREATE_DATATYPE_CONVERTER(long, DTypeS64);
  CREATE_DATATYPE_CONVERTER_EXTENDED(unsigned int, DTypeBitmap, DTypeS32, 32);

} //namespace Ocelot

#endif // DATATYPE_H_
