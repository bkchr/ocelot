#ifndef CACHE_H_
#define CACHE_H_

#include "Defines.h"
#include "Buffer.h"

#include <unordered_map>
#include <memory>
#include <atomic>

#include <boost/thread/shared_mutex.hpp>

namespace Ocelot
{
  class Device;

  namespace CacheCost
  {
    class Calculator;
    typedef std::shared_ptr<Calculator> CalculatorPtr;
  }

  class Cache
  {
  public:
    Cache(Device* device);
    ~Cache();

    void registerBuffer(const BufferPtr& ptr, const ResourceId id,
                        const CacheCost::CalculatorPtr& cost);
    void unregisterBuffer(const ResourceId id);

    BufferPtr getBuffer(const ResourceId id);
    bool hasBuffer(const ResourceId id);
    CacheCost::CalculatorPtr getBufferCost(const ResourceId id);

    BufferPtr transferBuffer(const ResourceId id, Cache* dst_cache);

    bool freeUpSpace(size_t required_space);

  private:
    size_t searchAndFreeUpBuffer();

    struct BufferInfo
    {
      BufferPtr sBuffer;
      std::time_t sLastAccess;
      CacheCost::CalculatorPtr sCost;

      BufferInfo(const BufferPtr& ptr, const CacheCost::CalculatorPtr& cost) :
        sBuffer(ptr),
        sLastAccess(std::time(nullptr)),
        sCost(cost) {}
    };

    std::unordered_map<ResourceId, std::shared_ptr<BufferInfo>> mBuffers;
    Device* pDevice;
    boost::shared_mutex mMutex;
  };

} //namespace Ocelot

#endif // CACHE_H_
