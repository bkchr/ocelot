#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include "Defines.h"
#include "Buffer.h"

#include <boost/compute/context.hpp>
#include <boost/compute/device.hpp>
#include <boost/compute/command_queue.hpp>

namespace Ocelot
{
  class Device;

  class Scheduler
  {
  public:
    Scheduler(const boost::compute::context& context, Device* dev);
    ~Scheduler();

    Future<void> writeBuffer(
        const boost::compute::buffer& buffer, const void* host_ptr, size_t size,
        size_t offset, bool event_profiling = true);

    Future<void*> readBuffer(
        const boost::compute::buffer& buffer, void* host_ptr, size_t size,
        size_t offset, const boost::compute::wait_list& wait_list);

    BufferPtr cloneBuffer(
        const Buffer* src_buffer, Pointer<Device> dst_device);

    void copyBuffer(const Buffer* src, Buffer* dst, size_t src_offset,
                    size_t dst_offset, size_t size);

    Future<void> executeKernel(
        const boost::compute::kernel& kernel, size_t global_size,
        size_t local_size, const boost::compute::wait_list& wait_list);

    bool testBufferCreation(const boost::compute::buffer& buffer);

    std::pair<Event, Event> createMarkerEvents();

  private:
    void init();
    void deinit();

    BufferPtr cloneBufferDirect(const Buffer* src_buffer,
                                Pointer<Device> dst_device);

    BufferPtr cloneBufferInDirect(const Buffer* src_buffer,
                                  Pointer<Device> dst_device);

    void copyBufferDirect(const Buffer* src, Buffer* dst, size_t src_offset,
                          size_t dst_offset, size_t size);

    void copyBufferInDirect(const Buffer* src, Buffer* dst, size_t src_offset,
                            size_t dst_offset, size_t size);

    boost::compute::context mContext;
    Device* pDevice;
    boost::compute::command_queue mTransferQueue;
    boost::compute::command_queue mCommandQueue;
    std::recursive_mutex mMutex;
  };

} //namespace Ocelot

#endif // SCHEDULER_H_
