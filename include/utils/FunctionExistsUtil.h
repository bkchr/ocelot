#ifndef FUNCTIONEXISTSUTIL_H_
#define FUNCTIONEXISTSUTIL_H_

namespace Ocelot
{

  #define OCL_FUNCTION_EXISTS_STRUCTURE(name) \
    template<typename Class>                                            \
    struct FunctionExistsUtil_##name                                    \
    {                                                                   \
    private:                                                            \
      typedef char yes[1];                                              \
      typedef char no[2];                                               \
                                                                        \
      template<typename T>                                              \
      static yes &test(decltype(&T::name));                             \
      template<typename T>                                              \
      static no &test(...);                                             \
                                                                        \
    public:                                                             \
      const static bool value = sizeof(test<Class>(0)) == sizeof(yes);  \
    }

  #define OCL_FUNCTION_EXISTS(class_, name)                             \
    FunctionExistsUtil_##name<class_>::value

} //namespace Ocelot

#endif // FUNCTIONEXISTSUTIL_H_
