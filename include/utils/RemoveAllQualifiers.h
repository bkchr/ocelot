#ifndef REMOVEALLQUALIFIERS_H_
#define REMOVEALLQUALIFIERS_H_

namespace Ocelot
{

  template<typename InputType>
  struct remove_all_qualifiers
  {
  public:
    typedef typename std::remove_reference<typename std::remove_pointer<
    typename std::decay<InputType>::type>::type>::type type;
  };

}

#endif // REMOVEALLQUALIFIERS_H_
