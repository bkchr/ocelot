#ifndef TYPEDEFEXISTSUTIL_H_
#define TYPEDEFEXISTSUTIL_H_

namespace Ocelot
{

  #define OCL_TYPEDEF_EXISTS_STRUCTURE(name)                            \
    template<typename Class>                                            \
    struct TypedefExistsUtil_##name                                     \
    {                                                                   \
    private:                                                            \
      typedef char yes[1];                                              \
      typedef char no[2];                                               \
                                                                        \
      template<typename T>                                              \
      static yes &test(typename T::name*);                              \
      template<typename T>                                              \
      static no &test(...);                                             \
                                                                        \
    public:                                                             \
      const static bool value = sizeof(test<Class>(0)) == sizeof(yes);  \
    }

  #define OCL_TYPEDEF_EXISTS(class_, name)                              \
    TypedefExistsUtil_##name<class_>::value

} //namespace Ocelot

#endif // TYPEDEFEXISTSUTIL_H_

