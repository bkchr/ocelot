#ifndef PROFILER_H_
#define PROFILER_H_

#include "Defines.h"
#include "Singleton.h"
#include "OperationIndex.h"
#include "Event.h"

#include <map>
#include <mutex>
#include <atomic>
#include <functional>
#include <unordered_set>
#include <condition_variable>

#include <boost/compute/command_queue.hpp>

namespace Ocelot
{
  class Device;

  enum ProfilingInformation
  {
    PI_Host_Runtime = 1,
    PI_Host_Stalled = 2,
    PI_Kernel_Runtime = 4,
    PI_Input_Transfer_Time = 8,
    PI_Result_Transfer_Time = 16,
    PI_Device_Transfer_Time = 32,
    PI_Internal_Transfer_Time = 64,
    PI_All_Runtimes_Excluding_Nested = 127,
    PI_Nested_Operations_Runtime = 128,
    PI_All_Runtimes = 255
  };

  class Profiler : public Singleton<Profiler>
  {
  public:
    unsigned long getProfilingInformation(const OperationId id,
                                          const int request);

    typedef std::function<void(unsigned long)> Callback;
    void registerCallback(const OperationId id, const int flags,
                          const Callback& callback);

    template<typename Function, typename ...Payloads>
    void registerCallback(const OperationId id, const int flags,
                          const Function& function, Payloads&&... payloads) {
      registerCallback(id, flags,
                       Callback(
                         std::bind(function, std::placeholders::_1,
                                   std::forward<Payloads>(payloads)...)));
    }

    template<typename Class, typename Function, typename ...Payloads>
    void registerCallback(const OperationId id, const int flags,
                          Class* classptr, const Function& function,
                          Payloads&&... payloads) {
      registerCallback(id, flags,
                       Callback(
                         std::bind(function, classptr, std::placeholders::_1,
                                   std::forward(payloads)...)));
    }

    void registerEventForProfiling(
        Event& event, OperationId id,
        const ProfilingInformation measurement_flags, Pointer<Device> dev);
    void registerHostTime(OperationId id, unsigned long time);
    void registerStallTime(OperationId id, unsigned long time);
    void registerNestedOperationAtParent(OperationId child, OperationId parent);
    void registerNameForOperation(OperationId id, const std::string& name);

  private:
    struct Record
    {
      Record();

      typedef std::unordered_map<OperationId,
                                 std::atomic<unsigned long>> TTimeMap;

      // Time spent productively within host code.
      TTimeMap sHostRuntime;
      // Time spent stalling within host code.
      TTimeMap sHostStallTime;
      // Total runtime of the kernels for this operator.
      TTimeMap sKernelRuntime;
      // Total runtime of transfers to the device for this operator.
      TTimeMap sInputTransferTime;
      // Total runtime of transfers from the device for this operator.
      TTimeMap sResultTransferTime;
      // Total runtime of transfers from device to device
      TTimeMap sDeviceTransferTime;
      // Total runtime of transfers on the device for this operator.
      TTimeMap sInternalTransferTime;

      std::recursive_mutex sLock;
      std::vector<Pointer<Device>> sProfiledDevices;
      std::vector<Event> sProfiledEvents;
      std::vector<std::pair<int, Callback>> sCallbacks;
      std::unordered_set<OperationId> sNestedOperations;
      std::atomic_bool sHostRuntimeRegistered;
      std::condition_variable_any sFinishedConditionVar;
      std::atomic_bool sFinished;
      std::string sName;
      OperationId sId;
    };

    Record& getRecord(const OperationId id);
    void handleOnProfiledEventCompleted(
        Event event, const OperationId id,
        const ProfilingInformation flags, const OperationId parent_id);
    void removeCompletedEventFromRecord(Event& event, Record& record);

    unsigned long extractProfilingInformation(
        Record& record, const int flags,
        const OperationId parent_id = Invalid_OperationId);
    unsigned long extractProfilingInformationForNestedOps(
        Record& record, const int flags);
    unsigned long extractTiming(const Record::TTimeMap& times,
                                const OperationId parent_id);
    unsigned long getProfilingInformation(const OperationId id,
                                          const int request,
                                          const OperationId parent_id);

    void finishRecord(Record& record);

    std::map<OperationId, Record> mRecords;
    std::mutex mRecordsMutex;
  };

  class ProfilerScope
  {
  public:
    enum Type {
      HostScope,
      StallScope
    };

    ProfilerScope(const OperationId id, const Type type);
    ~ProfilerScope();

  private:
    OperationId mId;
    Type mType;
    std::chrono::high_resolution_clock::time_point mStartTime;
  };

  #define OCL_PROFILER_EVENT_REGISTER(event, measurement_flags, device)        \
            Ocelot::Profiler::getSingleton().registerEventForProfiling(        \
              event, Ocelot::OperationIndex::getCurrentOperationId(),          \
              measurement_flags, device)

  #define OCL_PROFILE_HOST_SCOPE() Ocelot::ProfilerScope                       \
            profhostscope(Ocelot::OperationIndex::getCurrentOperationId(),     \
              Ocelot::ProfilerScope::HostScope)

  #define OCL_PROFILE_STALL_SCOPE() Ocelot::ProfilerScope                      \
            profstallscope(Ocelot::OperationIndex::getCurrentOperationId(),    \
              Ocelot::ProfilerScope::StallScope)

  #define OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(id, name)                      \
          OCL_OPERATION_SCOPE(id, name);                                       \
          Ocelot::Profiler::getSingleton().                                    \
                   registerNameForOperation(OCL_CURRENT_OPERATION_ID(), name); \
          OCL_PROFILE_HOST_SCOPE()

  #define OCL_PROFILE_OPERATION_SCOPE(name)                                    \
            OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(OCL_GENERATE_OPERATION_ID(), \
                                                   name)

  #define OCL_PROFILE(request, code)                                           \
          [&]() {                                                              \
            OCL_OPERATION_SCOPE(OCL_GENERATE_OPERATION_ID(), #code);           \
            {                                                                  \
              OCL_PROFILE_HOST_SCOPE();                                        \
              code                                                             \
            };                                                                 \
            return Ocelot::Profiler::getSingleton().getProfilingInformation(   \
              OCL_CURRENT_OPERATION_ID(), request);                            \
          }()

} //namespace Ocelot

#endif // PROFILER_H_
