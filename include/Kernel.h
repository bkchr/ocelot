#ifndef KERNEL_H_
#define KERNEL_H_

#include "Defines.h"

#include "Buffer.h"
#include "ConcurrentContext.h"

#include <boost/compute/kernel.hpp>

namespace Ocelot
{
  class Device;

  #define SPECIAL_KERNEL_ARG_TYPE(name)                   \
    struct name                                           \
    {                                                     \
      name(const BufferPtr& ptr) : sBuffer(ptr) {}        \
                                                          \
      BufferPtr sBuffer;                                  \
    }

  SPECIAL_KERNEL_ARG_TYPE(in);
  SPECIAL_KERNEL_ARG_TYPE(out);
  SPECIAL_KERNEL_ARG_TYPE(inout);

  class LocalMemory
  {
  public:
    LocalMemory(DataType type, const size_t count);

    size_t getSize() const;

  private:
    size_t mCount;
    DataType mType;
  };

  class Kernel
  {
  public:
    Kernel(const boost::compute::kernel& kernel, Pointer<Device> device);

    void setArg(size_t index, const in& value);
    void setArg(size_t index, const out& value);
    void setArg(size_t index, const inout& value);
    void setArg(size_t index, const LocalMemory& value);
    void setArg(size_t index, const bool& value);

    template<typename T>
    void setArg(size_t index, const T& value) {
      static_assert(!std::is_same<T, BufferPtr>::value,
                    "No direct setArg() for BufferPtr, please specify if"
                    " the buffer is only an input(in class), "
                    "an output(out class) or an "
                    "in- and output(inout class). To specify the type, "
                    "use the classes marked in the brackets.");

      mKernel.set_arg(index, value);
    }

    template<typename... Args>
    void setArgs(Args&&... args) {
      setArgsInternal<0>(std::forward<Args>(args)...);
    }

    Future<void> execute(size_t global_size, size_t local_size,
                         ConcurrentContext& context);

    Future<void> execute(size_t global_size, size_t local_size = 0) {
      ConcurrentContext context;
      return execute(global_size, local_size, context);
    }

    Future<void> execute() {
      return execute(getGlobalSize(), getLocalSize());
    }

    unsigned int getWorkGroupSize();

    static unsigned int getBestWorkGroupSize(
        const std::vector<std::shared_ptr<Kernel>>& kernels);

    unsigned int getLocalMemSize();
    /**
     * @brief Calculates the amount of tuples per thread.
     * The implementation uses the maximum number of compute devices of the
     * device, the workgroup size and the number of tuples to generate the
     * number of tuples per thread. The implementation should work in the most
     * cases.
     * @param tuples The number of tuples to process.
     */
    unsigned int getTuplesPerThread(unsigned int tuples);
    /**
     * @brief Returns the global size for the execute call.
     * Simple implementation which relies on the work group size and the maximum
     * number of compute devices of the device. This global size should be
     * sufficient for the most cases.
     */
    unsigned int getGlobalSize();
    // Syntactic sugar
    unsigned int getLocalSize() { return getWorkGroupSize(); }

  private:
    template<size_t N, typename Arg, typename... Args>
    void setArgsInternal(Arg&& arg, Args&&... args) {
      setArgWithTry(N, std::forward<Arg>(arg));
      setArgsInternal<N + 1>(std::forward<Args>(args)...);
    }

    template<size_t N, typename Arg>
    void setArgsInternal(Arg&& arg) {
      setArgWithTry(N, std::forward<Arg>(arg));
    }

    template<typename Arg>
    void setArgWithTry(size_t index, Arg&& arg) {
      try {
        setArg(index, arg);
      }
      catch(const std::exception& ex) {
        THROW_EXCEPTION("Failed setting arg with index ", index, " for kernel ",
                        mKernel.name(), "! OpenCL error description: ",
                        ex.what());
      }
    }

    void setArgNull(size_t index, size_t size);

    boost::compute::kernel mKernel;
    Pointer<Device> pDevice;
    std::vector<BufferPtr> mInBuffer;
    std::vector<BufferPtr> mOutBuffer;
  };

  using KernelPtr = std::shared_ptr<Kernel>;

} //namespace Ocelot

#endif // KERNEL_H_
