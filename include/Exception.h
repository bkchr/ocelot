#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <exception>
#include <string>
#include <sstream>

namespace Ocelot
{

  class Exception : public std::exception
  {
  public:
    template<typename ...Args>
    Exception(unsigned int line, const std::string& file, Args&&... args) {
      std::stringstream stream;
      generateMessage(stream, std::forward<Args>(args)...);
      mMessage = stream.str() + " - " + file + ":" + std::to_string(line);
    }

    virtual const char* what() const noexcept { return mMessage.c_str(); }

  private:
    template<typename Arg>
    void generateMessage(std::stringstream& stream, Arg&& arg) {
      stream<<arg;
    }

    template<typename Arg, typename ...Args>
    void generateMessage(std::stringstream& stream, Arg&& arg, Args&&... args) {
      stream<<arg;
      generateMessage(stream, std::forward<Args>(args)...);
    }

    std::string mMessage;
  };

  #define THROW_EXCEPTION(...) throw Exception(__LINE__, __FILE__, __VA_ARGS__)

  #ifdef NDEBUG
    #define OCELOT_ASSERT(cond, ...)
  #else
    #define OCELOT_ASSERT(cond, ...) { if (!(cond)) {                          \
      THROW_EXCEPTION(__VA_ARGS__); } }
  #endif  //#ifndef NDEBUG

}

#endif // EXCEPTION_H_
