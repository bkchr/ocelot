#ifndef DEFINES_H_
#define DEFINES_H_

#include "DataType.h"
#include "Future.h"
#include "DependencyInjection.h"

#include <memory>
#include <boost/compute/async.hpp>

namespace Ocelot
{

  typedef unsigned long ResourceId;

  enum : ResourceId
  {
    Invalid_ResourceId = 0,
    First_Valid_ResourceId = 1
  };

  typedef unsigned long OperationId;
  enum
  {
    Invalid_OperationId
  };

  DI_DECLARE_CONSTANT(GlobalVectorSize, cl_ulong);

  template<typename Type0, typename Type1>
  auto divisionWithRound(Type0&& data0, Type1&& data1) ->
  decltype(data0 / data1) {
    return data0 % data1 ? data0 / data1 + 1 : data0 / data1;
  }

  /**
   * @brief Gets the highest bit set in the input and sets the same bit in
   *        the output.
   */
  inline unsigned int setHighBit(unsigned int num) {
    if (num == 0)
      return 0;

    unsigned int result = 1;

    while (num >>= 1)
      result <<= 1;

    return result;
  }

  template<typename Type>
  using Pointer = std::shared_ptr<Type>;
  template<typename Type>
  using WPointer = std::weak_ptr<Type>;

} //namespace Ocelot

#endif // DEFINES_H_
