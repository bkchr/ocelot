// TYPE DEFINITION.
#include "types.h"

// #################################################################
// ## SUM
// #################################################################

// DTypeS8 and DTypeS16 work with int as result type
#if TYPE == DTypeS8 || TYPE == DTypeS16
  #define GROUPED_SUM_RTYPE int
  #define GROUPED_SUM_ADD AtomicAddInt
  #define GROUPED_SUM_ADDLOCAL AtomicAddLocalInt
#else
  #define GROUPED_SUM_RTYPE T
  #define GROUPED_SUM_ADD AtomicAdd
  #define GROUPED_SUM_ADDLOCAL AtomicAddLocal
#endif

// AGGREGATE VALUES IN LOCAL MEMORY
__kernel void TYPED_KERNEL(groupedSumLocal)(
	__global const T* data,
	__global const unsigned int* groups,
  __local GROUPED_SUM_RTYPE* scratch_space,
	const unsigned int tuples_per_thread,
	const unsigned int nr_of_groups,
	const unsigned int partitions_per_group,
  __global GROUPED_SUM_RTYPE* result
) {
	// Initialize local memory:
	event_t copy_event = async_work_group_copy(scratch_space,
      &(result[get_group_id(0) * nr_of_groups * partitions_per_group]),
      partitions_per_group * nr_of_groups, 0);
	wait_group_events(1, &copy_event);
	barrier(CLK_LOCAL_MEM_FENCE);
	// Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
	                 + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  // Now aggregate in local memory:
  for (unsigned int i = 0; i < tuples_per_thread; ++i) {
    GROUPED_SUM_RTYPE val = data[pos];
    unsigned int group = groups[pos];
#ifdef DEVICE_GPU
    // Compute the partitioned group ID:
    group += nr_of_groups * (pos % partitions_per_group);
#endif
    // And aggregate the value in the according accumulator.
		val = (val == NIL) ? 0 : val;	// Deal with NULL values.
    GROUPED_SUM_ADDLOCAL(&(scratch_space[group]), val);
		// Move to the next value.
#ifdef DEVICE_GPU
		pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
		pos++;                    // Walk sequential.
#endif
	}
	barrier(CLK_LOCAL_MEM_FENCE);
	// Now copy the result back
	copy_event = async_work_group_copy(
      &(result[get_group_id(0) * nr_of_groups * partitions_per_group]),
      scratch_space, nr_of_groups * partitions_per_group, 0);
	wait_group_events(1, &copy_event);
}

// AGGREGATE VALUES IN GLOBAL MEMORY
__kernel void TYPED_KERNEL(groupedSumGlobal)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  __global GROUPED_SUM_RTYPE* result
) {
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  unsigned int result_offset = get_group_id(0) * nr_of_groups;
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    GROUPED_SUM_RTYPE val = data[pos];
    val = (val == NIL) ? 0 : val; // Deal with NULL values.
    GROUPED_SUM_ADD(&(result[result_offset + groups[pos]]), val);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
}

// AGGREGATE VALUES SEQUENTIALLY
__kernel void TYPED_KERNEL(groupedSumSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global GROUPED_SUM_RTYPE* result,
  unsigned int result_offset
) {
  for (unsigned int i=0; i<tuples; ++i) {
    GROUPED_SUM_RTYPE val = data[data_offset + i];
    val = (val == NIL) ? 0 : val; // Deal with NULL values.
    result[result_offset + groups[data_offset + i]] += val;
  }
}

// AGGREGATE ALL PARTIAL RESULTS FOR A SINGLE GROUP
__kernel void TYPED_KERNEL(groupedSumFinal)(
  __global const GROUPED_SUM_RTYPE* intermediate_results,
  const unsigned int groups,
  const unsigned int processors,
  __global GROUPED_SUM_RTYPE* result
) {
  GROUPED_SUM_RTYPE agg = 0;
  for (unsigned int i = 0; i < processors; ++i) {
    agg += intermediate_results[i * groups + get_global_id(0)];
  }
  result[get_global_id(0)] = agg;
}

// #################################################################
// ## SUMLONG
// #################################################################

// AGGREGATE VALUES SEQUENTIALLY
__kernel void TYPED_KERNEL(groupedSumLongSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global long* result,
  unsigned int result_offset
) {
  for (unsigned int i = 0; i < tuples; ++i) {
    long val = data[data_offset + i];
    val = (val == NIL) ? 0 : val; // Deal with NULL values.
    result[result_offset + groups[data_offset + i]] += val;
  }
}

// AGGREGATE ALL PARTIAL RESULTS FOR A SINGLE GROUP
__kernel void TYPED_KERNEL(groupedSumLongFinal)(
  __global const T* intermediate_results,
  const unsigned int groups,
  const unsigned int processors,
  __global long* result
) {
  long agg = 0;
  for (unsigned int i = 0; i < processors; ++i) {
    agg += intermediate_results[i * groups + get_global_id(0)];
  }
  result[get_global_id(0)] = agg;
}


// #################################################################
// ## MAX
// #################################################################

// AGGREGATE VALUES IN LOCAL MEMORY
__kernel void TYPED_KERNEL(groupedMaxLocal)(
  __global const T* data,
  __global const unsigned int* groups,
  __local T* scratch_space,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  const unsigned int partitions_per_group,
  __global T* result
) {
  // Initialize local memory:
	event_t copy_event = async_work_group_copy(scratch_space,
	    &(result[get_group_id(0)*nr_of_groups*partitions_per_group]),
      partitions_per_group*nr_of_groups, 0);
  wait_group_events(1, &copy_event);
  barrier(CLK_LOCAL_MEM_FENCE);
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    T val = data[pos];
    unsigned int group = groups[pos];
#ifdef DEVICE_GPU
    // Compute the partitioned group ID:
    group += nr_of_groups * (pos % partitions_per_group);
#endif
    // And aggregate the value in the according accumulator.
    AtomicMaxLocal(&(scratch_space[group]), val);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  // Now copy the result back
	copy_event = async_work_group_copy(
      &(result[get_group_id(0)*nr_of_groups*partitions_per_group]),
	    scratch_space, nr_of_groups*partitions_per_group, 0);
  wait_group_events(1, &copy_event);
}

// AGGREGATE VALUES IN GLOBAL MEMORY
__kernel void TYPED_KERNEL(groupedMaxGlobal)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  __global T* result
) {
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  unsigned int result_offset = get_group_id(0) * nr_of_groups;
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    T val = data[pos];
    AtomicMax(&(result[result_offset + groups[pos]]), val);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
}

// AGGREGATE VALUES SEQUENTIALLY
__kernel void TYPED_KERNEL(groupedMaxSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global T* result,
  unsigned int result_offset
) {
  for (unsigned int i=0; i<tuples; ++i) {
    T val = data[data_offset + i];
    result[result_offset + groups[data_offset + i]] = max(
        result[result_offset + groups[data_offset + i]], val);
  }
}

// AGGREGATE ALL PARTIAL RESULTS FOR A SINGLE GROUP
__kernel void TYPED_KERNEL(groupedMaxFinal)(
  __global const T* intermediate_results,
  const unsigned int groups,
  const unsigned int processors,
  __global T* result
) {
  T agg = MIN;
  for (unsigned int i=0; i<processors; ++i) {
    agg = max(agg, intermediate_results[i*groups+get_global_id(0)]);
  }
  result[get_global_id(0)] = agg;
}


// #################################################################
// ## MIN
// #################################################################

// AGGREGATE VALUES IN LOCAL MEMORY
__kernel void TYPED_KERNEL(groupedMinLocal)(
  __global const T* data,
  __global const unsigned int* groups,
  __local T* scratch_space,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  const unsigned int partitions_per_group,
  __global T* result
) {
  // Initialize local memory:
	event_t copy_event = async_work_group_copy(scratch_space,
      &(result[get_group_id(0) * nr_of_groups * partitions_per_group]),
      partitions_per_group * nr_of_groups, 0);
  wait_group_events(1, &copy_event);
  barrier(CLK_LOCAL_MEM_FENCE);
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    T val = data[pos];
    unsigned int group = groups[pos];
#ifdef DEVICE_GPU
    // Compute the partitioned group ID:
    group += nr_of_groups * (pos % partitions_per_group);
#endif
    // And aggregate the value in the according accumulator.
    val = (val == NIL) ? MAX : val; // Deal with NULL values.
    AtomicMinLocal(&(scratch_space[group]), val);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  // Now copy the result back
	copy_event = async_work_group_copy(
      &(result[get_group_id(0) * nr_of_groups*partitions_per_group]),
	    scratch_space, nr_of_groups*partitions_per_group, 0);
  wait_group_events(1, &copy_event);
}

// AGGREGATE VALUES IN GLOBAL MEMORY
__kernel void TYPED_KERNEL(groupedMinGlobal)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  __global T* result
) {
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  unsigned int result_offset = get_group_id(0) * nr_of_groups;
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    T val = data[pos];
    val = (val == NIL) ? MAX : val; // Deal with NULL values.
    AtomicMin(&(result[result_offset + groups[pos]]), val);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
}

// AGGREGATE VALUES SEQUENTIALLY
__kernel void TYPED_KERNEL(groupedMinSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global T* result,
  unsigned int result_offset
) {
  for (unsigned int i=0; i<tuples; ++i) {
    T val = data[data_offset + i];
    val = (val == NIL) ? MAX : val; // Deal with NULL values.
    result[result_offset + groups[data_offset + i]] = min(
        result[result_offset + groups[data_offset + i]], val);
  }
}

// AGGREGATE ALL PARTIAL RESULTS FOR A SINGLE GROUP
__kernel void TYPED_KERNEL(groupedMinFinal)(
  __global const T* intermediate_results,
  const unsigned int groups,
  const unsigned int processors,
  __global T* result
) {
  T agg = MAX;
  for (unsigned int i = 0; i < processors; ++i) {
    agg = min(agg, intermediate_results[i * groups + get_global_id(0)]);
  }
  result[get_global_id(0)] = agg;
}


// #################################################################
// ## COUNT (WITH NULL CHECKING)
// #################################################################

// AGGREGATE VALUES IN LOCAL MEMORY
__kernel void TYPED_KERNEL(groupedCountWithNullLocal)(
  __global const T* data,
  __global const unsigned int* groups,
  __local unsigned int* scratch_space,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  const unsigned int partitions_per_group,
  __global unsigned int* result
) {
  // Initialize local memory:
	event_t copy_event = async_work_group_copy(scratch_space,
	    &(result[get_group_id(0)*nr_of_groups*partitions_per_group]),
      partitions_per_group*nr_of_groups, 0);
  wait_group_events(1, &copy_event);
  barrier(CLK_LOCAL_MEM_FENCE);
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    T val = data[pos];
    unsigned int group = groups[pos];
#ifdef DEVICE_GPU
    // Compute the partitioned group ID:
    group += nr_of_groups * (pos % partitions_per_group);
#endif
    // And aggregate the value in the according accumulator.
    unsigned int tmp = (val == NIL) ? 0 : 1;
    atomic_add(&(scratch_space[group]), tmp);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  // Now copy the result back
	copy_event = async_work_group_copy(
      &(result[get_group_id(0)*nr_of_groups*partitions_per_group]),
	    scratch_space, nr_of_groups*partitions_per_group, 0);
  wait_group_events(1, &copy_event);
}

// AGGREGATE VALUES IN GLOBAL MEMORY
__kernel void TYPED_KERNEL(groupedCountWithNullGlobal)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  __global unsigned int* result
) {
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  unsigned int result_offset = get_group_id(0) * nr_of_groups;
  // Now aggregate in local memory:
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    T val = data[pos];
    unsigned int tmp = (val == NIL) ? 0 : 1;
    atomic_add(&(result[result_offset + groups[pos]]), tmp);
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
}

// AGGREGATE VALUES SEQUENTIALLY
__kernel void TYPED_KERNEL(groupedCountWithNullSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global unsigned int* result,
  unsigned int result_offset
) {
  for (unsigned int i = 0; i < tuples; ++i) {
    T val = data[data_offset + i];
    unsigned int tmp = (val == NIL) ? 0 : 1;
    result[result_offset + groups[data_offset + i]] += tmp;
  }
}

__kernel void TYPED_KERNEL(groupedCountLongWithNullSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global long* result,
  unsigned int result_offset
) {
  for (unsigned int i = 0; i < tuples; ++i) {
    T val = data[data_offset + i];
    unsigned int tmp = (val == NIL) ? 0 : 1;
    result[result_offset + groups[data_offset + i]] += tmp;
  }
}

// #################################################################
// ## COUNT (WITHOUT NULL CHECKING)
// #################################################################

// AGGREGATE VALUES IN LOCAL MEMORY
__kernel void TYPED_KERNEL(groupedCountLocal)(
  __global const T* data,
  __global const unsigned int* groups,
  __local unsigned int* scratch_space,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  const unsigned int partitions_per_group,
  __global unsigned int* result
) {
  // Initialize local memory:
	event_t copy_event = async_work_group_copy(scratch_space,
      &(result[get_group_id(0) * nr_of_groups * partitions_per_group]),
      partitions_per_group * nr_of_groups, 0);
  wait_group_events(1, &copy_event);
  barrier(CLK_LOCAL_MEM_FENCE);
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  // Now aggregate in local memory:
  for (unsigned int i = 0; i < tuples_per_thread; ++i) {
    unsigned int group = groups[pos];
#ifdef DEVICE_GPU
    // Compute the partitioned group ID:
    group += nr_of_groups * (pos % partitions_per_group);
#endif
    // And increment the counter:
    atomic_inc(&(scratch_space[group]));
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
  barrier(CLK_LOCAL_MEM_FENCE);
  // Now copy the result back
	copy_event = async_work_group_copy(
      &(result[get_group_id(0) * nr_of_groups * partitions_per_group]),
      scratch_space, nr_of_groups * partitions_per_group, 0);
  wait_group_events(1, &copy_event);
}

// AGGREGATE VALUES IN GLOBAL MEMORY
__kernel void TYPED_KERNEL(groupedCountGlobal)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int tuples_per_thread,
  const unsigned int nr_of_groups,
  __global unsigned int* result
) {
  // Compute the starting address:
#ifdef DEVICE_GPU
  unsigned int pos = get_local_size(0) * tuples_per_thread * get_group_id(0)
                   + get_local_id(0);
#elif DEVICE_CPU
  unsigned int pos = tuples_per_thread * get_global_id(0);
#endif
  unsigned int result_offset = get_group_id(0) * nr_of_groups;
  // Now aggregate in global memory:
  for (unsigned int i = 0; i < tuples_per_thread; ++i) {
    atomic_inc(&(result[result_offset + groups[pos]]));
    // Move to the next value.
#ifdef DEVICE_GPU
    pos += get_local_size(0); // Walk strided.
#elif DEVICE_CPU
    pos++;                    // Walk sequential.
#endif
  }
}

// AGGREGATE VALUES SEQUENTIALLY
__kernel void TYPED_KERNEL(groupedCountSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global unsigned int* result,
  unsigned int result_offset
) {
  for (unsigned int i = 0; i < tuples; ++i) {
    result[result_offset + groups[data_offset + i]]++;
  }
}

__kernel void TYPED_KERNEL(groupedCountLongSequential)(
  __global const T* data,
  __global const unsigned int* groups,
  const unsigned int data_offset,
  const unsigned int tuples,
  __global long* result,
  unsigned int result_offset
) {
  for (unsigned int i = 0; i < tuples; ++i) {
    result[result_offset + groups[data_offset + i]]++;
  }
}
