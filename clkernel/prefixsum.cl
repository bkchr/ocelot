 #include "cl_utilities.h"
// This kernel should not be used to scan larger amounts of data!
__kernel void prefixsum_seq(
	__global const unsigned int * input,	// Input buffer for the prefix sum.
   const unsigned int input_offset,			// Offset into the input buffer where the sequential part starts.
   __global unsigned int * output,			// Output buffer for the computed prefix sum.
   const unsigned int output_offset,		// Offset into the output buffer.
   const unsigned int elements,				// Number of elments in the sequential part.
   __global unsigned int * partial_sums,	// Buffer to store the computed partial sum.
   const unsigned int partial_sum_offset  // Offset into the buffer for partial sums.	
) {
	int gid = get_global_id(0);
	if (gid != 0)
		return;
	unsigned int i = 0;
	unsigned int sum = 0;
	for (i = 0; i < elements; ++i) {
		unsigned int tmp = input[input_offset + i];
		output[output_offset + i] = sum;
		sum += tmp;
	}
	partial_sums[partial_sum_offset] = sum;	
}

__kernel void prefixsum_seq_bm(
   __global const unsigned int * input,   // Input buffer for the prefix sum.
   const unsigned int input_offset,       // Offset into the input buffer where the sequential part starts.
   __global unsigned int * output,        // Output buffer for the computed prefix sum.
   const unsigned int output_offset,      // Offset into the output buffer.
   const unsigned int elements,           // Number of elments in the sequential part.
   __global unsigned int * partial_sums,  // Buffer to store the computed partial sum.
   const unsigned int partial_sum_offset  // Offset into the buffer for partial sums.
) {
   int gid = get_global_id(0);

   if (gid != 0) {
      return;
   }

   unsigned int i = 0;
   unsigned int sum = 0;
   for (i = 0; i < elements; ++i) {
      unsigned int tmp = input[input_offset + i];
      output[output_offset + i] = sum;
      sum += bitcount(tmp);
   }

   partial_sums[partial_sum_offset] = sum;
}

__kernel void prefixsum_inc(
	__global const unsigned int* input,
	const unsigned int input_offset,
	const unsigned int elements,
	__global const unsigned int* partial_sums,
	const unsigned int partial_sum_offset,
	__global unsigned int* output,
	const unsigned int output_offset)
{
	const int local_size = get_local_size(0);
	int gi = get_global_id(0);
	if (gi < elements)
		output[output_offset + gi] = input[input_offset + gi] + partial_sums[partial_sum_offset + gi / (2 * local_size)];
}

// Kernel that performs a fast recursive scan in local memory if the
// number of elements is a power of 2.

__kernel void prefixsum_par(
	__global const unsigned int* input,
	const unsigned int input_offset,
	__global unsigned int* output,			
	const unsigned int output_offset,		
	__local unsigned int* buffer,
	__global unsigned int* partial_sums,
	const unsigned int partial_sum_offset
){
	const unsigned int local_size = get_local_size(0);
	unsigned int gid = get_global_id(0);
	unsigned int lid = get_local_id(0);

	// Summarize two data elements in local memory
	buffer[lid] = input[input_offset + 2 * gid] 
					+ input[input_offset + 2 * gid + 1];
	
	// Make sure all local summations are finished
	barrier(CLK_LOCAL_MEM_FENCE);

	// Perform the up-sweep. 
	unsigned int border = local_size / 2;
	unsigned int width = 2;
	for (; border > 1; border >>= 1, width <<= 1) {
		if (lid < border) {
			buffer[width * lid + width - 1] = 
				buffer[width * lid + (width - 2) / 2] 
				+ buffer[width * lid + width - 1];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	// now comes the turning point
	if (lid == 0) {
		// finish the local reduction and store the computed result sum to the result buffer
		partial_sums[partial_sum_offset + gid / local_size] = buffer[(width - 1) / 2] + buffer[width - 1];
		// Begin walking down the tree.
		buffer[width - 1] = buffer[(width - 2) / 2];
		buffer[(width - 2) / 2] = 0;	
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	border = 2;
	width = local_size / 2;
	for (; border < local_size; border <<= 1, width >>= 1) {
		if (lid < border) {
			unsigned int tmp = buffer[width * lid + width - 1];
			buffer[width * lid + width - 1] = tmp + buffer[width * lid + (width - 2) / 2];
			buffer[width * lid + (width - 2) / 2] = tmp;
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	// now we just have to store the result back
	unsigned int tmp = input[input_offset + 2*gid];
	output[output_offset + 2*gid] = buffer[lid];
	output[output_offset + 2*gid + 1] = tmp + buffer[lid];
}

__kernel void prefixsum_par_bm(
	__global const unsigned int* input,
	const unsigned int input_offset,
	__global unsigned int* output,			
	const unsigned int output_offset,		
	__local unsigned int* buffer,
	__global unsigned int* partial_sums,
	const unsigned int partial_sum_offset
){
	const unsigned int local_size = get_local_size(0);
	int gid = get_global_id(0);
	int lid = get_local_id(0);

	// Summarize two data elements in local memory
	buffer[lid] = bitcount(input[input_offset + 2 * gid]) 
					+ bitcount(input[input_offset + 2 * gid + 1]);
	
	// Make sure all local summations are finished
	barrier(CLK_LOCAL_MEM_FENCE);

	// Perform the up-sweep. 
	unsigned int border = local_size / 2;
	unsigned int width = 2;
	for (; border > 1; border >>= 1, width <<= 1) {
		if (lid < border) {
			buffer[width * lid + width - 1] = 
				buffer[width * lid + (width - 2) / 2] 
				+ buffer[width * lid + width - 1];
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	// now comes the turning point
	if (lid == 0) {
		// finish the local reduction and store the computed result sum to the result buffer
		partial_sums[partial_sum_offset + gid / local_size] = buffer[(width - 1) / 2] + buffer[width - 1];
		// Begin walking down the tree.
		buffer[width - 1] = buffer[(width - 2) / 2];
		buffer[(width - 2) / 2] = 0;	
	}
	barrier(CLK_LOCAL_MEM_FENCE);

	border = 2;
	width = local_size / 2;
	for (; border < local_size; border <<= 1, width >>= 1) {
		if (lid < border) {
			unsigned int tmp = buffer[width * lid + width - 1];
			buffer[width * lid + width - 1] = tmp + buffer[width * lid + (width - 2) / 2];
			buffer[width * lid + (width - 2) / 2] = tmp;
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}
	
	// now we just have to store the result back
	unsigned int tmp = bitcount(input[input_offset + 2*gid]);
	output[output_offset + 2*gid] = buffer[lid];
	output[output_offset + 2*gid + 1] = tmp + buffer[lid];
}
