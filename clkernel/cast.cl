// Kernels for casting between data types.
__kernel void int2float(
  __global int* input,
  __global float* output) {
  output[get_global_id(0)] = (float)input[get_global_id(0)];
}

__kernel void float2int(
  __global float* input,
  __global int* output) {
  output[get_global_id(0)] = (int)input[get_global_id(0)];
}
