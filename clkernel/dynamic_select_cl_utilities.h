/*
 * dynamic_select_cl_utilities.h
 *
 *  Created on: 21.11.2012
 *      Author: martin
 */

#ifndef DYNAMIC_SELECT_CL_UTILITIES_H_
#define DYNAMIC_SELECT_CL_UTILITIES_H_

typedef enum nodetype
{
  AND_OP = 0,
  OR_OP = 1,
  SM_OP = 2,
  SMEQ_OP = 3,
  GR_OP = 4,
  GREQ_OP = 5,
  EQ_OP = 6,
  SM_OP_COL = 7,
  SMEQ_OP_COL = 8,
  GR_OP_COL = 9,
  GREQ_OP_COL = 10,
  EQ_OP_COL = 11
} nodetype;

typedef struct innerNode
{
  nodetype type;
  int child1;
  int child2;
  int id;
} innerNode;

typedef struct colLeaf
{
  nodetype type;
  char * child1;
  char * child2;
  int id;

} colLeaf;

typedef struct leaf
{

  nodetype type;

  char child1[32];
  int child2;
  int id;

} leaf;

// Macro to evaluate a predicate
#define EVAL_AST_PREDICATE(val, id) {					          \
    pred = (val > leafValue) && leafType == GR_OP;		  \
    pred |= (val < leafValue) && leafType == SM_OP;		  \
    pred |= (val >= leafValue) && leafType == GREQ_OP;	\
    pred |= (val <= leafValue) && leafType == SMEQ_OP;	\
    pred |= (val == leafValue) && leafType == EQ_OP;	  \
    pred |= (id < tuples) && pred;						          \
    pattern |= pred ? (0x1 << offset) : 0;				      \
    offset++;											                      \
  }

// Macro to evaluate a predicate
#define EVAL_AST_PREDICATE_VECTOR(val, id) {			      \
    predv = (val > leafValue) && leafType == GR_OP;		  \
    predv |= (val < leafValue) && leafType == SM_OP;		\
    predv |= (val >= leafValue) && leafType == GREQ_OP;	\
    predv |= (val <= leafValue) && leafType == SMEQ_OP;	\
    predv |= (val == leafValue) && leafType == EQ_OP;	  \
    predv |= (id < tuples) && predv;						        \
  }

// Macro to evaluate a predicate
#define EVAL_AST_INNER_PREDICATE(val, val2, id) {		    \
    pred = (val && val2) && innerType == AND_OP;		    \
    pred |= (val || val2) && innerType == OR_OP;		    \
    pred |= (id < tuples) && pred;				              \
    pattern |= pred ? (0x1 << offset) : 0;			        \
    offset++;						                                \
  }

#endif // DYNAMIC_SELECT_CL_UTILITIES_H_
