#include "types.h"

// ADDITION between BAT and Constant.
__kernel void TYPED_KERNEL(addBatCnst) (
	__global const T* data,
	const T val,
	__global T* result
){
	size_t gi = get_global_id(0);
	T input_val = data[gi];
	result[gi] = (input_val == NIL || val == NIL)
			   ? NIL : input_val + val;
}

// ADDITION between BAT and BAT.
__kernel void TYPED_KERNEL(addBatBat) (
	__global const T* left,
	__global const T* right,
	__global T* result
){
	size_t gi = get_global_id(0);
	T left_val = left[gi];
	T right_val = right[gi];
	result[gi] = (left_val == NIL || right_val == NIL)
			   ? NIL : left_val + right_val;
}

// SUBTRACTION between BAT and Constant.
__kernel void TYPED_KERNEL(subBatCnst) (
	__global const T* data,
	const T val,
	__global T* result
){
	size_t gi = get_global_id(0);
	T input_val = data[gi];
	result[gi] = (input_val == NIL || val == NIL) ? NIL : input_val - val; 
}

// SUBTRACTION between Constant and BAT.
__kernel void TYPED_KERNEL(subCnstBat) (
	__global const T* data,
	const T val,
	__global T* result
){
	size_t gi = get_global_id(0);
	T input_val = data[gi];
	result[gi] = (input_val == NIL || val == NIL)
			   ? NIL : val - input_val;
}

// SUBTRACTION between BAT and BAT.
__kernel void TYPED_KERNEL(subBatBat) (
	__global const T* left,
	__global const T* right,
	__global T* result
){
	size_t gi = get_global_id(0);
	T left_val = left[gi];
	T right_val = right[gi];
	result[gi] = (left_val == NIL || right_val == NIL)
			   ? NIL : left_val - right_val;
}

// MULTIPLICATION between BAT and Constant.
__kernel void TYPED_KERNEL(mulBatCnst) (
	__global const T* data,
	const T val,
	__global T* result
){
	size_t gi = get_global_id(0);
	T input_val = data[gi];
	result[gi] = (input_val == NIL || val == NIL) ? NIL : val * input_val; 
}

// MULTIPLICATION between BAT and BAT.
__kernel void TYPED_KERNEL(mulBatBat) (
	__global const T* left,
	__global const T* right,
	__global T* result
){
	size_t gi = get_global_id(0);
	T left_val = left[gi];
	T right_val = right[gi];
	result[gi] = (left_val == NIL || right_val == NIL)
			   ? NIL : left_val * right_val;
}

// DIVISION between BAT and Constant.
__kernel void TYPED_KERNEL(divBatCnst) (
	__global const T* data,
	const T val,
	__global T* result
){
	size_t gi = get_global_id(0);
	T input_val = data[gi];
	result[gi] = (input_val == NIL || val == NIL) ? NIL : input_val / val; 
}

// DIVISION between Constant and BAT.
__kernel void TYPED_KERNEL(divCnstBat) (
	__global const T* data,
	const T val,
	__global T* result,
	__global unsigned int* flag
){
	size_t gi = get_global_id(0);
	T input_val = data[gi];
	if (input_val == 0)
		flag[0] = 1;
	else
		result[gi] = (input_val == NIL || val == NIL) ? NIL : val / input_val; 
}

// DIVISION between BAT and BAT.
__kernel void TYPED_KERNEL(divBatBat)(
	__global const T* left,
	__global const T* right,
	__global T* result,
	__global unsigned int* flag
){
	size_t gi = get_global_id(0);
	T left_val = left[gi];
	T right_val = right[gi];
	// sanity check to avoid kernel crashes
	if (right_val == 0)
		flag[0] = 1;
	else
		result[gi] = (left_val == NIL || right_val == NIL)
		           ? NIL : left_val / right_val;
}

// LOWER THAN between BAT and BAT.
__kernel void TYPED_KERNEL(ltBatBat)(
	__global const T* left,
	__global const T* right,
	__global unsigned char* result,
	unsigned int elements
) {
	unsigned int pos = 8 * get_global_id(0);
	unsigned char pattern = 0;
	unsigned char offset = 0;
	for (unsigned int i=0; i<8; ++i) {
	    if (pos >= elements)
	    	break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = left[pos] != NIL;
		pred &= (left[pos] < right[pos]);
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
    pos++;
	}
	result[get_global_id(0)] = pattern;
}

// LOWER THAN OR EQUAL between BAT and BAT.
__kernel void TYPED_KERNEL(ltEqBatBat)(
	__global const T* left,
	__global const T* right,
	__global unsigned char* result,
	unsigned int elements
) {
	unsigned int pos = 8 * get_global_id(0);
	unsigned char pattern = 0;
	unsigned char offset = 0;
	for (unsigned int i=0; i<8; ++i) {
	    if (pos >= elements)
	    	break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = left[pos] != NIL;
		pred &= (left[pos] <= right[pos]);
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
    pos++;
	}
	result[get_global_id(0)] = pattern;
}

// GREATER THAN between BAT and BAT.
__kernel void TYPED_KERNEL(gtBatBat)(
   __global const T* left,
   __global const T* right,
   __global unsigned char* result,
	unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
	    if (pos >= elements)
	    	break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = right[pos] != NIL;
		pred &= (left[pos] > right[pos]);
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
  	pos++;
	}
	result[get_global_id(0)] = pattern;
}

// GREATER THAN OR EQUAL between BAT and BAT.
__kernel void TYPED_KERNEL(gtEqBatBat)(
   __global const T* left,
   __global const T* right,
   __global unsigned char* result,
	unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
	    if (pos >= elements)
	    	break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = right[pos] != NIL;
		pred &= (left[pos] >= right[pos]);
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
  	pos++;
	}
	result[get_global_id(0)] = pattern;
}

// EQUAL between BAT and BAT.
__kernel void TYPED_KERNEL(eqBatBat)(
   __global const T* left,
   __global const T* right,
   __global unsigned char* result,
	unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
		if (pos >= elements)
		   		break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = right[pos] != NIL;
		pred &= left[pos] != NIL;
		pred &= (left[pos] == right[pos]);
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
    pos++;
	}
	result[get_global_id(0)] = pattern;
}

__kernel void TYPED_KERNEL(neqBatBat)(
   __global const T* left,
   __global const T* right,
   __global unsigned char* result,
	unsigned int elements
) {
  unsigned int pos = 8 * get_global_id(0);
  unsigned char pattern = 0;
  unsigned char offset = 0;
  for (unsigned int i=0; i<8; ++i) {
    if (pos >= elements)
      break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = right[pos] != NIL;
		pred &= left[pos] != NIL;
		pred &= (left[pos] != right[pos]);
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
    pos++;
  }
  result[get_global_id(0)] = pattern;
}
