cmake_minimum_required(VERSION 2.8)

file(GLOB CLKERNEL_HEADER "${CMAKE_CURRENT_SOURCE_DIR}/*.h")
add_files_to_project_ocelot(${CLKERNEL_HEADER})

file(GLOB CLKERNEL_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/*.cl")
add_files_to_project_ocelot(${CLKERNEL_SOURCE})
