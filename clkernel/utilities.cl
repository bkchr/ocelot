// generates a void column on the device
__kernel void generateVoidColumn(
  __global unsigned int* buffer,
  const unsigned int elements) {
  const size_t gi = get_global_id(0);
  if (gi >= elements)
    return;

  buffer[gi] = gi;
}

__kernel void generateFilledBitmap(
	__global unsigned int* result,
  const unsigned int tuple_count) {
  if (32 * (1 + get_global_id(0)) <= tuple_count) {
    result[get_global_id(0)] = ~0;
  } else if (32 * get_global_id(0) < tuple_count) {
		result[get_global_id(0)] = 
      (0x1 << (tuple_count - 32 * get_global_id(0))) - 1;
	} else {
		result[get_global_id(0)] = 0;
	}
}


__kernel void markUniqueValuesInSortedList(
	__global const unsigned int* values,
	__global unsigned int* result,
	const unsigned int tuples) {
	// Boundary checking.
	if (get_global_id(0) >= tuples) return;
	// The first value is always unique.
	if (get_global_id(0) == 0) result[0] = 1;
	else result[get_global_id(0)] = 
		values[get_global_id(0)] == values[get_global_id(0) - 1] ? 0 : 1;
}

__kernel void condenseUniqueSortedList(
	__global const unsigned int* values,
	__global const unsigned int* write_offsets,
	__global unsigned int* result,
	const unsigned int tuples) {
	// Boundary checking.
	if (get_global_id(0) >= tuples) return;
	if (get_global_id(0) == 0) {
		// The first value is always unique.
		result[0] = values[0];
	} else {
		if (values[get_global_id(0)] != values[get_global_id(0) - 1]) {
			result[write_offsets[get_global_id(0)]] = values[get_global_id(0)];
		}
	}
}
