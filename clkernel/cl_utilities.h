#ifndef CL_UTILITIES_H_
#define CL_UTILITIES_H_

/* countPartitions
 * Counts the number of partitions when dividing arrayLength by size.
 * Input:
 *   arrayLength	-	Number of elements
 *   size		-	Size of sub list.
 */
inline unsigned int countPartitions(const unsigned int arrayLength,
                                    const unsigned int size){
  return (arrayLength & (size - 1)) == 0 ? (arrayLength / size) :
                                           ((arrayLength / size) + 1);
}

// Counts the number of bits set in an integer. 
inline unsigned int bitcount(const unsigned int n) {
  // MIT bitcount
  unsigned int tmp;
  tmp = n - ((n >> 1) & 033333333333) - ((n >> 2) & 011111111111);

  return ((tmp + (tmp >> 3)) & 030707070707) % 63;
}

#endif  // CL_UTILITIES_H_
