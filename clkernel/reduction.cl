#include "cl_utilities.h"
#include "types.h"

#define REDUCTION_OPT_COUNT_IGNORE_NIL 1

/* ###################################################################################
 * # Kernels performing a sequential aggregation of a buffer taking a bitmap to 
 * # decide whether a given value should be included in the operation.
 * # 
 * # Kernels exist for the following operations:
 * # - count
 * # - bitcount
 * # - sum_int_int
 * # - sum_int_long
 * # - min
 * # - max
 * #
 * # The kernels are named "reduce_<op>_seq_bm". The arguments are: 
 * # 	data          - Global memory address of the input data buffer.
 * #    data_offset   - Offset into the input buffer.
 * # 	elements      - How many elements will be aggregated?
 ' # 	result        - Global memory address of the result buffer.
 * #    result_offset - Offset into the result buffer. 
 * # 	bitmap        - Global memory address of the bitmap.
 * ################################################################################### */

__kernel void TYPED_KERNEL(reduce_count_seq)(
    __global const T* const data,
    const unsigned int data_offset,
    const unsigned int elements,
    __global unsigned int* result,
    const unsigned int result_offset,
    __global const int* const bitmap,
    const unsigned int options
){
  unsigned int agg = 0;
  bool ignore_nil = options & REDUCTION_OPT_COUNT_IGNORE_NIL;
  if (bitmap) {
    int local_bitmap, mask;
    for (unsigned int i = 0; i < elements; ++i) {
      // After seeing 32 elements, fetch the next part
      // of the bitmap element into a register.
      if (i % 32 == 0) {
        local_bitmap = bitmap[(data_offset + i) / 32];
        mask = 0x1;
      }
      T val = data[data_offset + i];
      agg += ((local_bitmap & mask) && (ignore_nil || val != NIL)) ? 1 : 0;
      // Move the bitmap mask to the next value.
      mask <<= 1;
    }
  } else {
    for (unsigned int i = 0; i < elements; ++i) {
      T val = data[data_offset + i];
      agg += (ignore_nil || val != NIL) ? 1 : 0;
    }
  }
  result[result_offset] = agg;
}

__kernel void reduce_bitcount_seq(
    __global const int* const data,
    const unsigned int data_offset,
    const unsigned int elements,
    __global int* const result,
    const unsigned int result_offset,
    __global const int* const bitmap,
    const unsigned int options
){
  int agg = 0;
  result[result_offset] = agg;
  if (bitmap) {
    int local_bitmap, bitmap_mask;
    for (unsigned int i=0; i<elements; ++i) {
      // After seeing 32 elements, fetch the next part
      // of the bitmap element into a register.
      if (i%32 == 0) {
        local_bitmap = bitmap[(data_offset + i)/32];
        bitmap_mask = 0x1;
      }
      int val = data[data_offset + i];
      agg += (local_bitmap & bitmap_mask) ? bitcount(val) : 0;
      // Move the bitmap mask to the next value.
      bitmap_mask <<= 1;
    }
  } else {
    for (unsigned int i=0; i<elements; ++i) {
      int val = data[data_offset + i];
      agg += bitcount(val);
    }
  }
  result[result_offset] = agg;
} 

// DTypeS8 and DTypeS16 work with int as result type
#if TYPE == DTypeS8 || TYPE == DTypeS16
  #define REDUCE_SUM_RTYPE int
#else
  #define REDUCE_SUM_RTYPE T
#endif

__kernel void TYPED_KERNEL(reduce_sum_seq)(
    __global const T* const data,
    const unsigned int data_offset,
    const unsigned int elements,
    __global REDUCE_SUM_RTYPE* result,
    const unsigned int result_offset,
    __global const int* const bitmap,
    const unsigned int options
){
  REDUCE_SUM_RTYPE agg = 0;
  if (bitmap) {
    int local_bitmap, bitmap_mask;
    for (unsigned int i = 0; i < elements; ++i) {
      // After seeing 32 elements, fetch the next part
      // of the bitmap element into a register.
      if (i % 32 == 0) {
        local_bitmap = bitmap[(data_offset + i) / 32];
        bitmap_mask = 0x1;
      }
      T val = data[data_offset + i];
      agg += ((local_bitmap & bitmap_mask) && val != NIL) ? val : 0;
      // Move the bitmap mask to the next value.
      bitmap_mask <<= 1;
    }
  } else {
    for (unsigned int i = 0; i < elements; ++i) {
      T val = data[data_offset + i];
      agg += (val != NIL) ? val : 0;
    }
  }
  result[result_offset] = agg;
}

__kernel void reduce_sum_int_long_seq(
    __global const int* const data,
    const unsigned int data_offset,
    const unsigned int elements,
    __global long* const result,
    const unsigned int result_offset,
    __global const int* const bitmap,
    const unsigned int options
){
  long agg = 0;
  if (bitmap) {
    int local_bitmap, bitmap_mask;
    for (unsigned i=0; i<elements; ++i) {
      // After seeing 32 elements, fetch the next part
      // of the bitmap element into a register.
      if (i%32 == 0) {
        local_bitmap = bitmap[(data_offset + i)/32];
        bitmap_mask = 0x1;
      }
      int val = data[data_offset + i];
      agg += ((local_bitmap & bitmap_mask) && val!=NIL) ? val : 0;
      // Move the bitmap mask to the next value.
      bitmap_mask <<= 1;
    }
  } else {
    for (unsigned i=0; i<elements; ++i) {
      int val = data[data_offset + i];
      agg += (val != NIL) ? val : 0;
    }
  }
  result[result_offset] = agg;
}

__kernel void TYPED_KERNEL(reduce_max_seq)(
    __global const T* const data,
    const unsigned int data_offset,
    const unsigned int elements,
    __global T* result,
    const unsigned int result_offset,
    __global const int* const bitmap,
    const unsigned int options
){
  T agg = MIN;
  if (bitmap) {
    int local_bitmap, bitmap_mask;
    for (unsigned i=0; i<elements; ++i) {
      // After seeing 32 elements, fetch the next part
      // of the bitmap element into a register.
      if (i%32 == 0) {
        local_bitmap = bitmap[(data_offset + i)/32];
        bitmap_mask = 0x1;
      }
      agg = max(agg, (T)((local_bitmap & bitmap_mask) ? data[data_offset + i] : MIN));
      // Move the bitmap mask to the next value.
      bitmap_mask <<= 1;
    }
  } else {
    for (unsigned i=0; i<elements; ++i)
      agg = max(agg, data[data_offset + i]);
  }
  result[result_offset] = agg;
}

__kernel void TYPED_KERNEL(reduce_min_seq)(
    __global const T* const data,
    const unsigned int data_offset,
    const unsigned int elements,
    __global T* const result,
    const unsigned int result_offset,
    __global const int* const bitmap,
    const unsigned int options
){
  T agg = MAX;
  if (bitmap) {
    int local_bitmap, bitmap_mask;
    for (unsigned i=0; i<elements; ++i) {
      // After seeing 32 elements, fetch the next part
      // of the bitmap element into a register.
      if (i%32 == 0) {
        local_bitmap = bitmap[(data_offset + i)/32];
        bitmap_mask = 0x1;
      }
      T val = data[data_offset + i];
      val = val==NIL ? MAX : val;
      agg = min(agg, (T)((local_bitmap & bitmap_mask) ? val : MAX));
      // Move the bitmap mask to the next value.
      bitmap_mask <<= 1;
    }
  } else {
    for (unsigned i=0; i<elements; ++i) {
      T val = data[data_offset + i];
      val = val==NIL ? MAX : val;
      agg = min(agg, val);
    }
  }
  result[result_offset] = agg;
}

/* ###################################################################################
 * # Kernels performing a fast parallel aggregation of a buffer element. 
 * # 
 * # Kernels exist for the following operations:
 * # - count 
 * # - bitcount 
 * # - sum_int_int
 * # - sum_int_long
 * # - min
 * # - max
 * #
 * # The kernels are named "reduce_<op>_par". The arguments are: 
 * # 	data              - Global memory address of the input data buffer.
 ' # 	result            - Global memory address of the result buffer.
 * # 	tuples_per_thread - Number of tuples each thread aggregates locally.
 * # 	bitmap            - Global memory address of the bitmap.
 * ################################################################################### */
__kernel void TYPED_KERNEL(reduce_count_par)(
    __global const T* const data,
    __global unsigned int* const result,
    const unsigned int tuples_per_thread,
    __global const unsigned int* const bitmap,
    const unsigned int options
){
  unsigned int local_id = get_local_id(0);
  unsigned int global_id = get_global_id(0);
  // Now each thread does a sequential aggregation within a register.
  unsigned int agg = 0;
  bool ignore_nil = options & REDUCTION_OPT_COUNT_IGNORE_NIL;
  if (bitmap) {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    // Reserve a buffer to load a chunk of the bitmap into shared memory
    __local unsigned int local_bitmap[32]; // Maximum size of the bitmap for a stride size of 1024 elements.
    unsigned int bit_pattern = 0x1 << (local_id % 32);
    unsigned int group_start = get_local_size(0) * tuples_per_thread *
                               get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      // Transfer the bitmap chunk for this stride into shared memory.
      if (local_id < (get_local_size(0) / 32))
        local_bitmap[local_id] = bitmap[(group_start + i *
                                         get_local_size(0)) / 32 + local_id];
      barrier(CLK_LOCAL_MEM_FENCE);
      // Now do the local aggregation for this stride.
      T val = data[group_start + i * get_local_size(0) + local_id];
      agg += (((local_bitmap[local_id / 32] & bit_pattern) &&
             (ignore_nil || val != NIL)) ? 1 : 0);
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    unsigned int local_index = tuples_per_thread * global_id;
    unsigned int local_bitmap = bitmap[local_index / 32];
    unsigned int bitmask = 0x1 << local_index % 32;
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      if (local_index % 32 == 0) {
        local_bitmap = bitmap[local_index / 32];
        bitmask = 0x1;
      }
      T val = data[local_index];
      agg += ((local_bitmap & bitmask) && (ignore_nil || val != NIL) ? 1 : 0);
      bitmask <<= 1;
      local_index++;
    }
#endif
  } else {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    unsigned int group_start = get_local_size(0) * tuples_per_thread *
                               get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      T val = data[group_start + i * get_local_size(0) + local_id];
      agg += ((ignore_nil || val != NIL) ? 1 : 0);
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      T val = data[tuples_per_thread * global_id + i];
      agg += ((ignore_nil || val != NIL) ? 1 : 0);
    }
#endif
  }
  // Push the local result to the buffer, so we can aggregate recursively. Since the
  // kernel is very simplistic, we can safely assume that it is always run with MAXBLOCKSIZE.
  __local unsigned int buffer[MAXBLOCKSIZE > 1024 ? 1024 : MAXBLOCKSIZE];
  buffer[local_id] = agg;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Recursively aggregate the tuples in memory.
#if (MAXBLOCKSIZE >= 1024) 
  if (local_id < 512)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 512];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 512) 
  if (local_id < 256)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 256];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 256) 
  if (local_id < 128)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 128];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 128) 
  if (local_id < 64)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 64];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  // We assume there is a minimum blocksize of 128 threads.
  if (local_id < 32)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 32];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 16)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 16];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 8)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 8];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 4)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 4];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 2)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 2];
  barrier(CLK_LOCAL_MEM_FENCE);

  // Ok, we are done, write the result back.
  if (local_id == 0) {
    result[get_group_id(0)] = buffer[0] + buffer[1];
  }
}

__kernel void reduce_bitcount_par(
    __global const int* const data,
    __global unsigned int* const result,
    const unsigned int tuples_per_thread,
    __global const unsigned int* const bitmap,
    const unsigned int options
){
  unsigned int local_id = get_local_id(0);
  unsigned int global_id = get_global_id(0);
  // Now each thread does a sequential aggregation within a register.
  unsigned int agg = 0;
  if (bitmap) {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    // Reserve a buffer to load a chunk of the bitmap into shared memory
    __local unsigned int local_bitmap[32]; // Maximum size of the bitmap for a stride size of 1024 elements.
    unsigned int bit_pattern = 0x1 << (local_id%32);
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      // Transfer the bitmap chunk for this stride into shared memory.
      if (local_id < (get_local_size(0)/32))
        local_bitmap[local_id] = bitmap[(group_start+i*get_local_size(0))/32 + local_id];
      barrier(CLK_LOCAL_MEM_FENCE);
      // Now do the local aggregation for this stride.
      int val = data[group_start + i*get_local_size(0) + local_id];
      agg += ((local_bitmap[local_id/32] & bit_pattern) ? bitcount(val) : 0);
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    unsigned int local_index = tuples_per_thread*global_id;
    unsigned int local_bitmap = bitmap[local_index/32];
    unsigned int bitmask = 0x1 << local_index%32;
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      if (local_index%32 == 0) {
        local_bitmap = bitmap[local_index/32];
        bitmask = 0x1;
      }
      int val = data[local_index];
      agg += ((local_bitmap & bitmask) ? bitcount(val) : 0);
      bitmask <<= 1;
      local_index++;
    }
#endif
  } else {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      int val = data[group_start + i*get_local_size(0) + local_id];
      agg += bitcount(val);
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      int val = data[tuples_per_thread*global_id + i];
      agg += bitcount(val);
    }
#endif
  }

  // Push the local result to the buffer, so we can aggregate recursively. Since the
  // kernel is very simplistic, we can safely assume that it is always run with MAXBLOCKSIZE.
  __local unsigned int buffer[MAXBLOCKSIZE > 1024 ? 1024 : MAXBLOCKSIZE];
  buffer[local_id] = agg;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Recursively aggregate the tuples in memory.
#if (MAXBLOCKSIZE >= 1024) 
  if (local_id < 512)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 512];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 512) 
  if (local_id < 256)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 256];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 256) 
  if (local_id < 128)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 128];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 128) 
  if (local_id < 64)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 64];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  // We assume there is a minimum blocksize of 128 threads.
  if (local_id < 32)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 32];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 16)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 16];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 8)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 8];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 4)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 4];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 2)
    buffer[local_id] = buffer[local_id] + buffer[local_id + 2];
  barrier(CLK_LOCAL_MEM_FENCE);

  // Ok, we are done, write the result back.
  if (local_id == 0) {
    result[get_group_id(0)] = buffer[0] + buffer[1];
  }
}

__kernel void TYPED_KERNEL(reduce_sum_par) (
    __global const T* const data,
    __global REDUCE_SUM_RTYPE* const result,
    const unsigned int tuples_per_thread,
    __global const unsigned int* const bitmap,
    const unsigned int options
){
  unsigned int local_id = get_local_id(0);
  unsigned int global_id = get_global_id(0);
  // Now each thread does a sequential aggregation within a register.
  REDUCE_SUM_RTYPE agg = 0;
  if (bitmap) {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    // Reserve a buffer to load a chunk of the bitmap into shared memory
    __local unsigned int local_bitmap[32]; // Maximum size of the bitmap for a stride size of 1024 elements.
    unsigned int bit_pattern = 0x1 << (local_id%32);
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      // Transfer the bitmap chunk for this stride into shared memory.
      if (local_id < (get_local_size(0)/32))
        local_bitmap[get_local_id(0)] = bitmap[(group_start+i*get_local_size(0))/32 + local_id];
      barrier(CLK_LOCAL_MEM_FENCE);
      // Now do the local aggregation for this stride.
      T val = data[group_start + i*get_local_size(0) + local_id];
      agg += ((local_bitmap[local_id/32] & bit_pattern) && val!=NIL) ? val : 0;
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    unsigned int local_index = tuples_per_thread*global_id;
    unsigned int local_bitmap = bitmap[local_index/32];
    unsigned int bitmask = 0x1 << local_index%32;
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      if (local_index%32 == 0) {
        local_bitmap = bitmap[local_index/32];
        bitmask = 0x1;
      }
      T val = data[local_index];
      agg += ((local_bitmap & bitmask) && val!=NIL) ? val : 0;
      bitmask <<= 1;
      local_index++;
    }
#endif
  } else {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      T val = data[group_start + i*get_local_size(0) + local_id];
      agg += (val != NIL) ? val : 0;
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      T val = data[tuples_per_thread*global_id + i];
      agg += (val != NIL) ? val : 0;
    }
#endif	
  }
  // Push the local result to the buffer, so we can aggregate recursively. Since the
  // kernel is very simplistic, we can safely assume that it is always run with MAXBLOCKSIZE.
  __local REDUCE_SUM_RTYPE buffer[MAXBLOCKSIZE > 1024 ? 1024 : MAXBLOCKSIZE];
  buffer[local_id] = agg;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Recursively aggregate the tuples in memory.
#if (MAXBLOCKSIZE >= 1024) 
  if (local_id < 512)
    buffer[local_id] += buffer[local_id + 512];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 512) 
  if (local_id < 256)
    buffer[local_id] += buffer[local_id + 256];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 256) 
  if (local_id < 128)
    buffer[local_id] += buffer[local_id + 128];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 128) 
  if (local_id < 64)
    buffer[local_id] += buffer[local_id + 64];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  // We assume there is a minimum blocksize of 128 threads.
  if (local_id < 32)
    buffer[local_id] += buffer[local_id + 32];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 16)
    buffer[local_id] += buffer[local_id + 16];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 8)
    buffer[local_id] += buffer[local_id + 8];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 4)
    buffer[local_id] += buffer[local_id + 4];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 2)
    buffer[local_id] += buffer[local_id + 2];
  barrier(CLK_LOCAL_MEM_FENCE);

  // Ok, we are done, write the result back.
  if (local_id == 0) {
    result[get_group_id(0)] = buffer[0] + buffer[1];
  }
} 

__kernel void reduce_sum_int_long_par(
    __global const int* const data,
    __global long* const result,
    const unsigned int tuples_per_thread,
    __global const unsigned int* const bitmap,
    const unsigned int options
){
  unsigned int local_id = get_local_id(0);
  unsigned int global_id = get_global_id(0);
  // Now each thread does a sequential aggregation within a register.
  long agg = 0;
  if (bitmap) {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    // Reserve a buffer to load a chunk of the bitmap into shared memory
    __local unsigned int local_bitmap[32]; // Maximum size of the bitmap for a stride size of 1024 elements.
    unsigned int bit_pattern = 0x1 << (local_id%32);
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      // Transfer the bitmap chunk for this stride into shared memory.
      if (local_id < (get_local_size(0)/32))
        local_bitmap[get_local_id(0)] = bitmap[(group_start+i*get_local_size(0))/32 + local_id];
      barrier(CLK_LOCAL_MEM_FENCE);
      // Now do the local aggregation for this stride.
      int val = data[group_start + i*get_local_size(0) + local_id];
      agg += ((local_bitmap[local_id/32] & bit_pattern) && val!=NIL) ? val : 0;
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    unsigned int local_index = tuples_per_thread*global_id;
    unsigned int local_bitmap = bitmap[local_index/32];
    unsigned int bitmask = 0x1 << local_index%32;
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      if (local_index%32 == 0) {
        local_bitmap = bitmap[local_index/32];
        bitmask = 0x1;
      }
      int val = data[local_index];
      agg += ((local_bitmap & bitmask) && val!=NIL) ? val : 0;
      bitmask <<= 1;
      local_index++;
    }
#endif
  } else {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      int val = data[group_start + i*get_local_size(0) + local_id];
      agg += (val!=NIL) ? val : 0;
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      int val = data[tuples_per_thread*global_id + i];
      agg += (val!=NIL) ? val : 0;
    }
#endif
  }

  // Push the local result to the buffer, so we can aggregate recursively. Since the
  // kernel is very simplistic, we can safely assume that it is always run with MAXBLOCKSIZE.
  __local long buffer[MAXBLOCKSIZE > 1024 ? 1024 : MAXBLOCKSIZE];
  buffer[local_id] = agg;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Recursively aggregate the tuples in memory.
#if (MAXBLOCKSIZE >= 1024) 
  if (local_id < 512)
    buffer[local_id] += buffer[local_id + 512];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 512) 
  if (local_id < 256)
    buffer[local_id] += buffer[local_id + 256];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 256) 
  if (local_id < 128)
    buffer[local_id] += buffer[local_id + 128];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 128) 
  if (local_id < 64)
    buffer[local_id] += buffer[local_id + 64];
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  // We assume there is a minimum blocksize of 128 threads.
  if (local_id < 32)
    buffer[local_id] += buffer[local_id + 32];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 16)
    buffer[local_id] += buffer[local_id + 16];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 8)
    buffer[local_id] += buffer[local_id + 8];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 4)
    buffer[local_id] += buffer[local_id + 4];
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 2)
    buffer[local_id] += buffer[local_id + 2];
  barrier(CLK_LOCAL_MEM_FENCE);

  // Ok, we are done, write the result back.
  if (local_id == 0) {
    result[get_group_id(0)] = buffer[0] + buffer[1];
  }
} 

__kernel void TYPED_KERNEL(reduce_min_par)(
    __global const T* const data,
    __global T* const result,
    const unsigned int tuples_per_thread,
    __global const unsigned int* const bitmap,
    const unsigned int options
){
  unsigned int local_id = get_local_id(0);
  unsigned int global_id = get_global_id(0);
  // Now each thread does a sequential aggregation within a register.
  T agg = MAX;
  if (bitmap) {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    {
      // Reserve a buffer to load a chunk of the bitmap into shared memory
      __local unsigned int local_bitmap[32]; // Maximum size of the bitmap for a stride size of 1024 elements.
      unsigned int bit_pattern = 0x1 << (local_id%32);
      unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
      for (unsigned int i=0; i < tuples_per_thread; ++i) {
        // Transfer the bitmap chunk for this stride into shared memory.
        if (local_id < (get_local_size(0)/32))
          local_bitmap[get_local_id(0)] = bitmap[(group_start+i*get_local_size(0))/32 + local_id];
        barrier(CLK_LOCAL_MEM_FENCE);
        // Now do the local aggregation for this stride.
        T val = data[group_start + i*get_local_size(0) + local_id];
        val = val==NIL ? MAX : val;
        agg = min(agg, (T)((local_bitmap[local_id/32] & bit_pattern) ? val : MAX));
      }
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    unsigned int local_index = tuples_per_thread*global_id;
    unsigned int local_bitmap = bitmap[local_index/32];
    unsigned int bitmask = 0x1 << local_index%32;
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      if (local_index%32 == 0) {
        local_bitmap = bitmap[local_index/32];
        bitmask = 0x1;
      }
      T val = data[local_index];
      val = val==NIL ? MAX : val;
      agg = min(agg, (T)((local_bitmap & bitmask) ? val : MAX));
      bitmask <<= 1;
      local_index++;
    }
#endif
  } else {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      T val = data[group_start + i*get_local_size(0) + local_id];
      val = val==NIL ? MAX : val;
      agg = min(agg, val);
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      T val = data[tuples_per_thread*global_id + i];
      val = val==NIL ? MAX : val;
      agg = min(agg, val);
    }
#endif
  }
  // Push the local result to the buffer, so we can aggregate recursively. Since the
  // kernel is very simplistic, we can safely assume that it is always run with MAXBLOCKSIZE.
  __local T buffer[MAXBLOCKSIZE > 1024 ? 1024 : MAXBLOCKSIZE];
  buffer[local_id] = agg;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Recursively aggregate the tuples in memory.
#if (MAXBLOCKSIZE >= 1024) 
  if (local_id < 512)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 512]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 512) 
  if (local_id < 256)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 256]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 256) 
  if (local_id < 128)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 128]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 128) 
  if (local_id < 64)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 64]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  // We assume there is a minimum blocksize of 128 threads.
  if (local_id < 32)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 32]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 16)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 16]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 8)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 8]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 4)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 4]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 2)
    buffer[local_id] = min(buffer[local_id], buffer[local_id + 2]);
  barrier(CLK_LOCAL_MEM_FENCE);

  // Ok, we are done, write the result back.
  if (local_id == 0) {
    result[get_group_id(0)] = min(buffer[0], buffer[1]);
  }
}

__kernel void TYPED_KERNEL(reduce_max_par)(
    __global const T* const data,
    __global T* const result,
    const unsigned int tuples_per_thread,
    __global const unsigned int* const bitmap,
    const unsigned int options
){
  unsigned int local_id = get_local_id(0);
  unsigned int global_id = get_global_id(0);
  // Now each thread does a sequential aggregation within a register.
  T agg = MIN;
  if (bitmap) {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    // Reserve a buffer to load a chunk of the bitmap into shared memory
    __local unsigned int local_bitmap[32]; // Maximum size of the bitmap for a stride size of 1024 elements.
    unsigned int bit_pattern = 0x1 << (local_id%32);
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      // Transfer the bitmap chunk for this stride into shared memory.
      if (local_id < (get_local_size(0)/32))
        local_bitmap[get_local_id(0)] = bitmap[(group_start+i*get_local_size(0))/32 + local_id];
      barrier(CLK_LOCAL_MEM_FENCE);
      // Now do the local aggregation for this stride.
      agg = max(agg, (T)((local_bitmap[local_id/32] & bit_pattern) ? data[group_start + i*get_local_size(0) + local_id] : MIN));
    }
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    unsigned int local_index = tuples_per_thread*global_id;
    unsigned int local_bitmap = bitmap[local_index/32];
    unsigned int bitmask = 0x1 << local_index%32;
    for (unsigned int i=0; i < tuples_per_thread; ++i) {
      if (local_index%32 == 0) {
        local_bitmap = bitmap[local_index/32];
        bitmask = 0x1;
      }
      agg = max(agg, (T)((local_bitmap & bitmask) ? data[local_index] : MIN));
      bitmask <<= 1;
      local_index++;
    }
#endif
  } else {
#ifdef DEVICE_GPU
    // On the GPU we use a strided access pattern, so that the GPU
    // can coalesc memory access.
    unsigned int group_start = get_local_size(0)*tuples_per_thread*get_group_id(0);
    for (unsigned int i=0; i < tuples_per_thread; ++i)
      agg = max(agg, data[group_start + i*get_local_size(0) + local_id]);
#elif defined DEVICE_CPU
    // On the CPU we use a sequential access pattern to keep cache misses
    // local per thread.
    for (unsigned int i=0; i < tuples_per_thread; ++i)
      agg = max(agg, data[tuples_per_thread*global_id + i]);
#endif
  }

  // Push the local result to the buffer, so we can aggregate recursively. Since the
  // kernel is very simplistic, we can safely assume that it is always run with MAXBLOCKSIZE.
  __local T buffer[MAXBLOCKSIZE > 1024 ? 1024 : MAXBLOCKSIZE];
  buffer[local_id] = agg;
  barrier(CLK_LOCAL_MEM_FENCE);

  // Recursively aggregate the tuples in memory.
#if (MAXBLOCKSIZE >= 1024) 
  if (local_id < 512)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 512]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 512) 
  if (local_id < 256)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 256]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 256) 
  if (local_id < 128)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 128]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

#if (MAXBLOCKSIZE >= 128) 
  if (local_id < 64)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 64]);
  barrier(CLK_LOCAL_MEM_FENCE);
#endif

  // We assume there is a minimum blocksize of 128 threads.
  if (local_id < 32)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 32]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 16)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 16]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 8)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 8]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 4)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 4]);
  barrier(CLK_LOCAL_MEM_FENCE);

  if (local_id < 2)
    buffer[local_id] = max(buffer[local_id], buffer[local_id + 2]);
  barrier(CLK_LOCAL_MEM_FENCE);

  // Ok, we are done, write the result back.
  if (local_id == 0) {
    result[get_group_id(0)] = max(buffer[0], buffer[1]);
  }
}
