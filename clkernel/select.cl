/* select.cl
 *
 * Collection of select kernels.
 *
 */

// TYPE DEFINITION.
#include "types.h"

// Macro to evaluate an arbitrary range predicate.
// Macro to evaluate an arbitrary range predicate.
#define EVAL_PREDICATE(val) {              		\
   pred = (!lowerSet) || (val>lower);           \
   pred &= (!upperSet) || (val<upper);          \
   pred |= (val == lower) && (lowerInclusive);  \
   pred |= (val == upper) && (upperInclusive);  \
   pred = anti ? !pred : pred;                  \
   pred = (val == NIL) ? 0 : pred;				\
   pattern |= pred ? (0x1 << offset) : 0;       \
   offset++;                                    \
}

__kernel void TYPED_KERNEL(theta_select)(
	__global const T* data,         // Input buffer.
	__global unsigned char* result,	// Result buffer.
	const unsigned int elements,    // Total number of tuples.
	const T lower,                  // Lower range bound
	const T upper,                  // Upper range bound
	const char lowerSet,            // Is the lower value set or is it NULL?
	const char upperSet,            // Is the upper value set or is it NULL?
	const char lowerInclusive,      // Lower range inclusive?
	const char upperInclusive,      // Upper range inclusive?
	const char anti                 // Negate predicate?
) {
	// Read position.
  unsigned int pos = 8 * get_global_id(0);
	// The local bit pattern
	unsigned char pattern = 0;
	unsigned char offset = 0;
	// local predicate variable
	bool pred;
  for (unsigned int i = 0; i < 8; ++i) {
    if (pos >= elements) {
			break;	// Don't overshoot.
    }

		T val = data[pos++];
		EVAL_PREDICATE(val);
	}
	
	// and store back
  if (8 * get_global_id(0) < elements) {
    result[get_global_id(0)] = pattern;
  } else {
    result[get_global_id(0)] = 0;
  }
}

// ISNIL for BAT.
__kernel void TYPED_KERNEL(isNil)(
   __global const T* data,
   __global unsigned char* result,
	unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
	   	if (pos >= elements)
	   		break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = data[pos] == NIL;
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
		pos++;
	}
	result[get_global_id(0)] = pattern;
}

// ISEQ for BAT.
__kernel void TYPED_KERNEL(isEq)(
   __global const T* data,
   const T value,
   __global unsigned char* result,
	unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
	   	if (pos >= elements)
	   		break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = data[pos] == value;
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
		pos++;
	}
	result[get_global_id(0)] = pattern;
}

// ISNEQ for BAT.
__kernel void TYPED_KERNEL(isNeq)(
   __global const T* data,
   const T value,
   __global unsigned char* result,
	unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
	   	if (pos >= elements)
	   		break;	// Prevent overshooting.
		// Evaluate the predicate.
		char pred = data[pos] != value;
		// Set the corresponding bit.
		pattern |= pred ? (0x1 << offset) : 0;
  	 	// And move on.
		offset++;
		pos++;
	}
	result[get_global_id(0)] = pattern;
}

  // ISLT for BAT.
__kernel void TYPED_KERNEL(isLt)(
   __global const T* data,
   const T value,
   __global unsigned char* result,
   unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
     if (pos >= elements)
       break;  // Prevent overshooting.
    // Evaluate the predicate.
    char pred = data[pos] != NIL;
    pred &= data[pos] < value;
    // Set the corresponding bit.
    pattern |= pred ? (0x1 << offset) : 0;
      // And move on.
    offset++;
    pos++;
  }
  result[get_global_id(0)] = pattern;
}

  // ISLTEQ for BAT.
__kernel void TYPED_KERNEL(isLtEq)(
   __global const T* data,
   const T value,
   __global unsigned char* result,
   unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
     if (pos >= elements)
       break;  // Prevent overshooting.
    // Evaluate the predicate.
    char pred = data[pos] != NIL;
    pred &= data[pos] <= value;
    // Set the corresponding bit.
    pattern |= pred ? (0x1 << offset) : 0;
      // And move on.
    offset++;
    pos++;
  }
  result[get_global_id(0)] = pattern;
}

// ISGT for BAT.
__kernel void TYPED_KERNEL(isGt)(
   __global const T* data,
   const T value,
   __global unsigned char* result,
  unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
      if (pos >= elements)
        break;  // Prevent overshooting.
    // Evaluate the predicate.
    char pred = data[pos] > value;
    // Set the corresponding bit.
    pattern |= pred ? (0x1 << offset) : 0;
      // And move on.
    offset++;
    pos++;
  }
  result[get_global_id(0)] = pattern;
}

// ISGTEQ for BAT.
__kernel void TYPED_KERNEL(isGtEq)(
   __global const T* data,
   const T value,
   __global unsigned char* result,
   unsigned int elements
) {
   unsigned int pos = 8 * get_global_id(0);
   unsigned char pattern = 0;
   unsigned char offset = 0;
   for (unsigned int i=0; i<8; ++i) {
     if (pos >= elements)
       break;  // Prevent overshooting.
    // Evaluate the predicate.
    char pred = data[pos] != NIL;
    pred &= data[pos] >= value;
    // Set the corresponding bit.
    pattern |= pred ? (0x1 << offset) : 0;
      // And move on.
    offset++;
    pos++;
  }
  result[get_global_id(0)] = pattern;
}
