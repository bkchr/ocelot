#include "hash.h"

__kernel void prepareIDTable( 
	__global const int* hash_table,
	__global unsigned int* id_table
) {
  int val = hash_table[get_global_id(0)];
  id_table[get_global_id(0)] = (val == 0) ? 0 : 1;
}

__kernel void computeGroupingHT(
    __global const int* data,
    __global const int* hash_table,
    const unsigned int hash_table_size,
    __global const unsigned int* id_table,
    __global unsigned int* group_assignments
) {
  int value = data[get_global_id(0)];
  // Find the group id.
  unsigned int group = id_table[findInHashtable(value,
      hash_table, hash_table_size)];
  group_assignments[get_global_id(0)] = group;
}

__kernel void computeGroupingSorted(
	__global const int* data,
	__global unsigned int* group_assignments
) {
	unsigned int gid = get_global_id(0);
	if (gid == get_global_size(0) - 1)
		group_assignments[gid] = 1;
 	else
		group_assignments[gid] = data[gid] == data[gid + 1] ? 0 : 1;
}

__kernel void generateExtentsTable(
	__global const unsigned int* group_assignments,
	__global unsigned int* group_examples
) {
	unsigned int group = group_assignments[get_global_id(0)];
	group_examples[group] = get_global_id(0);
}

__kernel void combineGroupIDs(
	__global const unsigned int* left_grouping,
	__global const unsigned int* right_grouping,
	unsigned int left_groups,
	__global unsigned int* new_grouping
) {
	new_grouping[get_global_id(0)] 
		= left_groups * right_grouping[get_global_id(0)] 
		+ left_grouping[get_global_id(0)];
}

__kernel void fillGroupIDMap(
	__global const unsigned int* groups,
	__global unsigned int* group_map
) {
	unsigned int group = groups[get_global_id(0)];
	group_map[group] = 1;
}

__kernel void mapGroupIDs(
	__global unsigned int* groups,
	__global const unsigned int* group_map
) {
	int group = groups[get_global_id(0)]; 
	group = group_map[group];
	groups[get_global_id(0)] = group;
}
