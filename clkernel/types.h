#ifndef TYPES_H_
#define TYPES_H_

// Required OpenCL extension
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

#define DTypeS8 0
#define DTypeS16 1
#define DTypeS32 2
#define DTypeS64 3
#define DTypeF32 5

// Type system. Assumes that a type T is defined before.
#ifndef TYPE
#error No type defined!
#endif

// Wrapper for atomic functions.
// ADDITION IN GLOBAL MEMORY.
inline void AtomicAddChar(volatile __global char* src, const char val) {
  union {
    unsigned int i;
    char c;
  } prev;
  union {
    unsigned int i;
    char c;
  } new;
  do {
    prev.i = new.i = *(__global unsigned int*)src;
    new.c = prev.c + val;
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// ADDITION IN LOCAL MEMORY.
inline void AtomicAddLocalChar(volatile __local char* src, const char val) {
  union {
    unsigned int i;
    char c;
  } prev;
  union {
    unsigned int i;
    char c;
  } new;
  do {
    prev.i = new.i = *(__local unsigned int*)src;
    new.c = prev.c + val;
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MINIMUM IN GLOBAL MEMORY
inline void AtomicMinChar(volatile __global char* src, const char val) {
  union {
    unsigned int i;
    char c;
  } prev;
  union {
    unsigned int i;
    char c;
  } new;
  do {
    prev.i = new.i = *(__global unsigned int*)src;
    new.c = min(prev.c, val);
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MINIMUM IN LOCAL MEMORY.
inline void AtomicMinLocalChar(volatile __local char* src, const char val) {
  union {
    unsigned int i;
    char c;
  } prev;
  union {
    unsigned int i;
    char c;
  } new;
  do {
    prev.i = new.i = *(__local unsigned int*)src;
    new.c = min(prev.c, val);
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MAXIMUM IN GLOBAL MEMORY
inline void AtomicMaxChar(volatile __global char* src, const char val) {
  union {
    unsigned int i;
    char c;
  } prev;
  union {
    unsigned int i;
    char c;
  } new;
  do {
    prev.i = new.i = *(__global unsigned int*)src;
    new.c = max(prev.c, val);
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MAXIMUM IN LOCAL MEMORY.
inline void AtomicMaxLocalChar(volatile __local char* src, const char val) {
  union {
    unsigned int i;
    char c;
  } prev;
  union {
    unsigned int i;
    char c;
  } new;
  do {
    prev.i = new.i = *(__local unsigned int*)src;
    new.c = max(prev.c, val);
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}

// Wrapper for atomic functions.
// ADDITION IN GLOBAL MEMORY.
inline void AtomicAddShort(volatile __global short* src, const short val) {
  union {
    unsigned int i;
    short s;
  } prev;
  union {
    unsigned int i;
    short s;
  } new;
  do {
    prev.i = new.i = *(__global unsigned int*)src;
    new.s = prev.s + val;
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// ADDITION IN LOCAL MEMORY.
inline void AtomicAddLocalShort(volatile __local short* src, const short val) {
  union {
    unsigned int i;
    short s;
  } prev;
  union {
    unsigned int i;
    short s;
  } new;
  do {
    prev.i = new.i = *(__local unsigned int*)src;
    new.s = prev.s + val;
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MINIMUM IN GLOBAL MEMORY
inline void AtomicMinShort(volatile __global short* src, const short val) {
  union {
    unsigned int i;
    short s;
  } prev;
  union {
    unsigned int i;
    short s;
  } new;
  do {
    prev.i = new.i = *(__global unsigned int*)src;
    new.s = min(prev.s, val);
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MINIMUM IN LOCAL MEMORY.
inline void AtomicMinLocalShort(volatile __local short* src, const short val) {
  union {
    unsigned int i;
    short s;
  } prev;
  union {
    unsigned int i;
    short s;
  } new;
  do {
    prev.i = new.i = *(__local unsigned int*)src;
    new.s = min(prev.s, val);
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MAXIMUM IN GLOBAL MEMORY
inline void AtomicMaxShort(volatile __global short* src, const short val) {
  union {
    unsigned int i;
    short s;
  } prev;
  union {
    unsigned int i;
    short s;
  } new;
  do {
    prev.i = new.i = *(__global unsigned int*)src;
    new.s = max(prev.s, val);
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MAXIMUM IN LOCAL MEMORY.
inline void AtomicMaxLocalShort(volatile __local short* src, const short val) {
  union {
    unsigned int i;
    short s;
  } prev;
  union {
    unsigned int i;
    short s;
  } new;
  do {
    prev.i = new.i = *(__local unsigned int*)src;
    new.s = max(prev.s, val);
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}

// Wrapper for atomic functions.
// ADDITION IN GLOBAL MEMORY.
inline void AtomicAddInt(volatile __global int* src, const int val) {
  atomic_add(src, val);
}
// ADDITION IN LOCAL MEMORY.
inline void AtomicAddLocalInt(volatile __local int* src, const int val) {
  atomic_add(src, val);
}
// MINIMUM IN GLOBAL MEMORY
inline void AtomicMinInt(volatile __global int* src, const int val) {
  atomic_min(src, val);
}
// MINIMUM IN LOCAL MEMORY.
inline void AtomicMinLocalInt(volatile __local int* src, const int val) {
  atomic_min(src, val);
}
// MAXIMUM IN GLOBAL MEMORY
inline void AtomicMaxInt(volatile __global int* src, const int val) {
  atomic_max(src, val);
}
// MAXIMUM IN LOCAL MEMORY.
inline void AtomicMaxLocalInt(volatile __local int* src, const int val) {
  atomic_max(src, val);
}

// Atomic functions for float using atomic_cmpxchg, as this is undefined
// in the OpenCL standard.
// ADDITION IN GLOBAL MEMORY.
inline void AtomicAddFloat(volatile __global float* src, const float val) {
  union {
    unsigned int i;
    float f;
  } prev;
  union {
    unsigned int i;
    float f;
  } new;
  do {
    prev.f = *src;
    new.f = prev.f + val;
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// ADDITION IN LOCAL MEMORY.
inline void AtomicAddLocalFloat(volatile __local float* src, const float val) {
  union {
    unsigned int i;
    float f;
  } prev;
  union {
    unsigned int i;
    float f;
  } new;
  do {
    prev.f = *src;
    new.f = prev.f + val;
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MINIMUM IN GLOBAL MEMORY.
inline void AtomicMinFloat(volatile __global float* src, const float val) {
  union {
    unsigned int i;
    float f;
  } prev;
  union {
    unsigned int i;
    float f;
  } new;
  do {
    prev.f = *src;
    new.f = min(prev.f, val);
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MINIMUM IN LOCAL MEMORY.
inline void AtomicMinLocalFloat(volatile __local float* src, const float val) {
  union {
    unsigned int i;
    float f;
  } prev;
  union {
    unsigned int i;
    float f;
  } new;
  do {
    prev.f = *src;
    new.f = min(prev.f, val);
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MAXIMUM IN GLOBAL MEMORY.
inline void AtomicMaxFloat(volatile __global float* src, const float val) {
  union {
    unsigned int i;
    float f;
  } prev;
  union {
    unsigned int i;
    float f;
  } new;
  do {
    prev.f = *src;
    new.f = max(prev.f, val);
  } while (atomic_cmpxchg((volatile __global unsigned int*)src, prev.i, new.i)
           != prev.i);
}
// MAXIMUM IN LOCAL MEMORY.
inline void AtomicMaxLocalFloat(volatile __local float* src, const float val) {
  union {
    unsigned int i;
    float f;
  } prev;
  union {
    unsigned int i;
    float f;
  } new;
  do {
    prev.f = *src;
    new.f = max(prev.f, val);
  } while (atomic_cmpxchg((volatile __local unsigned int*)src, prev.i, new.i)
           != prev.i);
}

// Now define the types
#if TYPE == DTypeS8
  // Type definition
  #define T char
  // Naming macro.
  #define TYPED_KERNEL(kernel) kernel
  // Important constants.
  #define NIL CHAR_MIN
  #define MIN CHAR_MIN
  #define MAX CHAR_MAX

  #define AtomicAdd AtomicAddChar
  #define AtomicAddLocal AtomicAddLocalChar
  #define AtomicMin AtomicMinChar
  #define AtomicMinLocal AtomicMinLocalChar
  #define AtomicMax AtomicMaxChar
  #define AtomicMaxLocal AtomicMaxLocalChar

#elif TYPE == DTypeS16
  // Type definition
  #define T short
  // Naming macro.
  #define TYPED_KERNEL(kernel) kernel
  // Important constants.
  #define NIL SHRT_MIN
  #define MIN SHRT_MIN
  #define MAX SHRT_MAX

  #define AtomicAdd AtomicAddShort
  #define AtomicAddLocal AtomicAddLocalShort
  #define AtomicMin AtomicMinShort
  #define AtomicMinLocal AtomicMinLocalShort
  #define AtomicMax AtomicMaxShort
  #define AtomicMaxLocal AtomicMaxLocalShort

#elif TYPE == DTypeS32
  // Type definition
  #define T int
  // Naming macro.
  #define TYPED_KERNEL(kernel) kernel
  // Important constants.
  #define NIL INT_MIN
  #define MIN INT_MIN
  #define MAX INT_MAX

  #define AtomicAdd AtomicAddInt
  #define AtomicAddLocal AtomicAddLocalInt
  #define AtomicMin AtomicMinInt
  #define AtomicMinLocal AtomicMinLocalInt
  #define AtomicMax AtomicMaxInt
  #define AtomicMaxLocal AtomicMaxLocalInt

#elif TYPE == DTypeS64
  // Value definition
  #define T long
  // Naming Macro:
  #define TYPED_KERNEL(kernel) kernel
  // Important constants:
  #define NIL LONG_MIN
  #define MIN LONG_MIN
  #define MAX LONG_MAX

  // we don't support atomic operations for long!
  #define AtomicAdd(x, y)
  #define AtomicAddLocal(x, y)
  #define AtomicMin(x, y)
  #define AtomicMinLocal(x, y)
  #define AtomicMax(x, y)
  #define AtomicMaxLocal(x, y)

#elif TYPE == DTypeF32
  // Value definition
  #define T float
  // Naming Macro:
  #define TYPED_KERNEL(kernel) kernel
  // Important constants:
  #define NIL (-((float)FLT_MAX))
  #define MIN (-FLT_MAX)
  #define MAX (FLT_MAX)

  #define AtomicAdd AtomicAddFloat
  #define AtomicAddLocal AtomicAddLocalFloat
  #define AtomicMin AtomicMinFloat
  #define AtomicMinLocal AtomicMinLocalFloat
  #define AtomicMax AtomicMaxFloat
  #define AtomicMaxLocal AtomicMaxLocalFloat

#else
  #error Undefined type!
#endif

#endif  // TYPES_H_
