__kernel void partition_count(
	__global const int * input,
	__global unsigned int * output,
	const unsigned int elements,
	const unsigned int partition_num,
	const unsigned int thread_number,
	const unsigned int tuples_per_thread)
{
	unsigned int threadid = get_global_id(0);
	unsigned int start_pos = threadid * tuples_per_thread;
	unsigned int i = 0;
	unsigned int mask = partition_num - 1;

	if(threadid >= thread_number)
		return;

	for (i = 0; i < tuples_per_thread && start_pos + i < elements; ++i)
	{
		unsigned int partition = input[start_pos + i] & mask;

		output[partition * thread_number + threadid]++;
	}
}

__kernel void partition_check_size(
	__global const unsigned int * offsets,
	__global unsigned int * partition_start_pos,
	__global unsigned char * failed,
	const unsigned int elements,
	const unsigned int partition_num,
	const unsigned int thread_number,
	const unsigned int max_elements_per_partition)
{
	unsigned int threadid = get_global_id(0);

	if(threadid != 0)
		return;

	*failed = 0;

	unsigned int i = 0;
	unsigned int last_start_pos = 0;
	for (i = 0; i < partition_num - 1; ++i)
	{
		unsigned int offset = offsets[thread_number * (i + 1)];
		unsigned int size = offset - last_start_pos;

		if(size	> max_elements_per_partition)
		{
			*failed = 1;
			return;
		}

		partition_start_pos[i] = last_start_pos;
		last_start_pos = offset;
	}

	partition_start_pos[partition_num - 1] = last_start_pos;

	if(elements - last_start_pos > max_elements_per_partition)
		*failed = 1;
}

__kernel void partition(
	__global const int * input,
	__global unsigned int * offsets,
	__global unsigned int * output_indices,
	const unsigned int elements,
	const unsigned int partition_num,
	const unsigned int thread_number,
	const unsigned int tuples_per_thread)
{
	unsigned int threadid = get_global_id(0);
	unsigned int start_pos = threadid * tuples_per_thread;

	unsigned int i = 0;
	unsigned int mask = partition_num - 1;
	for (i = 0; i < tuples_per_thread && start_pos + i < elements; ++i)
	{
		unsigned int partition = input[start_pos + i] & mask;
		unsigned int output_pos = offsets[partition * thread_number + threadid]++;

		output_indices[output_pos] = start_pos + i;
	}
}
