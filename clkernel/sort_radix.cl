#ifdef DEVICE_GPU
#define SORT_BITS 4
#define SORT_RADIX 16
#else
#define SORT_BITS 8
#define SORT_RADIX 256
#endif


// change of index for the transposition
int index(const unsigned int i, const unsigned int n, const unsigned int elementsPerThread)
{
	int ip;
#ifdef DEVICE_GPU
	// transpose for
	if(i < ((n / (get_local_size(0) * elementsPerThread)) * get_local_size(0) * elementsPerThread))
	{
		int k,l, globalTranspose;
		k = i / elementsPerThread; 
		l = i % elementsPerThread; 
		globalTranspose = ((n % (get_local_size(0) * elementsPerThread)) ? (get_num_groups(0) - 1) * get_local_size(0) : get_global_size(0));
		ip = l * globalTranspose + k;
	}
	else
	{
    ip = i;
	}
#elif defined DEVICE_CPU
  ip = i;
#endif
	return ip;
}

// change location to copy to global histogram based on sort direction
int sortDirOffset(const unsigned int ir, const unsigned int sortDir)
{
	if(sortDir == 1)
		return ir; 
	else
		return SORT_RADIX - 1 - ir;
}

unsigned int convertInt(int input)
{
	unsigned int i = 0;
	if (input < 0)
	{
		int tmp = input + INT_MAX;
		i = tmp;
	}
	else
	{
		i = input;
		i += INT_MAX;
	}
	return i;
}

int convertUInt(unsigned int input)
{
	int i = 0;
	if (input > INT_MAX) {
		unsigned int tmp = input - INT_MAX;
		i = tmp;
	}
	else
	{
		i = input;
		i -= INT_MAX;
	}
	return i;
}

// compute the histogram for each radix and each virtual processor for the pass
// converts int to unsigned int to get the sort order right
__kernel void histogram_par_convert(
	__global int * d_Keys,
	__global unsigned int * d_Histograms,
	const unsigned int pass,
	__local unsigned int * loc_histo,
	const unsigned int sortDir,
	const unsigned int n,
	const unsigned int elementsPerThread
){
  size_t li = get_local_id(0);
  size_t gi = get_global_id(0);

  // set the local histograms to zero
  for(unsigned int ir = 0; ir < SORT_RADIX; ++ir)
  {
	  loc_histo[ir * get_local_size(0) + li] = 0;
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  // range of keys that are analyzed by the work item
  size_t start = gi * elementsPerThread;
  unsigned int shortkey;

  for(size_t i = start; (i < (start + elementsPerThread)) && (i < n); ++i)
  {
	  // extract the group of _BITS bits of the pass
	  // the result is in the range 0.._RADIX-1
	  unsigned int v = convertInt(d_Keys[index(i,n,elementsPerThread)]);
	  d_Keys[index(i,n,elementsPerThread)] = v;
	  shortkey=(( v >> (pass * SORT_BITS)) & (SORT_RADIX - 1)); 
	  ++loc_histo[shortkey * get_local_size(0) + li];
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  // copy the local histogram to the global one
  for(unsigned int ir = 0; ir < SORT_RADIX; ++ir)
  {
	  d_Histograms[ir * get_global_size(0) + get_local_size(0) * get_group_id(0) + li] = loc_histo[sortDirOffset(ir, sortDir) * get_local_size(0) + li];
  }
}

__kernel void histogram_par(
	const __global unsigned int * d_Keys,
	__global unsigned int * d_Histograms,
	const unsigned int pass,
	__local unsigned int * loc_histo,
	const unsigned int sortDir,
	const unsigned int n,
	const unsigned int elementsPerThread
){
  size_t li = get_local_id(0);
  size_t gi = get_global_id(0);

  // set the local histograms to zero
  for(unsigned int ir = 0; ir < SORT_RADIX; ++ir)
  {
	  loc_histo[ir * get_local_size(0) + li] = 0;
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  // range of keys that are analyzed by the work item
  size_t start = gi * elementsPerThread;
  unsigned int shortkey;

  for(size_t i = start; (i < (start + elementsPerThread)) && (i < n); ++i)
  {
	  // extract the group of _BITS bits of the pass
	  // the result is in the range 0.._RADIX-1
	  shortkey=(( d_Keys[index(i,n,elementsPerThread)] >> (pass * SORT_BITS)) & (SORT_RADIX - 1)); 
	  ++loc_histo[shortkey * get_local_size(0) + li];
  }
  barrier(CLK_LOCAL_MEM_FENCE);

  // copy the local histogram to the global one
  for(unsigned int ir = 0; ir < SORT_RADIX; ++ir)
  {
	  d_Histograms[ir * get_global_size(0) + get_local_size(0) * get_group_id(0) + li] = loc_histo[sortDirOffset(ir, sortDir) * get_local_size(0) + li];
  }
}

// each virtual processor reorders its data using the scanned histogram
__kernel void reorder_par(
	const __global unsigned int * d_inKeys,
	__global unsigned int * d_outKeys,
	__global unsigned int * d_Histograms,
	const unsigned int pass,
	const __global int * d_inValues,
	__global int * d_outValues,
	__local unsigned int * loc_histo,
	const unsigned int sortDir,
	const unsigned int n,
	const unsigned int elementsPerThread
){
	size_t li = get_local_id(0);
	size_t gi = get_global_id(0);
	size_t start = gi * elementsPerThread;

	// take the histograms in the cache
	for(unsigned int ir = 0; ir < SORT_RADIX; ++ir){
		loc_histo[sortDirOffset(ir, sortDir) * get_local_size(0) + li]=
			d_Histograms[ir * get_global_size(0) + get_local_size(0) * get_group_id(0) + li];
	}
	barrier(CLK_LOCAL_MEM_FENCE);  

	unsigned int newpos,key,shortkey;
	for(size_t i = start; (i < (start + elementsPerThread)) && (i < n); ++i){
		key = d_inKeys[index(i,n,elementsPerThread)];
		shortkey = ((key >> (pass * SORT_BITS)) & (SORT_RADIX - 1));
		newpos = loc_histo[shortkey * get_local_size(0) + li];
		d_outKeys[index(newpos,n,elementsPerThread)] = key;
		d_outValues[index(newpos,n,elementsPerThread)] = d_inValues[index(i,n,elementsPerThread)];
		newpos++;
		loc_histo[shortkey * get_local_size(0) + li] = newpos;
	}
}

__kernel void reorder_par_convert(
	const __global unsigned int * d_inKeys,
	__global int * d_outKeys,
	__global unsigned int * d_Histograms,
	const unsigned int pass,
	const __global int * d_inValues,
	__global int * d_outValues,
	__local unsigned int * loc_histo,
	const unsigned int sortDir,
	const unsigned int n,
	const unsigned int elementsPerThread
){
	size_t li = get_local_id(0);
	size_t gi = get_global_id(0);
	size_t start = gi * elementsPerThread;

	// take the histograms in the cache
	for(unsigned int ir = 0; ir < SORT_RADIX; ++ir){
		loc_histo[sortDirOffset(ir, sortDir) * get_local_size(0) + li]=
			d_Histograms[ir * get_global_size(0) + get_local_size(0) * get_group_id(0) + li];
	}
	barrier(CLK_LOCAL_MEM_FENCE);  

	unsigned int newpos,key,shortkey;
	int convertedKey;
	for(size_t i = start; (i < (start + elementsPerThread)) && (i < n); ++i){
		key = d_inKeys[index(i,n,elementsPerThread)];
		shortkey = ((key >> (pass * SORT_BITS)) & (SORT_RADIX - 1));
		newpos = loc_histo[shortkey * get_local_size(0) + li];
		convertedKey = convertUInt(key);
		d_outKeys[index(newpos,n,elementsPerThread)] = convertedKey;
		d_outValues[index(newpos,n,elementsPerThread)] = d_inValues[index(i,n,elementsPerThread)];
		newpos++;
		loc_histo[shortkey * get_local_size(0) + li] = newpos;
	}
}

__kernel void transpose(
	const __global int * invect,
	__global int * outvect,
	const int nbcol,
	const int nbrow,
	const __global int * invalues,
	__global int * outvalues,
	__local int * blockmat,
	__local int * blockval,
	const int tilesize
){
	size_t i0 = (get_global_id(0) % (nbrow / tilesize)) * tilesize;  // first row index
	size_t j = get_global_id(0) / (nbrow / tilesize);  // column index

	size_t iloc = 0;  // first local row index
	size_t jloc = get_local_id(0);  // local column index

	// fill the cache
	for(iloc = 0; iloc < tilesize; ++iloc){
		int k = (i0 + iloc) * nbcol + j;  // position in the matrix
		blockmat[iloc * tilesize + jloc] = invect[k];
		blockval[iloc * tilesize + jloc] = invalues[k];
	}
	barrier(CLK_LOCAL_MEM_FENCE);  

	// put the cache at the good place
	// loop on the rows
	for(iloc = 0; iloc < tilesize; ++iloc){
		int kt = j * nbrow + iloc + i0;  // position in the transpose
		outvect[kt]   = blockmat[iloc * tilesize + jloc];
		outvalues[kt] = blockval[iloc * tilesize + jloc];
	}
}

