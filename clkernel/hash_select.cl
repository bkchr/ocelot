#include "hash.h"

/*
 * Counts how many tuples fulfill a given predicate.
 */
__kernel void hashSelectCount(
	__global const int* hash_table,
	const unsigned int hash_table_size,
	__global const int* offset_table,
	const int value,
	__global unsigned int* result
) {
  unsigned int pos = findInHashtable(value, hash_table, hash_table_size);
	unsigned int elements = 0;
	if ( pos < hash_table_size)  // findInHashTable returns hash_table_size if no entry was found
		elements = offset_table[pos+1]-offset_table[pos];
	result[0] = elements; 
	result[1] = offset_table[pos];
}

__kernel void hashSelect(
	__global const unsigned int* dense_ids,
	const unsigned int read_offset,
	__global unsigned int* result
) {
	result[get_global_id(0)] = dense_ids[read_offset + get_global_id(0)];
}
