#include "hash.h"

// use binary search to find the partition based on the index
unsigned int findPartition(const unsigned int index,
                          __global const unsigned int* p_start,
                          const unsigned int partition_num) {
  unsigned int lo = 0;
  unsigned int hi = partition_num - 1;
  unsigned int mid;
  while (hi >= lo) {
    mid = (lo+hi) / 2;
    if (mid == partition_num - 1)
      break;
    else if (p_start[mid] <= index && index < p_start[mid + 1])
      break;
    else if (p_start[mid] > index)
      hi = mid - 1;
    else
      lo = mid + 1;
  }

  return mid;
}

__kernel void partitionJoinBuildHashTablesOptimistic(
	__global const int* data,
	__global const unsigned int* p_i,
	__global const unsigned int* p_start,
	__global int *hash_table,
	__global unsigned int *index_table,
	const unsigned int hash_table_size,
	const unsigned int partition_num)
{
	unsigned int gid = get_global_id(0);
	unsigned int value_index = p_i[gid];
	int value = data[value_index];
  unsigned int partition = findPartition(gid, p_start, partition_num);

	unsigned int hash_table_offset = partition * hash_table_size;
	unsigned int bucket = buildHashTableOptimisticIntern(value, hash_table, hash_table_size, hash_table_offset);

	index_table[hash_table_offset + bucket] = value_index;
}

__kernel void partitionJoinValidateHashTable(
	__global const int* data,
	__global const unsigned int* p_i,
	__global const unsigned int* p_start,
	__global int *hash_table,
	__global unsigned int *index_table,
	const unsigned int hash_table_size,
	const unsigned int partition_num,
	__global unsigned char* error)
{
	unsigned int gid = get_global_id(0);
	int value = data[p_i[gid]];
  unsigned int partition = findPartition(gid, p_start, partition_num);

	validateHashTableIntern(value, hash_table, hash_table_size, partition * hash_table_size, error);
}

__kernel void partitionJoinBuildHashTablePessimistic(
	__global const int* data,
	__global const unsigned int* p_i,
	__global const unsigned int* p_start,
	__global int *hash_table,
	__global unsigned int *index_table,
	const unsigned int hash_table_size,
	const unsigned int partition_num,
	__global unsigned char* error)
{
	unsigned int gid = get_global_id(0);
	unsigned int value_index = p_i[gid];
	int value = data[value_index];
  unsigned int partition = findPartition(gid, p_start, partition_num);

	unsigned int hash_table_offset = partition * hash_table_size;
	unsigned int bucket = buildHashTablePessimisticIntern(value, hash_table, hash_table_size, hash_table_offset, error);
}

__kernel void partitionJoin(
	__global const int* right,
	__global const unsigned int* right_p_i,
	__global const unsigned int* right_p_start,
	const unsigned int right_tuples,
	__global const int* g_hash_table,
	__global const unsigned int* g_index_table,
	const unsigned int hash_table_size,
	__local int* hash_table,
	__local unsigned int* index_table,
	__global unsigned int* result_left,
	__global unsigned int* result_right,
	__global unsigned int* result_index,
  const unsigned int partition_num)
{
  unsigned int partition = get_group_id(0);
	unsigned int hash_table_offset = partition * hash_table_size;

  event_t events[2];
  // copy the hash table into the local memory
  events[0] = async_work_group_copy(hash_table,
                                         g_hash_table + hash_table_offset,
                                         hash_table_size,
                                         0);

  // copy the index table into the local memory
  events[1] = async_work_group_copy(index_table,
                                         g_index_table + hash_table_offset,
                                         hash_table_size,
                                         0);

  wait_group_events(2, events);

  unsigned int start = right_p_start[partition];
  unsigned int end = partition == partition_num - 1 ? right_tuples : right_p_start[partition + 1];

  unsigned int local_id = get_local_id(0);
  unsigned int tuples_per_thread = (end - start) / get_local_size(0);

  if ((end - start) % get_local_size(0))
    ++tuples_per_thread;

  // adjust start and end for the current thread
  start += local_id * tuples_per_thread;
  end = end < start + tuples_per_thread ? end : start + tuples_per_thread;

  for (unsigned int i = start; i < end; ++i) {
		unsigned int value_index = right_p_i[i];
		int value = right[value_index];
    unsigned int bucket = findInHashtableLocal(value, hash_table, hash_table_size);

    if (bucket < hash_table_size) {
			unsigned int rindex = atomic_inc(result_index);

      result_left[rindex] = index_table[bucket];
      result_right[rindex] = value_index;
		}
  }
}
