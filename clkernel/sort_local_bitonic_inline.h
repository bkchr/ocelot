#ifndef BITONIC_INLINE_H_
#define BITONIC_INLINE_H_

#include "cl_utilities.h"

/* bitonicSortLocalAny
 * Sorts keys and values in-place in local memory using a bitonic sorting
 * network of width 8.
 *
 * Input:
 *   l_key		    -	Local memory array containing the keys to be sorted.
 *   l_val		    -	Local memory array containing the values.
 *   arrayLength	-	Number of elements in the array.
 *   sortDir		  -	Flag indicating whether to sort in ascending (1) or
 *                  descending (0) order.
 */
void bitonicSortLocalAny(__local   int* l_key, __local short* l_val,
                         const unsigned int arrayLength,
                         const unsigned int sortDir);

/* bitonicSortLocalAnyKeysOnly
 * Sorts keys in-place in local memory using a bitonic sorting network of
 * width 8.
 *
 * Input:
 *   l_key        -	Local memory array containing the keys to be sorted.
 *   arrayLength	-	Number of elements in the array.
 *   sortDir		  -	Flag indicating whether to sort in ascending (1) or
 *                  descending (0) order.
 */
void bitonicSortLocalAnyKeysOnly(__local int* l_key,
                                 const unsigned int arrayLength,
                                 const unsigned int sortDir);


#endif  // BITONIC_INLINE_H_
