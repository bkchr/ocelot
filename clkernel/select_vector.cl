// required OpenCL extension
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store : enable

#define VALUES_PER_THREAD 8

//TODO: anti and INT_MIN not supported yet!
__kernel void theta_select_vector(
		__global const int8* data, // pointer to data buffer
		__global unsigned char* result, // pointer to result buffer
		const unsigned int tuples, // Total number of tuples
		const int lower, // Lower range bound
		const int upper, // Upper range bound
		const char lowerSet, // Is the lower value set or is it NULL?
		const char upperSet, // Is the upper value set or is it NULL?
		const char lowerInclusive, // Lower range inclusive?
		const char upperInclusive, // Upper range inclusive?
		const char anti // Negate predicate?
) {
	size_t gi = get_global_id(0);
	int offset = VALUES_PER_THREAD*(gi+1) - tuples;
	
	if (gi >= get_global_size(0)) // make sure we don't overshoot 
	return;

	// The local bit pattern
	int8 pattern = 1;

	// local predicate variable
	bool pred;
	
	int8 val = data[gi];
	int id = VALUES_PER_THREAD * gi;
	
	if(lowerSet)
	{
		if(lowerInclusive)
		{
			pattern &= (val >= lower);			
		}else{
			pattern &= (val > lower);
		}
	}
	
	if(upperSet)
	{
		if(upperInclusive)
		{
			pattern &= (val <= upper);
		}else{
			pattern &= (val < upper);
		}
		
		/*printf("GI: %d\tval1: %d\tval2: %d\tval3: %d\tval4: %d\tval5: %d\tval6: %d\tval7: %d\tval8: %d\n",
		gi, val.s0, val.s1, val.s2, val.s3,
		val.s4, val.s5, val.s6, val.s7);
		
		printf("GI: %d\t1: %d\t2: %d\t3: %d\t4: %d\t5: %d\t6: %d\t7: %d\t8: %d\n",
		gi, pattern.s0, pattern.s1, pattern.s2, pattern.s3,
		pattern.s4, pattern.s5, pattern.s6, pattern.s7);*/
	}
		
//	pattern = anti ? !pattern : pattern;
//	pattern = (id < tuples) && pattern;
//	pattern = (val == INT_MIN) ? 0 : pattern;
//	
	//EVAL_PREDICATE_VECTOR(val, VALUES_PER_THREAD * gi);

/*	printf("GI: %d\t res1: %d\t res2: %d\t res3: %d\t res4: %d\t res5: %d\t res6: %d\t res7: %d\t res8: %d\n",
	gi, (char)-pattern.s0, (char)-pattern.s1, (char)-pattern.s2, (char)-pattern.s3,
	(char)-pattern.s4, (char)-pattern.s5, (char)-pattern.s6, (char)-pattern.s7);
*/
	result[gi] = ((unsigned char) pattern.s0)  << 0;
	result[gi] |= ((unsigned char) pattern.s1) << 1;
	result[gi] |= ((unsigned char) pattern.s2) << 2;
	result[gi] |= ((unsigned char) pattern.s3) << 3;
	result[gi] |= ((unsigned char) pattern.s4) << 4;
	result[gi] |= ((unsigned char) pattern.s5) << 5;
	result[gi] |= ((unsigned char) pattern.s6) << 6;
	result[gi] |= ((unsigned char) pattern.s7) << 7;
	
/*	printf("GI: %d\t clean1: %d\t clean2: %d\t clean3: %d\t clean4: %d\t clean5: %d\t clean6: %d\t clean7: %d\t clean8: %d\n",
		gi, ((char)-pattern.s0) << 0, ((char)-pattern.s1) << 1, ((char)-pattern.s2) << 2, ((char)-pattern.s3) << 3, 
		((char)-pattern.s4) << 4, ((char)-pattern.s5) << 5, ((char)-pattern.s6) << 6, 
		((char)-pattern.s7) << 7);
*/
	//printf("Result pre: %d GI: %d\n", result[gi], gi);

	if(offset > 0){
		result[gi] &= (0xFF >> offset) ;  		
	}
	
	//printf("Result: %d GI: %d\n", result[gi], gi);
	
	// and store back
	//result[gi] = pattern;
}
