/* 
 * This software contains source code provided by NVIDIA Corporation.
 */

#include "cl_utilities.h"

////////////////////////////////////////////////////////////////////////////////
// Comparator functions to swap objects if necessary
////////////////////////////////////////////////////////////////////////////////

inline void ComparatorPrivate(
	int * keyA,
	int * valA,
	int * keyB,
	int * valB,
	const unsigned int arrowDir
){
	if( (*keyA > *keyB) == arrowDir || ((*keyA == *keyB) && ((*valA > *valB) == arrowDir))){
		int t;
		t = *keyA; *keyA = *keyB; *keyB = t;
		t = *valA; *valA = *valB; *valB = t;
	}
}

inline void ComparatorLocal(
	__local int *keyA,
	__local int *valA,
	__local int *keyB,
	__local int *valB,
	const unsigned int arrowDir
){
	if( (*keyA > *keyB) == arrowDir || ((*keyA == *keyB) && ((*valA > *valB) == arrowDir))){
		int t;
		t = *keyA; *keyA = *keyB; *keyB = t;
		t = *valA; *valA = *valB; *valB = t;
	}
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Bitonic sort kernel
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
// Bottom-level bitonic sort
// Almost the same as bitonicSortLocal with the only exception
// of even / odd subarrays (of 2^k points) being
// sorted in opposite directions
////////////////////////////////////////////////////////////////////////////////
__kernel void k_bitonicSortLocal(
	__global int * d_DstKey,
	const unsigned int dstKeyOffset,
	__global int * d_DstVal,
	const unsigned int dstValOffset,
	__global int * d_SrcKey,
	const unsigned int srcKeyOffset,
	__global int * d_SrcVal,
	const unsigned int srcValOffset,
	const unsigned int arrayLength,
	const unsigned int sortDir,
	__local int * l_key,
	__local int * l_val
){
	//Offset to the beginning of subarray and load data
	uint global_pos = get_group_id(0) * get_local_size(0) * 2;

	d_SrcKey += get_group_id(0) * get_local_size(0) * 2 + get_local_id(0) + srcKeyOffset;
	d_SrcVal += get_group_id(0) * get_local_size(0) * 2 + get_local_id(0) + srcValOffset;
	d_DstKey += get_group_id(0) * get_local_size(0) * 2 + get_local_id(0) + dstKeyOffset;
	d_DstVal += get_group_id(0) * get_local_size(0) * 2 + get_local_id(0) + dstValOffset;

	// load data into local memory if offset is not bigger than the array size
	if( (get_local_id(0) + global_pos) < arrayLength)
	{
		l_key[get_local_id(0) +                      0 ] = d_SrcKey[                0  ];
		l_val[get_local_id(0) +                      0 ] = d_SrcVal[                0  ];
	}
	if( (get_local_id(0) + get_local_size(0) + global_pos) < arrayLength)
	{
		l_key[get_local_id(0) +       get_local_size(0)] = d_SrcKey[(get_local_size(0))];
		l_val[get_local_id(0) +       get_local_size(0)] = d_SrcVal[(get_local_size(0))];
	}

	for(uint size = 2; size < get_local_size(0) * 2 ; size <<= 1){
		//Bitonic merge
		// uint dir = (get_local_id(0) & (size / 2)) != 0;
		uint chunkCount = countPartitions(arrayLength, size);
		uint dir = ((get_local_id(0) & (size / 2)) != 0 );
		dir = (chunkCount & 1) == 0 ? dir : !dir ;
		dir = sortDir ? dir : !dir;
		for(uint stride = size / 2; stride > 0; stride >>= 1)
		{
			barrier(CLK_LOCAL_MEM_FENCE);
			uint pos = 2 * get_local_id(0) - (get_local_id(0) & (stride - 1));
			if( ((pos + global_pos) < arrayLength) && ((pos + stride + global_pos) < arrayLength) )
			{
				ComparatorLocal(
					&l_key[pos +      0], &l_val[pos +      0],
					&l_key[pos + stride], &l_val[pos + stride],
					dir
				);
			}
		}
	}

	//Odd / even arrays of LOCAL_SIZE_LIMIT elements
	//sorted in opposite directions
	{
		uint chunkCount = countPartitions(arrayLength, get_local_size(0) * 2);
		uint dir = (get_group_id(0) & 1); 
		dir = (chunkCount & 1) == 0 ? dir : !dir;
		dir = sortDir ? dir : !dir;
		for(uint stride = get_local_size(0); stride > 0; stride >>= 1)
		{
			barrier(CLK_LOCAL_MEM_FENCE);
			uint pos = 2 * get_local_id(0) - (get_local_id(0) & (stride - 1));
			if( ((pos + global_pos) < arrayLength) && ((pos + stride + global_pos) < arrayLength) )
			{	
				ComparatorLocal(
					&l_key[pos +      0], &l_val[pos +      0],
					&l_key[pos + stride], &l_val[pos + stride],
					dir
				);
			}
		}
	}

	barrier(CLK_LOCAL_MEM_FENCE);
	if( (get_local_id(0) + global_pos) < arrayLength)
	{
		d_DstKey[                   0  ] = l_key[get_local_id(0) +                    0  ];
		d_DstVal[                   0  ] = l_val[get_local_id(0) +                    0  ];
	}
	if( (get_local_id(0) + get_local_size(0) + global_pos) < arrayLength)
	{
		d_DstKey[   (get_local_size(0))] = l_key[get_local_id(0) +    (get_local_size(0))];
		d_DstVal[   (get_local_size(0))] = l_val[get_local_id(0) +    (get_local_size(0))];
	}
}

////////////////////////////////////////////////////////////////////////////////
// Bitonic merge iteration for 'stride' >= LOCAL_SIZE_LIMIT
////////////////////////////////////////////////////////////////////////////////
__kernel void k_bitonicMergeGlobal(
	__global int * d_DstKey,
	const unsigned int dstKeyOffset,
	__global int * d_DstVal,
	const unsigned int dstValOffset,
	const unsigned int arrayLength,
	const unsigned int size,
	const unsigned int stride,
	const unsigned int sortDir,
	const unsigned int powof2
){
	// integrate offsets
	d_DstKey += dstKeyOffset;
	d_DstVal += dstValOffset;

	//Bitonic merge
	uint pos = 2 * get_global_id(0) - (get_global_id(0) & (stride - 1));

	// calculate number of partitions in this run
	uint partCount = countPartitions(arrayLength, size);
	// get partition identifier for thread
	uint partNr = get_global_id(0) / ( size / 2 );
	// define sorting direction
	uint dir = (size == powof2) ? sortDir : ((partNr & 1) != (partCount & 1));

	if( (pos < arrayLength) && ((pos + stride) < arrayLength))
	{
		// load values into register	
		int keyA = d_DstKey[pos +      0];
		int valA = d_DstVal[pos +      0];
		int keyB = d_DstKey[pos + stride];
		int valB = d_DstVal[pos + stride];
		// swap values if necessary
		ComparatorPrivate(
			&keyA, &valA,
			&keyB, &valB,
			dir
		);
		// write values back to global memory
		d_DstKey[pos +      0] = keyA;
		d_DstVal[pos +      0] = valA;
		d_DstKey[pos + stride] = keyB;
		d_DstVal[pos + stride] = valB;
	}
}


////////////////////////////////////////////////////////////////////////////////
// Combined bitonic merge steps for
// 'size' > LOCAL_SIZE_LIMIT and 'stride' = [1 .. LOCAL_SIZE_LIMIT / 2] (stride max thread number, size only for sort direction)
////////////////////////////////////////////////////////////////////////////////
__kernel void k_bitonicMergeLocal(
	__global int * d_DstKey,
	const unsigned int dstKeyOffset,
	__global int * d_DstVal,
	const unsigned int dstValOffset,
	const unsigned int arrayLength,
	uint stride,
	const unsigned int size,
	const unsigned int sortDir,
	const unsigned int powof2,
	__local int * l_key,
	__local int * l_val
){
	uint global_pos = get_group_id(0) * get_local_size(0) * 2;
	
	d_DstKey += get_group_id(0) * get_local_size(0) * 2 + get_local_id(0) + dstKeyOffset;
	d_DstVal += get_group_id(0) * get_local_size(0) * 2 + get_local_id(0) + dstValOffset;
	
	// load data into local memory if offset is not bigger than the array size	
	if( (get_local_id(0) + global_pos) < arrayLength)
	{
		l_key[get_local_id(0) +                      0] = d_DstKey[                     0];
		l_val[get_local_id(0) +                      0] = d_DstVal[                     0];
	}
	if( (get_local_id(0) + get_local_size(0) + global_pos) < arrayLength)
	{
		l_key[get_local_id(0) + (get_local_size(0))] = d_DstKey[(get_local_size(0))];
		l_val[get_local_id(0) + (get_local_size(0))] = d_DstVal[(get_local_size(0))];
	}

	//Bitonic merge
	// calculate number of partitions in this run
	uint partCount = countPartitions(arrayLength, size);
	// get partition identifier for thread
	uint partNr = get_global_id(0) / ( size / 2 ) ;
	// define sorting direction
	uint dir = (size == powof2) ? sortDir : ((partNr & 1) != (partCount & 1));
	for(; stride > 0; stride >>= 1)
	{
		barrier(CLK_LOCAL_MEM_FENCE);
		uint pos = 2 * get_local_id(0) - (get_local_id(0) & (stride - 1));
		if( ((pos + global_pos) < arrayLength) && ((pos + stride + global_pos) < arrayLength))
		{
			ComparatorLocal(
				&l_key[pos +      0], &l_val[pos +      0],
				&l_key[pos + stride], &l_val[pos + stride],
				dir
			);
		}
	}

	barrier(CLK_LOCAL_MEM_FENCE);
	if( (get_local_id(0) + global_pos) < arrayLength)
	{
		d_DstKey[                     0] = l_key[get_local_id(0) +                      0];
		d_DstVal[                     0] = l_val[get_local_id(0) +                      0];
	}
	if( (get_local_id(0) + get_local_size(0) + global_pos) < arrayLength)
	{
		d_DstKey[(get_local_size(0))] = l_key[get_local_id(0) + (get_local_size(0))];
		d_DstVal[(get_local_size(0))] = l_val[get_local_id(0) + (get_local_size(0))];
	}
}


////////////////////////////////////////////////////////////////////////////////
// Filters the keys array using a bitmap and replaces the non-passing values as INT_MAX / INT_MIN depending on the sorting order.
////////////////////////////////////////////////////////////////////////////////
__kernel void k_sortFilterElements(
	__global int * d_DstKey,
	const unsigned int dstKeyOffset,
	__global int * d_DstVal,
	const unsigned int dstValOffset,
	__global int * d_SrcKey,
	const unsigned int srcKeyOffset,
	__global int * d_SrcVal,
	const unsigned int srcValOffset,
	__global char * bitmap,
	const unsigned int bitmapOffset,
	const unsigned int sortDir,
	const unsigned int elements
){
	// add offsets to addresses
	d_DstKey += dstKeyOffset;
	d_DstVal += dstValOffset;
	d_SrcKey += srcKeyOffset;
	d_SrcVal += srcValOffset;
	bitmap   += bitmapOffset;

	// abort if all elements have been processed		
	if(get_global_id(0) >= elements)
		return;

	if( (bitmap[get_global_id(0) / 8] & ( 0x1 << ( get_global_id(0) & 7 ) ) ) == 0)
	{
		d_DstKey[get_global_id(0)] = sortDir ? INT_MAX : INT_MIN + 1;
	}
	else
	{
		d_DstKey[get_global_id(0)] = d_SrcKey[get_global_id(0)];
	}
	// copy value
	d_DstVal[get_global_id(0)] = d_SrcVal[get_global_id(0)];
}
