#include "hash.h"

__kernel void buildHashTableOptimistic(
	__global const int* const data,
	__global int* hash_table,
	const unsigned int hash_table_size
) {
	int value = data[get_global_id(0)];
	buildHashTableOptimisticIntern(value, hash_table, hash_table_size, 0);
}

__kernel void validateHashTable(
	__global const int* const data,
	__global int* hash_table,
	const unsigned int hash_table_size,
	__global unsigned char* error
) {
	int value = data[get_global_id(0)];
	validateHashTableIntern(value, hash_table, hash_table_size, 0, error);
}

__kernel void buildHashTablePessimistic(
	__global const int* const data,
	__global int* hash_table,
	const unsigned int hash_table_size,
	__global unsigned char* error
) {
	int value = data[get_global_id(0)];
	buildHashTablePessimisticIntern(value, hash_table, hash_table_size, 0, error);
}

__kernel void ht_retrieve(
	__global const int* const data,
	__global const int* const hash_table,
	const unsigned int hash_table_size,
	__global unsigned int* result
) {
	// Try the initial position
	int value = data[get_global_id(0)];
  result[get_global_id(0)] = findInHashtable(value, hash_table, hash_table_size);
}

__kernel void init_id_table(
	__global const int* const hash_table,
	__global unsigned int* id_table
) {
	id_table[get_global_id(0)] = (hash_table[get_global_id(0)] == EMPTY_KEY) ? 0 : 1;
}

__kernel void buildKeyLookupTable(
	__global const int* const data, 
	__global const int* const hash_table,
	const unsigned int hash_table_size,
	__global unsigned int* lookup_table
) {
	int val = data[get_global_id(0)];
  unsigned int bucket = findInHashtable(val, hash_table, hash_table_size);
	lookup_table[bucket] = get_global_id(0);
}

__kernel void countKeyCardinality(
	__global const int* const data, 
	__global const int* const hash_table,
	const unsigned int hash_table_size,
	__global unsigned int* card_table,
	__global unsigned int* offset_table
) {
	int val = data[get_global_id(0)];
  unsigned int bucket = findInHashtable(val, hash_table, hash_table_size);
	offset_table[get_global_id(0)] = atomic_inc(card_table + bucket); 
}

__kernel void buildLookupTable(
	__global const int* const data, 
	__global const int* const hash_table,
	const unsigned int hash_table_size,
	__global const unsigned int* const global_offsets,
	__global const unsigned int* const local_offsets,
	__global unsigned int* lookup_table
) {
	int val = data[get_global_id(0)];
  unsigned int bucket = findInHashtable(val, hash_table, hash_table_size);
	lookup_table[global_offsets[bucket] + local_offsets[get_global_id(0)]] = get_global_id(0);
}
