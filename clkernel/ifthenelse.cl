/* ifthenelse.cl
 * 
 * Kernel for if-then-else operations.
 *
 */

// IF-THEN-ELSE between two BATs.
__kernel void ifThenElseBatBat(
	__global const char* bitmap,
	__global const unsigned int* left,
	__global const unsigned int* right,
	__global unsigned int* result,
	unsigned int elements
) {
	unsigned int pos = 8 * get_global_id(0);
	char pattern = bitmap[get_global_id(0)];
	for (unsigned int i = 0; i < 8; ++i) {
		if (pos >= elements)
			break;
		result[pos] = (pattern & 0x1 << i) ? left[pos] : right[pos];
		pos++;
	}		
}

// IF-THEN-ELSE between BAT and Constant
__kernel void ifThenElseBatCnst(
	__global const char* bitmap,
	__global const unsigned int* left,
	const unsigned int cnst,
	__global unsigned int* result,
	unsigned int elements
) {
	unsigned int pos = 8 * get_global_id(0);
	char pattern = bitmap[get_global_id(0)];
	for (unsigned int i = 0; i < 8; ++i) {
		if (pos >= elements)
			break;
		result[pos] = (pattern & 0x1 << i) ? left[pos] : cnst;
		pos++;
	}
}

// IF-THEN-ELSE between Constant and BAT
__kernel void ifThenElseCnstBat(
	__global const char* bitmap,
	unsigned int cnst,
	__global const unsigned int* right,
	__global unsigned int* result,
	unsigned int elements
) {
	unsigned int pos = 8 * get_global_id(0);
	char pattern = bitmap[get_global_id(0)];
	for (unsigned int i = 0; i < 8; ++i) {
		if (pos >= elements)
			break;
		result[pos] = (pattern & 0x1 << i) ? cnst : right[pos];
		pos++;
	}		
}

// IF-THEN-ELSE between Constant and Constant
__kernel void ifThenElseCnstCnst (
	__global const char* bitmap,
	unsigned int left_cnst,
	unsigned int right_cnst,
	__global unsigned int* result,
	unsigned int elements
) {
	unsigned int pos = 8 * get_global_id(0);
	char pattern = bitmap[get_global_id(0)];
	for (unsigned int i = 0; i < 8; ++i) {
		if (pos >= elements)
			break;
		result[pos] = (pattern & 0x1 << i) ? left_cnst : right_cnst;
		pos++;
	}		
}
