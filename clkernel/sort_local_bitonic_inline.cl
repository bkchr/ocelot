///* 
// * This software contains source code provided by NVIDIA Corporation.
// */
//
//#include "sort_local_bitonic_inline.h"
//
//// required OpenCL extension
//#pragma OPENCL EXTENSION cl_khr_byte_addressable_store: enable
//
//inline void ComparatorLocalIntShort(
//	__local   int * keyA,
//	__local short * valA,
//	__local   int * keyB,
//	__local short * valB,
//	uint arrowDir
//){
//	if( (*keyA > *keyB) == arrowDir ){
//		int k;
//		k = *keyA; *keyA = *keyB; *keyB = k;
//		short v;
//		v = *valA; *valA = *valB; *valB = v;
//	}
//}
//
//inline void ComparatorLocalInt(
//	__local   int * keyA,
//	__local   int * keyB,
//	uint arrowDir
//){
//	if( (*keyA > *keyB) == arrowDir ){
//		int k;
//		k = *keyA; *keyA = *keyB; *keyB = k;
//	}
//}
//////////////////////////////////////////////////////////////////////////////////
//// Monolithic bitonic sort kernel for short arrays fitting into local memory.
//// Performs an inplace sorting using a bitonic sorting network of width 8.
////
//// Work group size should be a power of 2.
//////////////////////////////////////////////////////////////////////////////////
//void bitonicSortLocalAny(
//	__local   int * l_key,
//	__local short * l_val,
//	const unsigned int arrayLength,
//	const unsigned int sortDir
//){
//	// find next power of 2 to use for the bitonic sorting network
//	uint powof2 = 1;
//	while(powof2 < arrayLength)
//	{
//		powof2 <<= 1;
//	}
//
//	// outer loop sorting sublists of local_size * 2  elements 
//	uint partCount = countPartitions(arrayLength, get_local_size(0) * 2); 
//	uint loopLimit = min((unsigned int) get_local_size(0) * 2, powof2);
//	for(uint size = 2; size < loopLimit; size <<= 1 )
//	{
//		// sort list of size elements
//		for(uint stride = size / 2; stride > 0; stride >>= 1)
//		{
//			uint chunkCount = countPartitions(arrayLength, size);
//			uint dir = ((get_local_id(0) & (size / 2)) != 0 );
//			dir = (chunkCount & 1) == 0 ? dir : !dir ;
//			dir = sortDir ? dir : !dir;
//			// sort list of size elements
//			for(uint partNr = 0; partNr < partCount; ++partNr)
//			{
//				barrier(CLK_LOCAL_MEM_FENCE);
//				uint pos = (2 * get_local_id(0) - (get_local_id(0) & (stride - 1))) + (partNr * get_local_size(0) * 2);
//				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
//				{
//					ComparatorLocalIntShort(
//					&l_key[pos +      0], &l_val[pos +      0],
//					&l_key[pos + stride], &l_val[pos + stride],
//					dir
//					);
//				}
//			}
//		}
//
//	}
//
//	// merge the created sublists into the final sorted list
//	for(uint size = loopLimit; size <= powof2; size <<= 1)
//	{
//
//		for(uint stride = size / 2; stride > 0; stride >>= 1)
//		{
//			uint chunkCount = countPartitions(arrayLength, size);
//			// perform the merge step of two size/2 element lists
//			for( uint partNr = 0; partNr < partCount; ++partNr)
//			{
//				uint dir = (size == powof2) ? sortDir : sortDir ? ( 0 != (chunkCount & 1) ) : ( 0 == (chunkCount & 1));
//				barrier(CLK_LOCAL_MEM_FENCE);
//				// calculate as if global id was used
//				uint pos = 2 * (get_local_id(0) + partNr * get_local_size(0)) - ((get_local_id(0) + partNr * get_local_size(0)) & (stride - 1));
//				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
//				{
//					ComparatorLocalIntShort(
//					&l_key[pos +      0], &l_val[pos +      0],
//					&l_key[pos + stride], &l_val[pos + stride],
//					dir
//					);
//				}
//
//				// get alternating sorting directions for lists of size elements
//				if( (((partNr + 1) * get_local_size(0)) & (size/2 - 1)) == 0)
//				{
//					chunkCount--;
//				}
//			}
//		}
//	}
//}
//
//
//////////////////////////////////////////////////////////////////////////////////
//// Monolithic bitonic sort kernel for short arrays fitting into local memory.
//// Performs an inplace sorting using a bitonic sorting network of width 8.
////
//// Work group size should be a power of 2.
//////////////////////////////////////////////////////////////////////////////////
//__attribute__((vec_type_hint(int))) void bitonicSortLocalAnyKeysOnly(
//	__local   int * l_key,
//	const unsigned int arrayLength,
//	const unsigned int sortDir
//){
//	// find next power of 2 to use for the bitonic sorting network
//	uint powof2 = 1;
//	while(powof2 < arrayLength)
//	{
//		powof2 <<= 1;
//	}
//
//	// outer loop sorting sublists of local_size * 2  elements 
//	uint partCount = countPartitions(arrayLength, get_local_size(0) * 2); 
//	uint loopLimit = min((unsigned int) get_local_size(0) * 2, powof2);
//	for(uint size = 2; size < loopLimit; size <<= 1 )
//	{
//		// sort list of size elements
//		for(uint stride = size / 2; stride > 0; stride >>= 1)
//		{
//			uint chunkCount = countPartitions(arrayLength, size);
//			uint dir = ((get_local_id(0) & (size / 2)) != 0 );
//			dir = (chunkCount & 1) == 0 ? dir : !dir ;
//			dir = sortDir ? dir : !dir;
//			// sort list of size elements
//			for(uint partNr = 0; partNr < partCount; ++partNr)
//			{
//				barrier(CLK_LOCAL_MEM_FENCE);
//				uint pos = (2 * get_local_id(0) - (get_local_id(0) & (stride - 1))) + (partNr * get_local_size(0) * 2);
//				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
//				{
//					ComparatorLocalInt(
//					&l_key[pos +      0],
//					&l_key[pos + stride],
//					dir
//					);
//				}
//			}
//		}
//
//	}
//
//	// merge the created sublists into the final sorted list
//	for(uint size = loopLimit; size <= powof2; size <<= 1)
//	{
//		for(uint stride = size / 2; stride > 0; stride >>= 1)
//		{
//			uint chunkCount = countPartitions(arrayLength, size);
//			// perform the merge step of two size/2 element lists
//			for( uint partNr = 0; partNr < partCount; ++partNr)
//			{
//				uint dir = (size == powof2) ? sortDir : sortDir ? ( 0 != (chunkCount & 1) ) : ( 0 == (chunkCount & 1));
//				barrier(CLK_LOCAL_MEM_FENCE);
//				// calculate as if global id was used
//				uint pos = 2 * (get_local_id(0) + partNr * get_local_size(0)) - ((get_local_id(0) + partNr * get_local_size(0)) & (stride - 1));
//				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
//				{
//					ComparatorLocalInt(
//					&l_key[pos +      0],
//					&l_key[pos + stride],
//					dir
//					);
//				}
//
//				// get alternating sorting directions for lists of size elements
//				if( (((partNr + 1) * get_local_size(0)) & (size/2 - 1)) == 0)
//				{
//					chunkCount--;
//				}
//			}
//		}
//	}
//}
//
//

#include "sort_local_bitonic_inline.h"

// required OpenCL extension
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store: enable

inline void ComparatorLocalIntShort(
	__local   int * keyA,
	__local short * valA,
	__local   int * keyB,
	__local short * valB,
	uint arrowDir
){
	if( (*keyA > *keyB) == arrowDir ){
		int k;
		k = *keyA; *keyA = *keyB; *keyB = k;
		short v;
		v = *valA; *valA = *valB; *valB = v;
	}
}

inline void ComparatorLocalInt(
	__local   int * keyA,
	__local   int * keyB,
	uint arrowDir
){
	if( (*keyA > *keyB) == arrowDir ){
		int k;
		k = *keyA; *keyA = *keyB; *keyB = k;
	}
}
////////////////////////////////////////////////////////////////////////////////
// Monolithic bitonic sort kernel for short arrays fitting into local memory.
// Performs an inplace sorting using a bitonic sorting network of width 8.
//
// Work group size should be a power of 2.
////////////////////////////////////////////////////////////////////////////////
void bitonicSortLocalAny(
	__local   int * l_key,
	__local short * l_val,
	const unsigned int arrayLength,
	const unsigned int sortDir
){
	// find next power of 2 to use for the bitonic sorting network
	uint powof2 = 1;
	while(powof2 < arrayLength)
	{
		powof2 <<= 1;
	}

	// outer loop sorting sublists of local_size * 2  elements 
	uint partCount = countPartitions(arrayLength, get_local_size(0) * 2); 
	uint loopLimit = min((unsigned int) get_local_size(0) * 2, powof2);
	for(uint size = 2; size < loopLimit; size <<= 1 )
	{
		// sort list of size elements
		for(uint stride = size / 2; stride > 0; stride >>= 1)
		{
			uint chunkCount = countPartitions(arrayLength, size);
			uint dir = ((get_local_id(0) & (size / 2)) != 0 );
			dir = (chunkCount & 1) == 0 ? dir : !dir ;
			dir = sortDir ? dir : !dir;
			// sort list of size elements
			for(uint partNr = 0; partNr < partCount; ++partNr)
			{
				barrier(CLK_LOCAL_MEM_FENCE);
				uint pos = (2 * get_local_id(0) - (get_local_id(0) & (stride - 1))) + (partNr * get_local_size(0) * 2);
				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
				{
					ComparatorLocalIntShort(
					&l_key[pos +      0], &l_val[pos +      0],
					&l_key[pos + stride], &l_val[pos + stride],
					dir
					);
				}
			}
		}

	}

	// merge the created sublists into the final sorted list
	for(uint size = loopLimit; size <= powof2; size <<= 1)
	{

		for(uint stride = size / 2; stride > 0; stride >>= 1)
		{
			uint chunkCount = countPartitions(arrayLength, size);
			// perform the merge step of two size/2 element lists
			for( uint partNr = 0; partNr < partCount; ++partNr)
			{
				uint dir = (size == powof2) ? sortDir : sortDir ? ( 0 != (chunkCount & 1) ) : ( 0 == (chunkCount & 1));
				barrier(CLK_LOCAL_MEM_FENCE);
				// calculate as if global id was used
				uint pos = 2 * (get_local_id(0) + partNr * get_local_size(0)) - ((get_local_id(0) + partNr * get_local_size(0)) & (stride - 1));
				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
				{
					ComparatorLocalIntShort(
					&l_key[pos +      0], &l_val[pos +      0],
					&l_key[pos + stride], &l_val[pos + stride],
					dir
					);
				}

				// get alternating sorting directions for lists of size elements
				if( (((partNr + 1) * get_local_size(0)) & (size/2 - 1)) == 0)
				{
					chunkCount--;
				}
			}
		}
	}
}


////////////////////////////////////////////////////////////////////////////////
// Monolithic bitonic sort kernel for short arrays fitting into local memory.
// Performs an inplace sorting using a bitonic sorting network of width 8.
//
// Work group size should be a power of 2.
////////////////////////////////////////////////////////////////////////////////
void bitonicSortLocalAnyKeysOnly(
	__local   int * l_key,
	const unsigned int arrayLength,
	const unsigned int sortDir
){
	// find next power of 2 to use for the bitonic sorting network
	uint powof2 = 1;
	while(powof2 < arrayLength)
	{
		powof2 <<= 1;
	}

	// outer loop sorting sublists of local_size * 2  elements 
	uint partCount = countPartitions(arrayLength, get_local_size(0) * 2); 
	uint loopLimit = min((unsigned int) get_local_size(0) * 2, powof2);
	for(uint size = 2; size < loopLimit; size <<= 1 )
	{
		// sort list of size elements
		for(uint stride = size / 2; stride > 0; stride >>= 1)
		{
			uint chunkCount = countPartitions(arrayLength, size);
			uint dir = ((get_local_id(0) & (size / 2)) != 0 );
			dir = (chunkCount & 1) == 0 ? dir : !dir ;
			dir = sortDir ? dir : !dir;
			// sort list of size elements
			for(uint partNr = 0; partNr < partCount; ++partNr)
			{
				barrier(CLK_LOCAL_MEM_FENCE);
				uint pos = (2 * get_local_id(0) - (get_local_id(0) & (stride - 1))) + (partNr * get_local_size(0) * 2);
				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
				{
					ComparatorLocalInt(
					&l_key[pos +      0],
					&l_key[pos + stride],
					dir
					);
				}
			}
		}

	}

	// merge the created sublists into the final sorted list
	for(uint size = loopLimit; size <= powof2; size <<= 1)
	{
		for(uint stride = size / 2; stride > 0; stride >>= 1)
		{
			uint chunkCount = countPartitions(arrayLength, size);
			// perform the merge step of two size/2 element lists
			for( uint partNr = 0; partNr < partCount; ++partNr)
			{
				uint dir = (size == powof2) ? sortDir : sortDir ? ( 0 != (chunkCount & 1) ) : ( 0 == (chunkCount & 1));
				barrier(CLK_LOCAL_MEM_FENCE);
				// calculate as if global id was used
				uint pos = 2 * (get_local_id(0) + partNr * get_local_size(0)) - ((get_local_id(0) + partNr * get_local_size(0)) & (stride - 1));
				if( (pos < arrayLength) && ( (pos + stride) < arrayLength) )
				{
					ComparatorLocalInt(
					&l_key[pos +      0],
					&l_key[pos + stride],
					dir
					);
				}

				// get alternating sorting directions for lists of size elements
				if( (((partNr + 1) * get_local_size(0)) & (size/2 - 1)) == 0)
				{
					chunkCount--;
				}
			}
		}
	}
}



