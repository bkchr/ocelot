// Checks whether a year is a leapyear
#define leapyear(y) ((y) % 4 == 0 && ((y) % 100 != 0 || (y) % 400 == 0))
// Calculate the number of leapyears
static inline int leapyears(int year) {
	int y4 = year / 4;
	int y100 = year / 100;
	int y400 = year / 400;
	return y4 + y400 - y100;
}


#define LEAPYEARS(y) (leapyears(y)+((y)>=0))
#define YEARDAYS(y) (leapyear(y)?366:365)

__kernel void extractYear(
	__global const int* data,
	__global int* result
){
	size_t gi = get_global_id(0);
	int input = data[gi];
	// Now extract the year.
	int year = input / 365;
	int day = (input - year * 365) - LEAPYEARS(year >= 0 ? year - 1 : year);
	if (input < 0) {
		year--;
		while (day >= 0) {
			year++;
			day -= YEARDAYS(year);
		}
	} else {
		while (day < 0) {
			year--;
			day += YEARDAYS(year);
		}
	}
	result[gi] = input == INT_MIN ? INT_MIN : year;
}
