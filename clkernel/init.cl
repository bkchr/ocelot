#include "types.h"

// Functions for initializing a buffers.
__kernel void TYPED_KERNEL(init_zero)(
  __global T* buffer
) {
  buffer[get_global_id(0)] = 0;
}

__kernel void TYPED_KERNEL(init_min)(
  __global T* buffer
) {
  buffer[get_global_id(0)] = MIN;
}

__kernel void TYPED_KERNEL(init_max)(
  __global T* buffer
) {
  buffer[get_global_id(0)] = MAX;
}
