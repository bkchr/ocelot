#include "types.h"

#include "hash.h"
#include "sort_local_bitonic_inline.h"

// used predicate

// For this join, we assume that the right side is sorted and unique.
__kernel void sorted_unique_join_count(
	__global const int* left,
	const int left_tuples,
	__global const int* right,
	const unsigned int right_tuples,
	__global unsigned int* result_counts,
	const unsigned int tuples_per_thread
) {
	unsigned int pos = tuples_per_thread*get_global_id(0); 
	unsigned int result_tuples = 0;
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= left_tuples)
			break;
		// Fetch the value.
		int val = left[pos];
		// Use binary search.
		unsigned int lo = 0;
		unsigned int hi = right_tuples - 1;
		unsigned int mid;
		while (hi >= lo) {
			mid = (lo+hi) / 2;
			if (right[mid] == val)
				break;
			else if (right[mid] < val)
				lo = mid + 1;
			else
				hi = mid - 1;
		}
		// Check if the tuple was correctly set.
		result_tuples += (mid < right_tuples && right[mid] == val) ? 1 : 0;
		// Move to the next tuple
		pos++;
	}
	result_counts[get_global_id(0)] = result_tuples; 
}

__kernel void sorted_unique_join_exec(
	__global const int* left,
	const unsigned int left_tuples,
	__global const int* right,
	const unsigned int right_tuples,
	__global const unsigned int* write_offsets,
	__global unsigned int* result_head,
	__global unsigned int* result_tail,
	const unsigned int tuples_per_thread
) {
		// Move to the next tuple
	unsigned int pos = tuples_per_thread*get_global_id(0); 
	unsigned int write_offset = write_offsets[get_global_id(0)];
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= left_tuples)
			break;
		// Fetch the value.
		int val = left[pos];
		// Use binary search.
		unsigned int lo = 0;
		unsigned int hi = right_tuples - 1;
		unsigned int mid;
		while (hi >= lo) {
			mid = (lo+hi) / 2;
			if (right[mid] == val)
				break;
			else {
				lo += (right[mid] < val) ? 1 : 0;
				hi -= (right[mid] < val) ? 0 : 1;
			}
		}
		// Check if the tuple was correctly set.
		if (mid < right_tuples && right[mid] == val) {
			result_head[write_offset] = pos;
			result_tail[write_offset] = mid;
			write_offset++;
		}
		// Move to the next tuple
		pos++;
	}
}

__kernel void bitmap_join_count(
	__global const int* left,
	const int left_tuples,
	__global const unsigned int* bitmap, 
	const unsigned int bitmap_size, 
	__global unsigned int* result_counts,
	const unsigned int tuples_per_thread
) {
	unsigned int pos = tuples_per_thread*get_global_id(0); 
	unsigned int result_tuples = 0;
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= left_tuples)
			break;
		// Fetch the value.
		int val = left[pos];
		// Check the bitmap. 
		unsigned int bitmap_offset = val / 32; 
		result_tuples += ((bitmap_offset < bitmap_size) &&
								(bitmap[bitmap_offset] & ((0x1 << val % 32))))
							  ? 1 : 0; 
		// Move to the next tuple
		pos++;
	}
	result_counts[get_global_id(0)] = result_tuples; 
}

__kernel void bitmap_join_exec(
	__global const int* left,
	const int left_tuples,
	__global const unsigned int* bitmap, 
	__global const unsigned int* partial_bitcounts,
	const unsigned int bitmap_size,
	__global const unsigned int* write_offsets,
	__global unsigned int* result_head,
	__global unsigned int* result_tail,
	const unsigned int tuples_per_thread
) {
	unsigned int pos = tuples_per_thread*get_global_id(0); 
	unsigned int write_offset = write_offsets[get_global_id(0)];
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= left_tuples)
			break;
		// Fetch the value.
		int val = left[pos];
		// Check the bitmap. 
		unsigned int bitmap_offset = val / 32; 
		unsigned int bitmask = 0x1 << (val % 32);
		if (bitmap_offset < bitmap_size) {
			// Fetch the requested portion of the bitmap.
			unsigned int local_bitmap = bitmap[bitmap_offset];
			if (local_bitmap & bitmask) { 
				result_head[write_offset] = pos;
				// The offset for the bitmap is simply the number of set bits
				// bfeore the requested position.
				result_tail[write_offset] = partial_bitcounts[bitmap_offset]
					+ bitcount(local_bitmap & (bitmask - 1));	
				write_offset++;
			}
		}
		pos++;
	}
}

__kernel void hj_count(
	__global const int* outer,
	const unsigned int outer_tuples,
	__global const int* inner_ht,
	const unsigned int inner_ht_size,
	__global const unsigned int* inner_ot,
	__global unsigned int* result_counts, 
	const unsigned int tuples_per_thread
) {
	unsigned int pos = tuples_per_thread*get_global_id(0); 
	unsigned int result_tuples = 0;
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= outer_tuples)
			break;
		// Check how many join partners this tuple has:
		int val = outer[pos];
		unsigned int bucket = findInHashtable(val, inner_ht, inner_ht_size);
		if (bucket < inner_ht_size) {
			result_tuples += (inner_ot[bucket+1] - inner_ot[bucket]);
		}
		// Move to the next tuple
		pos++;
	}
	result_counts[get_global_id(0)] = result_tuples; 
}

__kernel void hj_exec(
	__global const int* outer,
	const unsigned int outer_tuples,
	__global const int* inner_ht,
	const unsigned int inner_ht_size,
	__global const unsigned int* inner_ot,
	__global const unsigned int* inner_it,
	__global const unsigned int* write_offsets,
	__global unsigned int* result_head,
	__global unsigned int* result_tail,
	const unsigned int tuples_per_thread
) {
	int pos = tuples_per_thread*get_global_id(0); 
	unsigned int write_offset = write_offsets[get_global_id(0)];
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= outer_tuples)
			break;
		// Check how many join partners this tuple has:
		int val = outer[pos];
		unsigned int bucket = findInHashtable(val, inner_ht, inner_ht_size);
		if (bucket < inner_ht_size) {
			unsigned int read_offset = inner_ot[bucket];
			unsigned int results = inner_ot[bucket+1] - read_offset;
			// Now write out all results
			for (unsigned int j=0; j<results; ++j) {
				result_head[write_offset] = pos;
				result_tail[write_offset] = inner_it[read_offset+j];
				write_offset++;
			}
		}
		// Move to the next tuple
		pos++;
	}
}

__kernel void hj_key(
	__global const int* outer,
	const unsigned int outer_tuples,
	__global const int* inner_ht,
	__global const unsigned int* inner_ot,
	const unsigned int inner_ht_size,
	__global unsigned int* result_buffer,
  __global unsigned int* result_counts,
	const unsigned int tuples_per_thread
) {
	int pos = tuples_per_thread*get_global_id(0); 
	unsigned int result_tuples = 0;
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= outer_tuples)
			break;
		// Check how many join partners this tuple has:
		int val = outer[pos];
		unsigned int bucket = findInHashtable(val, inner_ht, inner_ht_size);
    // Fetch the result value:
    if (bucket < inner_ht_size) {
      result_buffer[pos] = inner_ot[bucket];
      result_tuples++;
    }
    // Move to the next tuple
		pos++;
	}
	result_counts[get_global_id(0)] = result_tuples; 
}

__kernel void hj_compact(
  __global const int* input,
  const unsigned int nr_of_tuples,
  __global const unsigned int* write_offsets,
  __global unsigned int* result_head,
  __global int* result_tail,
  const unsigned int tuples_per_thread
) {
  int pos = tuples_per_thread*get_global_id(0);
  unsigned int write_offset = write_offsets[get_global_id(0)];
  for (unsigned int i=0; i<tuples_per_thread; ++i) {
    if (pos >= nr_of_tuples)
      break;
    int val = input[pos];
    if (val != INT_MIN) {
      result_head[write_offset] = pos; 
      result_tail[write_offset] = val;
      write_offset++;
    } 
    pos++;
  }
}

// MonetDB operator definitions.
#define JOIN_LT      (-1)
#define JOIN_LE      (-2)
#define JOIN_EQ 		0
#define JOIN_GT      1
#define JOIN_GE      2

/* nlj_count
 * Performs a nested loop join, counting the number of qualifying tuples and storing the number of resulting tuples per thread in a temporary buffer
 */  
__kernel void TYPED_KERNEL(nlj_count)(
	__global const int* outer,
	const unsigned int outer_tuples,
	__global const int* inner,
	const unsigned int inner_tuples,
	__global unsigned int* temp_buffer,
	const unsigned int tuples_per_thread,
	const char op
) {
	int pos = tuples_per_thread*get_global_id(0); 
	unsigned int result_tuples = 0;	// Sum up the result tuples for this value.
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= outer_tuples)
			break;
		// Check how many join partners this tuple has:
		int oval = outer[pos];
		for (unsigned int j=0; j<inner_tuples; ++j) {
			int ival = inner[j];
			// Evaluate the join predicate.
			char predicate = (op == JOIN_LT || op == JOIN_LE) ? oval < ival : oval > ival; 
			predicate |= (op == JOIN_LE || op == JOIN_GE) ? oval == ival : 0;
			predicate = (op == JOIN_EQ) ? oval == ival : predicate;
			// If this matches, sum it up.
			result_tuples += predicate ? 1 : 0;	
		}
		// Move to the next tuple
		pos++;
	}
	temp_buffer[get_global_id(0)] = result_tuples; 
}

/* nlj_exec
 * Performs a nested loop join, writing the result tuples to the speficified offsets 
 */
__kernel void TYPED_KERNEL(nlj_exec)(
	__global const int* outer,
	const unsigned int outer_tuples,
	__global const int* inner,
	const unsigned int inner_tuples,
	__global const unsigned int* write_offsets,
	__global unsigned int* result_head,
	__global unsigned int* result_tail,
	const unsigned int tuples_per_thread,
	const char op
){
	unsigned int pos = tuples_per_thread*get_global_id(0); 
	unsigned int write_offset = write_offsets[get_global_id(0)];
	for (unsigned int i=0; i<tuples_per_thread; ++i) {
		if (pos >= outer_tuples)
			break;
		// Check how many join partners this tuple has:
		int oval = outer[pos];
		for (unsigned int j=0; j<inner_tuples; ++j) {
			int ival = inner[j];
			// Evaluate the join predicate.
			char predicate = (op == JOIN_LT || op == JOIN_LE) ? oval < ival : oval > ival; 
			predicate |= (op == JOIN_LE || op == JOIN_GE) ? oval == ival : 0;
			predicate = (op == JOIN_EQ) ? oval == ival : predicate;
			// If this matches, sum it up.
			if (predicate) {
				result_head[write_offset] = pos;
				result_tail[write_offset] = j;
				write_offset++;
			}
		}
		// Move to the next tuple
		pos++;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// fetch join kernel without bitmaps
////////////////////////////////////////////////////////////////////////////////////////////

__kernel void fetchJoin(
	__global const int* left,
	unsigned int left_tuples,
  __global const T* right,
  __global T* result,
	unsigned int tuples_per_thread
){
	unsigned int global_id = get_global_id(0);
#ifdef DEVICE_GPU
	// On the GPU we use a strided access pattern, so that the GPU
	// can coalesc memory access.
	unsigned int local_id = get_local_id(0);
  unsigned int group_start = get_local_size(0) * tuples_per_thread *
                             get_group_id(0);
  for (unsigned int i = 0; i < tuples_per_thread; ++i) {
    unsigned int pos = group_start + i * get_local_size(0) + local_id;

    if (pos < left_tuples) {
			result[pos] = right[left[pos]];			
    }
	}
#elif defined DEVICE_CPU
	// On the CPU we use a sequential access pattern to keep cache misses
	// local per thread.
	unsigned int pos = tuples_per_thread * global_id;
  for (unsigned int i = 0; i < tuples_per_thread; ++i) {
    if (pos >= left_tuples) {
			break;
    }

		result[pos] = right[left[pos]];
		pos++; 
	}
#endif
}

__kernel void fetchJoinBM(
	__global const unsigned int* bitmap,
	__global const unsigned int* offsets,
	__global const int* batRT,
	__global int* result
) {
	unsigned int local_bitmap = bitmap[get_global_id(0)];
	unsigned int local_offset = offsets[get_global_id(0)];
	for (unsigned int i=0; i<32; ++i) {
		if (local_bitmap & 0x1) {
			result[local_offset] = batRT[32*get_global_id(0) + i]; 
			local_offset++;
		}
		// Shift the bit pattern left.
		local_bitmap >>= 1;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////
// fetch join kernels which detect hit failures  
////////////////////////////////////////////////////////////////////////////////////////////

__kernel void buildFetchJoinBitmap(
	__global const unsigned int* data,
	unsigned int tuples,
	unsigned int right_oids,
	__global char* result_bitmap,
	__global char* missing_hit_flag
) {
	char bitmap = 0;
	char pattern = 0x1;
	char missing_hit = 0;
	unsigned int pos = 8 * get_global_id(0);
	for (unsigned int i=0; i<8; ++i) {
		if (pos >= tuples)
			break;
		char pred = data[pos] >= right_oids; 
		missing_hit |= pred;
		bitmap |= pred ? pattern : 0;
		pattern <<= 1;
	}
	result_bitmap[get_global_id(0)] = pattern;
	if (missing_hit)
		*missing_hit_flag = missing_hit;	
}

unsigned int count_upwards(int val, const __global int* values, unsigned int start)
{
    int result = 0;

    while(start > 0)
    {
        --start;

        if(values[start] == val)
            ++result;
        else
            break;
    }

    return result;
}

unsigned int count_downwards(int val, const __global int* values, unsigned int start, unsigned int value_num)
{
    int result = 0;
    ++start;
    for(; start < value_num && values[start] == val; ++start, ++result);

    return result;
}

unsigned int binary_search(int val, const __global int* values, unsigned int value_num)
{
    unsigned int lo = 0;
    unsigned int hi = value_num - 1;
    unsigned int mid;
    while (hi >= lo) {
            mid = (lo+hi) / 2;
            if (values[mid] == val)
                    break;
            else if (values[mid] < val)
                    lo = mid + 1;
            else
                    hi = mid - 1;
    }

    return mid;
}

unsigned int find_start_pos(int val, const __global int* values, unsigned int value_num)
{
    int pos = binary_search(val, values, value_num);

    if(pos < value_num && values[pos] == val)
        return pos - count_upwards(val, values, pos);
    else
        return value_num;
}

__kernel void sorted_merge_join_seq(
        __global const int* left,
        unsigned int left_tuples,
        __global unsigned int* left_offset,
        __global const int* right,
        unsigned int right_tuples,
        __global unsigned int* right_offset,
        __global unsigned int* resultl,
        __global unsigned int* resultr,
        unsigned int result_offset,
        unsigned int max_result_num,
        __global unsigned int* result_count,
        __global unsigned int* not_enough_space)
{
    unsigned int lefti = *left_offset;

    if(lefti < left_tuples)
    {
        unsigned int current_index = *right_offset;
        unsigned int last_valid_start_index = find_start_pos(left[lefti], right, right_tuples);
        unsigned int first_iteration_after_restart = *right_offset != 0;

        for(; lefti < left_tuples; ++lefti)
        {
            int val = left[lefti];

            if(result_offset < max_result_num)
                *left_offset = lefti;

            if(!first_iteration_after_restart)
            {
                // do we need to reset the current_index?
                if(right[last_valid_start_index] == val && right[current_index] != val)
                    current_index = last_valid_start_index;
                else
                    last_valid_start_index = current_index;
            }

            if(current_index < right_tuples)
            {
                for(; current_index < right_tuples; ++current_index)
                {
                    int rval = right[current_index];

                    if(rval == val)
                    {
                        if(result_offset >= max_result_num)
                            ++result_offset;
                        else
                        {
                            resultl[result_offset] = lefti;
                            resultr[result_offset++] = current_index;
                            *right_offset = current_index + 1;
                        }
                    }
                    else if(rval < val)
                        ++last_valid_start_index;
                    else
                        break;
                }
            }
            else if(!first_iteration_after_restart)
                break;

            first_iteration_after_restart = 0;
        }

        *not_enough_space = result_offset > max_result_num ? 1 : 0;
        *result_count = result_offset;
    }
}

__kernel void sort_merge_join_tpp_count(
        __global const int* left,
        unsigned int left_tuples,
        __global const int* right,
        unsigned int right_tuples,
        __global unsigned int* result_counts,
        __global unsigned int* result_first_indices,
        unsigned int tuples_per_thread)
{
    unsigned int lefti = get_global_id(0) * tuples_per_thread;
    unsigned int result_count = 0;

    if(lefti < left_tuples)
    {
        int val = left[lefti];
        unsigned int first_index = find_start_pos(val, right, right_tuples);
        unsigned int last_valid_start_index = first_index;
        unsigned int current_index = first_index;

        for(int i = 0; i < tuples_per_thread && lefti < left_tuples; ++lefti, ++i)
        {
            int val = left[lefti];

            if(right[last_valid_start_index] == val)
                current_index = last_valid_start_index;
            else
                last_valid_start_index = current_index;

            if(current_index < right_tuples)
            {
                for(; current_index < right_tuples; ++current_index)
                {
                    int rval = right[current_index];

                    if(rval == val)
                        ++result_count;
                    else if(rval < val)
                        ++last_valid_start_index;
                    else
                        break;
                }
            }
            else
                break;
        }

        result_counts[get_global_id(0)] = result_count;
        result_first_indices[get_global_id(0)] = first_index;
    }
}

__kernel void sort_merge_join_tpp(
        __global const int* left,
        unsigned int left_tuples,
        __global const int* right,
        unsigned int right_tuples,
        __global unsigned int* offsets,
        __global unsigned int* first_indices,
        __global unsigned int* resultl,
        __global unsigned int* resultr,
        unsigned int tuples_per_thread)
{
    unsigned int lefti = get_global_id(0) * tuples_per_thread;

    if(lefti < left_tuples)
    {
        unsigned int result_offset = offsets[get_global_id(0)];
        unsigned int current_index = first_indices[get_global_id(0)];
        unsigned int last_valid_start_index = current_index;

        for(int i = 0; i < tuples_per_thread && lefti < left_tuples; ++lefti, ++i)
        {
            int val = left[lefti];

            // do we need to reset the current_index?
            if(right[last_valid_start_index] == val)
                current_index = last_valid_start_index;
            else
                last_valid_start_index = current_index;

            if(current_index < right_tuples)
            {
                for(; current_index < right_tuples; ++current_index)
                {
                    int rval = right[current_index];

                    if(rval == val)
                    {
                        resultl[result_offset] = lefti;
                        resultr[result_offset++] = current_index;
                    }
                    else if(rval < val)
                        ++last_valid_start_index;
                    else
                        break;
                }
            }
            else
                break;
        }
    }
}

__kernel void sort_merge_join_atomic(
        __global const int* left,
        unsigned int left_tuples,
        __global const int* right,
        unsigned int right_tuples,
        unsigned int max_result_count,
        __global unsigned int* resultl,
        __global unsigned int* resultr,
        __global unsigned int* result_count,
        unsigned int tuples_per_thread)
{
    unsigned int lefti = get_global_id(0) * tuples_per_thread;

    if(lefti < left_tuples)
    {
        unsigned int current_index = find_start_pos(left[lefti], right, right_tuples);
        unsigned int last_valid_start_index = current_index;

        for(int i = 0; i < tuples_per_thread && lefti < left_tuples; ++lefti, ++i)
        {
            int val = left[lefti];

            // do we need to reset the current_index?
            if(right[last_valid_start_index] == val)
                current_index = last_valid_start_index;
            else
                last_valid_start_index = current_index;

            if(current_index < right_tuples)
            {
                for(; current_index < right_tuples; ++current_index)
                {
                    int rval = right[current_index];

                    if(rval == val)
                    {
                        unsigned int offset = atomic_inc(result_count);

                        if(offset < max_result_count)
                        {
                            resultl[offset] = lefti;
                            resultr[offset] = current_index;
                        }
                    }
                    else if(rval < val)
                        ++last_valid_start_index;
                    else
                        break;
                }
            }
            else
                break;
        }
    }
}
