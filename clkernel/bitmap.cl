/*
 * Compute the bitwise and of two bitmaps
 */
__kernel void bm_and(
	__global const unsigned int* const bitmap1,
	__global const unsigned int* const bitmap2,
	__global unsigned int* const result
) {
	size_t gi = get_global_id(0);
	if (gi >= get_global_size(0))
		return;
	result[gi] = bitmap1[gi] & bitmap2[gi];	
}


/*
 * Compute the bitwise or of two bitmaps
 */
__kernel void bm_or(
	__global const unsigned int* const bitmap1,
	__global const unsigned int* const bitmap2,
	__global unsigned int* const result
) {
	size_t gi = get_global_id(0);
	if (gi >= get_global_size(0))
		return;
	result[gi] = bitmap1[gi] | bitmap2[gi];	
}

__kernel void bm_xor(
	__global const unsigned int* const bitmap1,
	__global const unsigned int* const bitmap2,
	__global unsigned int* const result
) {
	size_t gi = get_global_id(0);
	if (gi >= get_global_size(0))
		return;
	result[gi] = bitmap1[gi] ^ bitmap2[gi];	
}

/*
 * Compute the bitwise complement of a bitmap
 */
__kernel void bm_not(
	__global const unsigned int* const bitmap,
	__global unsigned int* const result,
	const unsigned int tuples
) {
	size_t gi = get_global_id(0);
	if (gi >= get_global_size(0))
		return;
	// Negate the complete pattern
	unsigned int val = ~bitmap[gi];
	// Make sure to mask out bits that are out of scope
	unsigned int start = gi * 32;
	result[gi] = ((start + 32) > tuples) ? val & ((0x1 << (tuples-start))-1) : val;
}


__kernel void bm_materialize_oids(
	__global const unsigned int* const bitmap,
	__global unsigned int* result,
	__global const unsigned int* const offsets, 
	const unsigned int elements,
	const unsigned int hseqbase
) {
	const unsigned int gi = get_global_id(0);
	// int equals 32 bits, each thread works on one int
	if(gi >= elements) {
		return;
	}

	const unsigned int bm = bitmap[gi];
	unsigned int write_offset = offsets[gi];
	for(unsigned int i=0; i < 32; ++i) {
		unsigned int output = (bm >> i) & 0x1; 		
		if(output) {
			result[write_offset] = gi * 32 + i + hseqbase;
			write_offset++;
		}
	}
}

__kernel void bm_materialize(
	__global const unsigned int* const bitmap,
	__global const unsigned int* const offsets,
	__global unsigned int* result,
	const unsigned int elements
){
	const size_t gi = get_global_id(0);
  if(gi >= elements) {
		return;
  }

	const unsigned int bm = bitmap[gi];
	unsigned int write_offset = offsets[gi];
  for(unsigned int i = 0; i < 32; ++i) {
		unsigned int output = (bm >> i) & 0x1; 		
		if(output) {
			result[write_offset] = gi * 32 + i; 
			write_offset++;
		}
	}
}

__kernel void bm_expand(
	__global const char* const bitmap,
	__global char* result,
	const unsigned int elements
) {
  const size_t gi = get_global_id(0);
  if(gi >= elements) {
    return;
  }

  char pattern = bitmap[gi];
  unsigned int write_offset = 8 * gi;
	for (unsigned int i = 0; i < 8; ++i) {
    result[write_offset++] = pattern & 0x1;
    pattern >>= 1;
	}
}

__kernel void bm_encode_sorted(
	__global const unsigned int* values,
	unsigned int tuples,
	__global int* bitmap,
	unsigned int tuples_per_thread
) {
	unsigned int pos = tuples_per_thread * get_global_id(0);

  if (pos >= tuples) {
		return;
  }

  unsigned int local_bitmap = 0;
	unsigned int value = values[pos];
	unsigned int last_value = value;
	for (unsigned int i = 0; i < tuples_per_thread; ++i) {
    if ((pos + i) >= tuples) {
			break;
    }

		last_value = value;
		value = values[pos + i];
		// Check if this value went across bitmap boundaries. 
    if (value / 32 != last_value / 32) {
      atomic_or(&(bitmap[last_value / 32]), local_bitmap);
			local_bitmap = 0;
		}

    local_bitmap |= (0x1<<value % 32);
	}

  atomic_or(&(bitmap[value / 32]), local_bitmap);
}

// Specialized kernels for bitwise ifthenelse
__kernel void ifThenElseBMBatTrue(
	__global const int* bitmap,
	__global const int* left,
	__global int* result,
	unsigned int elements
) {
	unsigned int bm = bitmap[get_global_id(0)];
	unsigned int l = left[get_global_id(0)];
	// The mask is used to mask out bits that are out of scope.
	unsigned int mask = 32*(get_global_id(0) + 1) > elements ? (0x1 << (elements - 32*get_global_id(0))) - 1 : ~0;
	result[get_global_id(0)] = (~bm | l) & mask; 
}

__kernel void ifThenElseBMFalseBat(
	__global const int* bitmap,
	__global const int* right,
	__global int* result
) {
	unsigned int bm = bitmap[get_global_id(0)];
	unsigned int r = right[get_global_id(0)];
	result[get_global_id(0)] = ~bm & r;
}

__kernel void ifThenElseBMBatBat(
	__global const int* bitmap,
	__global const int* left,
	__global const int* right,
	__global int* result
) {
	unsigned int bm = bitmap[get_global_id(0)];
	unsigned int l = left[get_global_id(0)];
	unsigned int r = right[get_global_id(0)];
	result[get_global_id(0)] = (bm & l) | (~bm & r);
}
