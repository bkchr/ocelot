#include "OperatorInput.h"

#include "Operator.h"

namespace Ocelot
{

OperatorInput::OperatorInput(OperatorPtr input, unsigned short channel,
                             bool vectorized,
                             const GlobalVectorSize vector_size) :
  pInput(input),
  mChannel(channel),
  mVectorized(vectorized),
  mVectorSize(vector_size) {}

unsigned short OperatorInput::getMaximumNumberOfRuns() {
  // if the input is operating on vectorized data we will generate the buffer
  // here and we will use the number of items to compute the number of runs
  if (!pInput->isWorkingOnVectorizedData() && mVectorized) {
    generateBuffer(0, true);

    auto col = getResult();
    return Buffer::getMaxNumberOfSubBuffer(col->getCount(), col->getType(),
                                           mVectorSize);
  } else {
    return pInput->getMaximumNumberOfRuns();
  }
}

void OperatorInput::generateBuffer(unsigned short run_index,
                                   bool force_finish) {
  if (mFinished) {
    return;
  }

  if (runAlreadyExecuted(run_index))
    return;

  if (mVectorized && !force_finish) {
    pInput->next();
  } else {
    pInput->finish();
    mFinished = true;
  }

  std::lock_guard<std::mutex> lock(mExecutedRunsMutex);
  mExecutedRuns.insert(run_index);
}

ColumnPtr OperatorInput::getResult() {
  return pInput->getResult(mChannel);
}

bool OperatorInput::runAlreadyExecuted(unsigned short run_index) {
  std::lock_guard<std::mutex> lock(mExecutedRunsMutex);
  return mExecutedRuns.find(run_index) != mExecutedRuns.end();
}

} //namespace Ocelot
