#include "Kernel.h"

#include "Device.h"
#include "Scheduler.h"

namespace Ocelot
{

Kernel::Kernel(const boost::compute::kernel& kernel, Pointer<Device> device) :
  mKernel(kernel),
  pDevice(device) {

}

void Kernel::setArg(size_t index, const in& value) {
  if (value.sBuffer != nullptr) {
    value.sBuffer->addToKernel(index, mKernel);
    mInBuffer.push_back(value.sBuffer);
  } else {
    setArgNull(index, sizeof(cl_mem));
  }
}

void Kernel::setArg(size_t index, const out& value) {
  if (value.sBuffer != nullptr) {
    value.sBuffer->addToKernel(index, mKernel);
    mOutBuffer.push_back(value.sBuffer);
  } else {
    setArgNull(index, sizeof(cl_mem));
  }
}

void Kernel::setArg(size_t index, const inout& value) {
  if (value.sBuffer != nullptr) {
    value.sBuffer->addToKernel(index, mKernel);
    mInBuffer.push_back(value.sBuffer);
    mOutBuffer.push_back(value.sBuffer);
  } else {
    setArgNull(index, sizeof(cl_mem));
  }
}

void Kernel::setArgNull(size_t index, size_t size) {
  mKernel.set_arg(index, size, nullptr);
}

void Kernel::setArg(size_t index, const LocalMemory& value) {
  // Local memory only gets the size and no pointer to any data
  mKernel.set_arg(index, value.getSize(), nullptr);
}

void Kernel::setArg(size_t index, const bool& value) {
  // Bools are casted to chars
  mKernel.set_arg(index, static_cast<char>(value));
}

Future<void> Kernel::execute(size_t global_size, size_t local_size,
                             ConcurrentContext& context) {
  boost::compute::wait_list wait_list;
  for (auto& buffer : mInBuffer)
    context.addEventsToWaitList(buffer, wait_list);

  auto result = pDevice->getScheduler()->executeKernel(mKernel, global_size,
                                                       local_size, wait_list);

  for (auto& buffer : mOutBuffer)
    buffer->addEvents(result.getEvents());

  context.addEvents(result.getEvents());

  return result;
}

unsigned int Kernel::getWorkGroupSize() {
  return mKernel.get_work_group_info<size_t>(*pDevice,
                                             CL_KERNEL_WORK_GROUP_SIZE);
}

unsigned int Kernel::getBestWorkGroupSize(
    const std::vector<std::shared_ptr<Kernel>>& kernels) {
  if (kernels.empty())
    return 0;

  unsigned int result = kernels.front()->getWorkGroupSize();

  for (size_t i = 1; i < kernels.size(); ++i)
    result = std::min(kernels[i]->getWorkGroupSize(), result);

  return result;
}

unsigned int Kernel::getLocalMemSize() {
  return mKernel.get_work_group_info<cl_ulong>(*pDevice,
                                               CL_KERNEL_LOCAL_MEM_SIZE);
}

unsigned int Kernel::getTuplesPerThread(unsigned int tuples) {
  return divisionWithRound(tuples, getGlobalSize());
}

unsigned int Kernel::getGlobalSize() {
  return pDevice->getMaxComputeUnits() * getWorkGroupSize();
}

LocalMemory::LocalMemory(DataType type, const size_t count) :
  mCount(count),
  mType(type) {

}

size_t LocalMemory::getSize() const {
  return GET_TYPE_SIZE(mType) * mCount;
}

} //namespace Ocelot
