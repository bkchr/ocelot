#include "Device.h"

#include "Platform.h"
#include "Scheduler.h"
#include "CacheManager.h"
#include "Exception.h"
#include "KernelLibrary.h"

#include <cstring>

namespace Ocelot
{

Device::Device(const boost::compute::device& device, Platform* platform,
               Pointer<CacheManager> cachemgr) :
  mDevice(device),
  mVendor(Vendor_Unknown),
  pPlatform(platform),
  pScheduler(nullptr),
  pCacheManager(cachemgr),
  pKernelLibrary(nullptr) {
}

Device::~Device() {
  delete pKernelLibrary;
  pKernelLibrary = nullptr;

  pCacheManager->unregisterDevice(this);

  delete pScheduler;
  pScheduler = nullptr;
}

void Device::init(const std::string& kernel_path) {
  mRemainingSpace = getGlobalMemorySize();
  initPlatformNameAndVendor();
  mUniqueId = getUniqueId(mDevice);

  pScheduler = new Scheduler(pPlatform->getContext(), this);
  pCacheManager->registerDevice(this);
  pKernelLibrary = new KernelLibrary(kernel_path, this);
}

void Device::initPlatformNameAndVendor() {
  mVendor = identifyVendor(mDevice.vendor());
}

size_t Device::getUniqueId(const boost::compute::device& device) {
  size_t uniqueid = 0;
  std::hash<std::string> hash;

  uniqueid = hash(device.name());
  uniqueid ^= hash(device.vendor());
  uniqueid ^= hash(device.version());
  uniqueid ^= std::hash<cl_uint>()(device.get_info<cl_uint>
                                    (CL_DEVICE_MAX_CLOCK_FREQUENCY));
  uniqueid ^= std::hash<unsigned long>()(device.global_memory_size());

  return uniqueid;
}

BufferPtr Device::allocateBuffer(DataType type, unsigned int count,
                                 void* host_ptr, bool ptr_stays_valid) {
  auto sizeinbytes = calculateBufferSizeInBytes(count, type);
  if (sizeinbytes > getMaxMemoryAlloc()) {
    THROW_EXCEPTION(sizeinbytes,
                    " bytes for one buffer aren't supported at the device",
                    mDevice.name(), " !");
  }

  cl_mem_flags flags;

  if (isHostUnifiedMemory() && host_ptr && ptr_stays_valid) {
    flags = boost::compute::buffer::use_host_ptr |
        boost::compute::buffer::read_only;
  } else if (host_ptr) {
    flags = boost::compute::buffer::copy_host_ptr |
        boost::compute::buffer::read_write;
  } else {
    flags = boost::compute::buffer::read_write;
  }

  auto mem = allocateBufferWithTest(sizeinbytes, flags, host_ptr);
  mem.set_destructor_callback([this, sizeinbytes]()
                              { mRemainingSpace += sizeinbytes; });

  mRemainingSpace -= sizeinbytes;

  auto buffer = DI_CREATE_INSTANCE(Buffer, mem, count,
                                   WPointer<Device>(shared_from_this()),
                                   type);

  return BufferPtr(buffer);
}

BufferPtr Device::allocateBufferOutOfVectors(
    const std::vector<ResourceId>& vectors) {
  auto vector_buffers = getVectorBuffersForResourceIds(vectors);

  unsigned int count = 0;
  for (auto& buffer : vector_buffers) {
    count += buffer->getCount();
  }

  auto result = allocateBuffer(vector_buffers.front()->getType(), count);
  size_t offset = 0;
  for (auto& buffer : vector_buffers) {
    buffer->copy(result, buffer->getCount(), 0, offset);
    offset += buffer->getCount();
  }

  return result;
}

std::vector<BufferPtr> Device::getVectorBuffersForResourceIds(
    const std::vector<ResourceId>& ids) {
  std::vector<BufferPtr> vector_buffers;

  for (auto& vec : ids) {
    auto buffer = pCacheManager->getBuffer(vec,
      [this](const Pointer<Device> dev) {
        if (dev == shared_from_this()) {
          return 3;
        } else if (dev->isHostUnifiedMemory()) {
          return 2;
        } else {
          return 1;
        }
      });
    vector_buffers.push_back(buffer);
  }

  return vector_buffers;
}

unsigned int Device::calculateBufferSizeInBytes(
    unsigned int count, DataType type) {
  auto type_size_in_bytes = GET_TYPE_SIZE(type);

  auto size_in_bytes = Buffer::getSizeInBytes(type, count);

  // the alignment is important for the atomic functions, because the atomic
  // functions only support 4 byte width elements
  if (type_size_in_bytes == 1) {
    size_in_bytes += 3 - size_in_bytes % 4;
  } else if (type_size_in_bytes == 2) {
    size_in_bytes += 2 - size_in_bytes % 4;
  }

  return size_in_bytes;
}

boost::compute::buffer Device::allocateBufferWithTest(
    unsigned int size, cl_mem_flags flags, void* host_ptr) {
  boost::compute::buffer mem(pPlatform->getContext(), size, flags, host_ptr);

  size_t trials = 0;
  const size_t max_trials = 5;
  double safety_margin = 1.3;
  bool buffer_created = false;
  for (; !(buffer_created = pScheduler->testBufferCreation(mem)) &&
         trials < max_trials;
       ++trials) {
    // if we couldn't freeup anymore space, we set the trial count to max_trials
    // and test a last time if we maybe now are able to allocate the buffer
    if (!pCacheManager->freeUpSpace(
          static_cast<size_t>(size * safety_margin), shared_from_this())) {
      trials = max_trials;
    }

    mem = boost::compute::buffer(pPlatform->getContext(), size, flags,
                                 host_ptr);
    safety_margin *= 1.3;
  }

  if (buffer_created)
    return mem;

  THROW_EXCEPTION("Couldn't allocate buffer!");
}

bool Device::isHostUnifiedMemory() const {
  return mDevice.get_info<bool>(CL_DEVICE_HOST_UNIFIED_MEMORY);
}

cl_ulong Device::getGlobalMemorySize() const {
  return mDevice.global_memory_size();
}

cl_ulong Device::getMaxMemoryAlloc() const {
  return mDevice.max_memory_alloc_size();
}

ResourceId Device::registerBufferInCache(const BufferPtr& ptr,
                                         const CacheCost::CalculatorPtr& cost) {
  return pCacheManager->registerBuffer(ptr, cost);
}

void Device::unregisterBufferInCache(const ResourceId id) {
  pCacheManager->unregisterBuffer(id);
}

BufferPtr Device::getCachedBuffer(const ResourceId id) {
  return pCacheManager->getBuffer(id, shared_from_this());
}

cl_uint Device::getMemBaseAddrAlign() const {
  return mDevice.get_info<CL_DEVICE_MEM_BASE_ADDR_ALIGN>();
}

bool Device::checkOpenClVersion(int major, int minor) const {
  return mDevice.check_version(major, minor);
}

} //namespace Ocelot
