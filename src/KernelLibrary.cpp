#include "KernelLibrary.h"

#include "Device.h"
#include "Platform.h"
#include "Vendor.h"
#include "Kernel.h"

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <iostream>

namespace Ocelot
{

KernelLibrary::KernelLibrary(const std::string& dir, Device* device) :
  pDevice(device),
  mHasCacheSupport(false),
  mKernelDir(dir) {
  init();
}

KernelLibrary::~KernelLibrary() {
  if (!mKernelSourceDebuggingFileName.empty()) {
    std::remove(mKernelSourceDebuggingFileName.c_str());
  }
}

void KernelLibrary::init() {
  // If we got to many threads, some kernels are reused while they are still
  // running
  mHasCacheSupport = false;//pDevice->getVendor() == Vendor_Intel;

  auto source = readAllKernels();
  exportKernelsForDebugging(source);
  OCELOT_ASSERT(!source.empty(), "Couldn't load any kernel sources!");

  initPrograms(source);

  mCache.resize(DataTypeNum);
}

void KernelLibrary::initPrograms(const std::string& source) {
  mPrograms.resize(DataTypeNum);

  for (DataType type = 0; type < DataTypeNum; ++type) {
    auto program = boost::compute::program::create_with_source(
          source, pDevice->getPlatform()->getContext());

    try {
      program.build(getBuildOptions(type));
    } catch(const boost::compute::opencl_error& error) {
      std::cout<<"Error compiling for type with  "<<
                 GET_DATATYPE_NAME(type)<<std::endl;
      std::cout<<error.error_string()<<std::endl;
      if (!program.get_devices().empty()) {
        std::cout<<program.build_log()<<std::endl;
      }
    }

    mPrograms[type] = program;
  }
}

std::string KernelLibrary::getBuildOptions(const DataType type) {
  std::stringstream ss;

  ss<<"-I"<<mKernelDir;
  ss<<" -DMAXBLOCKSIZE="<<pDevice->getMaxWorkgroupSize();

  if (pDevice->getType() == Device::CPU) {
    ss<<" -DDEVICE_CPU=1";
  } else if (pDevice->getType() == Device::GPU) {
    ss<<" -DDEVICE_GPU=1";
  }

  ss<<" -DTYPE="<<GET_DATATYPE_OPENCL_MAPPING(type);

#ifndef NDEBUG
  if (pDevice->getType() == Device::CPU &&
      pDevice->getVendor() == Vendor_Intel && pDevice->getPlatform()->getVendor() == Vendor_Intel) {
    ss<<" -g -s \""<<mKernelSourceDebuggingFileName<<"\"";
  }
#endif

  return ss.str();
}

std::string KernelLibrary::readAllKernels() {
  boost::filesystem::path path(mKernelDir);
  std::string source = "";

  for (boost::filesystem::directory_iterator itr(path), end;
       itr != end; ++itr) {
    if (boost::filesystem::is_regular_file(itr->path()) &&
        itr->path().generic_string().find(".cl") != std::string::npos) {
      std::ifstream stream(itr->path().generic_string());

      source += std::string(std::istreambuf_iterator<char>(stream),
                            std::istreambuf_iterator<char>());
    }
  }

  return source;
}

void KernelLibrary::exportKernelsForDebugging(const std::string& source) {
  if (pDevice->getType() != Device::CPU ||
      pDevice->getVendor() != Vendor_Intel)
    return;

  mKernelSourceDebuggingFileName = boost::filesystem::unique_path().string();

  while (boost::filesystem::exists(
           boost::filesystem::temp_directory_path().string() + "/" +
           mKernelSourceDebuggingFileName)) {
    mKernelSourceDebuggingFileName = boost::filesystem::unique_path().string();
  }

  mKernelSourceDebuggingFileName =
      boost::filesystem::temp_directory_path().string() + "/" +
      mKernelSourceDebuggingFileName;

  std::ofstream of(mKernelSourceDebuggingFileName, std::ofstream::out);
  of<<source;
  of.close();
}

boost::compute::kernel KernelLibrary::createKernel(const std::string& name,
                                                   const DataType type) {
  if (mHasCacheSupport) {
    std::lock_guard<std::mutex> lock(mCacheMutex);

    auto& typecache = mCache[type];
    auto find = typecache.find(name);

    if (find != typecache.end())
      return find->second;

    return typecache[name] = mPrograms[type].create_kernel(name);
  } else {
    return mPrograms[type].create_kernel(name);
  }
}

boost::compute::kernel KernelLibrary::createKernel(const std::string& name,
                                                   const std::string& source,
                                                   const DataType type) {
  auto prog = boost::compute::program::create_with_source(
        source, pDevice->getPlatform()->getContext());

  try {
    prog.build(getBuildOptions(type));
  } catch(...) {
    THROW_EXCEPTION("Error building kernel \"", name, "\"\n", prog.build_log());
  }

  return prog.create_kernel(name);
}

KernelPtr KernelLibrary::createKernelPtr(const boost::compute::kernel& kernel) {
  return std::make_shared<Kernel>(kernel, pDevice->shared_from_this());
}

} //namespace Ocelot
