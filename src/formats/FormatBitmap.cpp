#include "FormatBitmap.h"

#include "Column.h"
#include "CacheCostCalculator.h"
#include "FormatRaw.h"
#include "PrefixSum.h"

namespace Ocelot
{

namespace Format
{

Bitmap::Bitmap(const Pointer<Column>& col, DataType type) :
  mColumn(col),
  mType(type) {
  mBitmap = std::make_shared<Raw>(col, DTypeBitmap);
  mOffsets = std::make_shared<Raw>(col, DTypeS32);

  if (mType != DTypeS32) {
    THROW_EXCEPTION("Not supported!");
  }
}

Bitmap::Bitmap(const Pointer<Column>& col, DataType type,
               const Pointer<Buffer>& ptr,
               const Pointer<CacheCost::Calculator>& cost) :
  mColumn(col),
  mType(type) {
  mBitmap = std::make_shared<Raw>(col, DTypeBitmap, ptr, cost);
  if (mType != DTypeS32) {
    THROW_EXCEPTION("Not supported!");
  }
}

BufferPtr Bitmap::getDeviceData(Pointer<Device> dev, bool offsets) {
  if (offsets) {
    if (mOffsets) {
      return mOffsets->getDeviceData(dev);
    }

    std::lock_guard<std::mutex> lock(mOffsetsMutex);

    if (mOffsets) {
      return mOffsets->getDeviceData(dev);
    }

    auto offsets_buffer = createOffsets(mBitmap->getDeviceData(dev));

    mOffsets = std::make_shared<Raw>(mColumn, DTypeS32, offsets_buffer.first,
                                     offsets_buffer.second);
    return offsets_buffer.first;
  } else {
    return mBitmap->getDeviceData(dev);
  }
}

BufferPtr Bitmap::getDeviceDataVector(Pointer<Device> dev,
                                      unsigned short index, bool offsets) {
  if (offsets) {
    auto tmp_buff = mOffsets->getDeviceDataVector(dev, index);

    if (tmp_buff) {
      return tmp_buff;
    }

    auto offsets_buffer = createOffsets(
                            mBitmap->getDeviceDataVector(dev, index));
    mOffsets->registerDeviceDataVector(offsets_buffer.first,
                                       offsets_buffer.second, index);
    return offsets_buffer.first;
  }

  return mBitmap->getDeviceDataVector(dev, index);
}

void Bitmap::registerDeviceDataVector(
    const Pointer<Buffer>& ptr, const Pointer<CacheCost::Calculator>& cost,
    unsigned short index) {
  mBitmap->registerDeviceDataVector(ptr, cost, index);
}

cl_uint Bitmap::getCount() {
  return mBitmap->getCount();
}

cl_uint Bitmap::getSizeInBytes() {
  return getCount() * GET_TYPE_SIZE(mType);
}

std::pair<Pointer<Buffer>, Pointer<CacheCost::Calculator>>
  Bitmap::createOffsets(Pointer<Buffer> bitmap) {
  OperationId opid;
  BufferPtr result;

  {
    OCL_PROFILE_OPERATION_SCOPE("Bitmap::createOffsets");
    opid = OCL_CURRENT_OPERATION_ID();
    result = PrefixSum(bitmap).getResultBuffer();
  }

  return std::make_pair(result, CacheCost::Operation::createInstance(opid));
}

} //namespace Format

} //namespace Ocelot
