#include "FormatBitmapToRawConverter.h"

#include "Column.h"
#include "FormatBitmap.h"
#include "FormatRaw.h"

namespace Ocelot
{

namespace Format
{

OCL_FORMAT_REGISTER_CONVERTER(BitmapToRawConverter, Bitmap, Raw);

BitmapToRawConverter::BitmapToRawConverter(Pointer<Column> col) :
  mCol(col) {

}

void BitmapToRawConverter::convertHostData() {
  THROW_EXCEPTION("Not supported!");
}

void BitmapToRawConverter::convertDeviceData(Pointer<Device> dev) {
  BufferPtr result;
  CacheCost::CalculatorPtr cost;

  if (mCol->getType() == DTypeS8) {
    expandBuffer(dev, mCol->getData<Bitmap>(dev), result, cost);
  } else {
    materializeBuffer(dev, mCol->getData<Bitmap>(dev),
                      mCol->getData<Bitmap>(dev, true), result, cost);
  }

  mCol->registerData<Raw>(result, cost);
}

void BitmapToRawConverter::convertDeviceDataVector(
    Pointer<Device> dev, unsigned short index) {
  BufferPtr result;
  CacheCost::CalculatorPtr cost;

  if (mCol->getType() == DTypeS8) {
    expandBuffer(dev, mCol->getDeviceDataVector<Bitmap>(dev, index), result,
                 cost);
  } else {
    materializeBuffer(dev, mCol->getDeviceDataVector<Bitmap>(dev, index),
                      mCol->getDeviceDataVector<Bitmap>(dev, index, true),
                      result, cost);
  }

  mCol->registerDeviceDataVector<Raw>(result, cost, index);
}

void BitmapToRawConverter::materializeBuffer(
    Pointer<Device> dev, Pointer<Buffer> buffer, Pointer<Buffer> offsets,
    Pointer<Buffer>& result, CacheCost::CalculatorPtr& cost) {
  OperationId id;

  {
    OCL_PROFILE_OPERATION_SCOPE("BitmapToRawConverter::materializeBuffer");
    id = OCL_CURRENT_OPERATION_ID();
    auto kernel = dev->getKernel("bm_materialize");

    auto local_size = kernel->getWorkGroupSize();
    auto global_size = divisionWithRound(buffer->getElementCount(), local_size)
                       * local_size;
    result = dev->allocateBuffer(mCol->getType(), buffer->getCount());

    kernel->setArgs(in(buffer), in(offsets), out(result),
                    buffer->getElementCount());
    kernel->execute(global_size, local_size);
  }

  cost = CacheCost::Operation::createInstance(id);
}

void BitmapToRawConverter::expandBuffer(
    Pointer<Device> dev, Pointer<Buffer> buffer, Pointer<Buffer>& result,
    CacheCost::CalculatorPtr& cost) {
  OperationId id;

  {
    OCL_PROFILE_OPERATION_SCOPE("BitmapToRawConverter::expandBuffer");
    id = OCL_CURRENT_OPERATION_ID();
    auto kernel = dev->getKernel("bm_expand");

    auto local_size = kernel->getWorkGroupSize();
    auto global_size = divisionWithRound(buffer->getElementCount(), local_size)
                       * local_size;
    result = dev->allocateBuffer(mCol->getType(), buffer->getCount());

    kernel->setArgs(in(buffer), out(result), buffer->getElementCount());
    kernel->execute(global_size, local_size);
  }

  cost = CacheCost::Operation::createInstance(id);
}

} //namespace Format

} //namespace Ocelot
