#include "FormatRawToBitmapConverter.h"

#include "Column.h"
#include "FormatBitmap.h"
#include "FormatRaw.h"
#include "InitBuffer.h"

namespace Ocelot
{

namespace Format
{

OCL_FORMAT_REGISTER_CONVERTER(RawToBitmapConverter, Raw, Bitmap);

RawToBitmapConverter::RawToBitmapConverter(Pointer<Column> col) :
  mCol(col) {

}

void RawToBitmapConverter::convertHostData() {
  THROW_EXCEPTION("Not supported!");
}

void RawToBitmapConverter::convertDeviceData(Pointer<Device> dev) {
  BufferPtr result;
  CacheCost::CalculatorPtr cost;

  if (mCol->isDense()) {
    generateFilledBitmap(dev, mCol->getCount(), result, cost);
  } else {
    encodeBitmap(dev, mCol->getData<Raw>(dev), result, cost);
  }

  mCol->registerData<Bitmap>(result, cost);
}

void RawToBitmapConverter::convertDeviceDataVector(
    Pointer<Device> dev, unsigned short index) {
  BufferPtr result;
  CacheCost::CalculatorPtr cost;

  if (mCol->isDense()) {
    generateFilledBitmap(dev, mCol->getDeviceDataVector<Bitmap>(
                           dev, index)->getCount(), result, cost);
  } else {
    encodeBitmap(dev, mCol->getDeviceDataVector<Bitmap>(dev, index), result,
                 cost);
  }

  mCol->registerDeviceDataVector<Bitmap>(result, cost, index);
}

void RawToBitmapConverter::generateFilledBitmap(
    Pointer<Device> dev, unsigned int tuples, Pointer<Buffer>& result,
    CacheCost::CalculatorPtr& cost) {
  OperationId id;

  {
    OCL_PROFILE_OPERATION_SCOPE("RawToBitmapConverter::generateFilledBitmap");
    id = OCL_CURRENT_OPERATION_ID();

    result = dev->allocateBuffer(DTypeBitmap, tuples);

    auto kernel = dev->getKernel("generateFilledBitmap");
    kernel->setArgs(out(result), result->getCount());
    kernel->execute(result->getElementCount(), 0);
  }

  cost = CacheCost::Operation::createInstance(id);
}

void RawToBitmapConverter::encodeBitmap(
    Pointer<Device> dev, Pointer<Buffer> buffer, Pointer<Buffer>& result,
    CacheCost::CalculatorPtr& cost) {
  if (!mCol->isSorted() && !mCol->isRevSorted()) {
    THROW_EXCEPTION("Only sorted columns are supported!");
  }

  OperationId id;
  {
    OCL_PROFILE_OPERATION_SCOPE("RawToBitmapConverter::encodeBitmap");
    id = OCL_CURRENT_OPERATION_ID();

    unsigned int highest_val =
        *buffer->read<unsigned int>(1,
          mCol->isSorted() ? buffer->getCount() - 1 : 0).get().get();
    result = dev->allocateBuffer(DTypeBitmap, highest_val + 1);
    InitBuffer(result, InitBuffer::WithZeros);

    auto kernel = dev->getKernel("bm_encode_sorted");
    auto local_size = kernel->getWorkGroupSize();
    auto tuples_per_thread =
        divisionWithRound(buffer->getCount(),
                          dev->getMaxComputeUnits() *  local_size);

    if (tuples_per_thread < 32) {
      tuples_per_thread = 32;
    }

    kernel->setArgs(in(buffer), buffer->getCount(), inout(result),
                    tuples_per_thread);
    kernel->execute(local_size * dev->getMaxComputeUnits(), local_size);
  }

  cost = CacheCost::Operation::createInstance(id);
}

} //namespace Format

} //namespace Ocelot
