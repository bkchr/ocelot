#include "DeviceManager.h"

#include "Device.h"
#include "PlatformManager.h"
#include "Platform.h"
#include "DependencyInjection.h"
#include "DeviceSelectionManager.h"
#include "GlobalVectorSizeDetermination.h"
#include "Log.h"

namespace Ocelot
{

DeviceManager::DeviceManager(Pointer<PlatformManager> plmgr,
                             Pointer<CacheManager> cachemgr,
                             const std::string& kernel_path) :
  mPlatformManager(plmgr) {
  DI_REGISTER_OBJECT(DeviceManager);
  DI_REGISTER_CONSTANT(GlobalVectorSize,
                       [this]() { return mGlobalVectorSizeValue; });
  init(cachemgr, kernel_path);
}

DeviceManager::~DeviceManager() {
  deinit();
  DI_UNREGISTER_OBJECT(DeviceManager);
  DI_UNREGISTER_CONSTANT(GlobalVectorSize);
}

void DeviceManager::init(Pointer<CacheManager> cachemgr,
                         const std::string& kernel_path) {
  auto pldevices = getDevicesPerPlatform();
  initPlatformContexts(pldevices);
  initDevices(pldevices, cachemgr, kernel_path);
  initGlobalVectorSizeConstant();
  mDeviceSelectionManager = std::make_shared<DeviceSelectionManager>(this);
}

void DeviceManager::deinit() {
  mDevices.clear();
}

DeviceManager::PlatformDeviceMap DeviceManager::getDevicesPerPlatform() {
  auto platforms = mPlatformManager->getPlatforms();
  std::unordered_map<std::size_t,
      std::pair<boost::compute::device, Platform*>> devices;

  for (auto pl : platforms) {
    auto pldevices = pl->getAvailableDevices();

    for (auto& adevice : pldevices) {
      auto uniqueid = Device::getUniqueId(adevice.sDevice);
      auto find = devices.find(uniqueid);

      if (find == devices.end()) {
        devices[uniqueid] = std::make_pair(adevice.sDevice, pl);
      } else if(find != devices.end() && adevice.sNativePlatform) {
        find->second = std::make_pair(adevice.sDevice, pl);
      }
    }
  }

  PlatformDeviceMap result;
  for (const auto& itr : devices)
    result[itr.second.second].push_back(itr.second.first);

  return result;
}

void DeviceManager::initPlatformContexts(const PlatformDeviceMap& devices) {
  for (const auto& itr : devices)
    itr.first->createOpenCLContext(itr.second);
}

void DeviceManager::initDevices(const PlatformDeviceMap& devices,
                                Pointer<CacheManager> cachemgr,
                                const std::string& kernel_path) {
  for (const auto& itr : devices) {
    for (const auto& device : itr.second) {
      auto new_device = std::make_shared<Device>(device, itr.first, cachemgr);
      new_device->init(kernel_path);
      mDevices.push_back(new_device);
    }
  }

  LOG_INFO("Initialized ", mDevices.size(), " devices.");
}

void DeviceManager::initGlobalVectorSizeConstant() {
  //auto determination = GlobalVectorSizeDetermination(this);
  //mGlobalVectorSizeValue = determination.getGlobalVectorSize();
  mGlobalVectorSizeValue = 4096 * 4;

  LOG_INFO("Set global vector size to ", mGlobalVectorSizeValue, " Bytes.");
}

}	//namespace Ocelot
