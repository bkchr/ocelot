#include "CachedBuffer.h"

#include "Device.h"
#include "Exception.h"
#include "Profiler.h"

namespace Ocelot
{

CachedBuffer::CachedBuffer() :
  mBufferId(Invalid_ResourceId) {

}

void CachedBuffer::registerVector(
    const BufferPtr& ptr, const CacheCost::CalculatorPtr& cost,
    unsigned short index) {
  boost::unique_lock<boost::shared_mutex> lock(mMutex);

  if (mBufferId != Invalid_ResourceId)
    THROW_EXCEPTION("The buffer was already created! Adding another vector "
                    "isn't supported at the moment!");

  assert(mVectors.find(index) == mVectors.end() &&
         "A vector with the index is already registered!");

  auto id = ptr->getDevice()->registerBufferInCache(ptr, cost);
  mVectors[index] = id;
}

BufferPtr CachedBuffer::getVector(Pointer<Device> dev, unsigned short index) {
  boost::shared_lock<boost::shared_mutex> lock(mMutex);

  if (mVectors.empty()) {
    // TODO, yeah maybe it would be better if we wouldn't transfer the whole
    // buffer here...
    return dev->getCachedBuffer(mBufferId)->createSubBuffer(index);
  } else {
    assert(mVectors.find(index) != mVectors.end());
    return dev->getCachedBuffer(mVectors[index]);
  }
}

void CachedBuffer::registerBuffer(const BufferPtr& ptr,
                                  const CacheCost::CalculatorPtr& cost) {
  boost::unique_lock<boost::shared_mutex> lock(mMutex);

  mBufferId = ptr->getDevice()->registerBufferInCache(ptr, cost);
}

BufferPtr CachedBuffer::getBuffer(Pointer<Device> dev) {
  if (mBufferId != Invalid_ResourceId) {
    boost::shared_lock<boost::shared_mutex> lock(mMutex);
    return dev->getCachedBuffer(mBufferId);
  } else {
    boost::unique_lock<boost::shared_mutex> lock(mMutex);
    return mergeVectors(dev);
  }
}

BufferPtr CachedBuffer::mergeVectors(Pointer<Device> dev) {
  if (mBufferId != Invalid_ResourceId)
    return dev->getCachedBuffer(mBufferId);

  auto buffer = dev->allocateBufferOutOfVectors(getVectors());

  mBufferId = dev->registerBufferInCache(
                buffer, CacheCost::CalculatorPtr(
                  new CacheCost::Aggregation(getVectors())));

  // we merged all vectors into one big buffer, the vectors are
  // not needed anymore
  for (auto& itr : mVectors)
    dev->unregisterBufferInCache(itr.second);
  mVectors.clear();

  return buffer;
}

std::vector<ResourceId> CachedBuffer::getVectors() {
  std::vector<ResourceId> result;

  for (auto& itr : mVectors)
    result.push_back(itr.second);

  return result;
}

Future<std::shared_ptr<void>> CachedBuffer::getHostData() {
  boost::shared_lock<boost::shared_mutex> lock(mMutex);

  std::vector<BufferPtr> buffers = getBuffersForHostTransfer();

  size_t bytesize = 0;
  for (auto& buffer : buffers)
    bytesize += buffer->getSizeInBytes();

  char* ptr = new char[bytesize];

  auto delcallback = [](char* ptr) { delete[] ptr; };
  auto result_ptr = std::shared_ptr<void>(ptr, delcallback);
  auto future = getHostData(ptr, buffers);

  return make_Future(result_ptr, future);
}

Future<void> CachedBuffer::getHostData(void* ptr) {
  boost::shared_lock<boost::shared_mutex> lock(mMutex);

  std::vector<BufferPtr> buffers = getBuffersForHostTransfer();

  return getHostData(ptr, buffers);
}

std::vector<BufferPtr> CachedBuffer::getBuffersForHostTransfer() {
  if (mBufferId != Invalid_ResourceId)
    return { pCacheManager->getBufferForHostTransfer(mBufferId) };
  else
    return pCacheManager->getBuffersForHostTransfer(getVectors());
}

Future<void> CachedBuffer::getHostData(
    void* ptr, const std::vector<BufferPtr>& buffers) {

  size_t offset = 0;
  std::vector<Event> events;
  for (auto& buffer : buffers) {
    auto future = buffer->readBytes(static_cast<char*>(ptr) + offset,
                                    buffer->getSizeInBytes());

    for (auto& event : future.getEvents())
      events.push_back(event);

    offset += buffer->getSizeInBytes();
  }

  return make_Future(events);
}

} //namespace Ocelot
