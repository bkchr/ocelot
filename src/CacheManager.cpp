#include "CacheManager.h"

#include "Cache.h"
#include "DependencyInjection.h"
#include "Device.h"
#include "CacheCostCalculator.h"

namespace Ocelot
{

CacheManager::CacheManager() :
  mNextResourceId(First_Valid_ResourceId) {
  DI_REGISTER_OBJECT(CacheManager);
}

CacheManager::~CacheManager() {
  for (auto itr : mCaches)
    delete itr.second;
  mCaches.clear();
  DI_UNREGISTER_OBJECT(CacheManager);
}

void CacheManager::registerDevice(Device* device) {
  if (mCaches.find(device) != mCaches.end())
    return;

  mCaches[device] = new Cache(device);
}

void CacheManager::unregisterDevice(Device* device) {
  auto find = mCaches.find(device);

  if (find == mCaches.end())
    return;

  delete find->second;
  mCaches.erase(find);
}

ResourceId CacheManager::registerBuffer(const BufferPtr& ptr,
                                        const CacheCost::CalculatorPtr& cost) {
  assert(mCaches.find(ptr->getDevice().get()) != mCaches.end() &&
      "No cache for device registered!");

  cost->setCacheManager(this);
  ResourceId id = mNextResourceId++;
  mCaches[ptr->getDevice().get()]->registerBuffer(ptr, id, cost);

  return id;
}

void CacheManager::unregisterBuffer(const ResourceId id) {
  for (auto& itr : mCaches)
    itr.second->unregisterBuffer(id);
}

BufferPtr CacheManager::getBuffer(const ResourceId id, Pointer<Device> device) {
  assert(mCaches.find(device.get()) != mCaches.end() &&
      "No cache for device registered!");

  auto cache = mCaches[device.get()];
  auto ptr = cache->getBuffer(id);

  if (ptr != nullptr)
    return ptr;

  return findAndTransferBuffer(id, cache);
}

BufferPtr CacheManager::findAndTransferBuffer(const ResourceId id,
                                              Cache* dst_cache) {
  auto src_cache = findCacheWithBuffer(id);

  if (src_cache == nullptr)
    return nullptr;

  return src_cache->transferBuffer(id, dst_cache);
}

Cache* CacheManager::findCacheWithBuffer(const ResourceId id) {
  for (auto& itr : mCaches) {
    if (itr.second->hasBuffer(id))
      return itr.second;
  }

  return nullptr;
}

CacheCost::CalculatorPtr CacheManager::getBufferCost(const ResourceId id) {
  auto cache = findCacheWithBuffer(id);

  if (cache)
    return cache->getBufferCost(id);
  else
    return nullptr;
}

bool CacheManager::freeUpSpace(size_t required_space, Pointer<Device> device) {
  assert(mCaches.find(device.get()) != mCaches.end() &&
      "No cache for device registered!");

  return mCaches[device.get()]->freeUpSpace(required_space);
}

BufferPtr CacheManager::getBuffer(
    const ResourceId id, const DeviceChooseFunction& choose) {
  std::multimap<unsigned short, Cache*,
      std::greater<unsigned short>> best_caches;
  for (const auto& itr : mCaches)
    best_caches.insert({choose(itr.first->shared_from_this()), itr.second});

  for (auto& itr : best_caches) {
    auto buffer = itr.second->getBuffer(id);

    if (buffer != nullptr)
      return buffer;
  }

  return nullptr;
}

BufferPtr CacheManager::getBufferForHostTransfer(const ResourceId id) {
  return getBuffer(id, [](const Pointer<Device> dev) -> unsigned short {
    unsigned short result = dev->isHostUnifiedMemory();
    result += dev->getType() == Device::CPU;
    return result;
   });
}

std::vector<BufferPtr> CacheManager::getBuffersForHostTransfer(
    const std::vector<ResourceId>& ids) {
  std::vector<BufferPtr> result;

  for (auto id : ids)
    result.push_back(getBufferForHostTransfer(id));

  return result;
}

} //namespace Ocelot
