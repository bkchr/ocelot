#include "PlatformManager.h"

#include "Platform.h"
#include "DependencyInjection.h"

#include <boost/compute/system.hpp>

namespace Ocelot
{

PlatformManager::PlatformManager() {
  DI_REGISTER_OBJECT(PlatformManager);
  init();
}

PlatformManager::~PlatformManager() {
  DI_UNREGISTER_OBJECT(PlatformManager);
  deinit();
}

void PlatformManager::init() {
  for(auto& platform : boost::compute::system::platforms())
    mPlatforms.push_back(new Platform(platform));
}

void PlatformManager::deinit() {
  for (auto pl : mPlatforms)
    delete pl;

  mPlatforms.clear();
}

} //namespace Ocelot
