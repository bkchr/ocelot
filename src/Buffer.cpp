#include "Buffer.h"

#include "Device.h"
#include "Scheduler.h"

#include <functional>
#include <bitset>

#include <boost/compute/algorithm.hpp>

namespace Ocelot
{

#define LOCK() std::lock_guard<std::recursive_mutex> lock(mMutex)

Buffer::Buffer(const boost::compute::buffer& buffer, unsigned int count,
               WPointer<Device> device, const DataType type,
               const GlobalVectorSize vec_size) :
  mBuffer(buffer),
  mDevice(device),
  mType(type),
  mCount(count),
  mGlobalVectorSize(vec_size) {

}

Buffer::~Buffer() {
}

cl_uint Buffer::getCount() const {
  return mCount;
}

cl_uint Buffer::getElementCount() const {
  return divisionWithRound(getCount(), GET_COUNT_PER_ELEMENT(mType));
}

cl_uint Buffer::getSizeInBytes() const {
  return getSizeInBytes(mType, getCount());
}

cl_uint Buffer::getSizeInBytes(DataType type, cl_uint count) {
  auto size_in_bytes = divisionWithRound(GET_TYPE_SIZE(type) * count,
                                         GET_COUNT_PER_ELEMENT(type));

  auto modulo = size_in_bytes % GET_TYPE_SIZE(type);
  return size_in_bytes + (modulo > 0 ? GET_TYPE_SIZE(type) - modulo : 0);
}

Future<void> Buffer::writeBytes(const void* host_ptr, size_t size,
                                size_t offset) {
  size = size > 0 ? size : getSizeInBytes();
  assert(offset + size <= getSizeInBytes());

  auto result = mDevice.lock()->getScheduler()->writeBuffer(
                  mBuffer, host_ptr, size, offset);

  addEvents(result.getEvents());

  return result;
}

template<>
Future<std::shared_ptr<void>> Buffer::read<void>(
    size_t count, size_t offset) const {
  count = count > 0 ? count : getCount();
  void* host_ptr = new char[count * GET_TYPE_SIZE(getType())];

  auto result = readBytes(host_ptr, count * GET_TYPE_SIZE(getType()),
                          offset * GET_TYPE_SIZE(getType()));
  auto resultptr = std::shared_ptr<void>(
        host_ptr, [](void* del) { delete[] static_cast<char*>(del); });
  return make_Future(resultptr, result);
}

Future<void*> Buffer::readBytes(void* host_ptr, size_t size,
                                size_t offset) const {
  boost::compute::wait_list wait_list;
  addEventsToWaitList(wait_list);

  if (!host_ptr) {
    host_ptr = new char[size];
  }

  return mDevice.lock()->getScheduler()->readBuffer(mBuffer, host_ptr, size,
                                                    offset, wait_list);
}

BufferPtr Buffer::clone() const {
  return mDevice.lock()->getScheduler()->cloneBuffer(this, mDevice.lock());
}

BufferPtr Buffer::clone(Pointer<Device> dst_device) const {
  return mDevice.lock()->getScheduler()->cloneBuffer(this, dst_device);
}

void Buffer::copy(std::shared_ptr<Buffer>& dst, size_t count, size_t src_offset,
                  size_t dst_offset) {
  auto src_size_offset = mapCountAndOffsetToByte(count, src_offset);
  auto dst_size_offset = mapCountAndOffsetToByte(0, dst_offset);

  mDevice.lock()->getScheduler()->copyBuffer(
        this, dst.get(), src_size_offset.second, dst_size_offset.second,
        src_size_offset.first);
}

BufferPtr Buffer::createSubBuffer(unsigned short index) {
  auto vector_count = mGlobalVectorSize / GET_TYPE_SIZE(mType);

  // make vector_count a multiple of GET_COUNT_PER_ELEMENT(DTypeBitmap)
  if (vector_count % GET_COUNT_PER_ELEMENT(DTypeBitmap) != 0) {
    vector_count *= vector_count / GET_COUNT_PER_ELEMENT(DTypeBitmap);
  }

  auto vector_size = getSizeInBytes(mType, vector_count);

  if (getCount() < vector_count) {
    return shared_from_this();
  }

  assert(vector_size * index < getSizeInBytes());

  auto size = vector_size * (index + 1) > getSizeInBytes() ?
        getSizeInBytes() - vector_size * index : vector_size;

  decltype(mEvents) events;
  BufferPtr sub_buffer;
  {
    LOCK();
    auto buffer = mBuffer.create_subbuffer(mBuffer.get_memory_flags(),
                                           vector_size * index, size);

    unsigned int count = size * GET_COUNT_PER_ELEMENT(mType) /
                         GET_TYPE_SIZE(mType);
    sub_buffer = BufferPtr(DI_CREATE_INSTANCE(
                             Buffer, buffer, count, mDevice, getType()));

    // prevent deadlocks
    events = mEvents;
  }

  sub_buffer->addEvents(events);

  return sub_buffer;
}

size_t Buffer::getMaxNumberOfSubBuffer() const {
  return getMaxNumberOfSubBuffer(getCount(), getType(), mGlobalVectorSize);
}

size_t Buffer::getMaxNumberOfSubBuffer(size_t count, DataType type,
                                       const cl_ulong vector_size) {
  auto vector_count = vector_size / GET_TYPE_SIZE(type);

  return divisionWithRound(count, vector_count);
}

void Buffer::addEvent(Event event) {
  LOCK();

  addEventIntern(event);
}

void Buffer::addEvents(std::vector<Event> events) {
  LOCK();

  for (auto& event : events) {
    addEventIntern(event);
  }
}

void Buffer::addEventIntern(Event event) {
  mEvents.push_back(event);

  std::function<void ()> callback =
      std::bind(&Buffer::handleOnEventCompleted, this, event,
                shared_from_this());

  event.set_callback(callback, boost::compute::event::complete);
}

void Buffer::handleOnEventCompleted(
    Event event, const std::shared_ptr<Buffer>&) {
  LOCK();

  auto find = std::find(mEvents.begin(), mEvents.end(), event);

  if (find != mEvents.end())
    mEvents.erase(find);
}

void Buffer::addEventsToWaitList(boost::compute::wait_list& wait_list,
                                 const EventFilterFunction& filter) const {
  std::lock_guard<std::recursive_mutex>
      lock(const_cast<std::recursive_mutex&>(mMutex));

  for (const auto& event : mEvents) {
    if (!filter || filter(event)) {
      wait_list.insert(event);
    }
  }
}

void Buffer::addToKernel(size_t index, boost::compute::kernel& kernel) {
  kernel.set_arg(index, mBuffer);
}

void Buffer::writeToFile(const std::string& file) {
  std::ofstream stream(file);
  writeToFile(stream);
}

void Buffer::writeToFile(std::ofstream& stream) {
  switch (getType()) {
    case DTypeS8:
      writeToFileTemplate<DTypeS8, DTypeS16>(stream);
      break;
    case DTypeS16:
      writeToFileTemplate<DTypeS16>(stream);
      break;
    case DTypeS32:
      writeToFileTemplate<DTypeS32>(stream);
      break;
    case DTypeS64:
      writeToFileTemplate<DTypeS64>(stream);
      break;
    case DTypeBitmap:
      writeToFileTemplate<DTypeBitmap>(stream);
      break;
    case DTypeF32:
      writeToFileTemplate<DTypeF32>(stream);
      break;
  }
}

template<>
void Buffer::writeToFileTemplate<DTypeBitmap>(std::ofstream& stream) {
  std::vector<GET_TYPE(DTypeBitmap)> data;
  data.resize(getElementCount());
  read(data.data()).wait();

  for (auto itr : data) {
    std::bitset<sizeof(GET_TYPE(DTypeBitmap)) * 8> bits(itr);
    stream<<bits.to_string()<<std::endl;
  }
}

std::pair<unsigned int, unsigned int> Buffer::mapCountAndOffsetToByte(
    const unsigned int count, const unsigned int offset) const {
  auto offset_byte = (offset * GET_TYPE_SIZE(mType)) /
                     GET_COUNT_PER_ELEMENT(mType);
  auto size = divisionWithRound(
                GET_TYPE_SIZE(mType) * (count + offset),
                GET_COUNT_PER_ELEMENT(mType)) - offset_byte;

  return std::make_pair(size, offset_byte);
}

#undef LOCK

} //namespace Ocelot
