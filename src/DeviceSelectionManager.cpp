#include "DeviceSelectionManager.h"

#include "DeviceManager.h"
#include "DependencyInjection.h"

namespace Ocelot
{

DeviceSelectionManager::DeviceSelectionManager(DeviceManager* manager) :
  pDeviceManager(manager) {
  DI_REGISTER_OBJECT(DeviceSelectionManager);
}

DeviceSelectionManager::~DeviceSelectionManager() {
  DI_UNREGISTER_OBJECT(DeviceSelectionManager);
}

Pointer<Device> DeviceSelectionManager::getDevice() {
  // TODO
  return pDeviceManager->getDevices().front();
}

}  //namespace Ocelot
