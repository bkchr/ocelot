#include "Platform.h"

#include "Device.h"

namespace Ocelot
{

Platform::Platform(const boost::compute::platform& platform) :
  mPlatform(platform),
  mVendor(Vendor_Unknown) {
  init();
}

void Platform::init() {
  mVendor = identifyVendor(mPlatform.name());
}

void Platform::createOpenCLContext(
    const std::vector<boost::compute::device>& devices) {
  cl_context_properties context_properties[] = {
      CL_CONTEXT_PLATFORM, (cl_context_properties)mPlatform.id(), 0
  };

  mContext = boost::compute::context(devices, context_properties);
}

std::vector<Platform::AvailableDevice> Platform::getAvailableDevices() {
  std::vector<AvailableDevice> result;

  for (const auto& device : mPlatform.devices()) {
    bool native = identifyVendor(device.vendor()) == mVendor &&
        device.vendor() == getName();

    result.push_back(AvailableDevice(device, native));
  }

  return result;
}

} //namespace Ocelot
