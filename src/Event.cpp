#include "Event.h"

#include "Profiler.h"

namespace Ocelot
{

void Event::wait() const {
  OCL_PROFILE_STALL_SCOPE();
  // Not the best solution..
  static_cast<boost::compute::event*>(const_cast<Event*>(this))->wait();
}

void Event::wait(const std::vector<Event>& events) {
  for (const auto event : events)
    event.wait();
}

} //namespace Ocelot
