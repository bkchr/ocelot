#include "OperationIndex.h"

namespace Ocelot
{

thread_local std::vector<OperationIndex::OperationInfo>
  OperationIndex::mCurrentOperation;
std::atomic<OperationId> OperationIndex::mNextOperationId(
                                                  Invalid_OperationId + 1);

OperationId OperationIndex::generateId() {
  return mNextOperationId++;
}

void OperationIndex::startOperation(OperationId id, const std::string& name) {
  mCurrentOperation.push_back(OperationInfo(id, name));
}

void OperationIndex::endOperation() {
  if (!mCurrentOperation.empty()) {
    mCurrentOperation.pop_back();
  }
}

OperationId OperationIndex::getCurrentOperationId() {
  if (!mCurrentOperation.empty()) {
    return mCurrentOperation.back().sId;
  }

  return Invalid_OperationId;
}

OperationId OperationIndex::getParentOperationId() {
  if (hasParentOperation()) {
    return mCurrentOperation[mCurrentOperation.size() - 2].sId;
  }

  return Invalid_OperationId;
}

bool OperationIndex::hasParentOperation() {
  return mCurrentOperation.size() > 1;
}

const std::string& OperationIndex::getCurrentOperationName() {
  if (!mCurrentOperation.empty()) {
    return mCurrentOperation.back().sName;
  }

  static std::string noactive("NoActiveOperation");
  return noactive;
}

OperationId OperationIndex::getNumberOfOperations() {
  return mNextOperationId - 1;
}

} //namespace Ocelot
