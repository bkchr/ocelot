#include "Operator.h"

#include "DeviceManager.h"
#include "OperationIndex.h"
#include "Profiler.h"
#include "DeviceSelectionManager.h"

#include <thread>

namespace Ocelot
{

thread_local unsigned short Operator::mCurrentRunIndex = 0;

Operator::Operator(DeviceSelectionManager* dsmgr) :
  pDeviceSelectionManager(dsmgr),
  mNumberOfRuns(0),
  mOperationId(OCL_GENERATE_OPERATION_ID()) {

}

void Operator::next() {
  if (mFinished) {
      return;
  }

  OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(mOperationId, getName());

  startRun();
  mFinished = ++mNumberOfRuns > getMaximumNumberOfRuns();
}

void Operator::finish() {
  std::lock_guard<std::mutex> lock(mFinishMutex);

  if (mFinished) {
      return;
  }

  OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(mOperationId, getName());

  std::vector<std::thread> run_threads;

  for (auto i = 1; i < getMaximumNumberOfRuns(); ++i) {
    run_threads.push_back(std::thread([i, this]() {
      mCurrentRunIndex = i;
      OCL_OPERATION_SCOPE(mOperationId, getName());
      startRun();
    }));
  }

  startRun();

  for (auto& itr : run_threads) {
    itr.join();
  }

  mFinished = true;
}

void Operator::startRun() {
  run(pDeviceSelectionManager->getDevice());
}

void Operator::registerParent(OperatorPtr op, unsigned short in,
                              unsigned short out) {
  assert(mInputChannels.find(in) == mInputChannels.end());

  auto input = Pointer<OperatorInput>(DI_CREATE_INSTANCE(OperatorInput,
                 op, out, doesInputChannelSupportVectorizedData(in)));
  mInputChannels[in] = input;

  mWorkingOnVectorizedData |= doesInputChannelSupportVectorizedData(in);
}

unsigned short Operator::getInputChannelMaximumNumberOfRuns(
    unsigned short channel) {
  return mInputChannels[channel]->getMaximumNumberOfRuns();
}

bool Operator::isWorkingOnVectorizedData() const {
  return mWorkingOnVectorizedData;
}

} //namespace Ocelot
