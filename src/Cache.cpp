#include "Cache.h"

#include "CacheCostCalculator.h"
#include "Device.h"

#define LOCKR() boost::shared_lock<boost::shared_mutex> lock(mMutex)
#define LOCKW() boost::unique_lock<boost::shared_mutex> lock(mMutex)

namespace Ocelot
{

Cache::Cache(Device* device) :
  pDevice(device) {

}

Cache::~Cache() {
  mBuffers.clear();
}

void Cache::registerBuffer(const BufferPtr& ptr, const ResourceId id,
                           const CacheCost::CalculatorPtr& cost) {
  LOCKW();
  mBuffers[id] = std::shared_ptr<BufferInfo>(new BufferInfo(ptr, cost));
}

void Cache::unregisterBuffer(const ResourceId id) {
  LOCKW();
  auto find = mBuffers.find(id);

  if (find != mBuffers.end()) {
    mBuffers.erase(find);
  }
}

BufferPtr Cache::getBuffer(const ResourceId id) {
  LOCKR();
  auto find = mBuffers.find(id);

  if (find == mBuffers.end())
    return nullptr;

  find->second->sLastAccess = std::time(nullptr);
  return find->second->sBuffer;
}

bool Cache::hasBuffer(const ResourceId id) {
  LOCKR();
  return mBuffers.find(id) != mBuffers.end();
}

CacheCost::CalculatorPtr Cache::getBufferCost(const ResourceId id) {
  LOCKR();
  auto find = mBuffers.find(id);

  if (find != mBuffers.end()) {
    return find->second->sCost;
  }

  return nullptr;
}

BufferPtr Cache::transferBuffer(const ResourceId id, Cache* dst_cache) {
  assert(getBuffer(id) != nullptr &&
                          "Buffer which should be transfered doesn't exist!");

  LOCKR();
  auto find = mBuffers.find(id);
  auto ptr = find->second->sBuffer->clone(
               dst_cache->pDevice->shared_from_this());

  dst_cache->registerBuffer(ptr, id, find->second->sCost);
  return ptr;
}

bool Cache::freeUpSpace(size_t required_space) {
  LOCKW();

  size_t freed_space = 0;
  while (freed_space < required_space) {
    size_t freed = searchAndFreeUpBuffer();

    if (freed == 0)
      return false;
    else
      freed_space += freed;
  }

  return true;
}

size_t Cache::searchAndFreeUpBuffer() {
  if (mBuffers.empty())
    return 0;

  auto selected = mBuffers.end();
  for (auto itr(mBuffers.begin()), end(mBuffers.end()); itr != end; ++itr) {

    if (itr->second->sBuffer.use_count() > 1)
      continue;

    if (selected == mBuffers.end()) {
      selected = itr;
      continue;
    }

    if (itr->second->sLastAccess < selected->second->sLastAccess &&
        itr->second->sCost->getCost() >= selected->second->sCost->getCost())
      selected = itr;
  }

  if (selected == mBuffers.end())
    return 0;

  auto result = selected->second->sBuffer->getSizeInBytes();
  mBuffers.erase(selected);

  return result;
}

} //namespace Ocelot

#undef LOCKR
#undef LOCKW
