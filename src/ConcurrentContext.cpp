#include "ConcurrentContext.h"

namespace Ocelot
{

void ConcurrentContext::addEventsToWaitList(
    const BufferPtr& ptr, boost::compute::wait_list& list) const {
  ptr->addEventsToWaitList(
        list, std::bind(&ConcurrentContext::filterEvent, this,
                        std::placeholders::_1));
}

void ConcurrentContext::addEvents(const std::vector<Event>& events) {
  for (auto& event : events)
    mEvents.push_back(event);
}

bool ConcurrentContext::filterEvent(const Event& event) const {
  if (mEvents.empty())
    return true;
  else
    return std::find(mEvents.begin(), mEvents.end(), event) == mEvents.end();
}

}
