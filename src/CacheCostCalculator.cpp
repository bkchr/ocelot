#include "CacheCostCalculator.h"

#include "CacheManager.h"

#include <chrono>

namespace Ocelot
{

namespace CacheCost
{

Operation::Operation(OperationId id, ProfilingInformation flags) :
  mCost(0) {
  auto sec = std::chrono::seconds(10);
  // set the initial value to 10 seconds
  mCost = std::chrono::duration_cast<std::chrono::nanoseconds>(sec).count();

  Profiler::getSingleton().registerCallback(
        id, flags, [this](unsigned long time) { mCost = time; });
}

Aggregation::Aggregation(const std::vector<ResourceId>& costids) :
  mCostIds(costids) {

}

unsigned long Aggregation::getCost() {
  unsigned long cost = 0;

  for (auto& calc : mCosts)
    cost += calc->getCost();

  return cost;
}

void Aggregation::setCacheManager(CacheManager* cmgr) {
  for (auto& id : mCostIds) {
    auto cost = cmgr->getBufferCost(id);

    if (cost != nullptr)
      mCosts.push_back(cost);
  }
}

} //namespace CacheCost

} //namespace Ocelot
