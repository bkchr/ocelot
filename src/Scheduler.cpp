#include "Scheduler.h"

#include "Device.h"
#include "Platform.h"
#include "Profiler.h"

#include <boost/compute/async.hpp>

namespace Ocelot
{

#define LOCK() std::lock_guard<std::recursive_mutex> lock(mMutex)

Scheduler::Scheduler(const boost::compute::context& context,
                     Device* dev) :
  mContext(context),
  pDevice(dev) {
  init();
}

Scheduler::~Scheduler() {
  deinit();
}

void Scheduler::init() {
  cl_command_queue_properties props =
      boost::compute::command_queue::enable_out_of_order_execution |
      boost::compute::command_queue::enable_profiling;

  mTransferQueue = boost::compute::command_queue(mContext, *pDevice, props);
  mCommandQueue = boost::compute::command_queue(mContext, *pDevice, props);
}

void Scheduler::deinit() {

}

Future<void> Scheduler::writeBuffer(
    const boost::compute::buffer& buffer, const void* host_ptr, size_t size,
    size_t offset, bool event_profiling) {
  LOCK();

  Event event =
      mTransferQueue.enqueue_write_buffer_async(buffer, offset, size, host_ptr);

  if (event_profiling) {
    OCL_PROFILER_EVENT_REGISTER(event, PI_Input_Transfer_Time,
                                pDevice->shared_from_this());
  }

  return make_Future(event);
}

Future<void*> Scheduler::readBuffer(
    const boost::compute::buffer& buffer, void* host_ptr, size_t size,
    size_t offset, const boost::compute::wait_list& wait_list) {
  LOCK();

  Event event = mTransferQueue.enqueue_read_buffer_async(buffer, offset, size,
                                                         host_ptr, wait_list);

  OCL_PROFILER_EVENT_REGISTER(event, PI_Result_Transfer_Time,
                              pDevice->shared_from_this());

  return make_Future(host_ptr, event);
}

BufferPtr Scheduler::cloneBuffer(const Buffer* src_buffer,
                                 Pointer<Device> dst_device) {
  if (dst_device->getPlatform()->getContext() == mTransferQueue.get_context()) {
    return cloneBufferDirect(src_buffer, dst_device);
  } else {
    return cloneBufferInDirect(src_buffer, dst_device);
  }
}

BufferPtr Scheduler::cloneBufferDirect(const Buffer* src_buffer,
                                       Pointer<Device> dst_device) {
  LOCK();

  auto dst_buffer = dst_device->allocateBuffer(src_buffer->getType(),
                                               src_buffer->getCount());

  copyBufferDirect(src_buffer, dst_buffer.get(), 0, 0,
                   src_buffer->getSizeInBytes());

  return dst_buffer;
}

BufferPtr Scheduler::cloneBufferInDirect(
    const Buffer* src_buffer, Pointer<Device> dst_device) {
  LOCK();

  auto dst_buffer = dst_device->allocateBuffer(src_buffer->getType(),
                                               src_buffer->getCount());

  copyBufferInDirect(src_buffer, dst_buffer.get(), 0, 0,
                     src_buffer->getSizeInBytes());

  return dst_buffer;
}

void Scheduler::copyBuffer(
    const Buffer* src, Buffer* dst, size_t src_offset, size_t dst_offset,
    size_t size) {
  if (dst->getDevice()->getPlatform()->getContext() ==
      mTransferQueue.get_context()) {
    copyBufferDirect(src, dst, src_offset, dst_offset, size);
  } else {
    copyBufferInDirect(src, dst, src_offset, dst_offset, size);
  }
}

void Scheduler::copyBufferDirect(
    const Buffer* src, Buffer* dst, size_t src_offset, size_t dst_offset,
    size_t size) {
  LOCK();

  boost::compute::wait_list wait_list;
  src->addEventsToWaitList(wait_list);

  Event event = dst->getDevice()->getScheduler()->
      mTransferQueue.enqueue_copy_buffer(*src, *dst, src_offset, dst_offset,
                                         size, wait_list);

  if (src->getDevice() == dst->getDevice()) {
    OCL_PROFILER_EVENT_REGISTER(event, PI_Internal_Transfer_Time,
                                pDevice->shared_from_this());
  } else {
    OCL_PROFILER_EVENT_REGISTER(event, PI_Device_Transfer_Time,
                                dst->getDevice());
  }

  dst->addEvent(event);
}

void Scheduler::copyBufferInDirect(
    const Buffer* src, Buffer* dst, size_t src_offset, size_t dst_offset,
    size_t size) {
  auto host_ptr = src->readBytes(nullptr, size, src_offset).get();

  auto events = dst->writeBytes(host_ptr, size, dst_offset).getEvents();
  // we store the host_ptr reference with this trick until the event is finished
  for (auto& event : events) {
    event.set_callback([host_ptr](){});
  }
}

Future<void> Scheduler::executeKernel(
    const boost::compute::kernel& kernel, size_t global_size,
    size_t local_size, const boost::compute::wait_list& wait_list) {
  LOCK();

  Event event;
  if (global_size == 0) {
    event = mCommandQueue.enqueue_task(kernel, wait_list);
   } else if (local_size == 0) {
    event = mCommandQueue.enqueue_nd_range_kernel(
          kernel, 1, nullptr, &global_size, nullptr, wait_list);
  } else {
    event = mCommandQueue.enqueue_nd_range_kernel(
          kernel, 1, nullptr, &global_size, &local_size, wait_list);
  }

  OCL_PROFILER_EVENT_REGISTER(event, PI_Kernel_Runtime,
                              pDevice->shared_from_this());

  return make_Future(event);
}

bool Scheduler::testBufferCreation(const boost::compute::buffer& buffer) {
  LOCK();

  try {
#ifdef CL_VERSION_1_2
    if (pDevice->checkOpenClVersion(1, 2)) {
      mTransferQueue.enqueue_migrate_memory_objects(1, &buffer.get(), 0);
    } else
#endif //#ifdef CL_VERSION_1_2
    {
      char test = 0;
      writeBuffer(buffer, &test, sizeof(char), 0, false);
    }
  }
  catch (...) {
    return false;
  }

  return true;
}

std::pair<Event, Event> Scheduler::createMarkerEvents() {
  return std::make_pair(mTransferQueue.enqueue_marker(),
                        mCommandQueue.enqueue_marker());
}

} //namespace Ocelot
