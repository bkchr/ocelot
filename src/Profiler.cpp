#include "Profiler.h"

#include "Device.h"
#include "Scheduler.h"
#include <iostream>
namespace Ocelot
{

#define LOCK_RECORD(record) std::lock_guard<std::recursive_mutex>              \
                            lock(record.sLock)

Profiler::Record::Record() :
  sHostRuntime(0),
  sHostStallTime(0),
  sKernelRuntime(0),
  sInputTransferTime(0),
  sResultTransferTime(0),
  sDeviceTransferTime(0),
  sInternalTransferTime(0),
  sHostRuntimeRegistered(false),
  sFinished(false),
  sId(Invalid_OperationId) {

}

unsigned long Profiler::getProfilingInformation(
    const OperationId id, const int request) {
  return getProfilingInformation(id, request, Invalid_OperationId);
}

unsigned long Profiler::getProfilingInformation(
    const OperationId id, const int request, const OperationId parent_id) {
  auto& record = getRecord(id);

  record.sLock.lock();
  auto devices = record.sProfiledDevices;
  auto events = record.sProfiledEvents;
  record.sLock.unlock();

  if (!devices.empty()) {
    std::vector<std::pair<Event, Event>> marker_events;

    for (auto device : devices)
      marker_events.push_back(device->getScheduler()->createMarkerEvents());

    for (auto& event_pair : marker_events) {
      event_pair.first.wait();
      event_pair.second.wait();
    }

    record.sLock.lock();
    record.sProfiledDevices.clear();
    record.sLock.unlock();
  }

  std::unique_lock<std::recursive_mutex> lock(record.sLock);
  record.sFinishedConditionVar.wait(
        lock, [&record]() { return record.sFinished.load(); });

  return extractProfilingInformation(record, request, parent_id);
}

void Profiler::registerCallback(const OperationId id, const int flags,
                                const Callback& callback) {
  auto& record = getRecord(id);

  LOCK_RECORD(record);
  record.sCallbacks.push_back(std::make_pair(flags, callback));
}

void Profiler::registerEventForProfiling(
    Event& event, const OperationId id,
    const ProfilingInformation measurement_flags, Pointer<Device> dev) {
  auto& record = getRecord(id);
  LOCK_RECORD(record);
  record.sProfiledEvents.push_back(event);
  record.sProfiledDevices.push_back(dev);

  event.set_callback(std::bind(&Profiler::handleOnProfiledEventCompleted, this,
                               event, id, measurement_flags,
                               OperationIndex::getParentOperationId()));
}

void Profiler::registerHostTime(OperationId id, unsigned long time) {
  auto& record = getRecord(id);
  LOCK_RECORD(record);

  record.sHostRuntime[OperationIndex::getParentOperationId()] += time;
  record.sHostRuntimeRegistered = true;

  finishRecord(record);
}

void Profiler::finishRecord(Record& record) {
  if (!record.sProfiledEvents.empty() || !record.sHostRuntimeRegistered)
    return;

  for (auto itr : record.sCallbacks) {
    itr.second(extractProfilingInformation(record, itr.first));
  }

  record.sCallbacks.clear();
  record.sProfiledDevices.clear();
  record.sFinished = true;
  record.sFinishedConditionVar.notify_all();
}

void Profiler::registerStallTime(OperationId id, unsigned long time) {
  auto& record = getRecord(id);
  LOCK_RECORD(record);

  record.sHostStallTime[OperationIndex::getParentOperationId()] += time;
}

void Profiler::registerNestedOperationAtParent(OperationId child,
                                               OperationId parent) {
  auto& record = getRecord(parent);
  LOCK_RECORD(record);
  record.sNestedOperations.insert(child);
}

void Profiler::registerNameForOperation(OperationId id,
                                        const std::string& name) {
  auto& record = getRecord(id);
  LOCK_RECORD(record);
  record.sName = name;
}

Profiler::Record& Profiler::getRecord(const OperationId id) {
  std::lock_guard<std::mutex> lock(mRecordsMutex);

  auto& record = mRecords[id];
  record.sId = id;
  return record;
}

void Profiler::handleOnProfiledEventCompleted(
    Event event, const OperationId id,
    const ProfilingInformation flags, const OperationId parent_id) {
  auto& record = getRecord(id);
  LOCK_RECORD(record);

  unsigned long duration = 0;
  // some events can't be profiled
  try {
    duration = static_cast<unsigned long>(
                 event.duration<boost::chrono::nanoseconds>().count());
  } catch(...) {
    removeCompletedEventFromRecord(event, record);
    return;
  }

  if (flags & PI_Kernel_Runtime) {
    record.sKernelRuntime[parent_id] += duration;
  } else if (flags & PI_Device_Transfer_Time) {
    record.sDeviceTransferTime[parent_id] += duration;
  } else if (flags & PI_Internal_Transfer_Time) {
    record.sInternalTransferTime[parent_id] += duration;
  } else if (flags & PI_Result_Transfer_Time) {
    record.sResultTransferTime[parent_id] += duration;
  } else if (flags & PI_Input_Transfer_Time) {
    record.sInputTransferTime[parent_id] += duration;
  }

  removeCompletedEventFromRecord(event, record);
}

void Profiler::removeCompletedEventFromRecord(Event& event, Record& record) {
  auto find = std::find(record.sProfiledEvents.begin(),
                        record.sProfiledEvents.end(), event);

  if (find != record.sProfiledEvents.end())
    record.sProfiledEvents.erase(find);

  finishRecord(record);
}

unsigned long Profiler::extractProfilingInformation(
    Record& record, const int flags, const OperationId parent_id) {
  LOCK_RECORD(record);
  unsigned long duration = 0;

  if (flags & PI_Host_Runtime) {
    duration += extractTiming(record.sHostRuntime, parent_id) -
                extractTiming(record.sHostStallTime, parent_id);
  }
  if (flags & PI_Host_Stalled) {
    duration += extractTiming(record.sHostStallTime, parent_id);
  }
  if (flags & PI_Kernel_Runtime) {
    duration += extractTiming(record.sKernelRuntime, parent_id);
  }
  if (flags & PI_Internal_Transfer_Time) {
    duration += extractTiming(record.sInternalTransferTime, parent_id);
  }
  if (flags & PI_Input_Transfer_Time) {
    duration += extractTiming(record.sInputTransferTime, parent_id);
  }
  if (flags & PI_Result_Transfer_Time) {
    duration += extractTiming(record.sResultTransferTime, parent_id);
  }
  if (flags & PI_Device_Transfer_Time) {
    duration += extractTiming(record.sDeviceTransferTime, parent_id);
  }

  // extract the timings of the childs
  if (flags & PI_Nested_Operations_Runtime) {
    duration += extractProfilingInformationForNestedOps(record, flags);
  }

  // as the parent operation tracks the whole host runtime, we need to remove
  // the child host and stalled time
  if (flags & PI_Host_Runtime) {
    duration -= extractProfilingInformationForNestedOps(
                  record, PI_Host_Runtime | PI_Host_Stalled);
  }

  return duration;
}

unsigned long Profiler::extractTiming(const Record::TTimeMap& times,
                                      const OperationId parent_id) {
  unsigned long duration = 0;

  if (parent_id == Invalid_OperationId) {
    for (const auto& time : times) {
      duration += time.second;
    }
  } else {
    auto find = times.find(parent_id);

    if (find != times.end()) {
      duration += find->second;
    }
  }

  return duration;
}

unsigned long Profiler::extractProfilingInformationForNestedOps(
    Record& record, const int flags) {
  unsigned long duration = 0;
  for (auto id : record.sNestedOperations) {
    duration += getProfilingInformation(id, flags, record.sId);
  }

  return duration;
}

ProfilerScope::ProfilerScope(const OperationId id, const Type type) :
  mId(id),
  mType(type),
  mStartTime(std::chrono::high_resolution_clock::now()) {
  if (OperationIndex::hasParentOperation() && type == HostScope) {
    Profiler::getSingleton().registerNestedOperationAtParent(
          id, OperationIndex::getParentOperationId());
  }
}

ProfilerScope::~ProfilerScope() {
  auto time = static_cast<unsigned long>(
                std::chrono::duration_cast<std::chrono::nanoseconds>(
                  std::chrono::high_resolution_clock::now() -
                  mStartTime).count());

  switch (mType) {
    case HostScope:
      Profiler::getSingleton().registerHostTime(mId, time);
      break;
    case StallScope:
      Profiler::getSingleton().registerStallTime(mId, time);
      break;
  }
}

} //namespace Ocelot
