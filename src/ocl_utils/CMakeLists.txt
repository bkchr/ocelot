cmake_minimum_required(VERSION 2.8)

# search for source files
file(GLOB SRC_OCL_UTILS_SOURCE "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp")

add_source_ocelot(${SRC_OCL_UTILS_SOURCE})
