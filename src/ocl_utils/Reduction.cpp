#include "Reduction.h"

#include "Device.h"
#include "InitBuffer.h"

namespace Ocelot
{

Reduction::Reduction(const BufferPtr& input, const BufferPtr& bitmap,
                     Operation op, Option opt) :
  mInput(input),
  mInputBitmap(bitmap),
  mOperation(op),
  mOption(opt) {
  run();
}

void Reduction::run() {
  checkOperation();
  auto device = mInput->getDevice();

  setupResultBuffer(device);
  setupKernels(device);

  auto local_size =
      setHighBit(Kernel::getBestWorkGroupSize({ mFastKernel, mSlowKernel }));

  // maybe only the Intel driver knows why he doesn't returns the corret value..
  if (device->getVendor() == Vendor_Intel)
    local_size = 1024;

  setupKernelArgs(device, local_size);

  if (mTuplesPerThread)
    mFastKernel->execute(local_size * device->getMaxComputeUnits(), local_size);

  if (mSlowKernelElements)
    mSlowKernel->execute();
}

void Reduction::checkOperation() {
  if (mInput->getType() == DTypeS64) {
    THROW_EXCEPTION("DTypeS64 as input type is not supported!");
  }

  if (mOperation == SumLong && mInput->getType() != DTypeS32) {
    THROW_EXCEPTION("SumLong only supports DTypeS32 as input type!");
  }

  if (mOperation == Sum && mInput->getType() == DTypeS32) {
    THROW_EXCEPTION("Sum doesn't support DTypeS32 as input type!");
  }

  if (mOperation == BitCount && mInput->getType() != DTypeS32) {
    THROW_EXCEPTION("BitCount only supports DTypeS32 as input type!");
  }
}

void Reduction::setupResultBuffer(Pointer<Device> dev) {
  auto size = dev->getMaxComputeUnits() + 1;

  if (mOperation == SumLong) {
    mResult = dev->allocateBuffer(DTypeS64, size);
  } else if (mOperation == Count || mOperation == BitCount) {
    mResult = dev->allocateBuffer(DTypeS32, size);
  } else if (mOperation == Sum && GET_TYPE_SIZE(mInput->getType()) < 4) {
    mResult = dev->allocateBuffer(DTypeS32, size);
  } else {
    mResult = dev->allocateBuffer(mInput->getType(), size);
  }

  InitBuffer(mResult, InitBuffer::WithZeros);
}

void Reduction::setupKernels(Pointer<Device> dev) {
  std::string fast_kernel_name, slow_kernel_name;
  bool use_typed_kernel = true;

  switch (mOperation) {
    case Sum:
      fast_kernel_name = "reduce_sum_par";
      slow_kernel_name = "reduce_sum_seq";
      break;
    case SumLong:
      fast_kernel_name = "reduce_sum_int_long_par";
      slow_kernel_name = "reduce_sum_int_long_seq";
      use_typed_kernel = false;
      break;
    case Min:
      fast_kernel_name = "reduce_min_par";
      slow_kernel_name = "reduce_min_seq";
      break;
    case Max:
      fast_kernel_name = "reduce_max_par";
      slow_kernel_name = "reduce_max_seq";
      break;
    case Count:
      fast_kernel_name = "reduce_count_par";
      slow_kernel_name = "reduce_count_seq";
      break;
    case BitCount:
      fast_kernel_name = "reduce_bitcount_par";
      slow_kernel_name = "reduce_bitcount_seq";
      use_typed_kernel = false;
      break;
  }

  if (use_typed_kernel) {
    mFastKernel = dev->getKernel(fast_kernel_name, mInput->getType());
    mSlowKernel = dev->getKernel(slow_kernel_name, mInput->getType());
  } else {
    mFastKernel = dev->getKernel(fast_kernel_name);
    mSlowKernel = dev->getKernel(slow_kernel_name);
  }
}

void Reduction::setupKernelArgs(Pointer<Device> dev, unsigned int local_size) {

  auto processors = dev->getMaxComputeUnits();
  mTuplesPerThread = mInput->getCount() / (processors * local_size);
  auto slow_kernel_offset = processors * mTuplesPerThread * local_size;
  mSlowKernelElements = mInput->getCount() - slow_kernel_offset;

  mFastKernel->setArgs(in(mInput), out(mResult), mTuplesPerThread,
                       in(mInputBitmap), static_cast<unsigned int>(mOption));

  mSlowKernel->setArgs(in(mInput), slow_kernel_offset, mSlowKernelElements,
                       out(mResult), mResult->getCount() - 1, in(mInputBitmap),
                       static_cast<unsigned int>(mOption));
}

template<>
long Reduction::getResult<long>() const {
  if (mOperation != SumLong) {
    THROW_EXCEPTION("SumLong operation is the only operation "
                    "which supports long as result!");
  }

  return getResultInternal<long>();
}

} //namespace Ocelot
