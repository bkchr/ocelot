#include "GroupedAggregation.h"

#include "Buffer.h"
#include "Device.h"
#include "Kernel.h"
#include "InitBuffer.h"

namespace Ocelot
{

GroupedAggregation::GroupedAggregation(
    const Pointer<Buffer>& input, const Pointer<Buffer>& groups,
    unsigned int group_count, bool no_nil, Operation op) :
  mInput(input),
  mGroups(groups),
  mOperation(op),
  mGroupCount(group_count),
  mNoNil(no_nil) {
  run();
}

void GroupedAggregation::run() {
  mDevice = mInput->getDevice();

  setupKernels();

  auto local_size = getLocalSize();
  auto processors = mDevice->getMaxComputeUnits();
  auto tuples_per_thread = mInput->getCount() / (processors * local_size);
  auto seq_offset = processors * tuples_per_thread * local_size;
  auto seq_size = mInput->getCount() - seq_offset;
  auto seq_result_offset = processors * mGroupCount * getPartitionsPerGroup();

  setupSeqAggKernels(tuples_per_thread);

  mResult = mDevice->allocateBuffer(getDataTypeForResultBuffer(), mGroupCount);

  BufferPtr tmp_result;
  if (tuples_per_thread) {
    tmp_result = mDevice->allocateBuffer(
                   getDataTypeForTmpBuffers(),
                   (processors * getPartitionsPerGroup() + 1) * mGroupCount);
    initBuffer(tmp_result);
  } else {
    seq_result_offset = 0;
    initBuffer(mResult);
  }

  ConcurrentContext context;
  if (seq_size) {
    executeSeqAgg(tmp_result, seq_offset, seq_size, seq_result_offset,
                  tuples_per_thread, context);
  }

  if (tuples_per_thread) {
    executeParPart(tmp_result, tuples_per_thread, seq_size, processors,
                   local_size, context);
  }
}

void GroupedAggregation::replaceSumLongSequentialKernel() {
  mSeqAgg = mDevice->getKernel("groupedSumLongSequential", mInput->getType());
}

void GroupedAggregation::setupSeqAggKernels(unsigned int tuples_per_thread) {
  switch (mOperation) {
    case Count:
    {
      std::string seq_agg = mNoNil ? "groupedCountLongSequential" :
                                     "groupedCountLongWithNullSequential";

      if (tuples_per_thread) {
        seq_agg = mNoNil ? "groupedCountSequential" :
                           "groupedCountWithNullSequential";
      }

      mSeqAgg = mDevice->getKernel(seq_agg, mInput->getType());
      break;
    }
    case SumLong:
    {
      std::string seq_agg = "groupedSumLongSequential";

      if (tuples_per_thread) {
        seq_agg = "groupedSumSequential";
      }

      mSeqAgg = mDevice->getKernel(seq_agg, mInput->getType());
      break;
    }
    default:
      break;
  }
}

void GroupedAggregation::setupKernels() {
  switch (mOperation) {
    case Sum:
      setupSumKernels();
      break;
    case Min:
      setupMinKernels();
      break;
    case Max:
      setupMaxKernels();
      break;
    case Count:
      setupCountKernels();
      break;
    case SumLong:
      setupSumLongKernels();
      break;
    default:
      THROW_EXCEPTION("Unknown operation!");
  }
}

void GroupedAggregation::setupSumKernels() {
  auto par_agg = useLocalMemory() ? "groupedSumLocal" : "groupedSumGlobal";
  setupKernelsGeneral(par_agg, "groupedSumSequential", "groupedSumFinal",
                      mInput->getType());
}

void GroupedAggregation::setupSumLongKernels() {
  auto par_agg = useLocalMemory() ? "groupedSumLocal" : "groupedSumGlobal";
  setupKernelsGeneral(par_agg, "", "groupedSumLongFinal", mInput->getType());
}

void GroupedAggregation::setupMinKernels() {
  auto par_agg = useLocalMemory() ? "groupedMinLocal" : "groupedMinGlobal";
  setupKernelsGeneral(par_agg, "groupedMinSequential", "groupedMinFinal",
                      mInput->getType());
}

void GroupedAggregation::setupMaxKernels() {
  auto par_agg = useLocalMemory() ? "groupedMaxLocal" : "groupedMaxGlobal";
  setupKernelsGeneral(par_agg, "groupedMaxSequential", "groupedMaxFinal",
                      mInput->getType());
}

void GroupedAggregation::setupCountKernels() {
  std::string par_agg;

  if (mNoNil) {
    par_agg = useLocalMemory() ? "groupedCountLocal" : "groupedCountGlobal";
  } else {
    par_agg = useLocalMemory() ? "groupedCountWithNullLocal" :
                                 "groupedCountWithNullGlobal";
  }

  setupKernelsGeneral(par_agg, "", "groupedSumLongFinal", DTypeS32);
}

void GroupedAggregation::setupKernelsGeneral(
    const std::string& par_agg, const std::string& seq_agg,
    const std::string& final_agg, const DataType final_agg_type) {
  mParAgg = mDevice->getKernel(par_agg, mInput->getType());

  if (!seq_agg.empty()) {
    mSeqAgg = mDevice->getKernel(seq_agg, mInput->getType());
  }

  mFinalAgg = mDevice->getKernel(final_agg, final_agg_type);
}

unsigned int GroupedAggregation::getPartitionsPerGroup() {
  if (mDevice->getType() == Device::CPU) {
    // atomic functions are cheap at the cpu
    return 1;
  } else {
    return 1 + (1024 / mGroupCount);
  }
}

unsigned int GroupedAggregation::getAggregationTableCount() {
  return mGroupCount * getPartitionsPerGroup();
}

unsigned int GroupedAggregation::getAggregationTableSize() {
  return GET_TYPE_SIZE(getDataTypeForTmpBuffers()) * getAggregationTableCount();
}

bool GroupedAggregation::useLocalMemory() {
  return getAggregationTableSize() < mDevice->getLocalMemorySize();
}

void GroupedAggregation::initBuffer(BufferPtr buffer) {
  InitBuffer::Operation op;

  switch (mOperation) {
    case Min:
      op = InitBuffer::WithMax;
      break;
    case Max:
      op = InitBuffer::WithMin;
      break;
    default:
      op = InitBuffer::WithZeros;
      break;
  }

  InitBuffer(buffer, op);
}

unsigned int GroupedAggregation::getLocalSize() {
  auto local_size = mParAgg->getWorkGroupSize();

  if (mDevice->getVendor() == Vendor_Intel && local_size > 1024) {
    local_size = 1024;
  }

  return local_size;
}

DataType GroupedAggregation::getDataTypeForResultBuffer() {
  if (mOperation == SumLong || mOperation == Count) {
    return DTypeS64;
  } else {
    return getDataTypeForTmpBuffers();
  }
}

DataType GroupedAggregation::getDataTypeForTmpBuffers() {
  if (mOperation == Count) {
    return DTypeS32;
  } else if (mOperation == Sum) {
    switch (mInput->getType()) {
      case DTypeS8:
      case DTypeS16:
        return DTypeS32;
    }
  }

  return mInput->getType();
}

void GroupedAggregation::executeSeqAgg(
    Pointer<Buffer> tmp_result, unsigned int seq_offset, unsigned int seq_size,
    unsigned int seq_result_offset, unsigned int tuples_per_thread,
    ConcurrentContext& context) {
  auto result_buffer = tuples_per_thread ? tmp_result : mResult;

  mSeqAgg->setArgs(in(mInput), in(mGroups), seq_offset, seq_size,
                   inout(result_buffer), seq_result_offset);

  if (tuples_per_thread) {
    mSeqAgg->execute(0, 0, context);
  } else {
    mSeqAgg->execute(0, 0);
  }
}

void GroupedAggregation::executeParPart(
    Pointer<Buffer> tmp_result, unsigned int tuples_per_thread,
    unsigned int seq_size, unsigned int processors, unsigned int local_size,
    ConcurrentContext& context) {
  if (useLocalMemory()) {
    mParAgg->setArgs(in(mInput), in(mGroups),
                     LocalMemory(getDataTypeForTmpBuffers(),
                                 getAggregationTableCount()),
                     tuples_per_thread, mGroupCount, getPartitionsPerGroup(),
                     inout(tmp_result));
  } else {
    mParAgg->setArgs(in(mInput), in(mGroups), tuples_per_thread, mGroupCount,
                     inout(tmp_result));
  }

  mParAgg->execute(local_size * processors, local_size, context);

  unsigned int partitions = processors * getPartitionsPerGroup() +
                            (seq_size > 0);

  mFinalAgg->setArgs(in(tmp_result), mGroupCount, partitions, out(mResult));
  mFinalAgg->execute(mGroupCount, 0);
}

} //namespace Ocelot
