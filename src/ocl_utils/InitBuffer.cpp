#include "InitBuffer.h"

#include "Buffer.h"
#include "Kernel.h"
#include "Device.h"

namespace Ocelot
{

InitBuffer::InitBuffer(const Pointer<Buffer>& buffer, const Operation op) {
  switch (op) {
    case WithZeros:
      initWithZeros(buffer);
      break;
    case WithMax:
      initWithMax(buffer);
      break;
    case WithMin:
      initWithMin(buffer);
      break;
  }
}

void InitBuffer::initWithZeros(const Pointer<Buffer>& buffer) {
  auto kernel = buffer->getDevice()->getKernel("init_zero", buffer->getType());
  kernel->setArg(0, inout(buffer));
  kernel->execute(buffer->getElementCount());
}

void InitBuffer::initWithMax(const Pointer<Buffer>& buffer) {
  auto kernel = buffer->getDevice()->getKernel("init_max", buffer->getType());
  kernel->setArg(0, inout(buffer));
  kernel->execute(buffer->getElementCount());
}

void InitBuffer::initWithMin(const Pointer<Buffer>& buffer) {
  auto kernel = buffer->getDevice()->getKernel("init_min", buffer->getType());
  kernel->setArg(0, inout(buffer));
  kernel->execute(buffer->getElementCount());
}

} //namespace Ocelot
