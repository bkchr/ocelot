#include "PrefixSum.h"

#include "Device.h"
#include "Profiler.h"

namespace Ocelot
{

PrefixSum::PrefixSum(const BufferPtr& buffer) :
  mInputBuffer(buffer),
  mResultSumRequested(false),
  mOperationId(OCL_GENERATE_OPERATION_ID()) {
  OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(mOperationId, "PrefixSum");

  auto type = buffer->getType() == DTypeBitmap ? DTypeS32 : buffer->getType();
  mResultBuffer = buffer->getDevice()->
      allocateBuffer(type, buffer->getElementCount());
  run();
}

PrefixSum::PrefixSum(const BufferPtr& buffer, const BufferPtr& resultbuffer) :
  mInputBuffer(buffer),
  mResultBuffer(resultbuffer),
  mResultSumRequested(false),
  mOperationId(OCL_GENERATE_OPERATION_ID()) {
  OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(mOperationId, "PrefixSum");
  run();
}

void PrefixSum::run() {
  auto device = mInputBuffer->getDevice();
  auto elements = mInputBuffer->getElementCount();

  bool bitmap = mInputBuffer->getType() == DTypeBitmap;
  setupKernels(device, bitmap);
  setupLocalAndWorkSize(bitmap);
  createTmpBuffer(device, elements);
  executePrefixSum(mInputBuffer, 0, elements, mResultBuffer, 0, mTmpBuffer, 0,
                   bitmap);
}

void PrefixSum::createTmpBuffer(Pointer<Device> device, unsigned int elements) {
  // First we need to calculate the size of the temp buffer.
  // The size of the temp buffer matches the number of running work groups
  // for the full prefix sum calculation
  auto buffer_size = 1;
  auto buffer_size_tmp = divisionWithRound(elements, mWorkSize);

  while (buffer_size_tmp > 1) {
    buffer_size += buffer_size_tmp;
    buffer_size_tmp = divisionWithRound(buffer_size_tmp, mWorkSize);
  }

  mTmpBuffer = device->allocateBuffer(DTypeS32, buffer_size);
}

void PrefixSum::setupKernels(Pointer<Device> device, bool bitmap) {
  mIncKernel = device->getKernel("prefixsum_inc");
  mSeqKernel = device->getKernel("prefixsum_seq");
  mParKernel = device->getKernel("prefixsum_par");

  if (bitmap) {
    mSeqKernelBM = device->getKernel("prefixsum_seq_bm");
    mParKernelBM = device->getKernel("prefixsum_par_bm");
  }
}

void PrefixSum::setupLocalAndWorkSize(bool bitmap) {
  std::vector<KernelPtr> kernel_list{ mSeqKernel, mParKernel, mIncKernel };

  if (bitmap) {
    kernel_list.push_back(mSeqKernelBM);
    kernel_list.push_back(mParKernelBM);
  }

  // we take the minimum value of all kernels as our localSize
  mLocalSize = setHighBit(Kernel::getBestWorkGroupSize(kernel_list));
  mWorkSize = 2 * mLocalSize;
}

void PrefixSum::executePrefixSum(
    const BufferPtr& input, const unsigned int input_offset,
    const unsigned int elements, const BufferPtr& output,
    const unsigned int output_offset, const BufferPtr& tmp,
    unsigned int tmp_offset, bool bitmap) {
  auto seq_kernel = bitmap ? mSeqKernelBM : mSeqKernel;
  auto par_kernel = bitmap ? mParKernelBM : mParKernel;

  if (elements < mWorkSize) {
    seq_kernel->setArgs(in(input), input_offset, out(output), output_offset,
                        elements, out(tmp), tmp_offset);
    seq_kernel->execute();
  } else {
    executeParallelPart(par_kernel, seq_kernel, input, input_offset,
                        elements, output, output_offset, tmp, tmp_offset);
  }
}

void PrefixSum::executeParallelPart(
    KernelPtr& par_kernel, KernelPtr& seq_kernel, const BufferPtr& input,
    const unsigned int input_offset, const unsigned int elements,
    const BufferPtr& output, const unsigned int output_offset,
    const BufferPtr& tmp, unsigned int tmp_offset) {
  ConcurrentContext ccontext;

  par_kernel->setArgs(in(input), input_offset, out(output), output_offset,
                      LocalMemory(DTypeS32, mLocalSize), out(tmp), tmp_offset);
  par_kernel->execute(mLocalSize * (elements / mWorkSize), mLocalSize, ccontext);

  auto processed_elements = mWorkSize * (elements / mWorkSize);
  auto remaining_elements = elements - processed_elements;
  auto result_size = divisionWithRound(elements, mWorkSize);

  if (remaining_elements > 0) {
    seq_kernel->setArgs(in(input), input_offset + processed_elements,
                        out(output), output_offset + processed_elements,
                        remaining_elements, out(tmp),
                        tmp_offset + result_size - 1);
    seq_kernel->execute(0, 0, ccontext);
  }

  if (result_size > 1) {
    // okay we need another round
    executePrefixSum(tmp, tmp_offset, result_size, tmp, tmp_offset, tmp,
                     tmp_offset + result_size, false);

    mIncKernel->setArgs(in(output), output_offset, elements, in(tmp),
                        tmp_offset, out(output), output_offset);

    auto global_size = elements % mLocalSize ? mLocalSize *
                                               (1 + elements / mLocalSize) :
                                               elements;
    mIncKernel->execute(global_size, mLocalSize);
  }
}

unsigned int PrefixSum::getSum() {
  OCL_PROFILE_OPERATION_SCOPE_CUSTOM_ID(mOperationId, "PrefixSum");

  if (mResultSumRequested) {
    return mResultSum;
  }

  auto future = mTmpBuffer->read<unsigned int>(1, mTmpBuffer->getCount() - 1);
  mResultSum = *future.get().get();
  mResultSumRequested = true;

  return mResultSum;
}

} //namespace Ocelot
