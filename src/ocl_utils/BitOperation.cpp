#include "BitOperation.h"

namespace Ocelot
{

const std::string BitOperation::KernelNames[] = { "bm_and", "bm_or", "bm_xor",
                                                  "bm_not" };

BitOperation::BitOperation(BitOp op, BufferPtr left, BufferPtr right,
                           bool reuse_left, const DevicePtr device) :
  mOperation(op),
  mDevice(device ? device : left->getDevice()),
  mLeft(left),
  mRight(right),
  mResult(nullptr) {
  initAndExecute(reuse_left);
}

BitOperation::BitOperation(BitOp op, BufferPtr input, bool reuse_input,
                           const DevicePtr device) :
  BitOperation(op, input, nullptr, reuse_input, device) {
  if (mOperation != BIT_NOT) {
    THROW_EXCEPTION("This constructor should only be used for the BIT_NOT "
                    "operation!");
  }
}

void BitOperation::initAndExecute(bool reuse_left) {
  initResult(reuse_left);

  if (mOperation == BIT_NOT) {
    executeNotOperation();
  } else {
    executeAndXorOrOperation();
  }
}

void BitOperation::initResult(bool reuse_left) {
  if (reuse_left) {
    if (mRight && mLeft->getCount() < mRight->getCount() &&
        (mOperation == BIT_OR || mOperation == BIT_XOR)) {
      THROW_EXCEPTION("If reuse_left is set, left and right count "
                      "must be equal!");
    }

    mResult = mLeft;
  } else {
    unsigned int count = 0;

    switch (mOperation) {
      case BIT_NOT:
        count = mLeft->getCount();
        break;
      case BIT_AND:
        count = std::min(mLeft->getCount(), mRight->getCount());
        break;
      default:
        count = std::max(mLeft->getCount(), mRight->getCount());
        break;
    };

    mResult = mDevice->allocateBuffer(DTypeBitmap, count);

    prepareResult();
  }
}

void BitOperation::prepareResult() {
  // if the operation is "or" or "xor" and the left and right buffer count is
  // different, we copy the rest of the bigger buffer into the result buffer
  if ((mOperation == BIT_OR || mOperation == BIT_XOR) &&
      mLeft->getElementCount() != mRight->getElementCount()) {
    BufferPtr bigger(mRight), smaller(mLeft);
    if (mLeft->getElementCount() > mRight->getElementCount()) {
      std::swap(bigger, smaller);
    }

    bigger->copy(mResult, bigger->getCount() - smaller->getCount(),
                 smaller->getCount(), smaller->getCount());
  }
}

void BitOperation::executeNotOperation() {
  auto kernel = mDevice->getKernel(KernelNames[BIT_NOT]);

  kernel->setArgs(in(mLeft), out(mResult), mLeft->getCount());
  kernel->execute(mLeft->getElementCount());
}

void BitOperation::executeAndXorOrOperation() {
  auto kernel = mDevice->getKernel(KernelNames[mOperation]);

  kernel->setArgs(in(mLeft), in(mRight), out(mResult));
  kernel->execute(std::min(mLeft->getElementCount(),
                           mRight->getElementCount()));
}

} //namespace Ocelot
