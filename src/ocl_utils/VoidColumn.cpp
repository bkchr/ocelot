#include "VoidColumn.h"

#include "Device.h"
#include "Kernel.h"

namespace Ocelot
{

VoidColumn::VoidColumn(Pointer<Device> device, unsigned int count) {
  generate(device, count);
}

Pointer<Buffer> VoidColumn::getResult() {
  return mResult;
}

void VoidColumn::generate(Pointer<Device> dev, unsigned int count) {
  mResult = dev->allocateBuffer<DTypeS32>(count);
  auto kernel = dev->getKernel("generateVoidColumn");

  auto local_size = kernel->getLocalSize();
  auto global_size = divisionWithRound(count, local_size);
  global_size *= local_size;

  kernel->setArgs(inout(mResult), count);
  kernel->execute(global_size, local_size);
}

} //namespace Ocelot
