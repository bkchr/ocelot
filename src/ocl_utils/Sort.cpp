#include "Sort.h"

#include "Device.h"
#include "PrefixSum.h"

namespace Ocelot
{

Sort::Sort(const BufferPtr& keys, const BufferPtr& values, Direction dir,
           Pointer<Device> dev) :
  Sort(keys, values, nullptr, nullptr, dir, dev) {

}

Sort::Sort(const BufferPtr& src_keys, const BufferPtr& src_values,
           const BufferPtr& dst_keys, const BufferPtr& dst_values,
           Direction dir, Pointer<Device> dev) :
  // we need to clone because the sort writes also into the src buffers
  mSrcValues(src_values->clone()),
  mSrcKeys(src_keys->clone()),
  mDstValues(dst_values),
  mDstKeys(dst_keys) {
  run(dir, dev);
}

void Sort::run(Direction dir, Pointer<Device> dev) {
  auto device = dev ? dev : mSrcValues->getDevice();

  setupBuffers(device);

  if (mSrcKeys->getCount() == 1) {
    executeSingleElementSort();
  } else {
    executeSort(device, dir);
  }
}

void Sort::setupBuffers(Pointer<Device> dev) {
  if (mDstValues == nullptr) {
    mDstValues = dev->allocateBuffer(mSrcValues->getType(),
                                     mSrcValues->getCount());
  }

  if (mDstKeys == nullptr) {
    mDstKeys = dev->allocateBuffer(mSrcKeys->getType(),
                                   mSrcKeys->getCount());
  }
}

void Sort::executeSingleElementSort() {
  if (mSrcKeys == mDstKeys && mSrcValues == mDstValues)
    return;

  // copy src into dst
  mSrcKeys->copy(mDstKeys, mSrcKeys->getCount());
  mSrcValues->copy(mDstValues, mSrcValues->getCount());
}

void Sort::executeSort(Pointer<Device> dev, Direction dir) {
  const unsigned int bits = useTranspose(dev) ? 4 : 8;
  const unsigned int radix = 1 << bits;

  auto histogram_par = dev->getKernel("histogram_par");
  auto histogram_par_convert = dev->getKernel("histogram_par_convert");
  auto reorder_par = dev->getKernel("reorder_par");
  auto reorder_par_convert = dev->getKernel("reorder_par_convert");

  auto sizes = calculateGlobalAndLocalSize(dev, radix, histogram_par);

  if (useTranspose(dev))
    executeTranspose(dev, sizes.first, sizes.second, false);

  executeSortPasses(dev, dir, radix, bits, sizes.first, sizes.second,
                    histogram_par, histogram_par_convert, reorder_par,
                    reorder_par_convert);

  if (useTranspose(dev))
    executeTranspose(dev, sizes.first, sizes.second, true);
}

bool Sort::useTranspose(Pointer<Device> dev) {
  return dev->getType() == Device::GPU;
}

std::pair<unsigned int, unsigned int> Sort::calculateGlobalAndLocalSize(
    Pointer<Device> dev, const unsigned int radix, KernelPtr& histogram_par) {
  auto max_local_memory = dev->getLocalMemorySize() -
                          histogram_par->getLocalMemSize();
  max_local_memory /= sizeof(int) * radix;

  auto local_size = histogram_par->getWorkGroupSize();
  while (local_size > max_local_memory)
    local_size >>= 1;

  auto global_size =
      local_size * divisionWithRound(mSrcValues->getCount(),
                                     calculateEPTValue(dev, local_size) *
                                     local_size);

  return std::make_pair(global_size, local_size);
}

unsigned int Sort::calculateEPTValue(Pointer<Device> dev, unsigned int local_size) {
  // 4 work groups per processing core (scheduling flexibility, minimum
  // number of work groups
  auto ept = 4 * dev->getMaxComputeUnits() * local_size;
  ept = divisionWithRound(mSrcValues->getCount(), ept);
  return std::pow(2, static_cast<unsigned int>(std::log2(ept)));
}

void Sort::executeTranspose(
    Pointer<Device> dev, unsigned int hist_global_size, unsigned int hist_local_size,
    bool swaprowsandcols) {
  auto cols = calculateEPTValue(dev, hist_local_size);
  auto rows = (mSrcValues->getCount() /
                                (cols * hist_local_size)) * hist_local_size;

  if (swaprowsandcols)
    std::swap(cols, rows);

  // size of the matrix loaded into local memory
  unsigned int tile_size = 16;
  if (rows < tile_size || cols < tile_size)
    tile_size = 1;

  if (rows != hist_global_size) {
    auto offset = rows * cols;
    mSrcKeys->copy(mDstKeys, mSrcKeys->getCount() - offset, offset, offset);
    mSrcValues->copy(mDstValues, mSrcValues->getCount() - offset,
                     offset, offset);
  }

  auto global_size = (rows / tile_size) * cols;
  auto local_size = tile_size;

  auto transpose = dev->getKernel("transpose");
  auto local_memory = LocalMemory(DTypeS32, tile_size * tile_size);
  transpose->setArgs(in(mSrcKeys), out(mDstKeys), cols, rows, in(mSrcValues),
                     out(mDstValues), local_memory, local_memory, tile_size);
  transpose->execute(global_size, local_size);
}

void Sort::executeSortPasses(Pointer<Device> dev, unsigned int dir, unsigned int radix,
                             unsigned int bits, unsigned int global_size,
                             unsigned int local_size, KernelPtr& histogram_par,
                             KernelPtr& histogram_par_convert,
                             KernelPtr& reorder_par,
                             KernelPtr& reorder_par_convert) {
  auto ept = calculateEPTValue(dev, local_size);
  unsigned int pass_count = sizeof(int) * 8 / bits;

  auto inkeys(mSrcKeys), invalues(mSrcValues), outkeys(mDstKeys),
      outvalues(mDstValues);

  KernelPtr histogram, reorder;

  auto histograms = dev->allocateBuffer(DTypeS32, radix * global_size);

  for (unsigned int pass = 0; pass < pass_count; ++pass) {
    histogram = pass == 0 ? histogram_par_convert : histogram_par;
    reorder = pass == (pass_count - 1) ? reorder_par_convert : reorder_par;

    histogram->setArgs(in(inkeys), out(histograms), pass,
                       LocalMemory(DTypeS32, radix * local_size), dir,
                       inkeys->getCount(), ept);
    histogram->execute(global_size, local_size);

    PrefixSum(histograms, histograms);

    reorder->setArgs(in(inkeys), out(outkeys), in(histograms), pass,
                     in(invalues), out(outvalues),
                     LocalMemory(DTypeS32, radix * local_size), dir,
                     inkeys->getCount(), ept);
    reorder->execute(global_size, local_size);

    std::swap(inkeys, outkeys);
    std::swap(invalues, outvalues);
  }

  if (inkeys != mDstKeys) {
    inkeys->copy(mDstKeys, inkeys->getCount());
    invalues->copy(mDstValues, invalues->getCount());
  }
}

} //namespace Ocelot
