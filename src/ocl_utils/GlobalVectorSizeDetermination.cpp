#include "GlobalVectorSizeDetermination.h"

#include "DeviceManager.h"
#include "Device.h"
#include "Platform.h"

#include "boost/compute/command_queue.hpp"

namespace Ocelot
{

GlobalVectorSizeDetermination::GlobalVectorSizeDetermination(
    DeviceManager* mgr) {
  init(mgr);
}

void GlobalVectorSizeDetermination::init(DeviceManager* mgr) {
  std::vector<cl_ulong> vector_sizes;

  for (auto device : mgr->getDevices()) {
    vector_sizes.push_back(getBestVectorSizeForDevice(device));
  }

  mGlobalVectorSize = *std::min_element(vector_sizes.begin(),
                                        vector_sizes.end());
  alignGlobalVectorSize(mgr->getDevices());
}

cl_ulong GlobalVectorSizeDetermination::getBestVectorSizeForDevice(
    Pointer<Device> device) {
  const unsigned int runs_per_size = 20;
  std::map<cl_ulong, double> size_time_map;
  auto queue = boost::compute::command_queue(
                 device->getPlatform()->getContext(), *device);

  for (auto size = device->getLocalMemorySize();
       size <= device->getMaxMemoryAlloc(); size *= 2) {
    auto count = divisionWithRound(size, GET_TYPE_SIZE(DTypeS32));
    auto buffer_size = count * GET_TYPE_SIZE(DTypeS32);

    std::vector<int> data;
    data.resize(count);

    auto buffer = boost::compute::buffer(device->getPlatform()->getContext(),
                                         buffer_size);

    auto start = std::chrono::high_resolution_clock::now();
    for (auto i = 0u; i < runs_per_size; ++i) {
      queue.enqueue_write_buffer(buffer, 0, buffer_size, data.data());
    }
    auto end = std::chrono::high_resolution_clock::now();

    size_time_map[size] +=
      std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count();
  }

  return getBestVectorSize(size_time_map);
}

cl_ulong GlobalVectorSizeDetermination::getBestVectorSize(
    const std::map<cl_ulong, double>& size_time_map) {
  cl_ulong best_size = 0;
  double best_transfer_rate = std::numeric_limits<double>::min();

  for (auto& itr : size_time_map) {
    auto transfer_rate = itr.first / itr.second;

    if (transfer_rate > best_transfer_rate) {
      best_transfer_rate = transfer_rate;
      best_size = itr.first;
    }
  }

  return best_size;
}

void GlobalVectorSizeDetermination::alignGlobalVectorSize(
    const std::vector<Pointer<Device>>& devices) {
  // The vector size needs to be a multiple of the MemBaseAddrAlign.
  // If everything is normal, the vector size is a multiple of 2, like
  // the MemBaseAddrAlign. We check in the function if the constraint is
  // fulfilled, if not we try to align the size to fulfill the constraint.

  bool already_aligned = false;
  bool restart = false;

  do {
    restart = false;

    for (auto device : devices) {
      auto addr_align = device->getMemBaseAddrAlign() / 8;

      if (mGlobalVectorSize % addr_align == 0) {
        continue;
      }

      if (already_aligned) {
        THROW_EXCEPTION("Should never happen, but if you see this, find a "
                        "better solution.");
      }

      // make the global vector size a multiple of the address align
      mGlobalVectorSize =
          divisionWithRound(mGlobalVectorSize, addr_align) * addr_align;

      already_aligned = true;
      restart = true;
      break;
    }
  } while(restart);
}

} //namespace Ocelot
