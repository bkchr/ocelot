#include "Vendor.h"

namespace Ocelot
{

Vendor identifyVendor(const std::string &name) {
  if (name == "Intel(R) Corporation") return Vendor_Intel;
  if (name == "Intel(R) OpenCL") return Vendor_Intel;
  if (name == "GenuineIntel") return Vendor_Intel;
  if (name == "Advanced Micro Devices, Inc.") return Vendor_AMD;
  if (name == "AuthenticAMD") return Vendor_AMD;
  if (name == "NVIDIA Corporation") return Vendor_Nvidia;
  if (name == "NVIDIA CUDA") return Vendor_Nvidia;

  return Vendor_Unknown;
}

} //namespace Ocelot
